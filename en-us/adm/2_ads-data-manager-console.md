---
title: adm console overview
description: Ads data manager console setup.
type: guide
interface: bulk-operations
---
# Ads data manager console

The Ads data manager console is designed to simplify the process of uploading and sharing your first-party (1P) data. To begin using the Ads data manager, simply set up your advertisers with the manager account, bring in your 1P data, and share it with the linked accounts.

## Set up your manager account for data manager

The steps to link advertiser accounts to a manager account are outlined below:

- It is assumed that you have [created a manager account](https://advertising.amazon.com/help/G69CDSR9MNSWJH95).

> [NOTE] In the current release, you are required to have at least one Amazon DSP Advertiser Account to use Ads Data Manager. We are working to expand capabilities to Sponsored Ads products in future releases.

- Now, link advertiser accounts to the manager account.

  - Invite **owned-accounts**. to the manager account. These accounts could be owned by you; and you have complete access to the accounts. 
      Follow the instructions listed under [Link accounts to your manager account](https://advertising.amazon.com/help/GU3YDB26FR7XT3C8).
  - Or, invite **non-owned accounts** to your manager account. These accounts could be managed by an agency or third-party and you do not have access to the account. This is a two-step process.

    - First, [request access to an advertising account](https://advertising.amazon.com/help/GU3YDB26FR7XT3C8) and then send an approval link to an admin user for approval. The admin of the account you're inviting must accept and redeem your invite. Alternatively, you could also get an approval link to select the account permission to grant to the account.
    - Set the right permissions for the linked accounts:

      - Data Sharing: To allow the sharing of uploaded dataset between accounts.
      - ADSP Reports: It will allow you to only view the Amazon DSP reports.

> [NOTE] Data sharing is automatically applied to advertiser accounts with an “Admin” link permission type. “Admin” links are automatically created when an Advertiser Account is created from within a parent Amazon DSP Entity.

## Explore the Ads data manager console

>[NOTE] Manager accounts have automatic access to Ads data manager.

**Ads data manager** sits within the Amazon Ads console and serves as a single point of ingress of 1P data in to Amazon Ads.

To access Ads data manager,

1. Log in to your Amazon Ads console using your Amazon Ads credentials.
2. In the account switcher, select the manager account where you would like to upload or access data.
3. Select **Data manager** from the left navigation pane of the Amazon ads console.
4. If this is your first time logging into Ads data manager, then review and accept the beta terms and conditions that show up before proceeding to the landing page of the application.

Ads data manager can be navigated through the Ads data manager pane on the left, which lists the following three primary functions of Ads data manager:

- [Datasets](adm/3_adm-data-upload-overview): Datasets are the representations of uploaded advertiser data in the Ads data manager. A dataset is a collection of data within a data table, comprising attributes (columns) as well as metadata, such as the dataset name and the schema template utilized to create the dataset.

- [Sources](adm/5_adm-source-connector): The Sources page allows you to ingest your data from various sources to a single Ads data manager account. This page lists the multiple third-party sources from which you could you could bring your data into Ads data manager. The page lists connector libraries for integrating partner applications.

- [Destinations](adm/7_adm-destinations): The Destinations page lists where your data is being activated across accounts and supported Amazon Ads functions.

The Datasets page serves as a dashboard, providing an overview of the advertiser data uploaded through your manager account to Amazon Ads.
