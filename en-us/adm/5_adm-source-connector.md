---
title: adm source connector
description: Information about ads data manager source connector.
type: guide
interface: bulk-operations
---
# Source connector integrations

The Sources page allows you to ingest your data from various sources to a single Ads data manager account. This page lists the multiple third-party sources from which you could you could bring your data into Ads data manager. The page lists connector libraries for integrating partner applications.
For customers already using a Customer Data Platform (CDP), Ads data manager offers seamless integration with most popular platforms. This method allows for automated, real-time data syncing between your CDP and Ads data. If you are currently integrated with a Customer Data Platform (CDP), you can set up Ads data manager to connect with your CDP to obtain datasets directly from your CDP to Ads data manager.

> [NOTE] You do not need to configure any new API integrations; your existing API integrations will work.

To connect your CDP with Ads data manager, perform the following steps:

1. Click **Sources**.

   The page displays a list of popular CDPs through which you could ingest data.
2. Locate your CDP and click **Connect** to access the connection setup for each partner.

   For information on how to bring your data from your source to Amazon Ads, refer to the documentation of the respective CDP.
3. After a connection is established and Ads data manager receives your data, a new dataset will be made available in your account.

> [NOTE] If the CDP is uploading to a specific advertiser account rather than your manager account, you will be prompted to import the data into your manager account when the data is received.
