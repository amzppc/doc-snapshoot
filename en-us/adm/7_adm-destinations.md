---
title: adm destination information
description: Information about the destinations function in ads data manager.
type: guide
interface: bulk-operations
---
# Destinations

The "Destinations" tab displays the various locations where your data is being used. This tab also shows the currently supported product functionalities that utilize your first party data.

- You can navigate to the "Destinations" tab within Ads data Manager to find the audience you just uploaded and shared in the list of destinations.
- You can view that the audience is correctly linked to the intended Advertiser Account(s).
- You can view datasets in the destination account, create the dataset in another account, or deactivate the destination account through the Options column.
- From a given destination record, you can access your Amazon DSP Account to access the audience detail page, if you  have the right permissions to do so. For datasets shared to linked accounts, you may receive a 404 (not found) error page.
