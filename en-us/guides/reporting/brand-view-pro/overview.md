---
title: Overview of Brand Benchmarks
description: Learn about the Brand Benchmarks API
type: guide
interface: api
tags:
    - Reporting
---

# Overview

Amazon Brand View Pro (ABVP) is a competitive insights tool that provides Amazon sellers and advertisers with retail and media metrics to benchmark their on-Amazon brand performance against their peers. ABVP helps businesses optimize their Amazon marketing strategies by offering insights into sales growth, share of voice, traffic and conversion trends, and more. With ABVP 3P API access, advertisers can programmatically incorporate their ABVP reporting into their own internal tools and dashboards. Users can use the API to obtain the most recently ASIN-level and brand-level performance metrics.

## Eligibility

In order to be eligible for the Amazon Brand View Pro (ABVP), advertisers must meet several advertising and retail criteria. Please reach out to your Amazon account team for details. Eligibility is assessed annually and advertisers with access to ABVP must continue to meet the criteria to maintain their participation. We do not offer ABVP API access to advertising agencies. We offer it only directly with the advertisers who have completed onboarding to ABVP.

## Next steps

Learn [how to get started with the Amazon Brand View Pro (ABVP) API](guides/reporting/brand-view-pro/getting-started).
