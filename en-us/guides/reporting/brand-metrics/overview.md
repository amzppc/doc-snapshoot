---
title: Brand Metrics API Overview
description: Overview of the brand metrics API
type: guide
interface: api
tags:
    - Brand Metrics
keywords:
    - overview
---

# Brand Metrics API open beta overview

Brand Metrics provides a new measurement solution that quantifies opportunities for your brand at each stage of the customer journey on Amazon, and helps brands understand the value of different shopping engagements that impact stages of that journey. You can now access Awareness and Consideration indices that compare your performance to peers using models predictive of consideration and sales. Brand Metrics quantifies the number of customers in the awareness and consideration marketing funnel stages and is built at scale to measure all shopping engagements with your brand on Amazon, not just ad-attributed engagements. Additionally, BM breaks out key shopping engagements at each stage of the shopping journey, along with the Return on Engagement, so you can measure the historical sales following a consideration event or purchase.

The Brand Metrics API enables integrators to easily access Brand Metrics for Brands they support. 

## Brand Metrics API open beta eligibility

Please note: Brand Metrics is in open beta and available to professional sellers enrolled in Amazon Brand Registry and vendors that sell products on Amazon in the following markets: United States, Canada, Mexico, United Kingdom, Germany, France, India, Italy, Spain, Japan and Australia.

## Next steps

Learn how to [get started](guides/reporting/brand-metrics/getting-started) with the Brand Metrics API.
















