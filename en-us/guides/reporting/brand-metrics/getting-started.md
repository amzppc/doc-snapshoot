---
title: Getting started with the Brand Metrics API
description: Getting started with the Brand Metrics API
type: guide
interface: api
tags:
    - Brand Metrics
    - Authorization
keywords:
    - prerequisites
---

# Getting started with the Brand Metrics API

In order to get started with Brand Metrics API, you must first:

1. Set up your Advertising API developer account, if you have no already done so.
2. Obtain authorization or access for an eligible advertiser's Amazon Ads account. 

To set up your Advertising API developer account, follow the steps in the [Amazon Ads API Onboarding Overview](guides/onboarding/overview).

There are three ways to obtain authorization or access permission for an eligible advertiser's Amazon Ads account:

1. **Recommended approach:** Create an [OAuth Login With Amazon application](guides/onboarding/create-lwa-app) and the advertiser uses it to grant you access to their profile.
2. The advertiser invites you to their ad console account via the email address associated with your API developer account.
3. The advertiser invites you to a manager account linked to their ad console account via the email address associated with your API developer account.

Once you have completed the above requirements, you can use the Amazon Attribution API to read the following resources associated with your advertiser:

* Profiles
* Publishers
* Attribution tags
* Reports

## Next steps

Learn how to use the [Brand Metrics API](guides/reporting/brand-metrics/how-to).
