---
title: Sponsored ads report types (version 3)
description: Learn what report types and metrics are available in version 3 reporting for sponsored ads using the Amazon Ads API.
type: guide
interface: api
tags:
    - Reporting
    - Sponsored Products
    - Sponsored Brands
---

# Report types

The Amazon Ads API supports a number of report types based on campaign entity structure. Depending on the ad type, campaign performance can be broken down by various dimensions including campaign, ad group, ad, keyword, target, and ASIN.

### Availability by ad type

>[NOTE] Sponsored Brands reports are currently available in preview. During the preview period, data related to Sponsored Brands campaigns with flag `isMultiAdGroupsEnabled=False` won’t be available. Once version 3 reporting supports all Sponsored Brands campaigns, we will announce general availability in the [release notes](release-notes/index).

Each type of sponsored ad supports different report types and has a separate path defined.

| Report type | Sponsored Products | Sponsored Brands | Sponsored Display | Sponsored Television | Amazon DSP | 
|-------------|--------------------|------------------|------|--------|--------|
| [Campaign](guides/reporting/v3/report-types/campaign) | x | x | x | x | x | 
| [Audience](guides/reporting/v3/report-types/audience) | | | | | x | 
| [Geo](guides/reporting/v3/report-types/geo) | | | | | x | 
| [Inventory](guides/reporting/v3/report-types/inventory) | | | | | x | 
| [Product](guides/reporting/v3/report-types/product) | | | | | x | 
| [Tech](guides/reporting/v3/report-types/tech) | | | | | x | 
| [Audio and video (beta)](guides/reporting/v3/report-types/audio-and-video) | | | | | x | 
| [Ad group](guides/reporting/v3/report-types/ad-group) | | x | x|| | 
| [Placement](guides/reporting/v3/report-types/placement) | | x|  || | 
| [Targeting](guides/reporting/v3/report-types/targeting) | x | x | x |x| |
| [Search term](guides/reporting/v3/report-types/search-term) | x | x | || |
| [Advertised product](guides/reporting/v3/report-types/advertised-product) | x |  | x|| | 
| [Ad](guides/reporting/v3/report-types/ad) | | x |  || |
| [Purchased product](guides/reporting/v3/report-types/purchased-product) | x | x  | x|| |
| [Gross and invalid traffic](guides/reporting/v3/report-types/gross-and-invalid-traffic) | x | x | x |  | |
| [Reach and frequency](guides/reporting/v3/report-types/reach) | | | | x | x |

