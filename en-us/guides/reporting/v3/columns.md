---
title: Reporting columns (version 3)
description: View descriptions for all sponsored ads version 3 reporting metrics supported by the Amazon Ads API.
type: guide
interface: api
tags:
    - Reporting
---

# Metrics 

All metrics available for the version 3 reporting API.

#### 3PFees

**Type**: Decimal

**Description**: The total CPM charges applied for using 3P data providers.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### 3PPreBidFee

**Type**: Decimal

**Description**: Total cost for using third-party pre-bid targeting.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### 3PPreBidFeeDoubleVerify

**Type**: Decimal

**Description**: The third party pre-bid fee applied for using DoubleVerify.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### 3PPreBidFeeIntegralAdScience

**Type**: Decimal

**Description**: The third party pre-bid fee applied for using IntegralAdScience.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### 3PPreBidFeeOracleDataCloud

**Type**: Decimal

**Description**: The third party pre-bid fee applied for using OracleDataCloud.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### 3PPreBidFeePixalate

**Type**: Decimal

**Description**: The third party pre-bid fee applied for using Pixalate.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### 3pFeeAutomotive

**Type**: Decimal

**Description**: A third-party fee applied to automotive data.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### 3pFeeAutomotiveAbsorbed

**Type**: Decimal

**Description**: CPM charge applied to impressions to track Automotive segment fees. The cost is itemized in reports, but absorbed by the user and excluded from total costs.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### 3pFeeCPM1

**Type**: Decimal

**Description**: A third-party fee applied.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### 3pFeeCPM1Absorbed

**Type**: Decimal

**Description**: CPM charge applied to impressions to track general 3P technology fees. The cost is itemized in reports, but absorbed by the user and excluded from total costs.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### 3pFeeCPM2

**Type**: Decimal

**Description**: A third-party fee applied.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### 3pFeeCPM2Absorbed

**Type**: Decimal

**Description**: CPM charge applied to impressions to track general 3P technology fees. The cost is itemized in reports, but absorbed by the user and excluded from total costs.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### 3pFeeCPM3

**Type**: Decimal

**Description**: A third-party fee applied.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### 3pFeeCPM3Absorbed

**Type**: Decimal

**Description**: CPM charge applied to impressions to track general 3P technology fees. The cost is itemized in reports, but absorbed by the user and excluded from total costs.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### 3pFeeComScore

**Type**: Decimal

**Description**: The third-party fee applied for using ComScore.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### 3pFeeComScoreAbsorbed

**Type**: Decimal

**Description**: CPM charge applied to impressions to track ComScore technology fees. The cost is itemized in reports, but absorbed by the user and excluded from total costs.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### 3pFeeDoubleVerify

**Type**: Decimal

**Description**: The third-party fee applied for using DoubleVerify.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### 3pFeeDoubleVerifyAbsorbed

**Type**: Decimal

**Description**: CPM charge applied to impressions to track DoubleVerify technology fees. The cost is itemized in reports, but absorbed by the user and excluded from total costs.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### 3pFeeDoubleclickCampaignManager

**Type**: Decimal

**Description**: The third-party fee applied for using DoubleClick Campaign Manager.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### 3pFeeDoubleclickCampaignManagerAbsorbed

**Type**: Decimal

**Description**: CPM charge applied to impressions to track Doubleclick Campaign Manager technology fees. The cost is itemized in reports, but absorbed by the user and excluded from total costs.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### 3pFeeIntegralAdScience

**Type**: Decimal

**Description**: The third-party fee applied for using Integral Ad Science.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### 3pFeeIntegralAdScienceAbsorbed

**Type**: Decimal

**Description**: CPM charge applied to impressions to track Integral Ad Science technology fees. The cost is itemized in reports, but absorbed by the user and excluded from total costs.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### NewToBrandECPP

**Type**: Decimal

**Description**: Effective (average) cost to acquire a new-to-brand purchase conversion for a promoted product. (New-to-brand eCPP = Total cost / New-to-brand purchases) Use Total new-to-brand eCPP to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### acosClicks14d

**Type**: Decimal

**Description**: Advertising cost of sales based on purchases made within 14 days of an ad click.

**Report types**: [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### acosClicks7d

**Type**: Decimal

**Description**: Advertising cost of sales based on purchases made within 7 days of an ad click.

**Report types**: [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### adGroupId

**Type**: Integer

**Description**: Unique numerical ID of the ad group.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### adGroupName

**Type**: String

**Description**: The name of the ad group as entered by the advertiser.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### adId

**Type**: Integer

**Description**: Unique numerical ID of the ad.

**Report types**: [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sbAds](guides/reporting/v3/report-types/ad), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### adKeywordStatus

**Type**: String

**Description**: Current status of a keyword.

**Report types**: [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdTargeting](guides/reporting/v3/report-types/targeting), [stTargeting](guides/reporting/v3/report-types/targeting)

#### adStatus

**Type**: String

**Description**: Status of the ad group. 

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting)

#### addToCart

**Type**: Integer

**Description**: Number of times shoppers added a brand's products to their cart, attributed to an ad view or click.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### addToCartBrandHalo

**Type**: Integer

**Description**: Number of times shoppers added a brand halo product to their cart, attributed to an ad view or click. Use Total ATC to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### addToCartBrandHaloClicks

**Type**: Integer

**Description**: Number of times shoppers added a brand halo product to their cart, attributed to an ad click. Use Total ATC clicks to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### addToCartBrandHaloViews

**Type**: Integer

**Description**: Number of times shoppers added a brand halo product to their cart, attributed to an ad view. Use Total ATC views to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### addToCartClicks

**Type**: Integer

**Description**: Number of times shoppers added a brand's products to their cart, attributed to an ad click.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### addToCartRate

**Type**: Decimal

**Description**: Calculated by divididing addToCart by impressions.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sbAds](guides/reporting/v3/report-types/ad), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### addToCartViews

**Type**: Integer

**Description**: Number of times shoppers added the brands' products to their cart, attributed to an ad view.

**Report types**: [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### addToList

**Type**: Integer

**Description**: Number of times shoppers added a promoted product to a wish list, gift list, or registry, attributed to an ad view or click. Use Total ATL to see all conversions for the brands' products.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sbAds](guides/reporting/v3/report-types/ad), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### addToListBrandHalo

**Type**: Integer

**Description**: Number of times shoppers added a brand halo product to a wish list, gift list, or registry, attributed to an ad view or click. Use Total ATL to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### addToListBrandHaloClicks

**Type**: Integer

**Description**: Number of times shoppers added a brand halo product to a wish list, gift list, or registry, attributed to an ad click. Use Total ATL clicks to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### addToListBrandHaloViews

**Type**: Integer

**Description**: Number of times shoppers added a brand halo product to a wish list, gift list, or registry, attributed to an ad view. Use Total ATL views to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### addToListClicks

**Type**: Integer

**Description**: Number of times shoppers added a promoted product to a wish list, gift list, or registry, attributed to an ad click. Use Total ATL clicks to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### addToListFromClicks

**Type**: Integer

**Description**: Number of times shoppers added a promoted product to a wish list, gift list, or registry, attributed to an ad click.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sbAds](guides/reporting/v3/report-types/ad)

#### addToListFromViews

**Type**: Integer

**Description**: Number of times shoppers added a promoted product to a wish list, gift list, or registry, attributed to an ad view.

**Report types**: [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### addToListRate

**Type**: Integer

**Description**: Rate of Add to List conversions for promoted products relative to the number of impressions. (ATLR = ATL / Impressions) Use Total ATLR to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### addToListViews

**Type**: Decimal

**Description**: Number of times shoppers added a promoted product to a wish list, gift list, or registry, attributed to an ad view. Use Total ATL views to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### addToShoppingCart

**Type**: Integer

**Description**: The number of Add to shopping cart conversions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### addToShoppingCartCPA

**Type**: Decimal

**Description**: The average cost to acquire an Add to shopping cart conversion. (ATSC CPA = Total cost / ATSC)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### addToShoppingCartCVR

**Type**: Decimal

**Description**: The number of Add to shopping cart conversions relative to the number of ad impressions. (ATSC CVR = ATSC / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### addToShoppingCartClicks

**Type**: Integer

**Description**: The number of Add to shopping cart conversions attributed to an ad click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### addToShoppingCartValueAverage

**Type**: Decimal

**Description**: Average value associated with an Add to Shopping cart conversion. (ATSC value average = ATSC value sum / ATSC)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### addToShoppingCartValueSum

**Type**: Decimal

**Description**: Sum of Add to shopping cart conversion values

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### addToShoppingCartViews

**Type**: Integer

**Description**: The number of Add to shopping cart conversions attributed to an ad view.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### addToWatchlist

**Type**: Integer

**Description**: The number of times Add to Watchlist was clicked on a featured product.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### addToWatchlistClicks

**Type**: Integer

**Description**: The number of Add to Watchlist clicks attributed to ad click-throughs.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### addToWatchlistRate

**Type**: Decimal

**Description**: The number of Add to Watchlist clicks relative to the number of impressions. (ATWR = ATW / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### addToWatchlistViews

**Type**: Integer

**Description**: The number of Add to Watchlist clicks attributed to ad impressions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### advertisedAsin

**Type**: String

**Description**: The ASIN associated to an advertised product.

**Report types**: [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### advertisedSku

**Type**: String

**Description**: The SKU being advertised. Not available for vendors.

**Report types**: [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### advertiserCountry

**Type**: String

**Description**: The country assigned to the advertiser.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### advertiserId

**Type**: String

**Description**: The unique identifier for the advertiser.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience), [dspAudioAndVideo](guides/reporting/v3/report-types/audio-and-video), [dspReachFrequency](guides/reporting/v3/report-types/reach#amazon-dsp)

#### advertiserName

**Type**: String

**Description**: The customer that has an advertising relationship with Amazon.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience), [dspAudioAndVideo](guides/reporting/v3/report-types/audio-and-video), [dspReachFrequency](guides/reporting/v3/report-types/reach#amazon-dsp)

#### advertiserTimezone

**Type**: String

**Description**: The time zone the advertiser uses for reporting and ad serving purposes.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### agencyFee

**Type**: Decimal

**Description**: A percentage or flat fee removed from the total budget to compensate the agency that is managing the media buy.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspGeo](guides/reporting/v3/report-types/geo)

#### alexaSkillEnable

**Type**: Integer

**Description**: The number of Alexa skill enable conversions

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### alexaSkillEnableClicks

**Type**: Integer

**Description**: The number of detail page view conversions attributed to ad click-throughs

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### alexaSkillEnableRate

**Type**: Decimal

**Description**: The number of Alexa skill enable conversions relative to the number of ad impressions. (Alexa skill enable rate = Alexa skill enable / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### alexaSkillEnableViews

**Type**: Integer

**Description**: The number of alexa skill enable conversions attributed to ad impressions views

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### amazonDSPAudienceFee

**Type**: Decimal

**Description**: CPM charge applied to impressions that leverage Amazon's behavioral targeting.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### amazonDSPConsoleFee

**Type**: Decimal

**Description**: The technology fee applied to the media supply costs.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### amazonExclusiveReachRate

**Type**: Decimal

**Description**: The percentage of unique viewers reached by a streaming TV inventory that were not reached by any other streaming TV inventory in the campaign. (Amazon exclusive reach rate = 100 X [exclusive reach for streaming TV inventory / total streaming TV reach]).

**Report types**: [dspReachFrequency](guides/reporting/v3/report-types/reach#amazon-dsp)

#### appStoreFirstOpens

**Type**: Integer

**Description**: The number of first-time app opens attributed to a click or view on an ad

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspProduct](guides/reporting/v3/report-types/product)

#### appStoreFirstOpensClicks

**Type**: Integer

**Description**: The number of first-time app opens attributed to a click on an ad

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspProduct](guides/reporting/v3/report-types/product)

#### appStoreFirstOpensRate

**Type**: Decimal

**Description**: The ratio of how often customers opened the app for the first time when an ad was displayed. This is calculated as first app opens divided by impressions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### appStoreFirstOpensViews

**Type**: Integer

**Description**: The number of first-time app opens attributed to ad views.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspProduct](guides/reporting/v3/report-types/product)

#### appStoreFirstSessionHours

**Type**: Integer

**Description**: The number of hours spent using the app during first-time app open sessions attributed to a click or view on ad ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspProduct](guides/reporting/v3/report-types/product)

#### appStoreFirstSessionHoursClicks

**Type**: Integer

**Description**: The number of hours spent using the app during first-time app open sessions attributed to a click on an ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspProduct](guides/reporting/v3/report-types/product)

#### appStoreFirstSessionHoursViews

**Type**: Integer

**Description**: The number of hours spent using the app during first-time app open sessions attributed to ad views.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspProduct](guides/reporting/v3/report-types/product)

#### appStoreOpens

**Type**: Integer

**Description**: The number of first-time and recurring app opens attributed to a click or view on an ad

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspProduct](guides/reporting/v3/report-types/product)

#### appStoreOpensClicks

**Type**: Integer

**Description**: The number of first-time and recurring app opens attributed to a click on an ad

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspProduct](guides/reporting/v3/report-types/product)

#### appStoreOpensRate

**Type**: Decimal

**Description**: The ratio of app opens to ad impressions. This is calculated as first-time and recurring app opens divided by impressions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### appStoreOpensViews

**Type**: Integer

**Description**: The number of first-time and recurring app opens attributed to ad views.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspProduct](guides/reporting/v3/report-types/product)

#### appStoreUsageHours

**Type**: Decimal

**Description**: The number of hours spent using the app during first-time and recurring app open sessions attributed to a click or view on ad ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspProduct](guides/reporting/v3/report-types/product)

#### appStoreUsageHoursClicks

**Type**: Decimal

**Description**: The number of hours spent using the app during first-time and recurring app open sessions attributed to a click on an ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspProduct](guides/reporting/v3/report-types/product)

#### appStoreUsageHoursViews

**Type**: Decimal

**Description**: The number of hours spent using the app during first-time and recurring app open sessions attributed to ad views.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspProduct](guides/reporting/v3/report-types/product)

#### appSubscriptionCost

**Type**: Decimal

**Description**: The cost to acquire free trial and paid app subscriptions. This is calculated as total cost divided by app subscription sign-ups.

**Report types**: [dspGeo](guides/reporting/v3/report-types/geo)

#### appSubscriptionFreeTrialCost

**Type**: Decimal

**Description**: The cost to acquire free trial app subscriptions. This is calculated as total cost divided by free trial app subscription sign-ups.

**Report types**: [dspGeo](guides/reporting/v3/report-types/geo)

#### appSubscriptionSignUp

**Type**: Integer

**Description**: The number of free trial and paid app subscriptions associated with a click or view on your ad.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### appSubscriptionSignUpClicks

**Type**: Integer

**Description**: The number of free trial and paid app subscriptions associated with a click on your ad.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### appSubscriptionSignUpFreeTrial

**Type**: Integer

**Description**: The number of free trial app subscriptions associated with a click or view on your ad.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### appSubscriptionSignUpFreeTrialClicks

**Type**: Integer

**Description**: The number of free trial app subscriptions associated with a click on your ad.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### appSubscriptionSignUpFreeTrialRate

**Type**: Decimal

**Description**: The ratio of how often customers signed up for a free trial subscription when your ad was displayed. This is calculated as free trial app subscription sign-ups divided by impressions.

**Report types**: [dspGeo](guides/reporting/v3/report-types/geo)

#### appSubscriptionSignUpFreeTrialViews

**Type**: Integer

**Description**: The number of free trial app subscriptions associated with ad impressions.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### appSubscriptionSignUpPaid

**Type**: Integer

**Description**: The number of paid app subscriptions associated with a click or view on your ad.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### appSubscriptionSignUpPaidClicks

**Type**: Integer

**Description**: The number of paid app subscriptions associated with a click on your ad.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### appSubscriptionSignUpPaidCost

**Type**: Decimal

**Description**: The cost to acquire paid app subscriptions. This is calculated as total cost divided by paid app subscription sign-ups.

**Report types**: [dspGeo](guides/reporting/v3/report-types/geo)

#### appSubscriptionSignUpPaidRate

**Type**: Decimal

**Description**: The ratio of how often customers signed up for a paid app subscription when your ad was displayed. This is calculated as paid app subscription sign-ups divided by impressions.

**Report types**: [dspGeo](guides/reporting/v3/report-types/geo)

#### appSubscriptionSignUpPaidViews

**Type**: Integer

**Description**: The number of paid app subscriptions associated with ad impressions.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### appSubscriptionSignUpRate

**Type**: Decimal

**Description**: The ratio of how often customers signed up for a free trial or paid app subscription when your ad was displayed. This is calculated as app subscription sign-ups divided by impressions.

**Report types**: [dspGeo](guides/reporting/v3/report-types/geo)

#### appSubscriptionSignUpViews

**Type**: Integer

**Description**: The number of free trial and paid app subscriptions associated with ad impressions.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### application

**Type**: Integer

**Description**: The number of Application conversions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### applicationCPA

**Type**: Decimal

**Description**: The average cost to acquire an Application conversion. (Application CPA = Total cost / Application)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### applicationCVR

**Type**: Decimal

**Description**: The number of Application conversions relative to the number of ad impressions. (Application CVR = Application / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### applicationClicks

**Type**: Integer

**Description**: The number of Application conversions attributed to an ad click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### applicationValueAverage

**Type**: Decimal

**Description**: Average value associated with an Application conversion. (Application value average = Application value sum / Application)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### applicationValueSum

**Type**: Decimal

**Description**: Sum of Application conversion values

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### applicationViews

**Type**: Integer

**Description**: The number of Application conversions attributed to an ad view.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### asin

**Type**: String

**Description**: A unique block of letters and/or numbers that identify all products sold on Amazon.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### asinBrandHalo

**Type**: String

**Description**: Represents a product that was purchased after an ad click but has a different SKU/ASIN that what was advertised.

**Report types**: [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### attributedSalesSameSku14d

**Type**: Decimal

**Description**: Total value of sales occurring within 14 days of ad click where the purchased SKU was the same as the SKU advertised. Sponsored Products only.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### attributedSalesSameSku1d

**Type**: Decimal

**Description**: Total value of sales occurring within 1 day of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### attributedSalesSameSku30d

**Type**: Decimal

**Description**: Total value of sales occurring within 30 days of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### attributedSalesSameSku7d

**Type**: Decimal

**Description**: Total value of sales occurring within 7 days of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### attributionType

**Type**: String

**Description**: Describes whether a purchase is attributed to a promoted product or brand-halo effect.

**Report types**: [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### audioAdCompanionBannerClicks

**Type**: Integer

**Description**: Tracks the number of times that the audio ad companion banner was clicked.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### audioAdCompanionBannerViews

**Type**: Integer

**Description**: Tracks the number of times that the audio ad companion banner was displayed.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### audioAdCompletionRate

**Type**: Decimal

**Description**: The number of audio completions relative to the number of audio starts.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### audioAdCompletions

**Type**: Integer

**Description**: Tracks the number of times the audio ad plays to the end.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### audioAdFirstQuartile

**Type**: Integer

**Description**: Tracks the number of times the audio ad plays to 25% of its length.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### audioAdMidpoint

**Type**: Integer

**Description**: Tracks the number of times the audio ad plays to 50% of its length.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### audioAdMute

**Type**: Integer

**Description**: Tracks the number of times a user mutes the audio ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### audioAdPause

**Type**: Integer

**Description**: The number of times a user pauses the audio ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### audioAdProgression

**Type**: Decimal

**Description**: Tracks an optimal audio ad time marker agreed upon with the publisher

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### audioAdResume

**Type**: Integer

**Description**: Tracks the number of times a user resumes playback of the audio ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### audioAdRewind

**Type**: Integer

**Description**: Tracks the number of times a user rewinds the audio ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### audioAdSkip

**Type**: Integer

**Description**: Tracks the number of times a user skips the audio ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### audioAdStart

**Type**: Integer

**Description**: Tracks the number of times audio ad starts playing.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### audioAdThirdQuartile

**Type**: Integer

**Description**: Tracks the number of times the audio ad plays to 75% of its length.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### audioAdUnmute

**Type**: Integer

**Description**: Tracks the number of times a user skips the audio ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### audioAdViews

**Type**: Integer

**Description**: Tracks the number of times that some playback of the audio ad has occurred.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### averageCostPerClickToApply

**Type**: Decimal

**Description**: The average cost to acquire a Click to apply conversion. (eCPCTA = Total cost / CTA)

**Report types**: [dspAudience](guides/reporting/v3/report-types/audience)

#### averageCostPerKindleDetailPageView

**Type**: Decimal

**Description**: The average cost to acquire an Ad Details page view conversion on a Kindle E Ink device. (eCPDPVK = Total cost / DPVK)

**Report types**: [dspAudience](guides/reporting/v3/report-types/audience)

#### bidOptimization

**Type**: String

**Description**: Bid optimization for Sponsored Display ad groups. For vCPM campaigns, the value is always reach. For CPC campaigns, value is either clicks or conversions.

**Report types**: [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### brandName

**Type**: String

**Description**: The name of the brand the campaign is advertising.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### brandSearchRate

**Type**: Decimal

**Description**: The number of branded search conversions relative to the number of ad impressions. (Branded search rate = Branded searches / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### brandSearches

**Type**: Integer

**Description**: The number of times a branded keyword was searched on Amazon based on keywords generated from the featured ASINs in your campaign.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### brandSearchesClicks

**Type**: Integer

**Description**: The number of branded search conversions attributed to ad click-throughs.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### brandSearchesViews

**Type**: Integer

**Description**: The number of branded search conversions attributed to ad impressions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### brandedSearchRate

**Type**: Decimal

**Description**: Rate of Branded Searches relative to the number of impressions. (BS/Impressions)

**Report types**: [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### brandedSearches

**Type**: Integer

**Description**: The number of searches that included the name of your brand occurring within 14 days of an ad click or view.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad)

#### brandedSearchesClicks

**Type**: Integer

**Description**: The number of searches that included the name of your brand occurring within 14 days of an ad click.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad)

#### brandedSearchesViews

**Type**: Integer

**Description**: The number of times a branded keyword was searched on Amazon based on the brands associated to the featured ASINs in your campaign, attributed to an ad view.

**Report types**: [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting)

#### browserName

**Type**: String

**Description**: The web browsers the customers used to view the ad

**Report types**: [dspTech](guides/reporting/v3/report-types/tech)

#### browserVersion

**Type**: String

**Description**: The specific browser versions the customers used to view the ad

**Report types**: [dspTech](guides/reporting/v3/report-types/tech)

#### campaignApplicableBudgetRuleId

**Type**: String

**Description**: The ID associated to the active budget rule for a campaign.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign)

#### campaignApplicableBudgetRuleName

**Type**: String

**Description**: The name associated to the active budget rule for a campaign.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign)

#### campaignBiddingStrategy

**Type**: String

**Description**: The bidding strategy associated with a campaign.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign)

#### campaignBudgetAmount

**Type**: Decimal

**Description**: Total budget allocated to the campaign.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### campaignBudgetCurrencyCode

**Type**: String

**Description**: The currency code associated with the campaign.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### campaignBudgetType

**Type**: String

**Description**: One of daily or lifetime.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### campaignId

**Type**: Integer

**Description**: The ID associated with a campaign.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### campaignName

**Type**: String

**Description**: The name associated with a campaign.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [spGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sdGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbAds](guides/reporting/v3/report-types/ad), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### campaignPriceTypeCode

**Type**: String

**Description**: The code of the campaign cost type.

**Report types**: [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### campaignRuleBasedBudgetAmount

**Type**: Decimal

**Description**: The value of the rule-based budget for a campaign.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign)

#### campaignStatus

**Type**: String

**Description**: The status of a campaign.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [spGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sdGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbAds](guides/reporting/v3/report-types/ad), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### checkout

**Type**: Integer

**Description**: The number of Checkout conversions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### checkoutCPA

**Type**: Decimal

**Description**: The average cost to acquire a Checkout conversion. (Checkout CPA = Total cost / Checkout)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### checkoutCVR

**Type**: Decimal

**Description**: The number of Checkout conversions relative to the number of ad impressions. (Checkout CVR = Checkout / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### checkoutClicks

**Type**: Integer

**Description**: The number of Checkout conversions attributed to an ad click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### checkoutValueAverage

**Type**: Decimal

**Description**: Average value associated with a Checkout conversion. (Checkout value average = Checkout value sum / Checkout)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### checkoutValueSum

**Type**: Decimal

**Description**: Sum of Checkout conversion values

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### checkoutViews

**Type**: Integer

**Description**: The number of Checkout conversions attributed to an ad view.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### city

**Type**: String

**Description**: City.

**Report types**: [dspGeo](guides/reporting/v3/report-types/geo)

#### clicks

**Type**: Integer

**Description**: Total number of clicks on an ad.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [spGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sdGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbAds](guides/reporting/v3/report-types/ad), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### clickThroughRate

**Type**: Decimal

**Description**: Clicks divided by impressions.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### clickToApply

**Type**: Integer

**Description**: The number of time Click to apply was clicked for a featured product.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### clickToApplyBrandHalo

**Type**: Integer

**Description**: The number of times Click to apply was clicked on other products from the same brands as the products tracked in the order, attributed to an ad.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### clickToApplyBrandHaloClicks

**Type**: Integer

**Description**: The number of Click to apply clicks attributed to ad click-throughs. These include clicks on other products from the same brands as the ASINs tracked in the order.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### clickToApplyBrandHaloViews

**Type**: Integer

**Description**: The number of Click to apply clicks attributed to impressions. These include clicks on other products from the same brands as the ASINs tracked in the order.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### clickToApplyClicks

**Type**: Integer

**Description**: The number of click to apply conversions attributed to ad click-throughs.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### clickToApplyConversionRate

**Type**: Decimal

**Description**: The number of Click to apply conversions relative to the number of impressions. (CTAR = CTA / Impressions)

**Report types**: [dspAudience](guides/reporting/v3/report-types/audience)

#### clickToApplyViews

**Type**: Integer

**Description**: The number of Click to apply conversions attributed to ad impressions.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### combinedECPP

**Type**: Decimal

**Description**: Effective (average) cost to acquire a purchase conversion on or off Amazon. (Total cost / Combined purchases)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### combinedERPM

**Type**: Decimal

**Description**: Effective (average) revenue for sales on and off Amazon generated per thousand impressions. (Combined Sales / (Impressions / 1000))

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### combinedProductSales

**Type**: Integer

**Description**: Sales (in local currency) for purchases on and off Amazon, attributed to an ad view or click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspGeo](guides/reporting/v3/report-types/geo)

#### combinedPurchaseRate

**Type**: Decimal

**Description**: Rate of attributed purchase events on and off Amazon, relative to ad impressions. (Combined purchases / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### combinedPurchases

**Type**: Integer

**Description**: Number of purchase events on and off Amazon, attributed to an ad view or click. (Off-Amazon purchases + Total purchases (Amazon))

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspGeo](guides/reporting/v3/report-types/geo)

#### combinedPurchasesClicks

**Type**: Integer

**Description**: Number of purchase events on and off Amazon, attributed to an ad click. (Off-Amazon purchases clicks + Total purchases clicks (Amazon))

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspGeo](guides/reporting/v3/report-types/geo)

#### combinedPurchasesViews

**Type**: Integer

**Description**: Number of purchase events on and off Amazon, attributed to an ad view. (Off-Amazon purchases views + Total purchases views (Amazon))

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspGeo](guides/reporting/v3/report-types/geo)

#### combinedROAS

**Type**: Decimal

**Description**: Return on advertising spend for products sold on and off Amazon, measured as ad-attributed sales per local currency unit of ad spend. (Combined product sales / Total cost)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### combinedUnitsSold

**Type**: Integer

**Description**: Units of product sold on and off Amazon, attributed to an ad view or click. A single purchase event can include multiple sold units.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspGeo](guides/reporting/v3/report-types/geo)

#### contact

**Type**: Integer

**Description**: The number of Contact conversions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### contactCPA

**Type**: Decimal

**Description**: The average cost to acquire a Contact conversion. (Contact CPA = Total cost / Contact)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### contactCVR

**Type**: Decimal

**Description**: The number of Contact conversions relative to the number of ad impressions. (Contact CVR = Contact / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### contactClicks

**Type**: Integer

**Description**: The number of Contact conversions attributed to an ad click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### contactValueAverage

**Type**: Decimal

**Description**: Average value associated with a Contact conversion. (Contact value average = Contact value sum / Contact)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### contactValueSum

**Type**: Decimal

**Description**: Sum of Contact conversion values

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### contactViews

**Type**: Integer

**Description**: The number of Contact conversions attributed to an ad view.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### contentGenre

**Type**: String

**Description**: Genre of the show where the ad impression was delivered. (e.g., Horror, Drama)

**Report types**: [dspAudioAndVideo](guides/reporting/v3/report-types/audio-and-video)

#### contentRating

**Type**: String

**Description**: DSP content rating for parental guidance (e.g., "PG-13", "R")

**Report types**: [dspAudioAndVideo](guides/reporting/v3/report-types/audio-and-video)

#### conversionType

**Type**: String

**Description**: The conversion type describes whether the conversion happened on a promoted or a brand halo ASIN.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### conversionsBrandHalo

**Type**: Integer

**Description**: For Sponsored Display vCPM campaigns, the total number of attributed conversion events occurring within 14 days of an ad click or view where the purchased SKU was different from the SKU advertised.

**Report types**: [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### conversionsBrandHaloClicks

**Type**: Integer

**Description**: For Sponsored Display vCPM campaigns, the total number of attributed conversion events occurring within 14 days of an ad click or view where the purchased SKU was different from the SKU advertised.

**Report types**: [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### cost

**Type**: Decimal

**Description**: Total cost of ad clicks.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### costPerClick

**Type**: Decimal

**Description**: Total cost divided by total number of clicks.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### costPerThousandImpressions

**Type**: Decimal

**Description**: The cost per thousand impressions.

**Report types**: [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [dspAudioAndVideo](guides/reporting/v3/report-types/audio-and-video)

#### costType

**Type**: String

**Description**: Determines how the campaign will bid and charge. One of vCPM (cost per thousand viewable impressions) or CPC (cost per click). 

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sbAds](guides/reporting/v3/report-types/ad)

#### country

**Type**: String

**Description**: Country.

**Report types**: [dspGeo](guides/reporting/v3/report-types/geo)

#### creativeAdId

**Type**: String

**Description**: A unique identifier assigned to a creative. When this is different from creativeId, creativeAdId is the value that can be used in our /dsp/creatives endpoints.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### creativeId

**Type**: String

**Description**: A unique identifier assigned to a creative. When this is different from creativeAdId, creativeId cannot be used in our /dsp/creatives endpoints.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudioAndVideo](guides/reporting/v3/report-types/audio-and-video)

#### creativeLanguage

**Type**: String

**Description**: The primary language in the creative.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### creativeName

**Type**: String

**Description**: Creative name.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudioAndVideo](guides/reporting/v3/report-types/audio-and-video)

#### creativeSize

**Type**: String

**Description**: The dimensions of the creative in pixels.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### creativeType

**Type**: String

**Description**: The type of creative (for example static image, third party, or video).

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### date

**Type**: String

**Description**: Date when the ad activity ocurred in the format YYYY-MM-DD.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [spGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sdGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbAds](guides/reporting/v3/report-types/ad), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [dspCampaign](guides/reporting/v3/report-types/campaign), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands), [dspReachFrequency](guides/reporting/v3/report-types/reach#amazon-dsp)

#### deal

**Type**: String

**Description**: The name of a deal.

**Report types**: [dspInventory](guides/reporting/v3/report-types/inventory)

#### dealId

**Type**: String

**Description**: The unique identifier for the deal.

**Report types**: [dspInventory](guides/reporting/v3/report-types/inventory)

#### dealType

**Type**: String

**Description**: The type of a deal.

**Report types**: [dspInventory](guides/reporting/v3/report-types/inventory)

#### detailPageViewBrandHalo

**Type**: Integer

**Description**: Number of detail page views for products in your brand halo, attributed to an ad view or click. Use Total DPV to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### detailPageViewBrandHaloClicks

**Type**: Integer

**Description**: Number of detail page views for products in your brand halo, attributed to an ad click. Use Total DPV clicks to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### detailPageViewBrandHaloViews

**Type**: Integer

**Description**: Number of detail page views for products in your brand halo, attributed to an ad view. Use Total DPV views to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### detailPageViewClicks

**Type**: Integer

**Description**: Number of detail page views for promoted products, attributed to an ad click. Use Total DPV clicks to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### detailPageViewRate

**Type**: Decimal

**Description**: Detail page view rate for promoted products relative to the number of ad impressions. (DPV / Impressions = DPVR) Use Total DPVR to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### detailPageViewRatePromotedClicks

**Type**: Decimal

**Description**: Rate of click-attributed detail page view conversions for promoted products relative to the number of ad impressions. (DPVR Clicks = DPV clicks / Impressions) Use Total DPV Click Rate to see all conversions for the brands' products.

**Report types**: [dspAudience](guides/reporting/v3/report-types/audience)

#### detailPageViewViews

**Type**: Integer

**Description**: Number of detail page views for promoted products, attributed to an ad view. Use Total DPV views to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### detailPageViews

**Type**: Integer

**Description**: Number of detail page views occurring within 14 days of an ad click or view.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### detailPageViewsClicks

**Type**: Integer

**Description**: Number of detail page views occurring within 14 days of an ad click.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad)

#### detailPageViewsViews

**Type**: Integer

**Description**: The number of times a product detail page was viewed attributed to an ad view.

**Report types**: [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting)

#### deviceName

**Type**: String

**Description**: The devices the customers used to view the ad

**Report types**: [dspTech](guides/reporting/v3/report-types/tech)

#### dmaCode

**Type**: String

**Description**: The Designated Market Areas¬¨¬®‚àö√ú (DMA) the customers were in when they viewed the ad. DMA regions are created and defined by Nielsen

**Report types**: [dspGeo](guides/reporting/v3/report-types/geo)

#### dmaName

**Type**: String

**Description**: The Designated Market Areas¬¨¬®‚àö√ú (DMA) the customers were in when they viewed the ad. DMA regions are created and defined by Nielsen.

**Report types**: [dspGeo](guides/reporting/v3/report-types/geo)

#### downloadedVideoPlayRate

**Type**: Decimal

**Description**: The number of downloaded video plays relative to the number of impressions. (Downloaded video play rate = Downloaded video plays / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### downloadedVideoPlays

**Type**: Integer

**Description**: The number of times a video was downloaded then played for the featured product.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### downloadedVideoPlaysClicks

**Type**: Integer

**Description**: The number of downloaded video plays attributed to ad click-throughs.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### downloadedVideoPlaysViews

**Type**: Integer

**Description**: The number of downloaded video plays attributed to ad impressions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPAddToCart

**Type**: Decimal

**Description**: Effect cost per add to cart, calculated by cost divided by add-to-cart.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sbAds](guides/reporting/v3/report-types/ad), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPAddToList

**Type**: Decimal

**Description**: Effective (average) cost to acquire an Add to List conversion for a promoted product. (eCPATL = Total cost / ATL) Use Total eCPATL to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPAddToWatchlist

**Type**: Decimal

**Description**: The average cost to acquire an Add to Watchlist click (eCPATW = Total cost / ATW)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPAlexaSkillEnable

**Type**: Decimal

**Description**: The average cost to acquire an Alexa skill Enable conversion on a Kindle E Ink device. (eCP Alexa skill enable = Total cost / Alexa skill enable)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### eCPAppStoreFirstOpens

**Type**: Decimal

**Description**: The average cost to acquire a first-time app open. This is calculated as total cost divided by first app opens.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### eCPAppStoreOpens

**Type**: Integer

**Description**: The number of first-time and recurring app opens attributed to a click or view on an ad

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### eCPAudioAdCompletion

**Type**: Decimal

**Description**: The average cost to acquire an audio complete conversion (eCPVC = Total cost / audio complete)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPBrandSearch

**Type**: Decimal

**Description**: Effective (average) cost to acquire a Branded Search.

**Report types**: [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPC

**Type**: Decimal

**Description**: The average cost paid per click-through.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPDetailPageView

**Type**: Decimal

**Description**: Effective (average) cost to acquire a detail page view for a promoted product. (eCPDPV = Total cost / DPV) Use Total eCPDPV to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPDownloadedVideoPlays

**Type**: Decimal

**Description**: The average cost to acquire a downloaded video play (eCPDVP = Total cost / Downloaded video plays)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPFreeTrialSubscriptionSignup

**Type**: Decimal

**Description**: The cost to acquire a free trial subscription for sponsored products. This is calculated as total cost divided by free trial subscription sign ups.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPM

**Type**: Decimal

**Description**: The total cost per thousand impressions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPP

**Type**: Decimal

**Description**: Effective (average) cost to acquire a purchase conversion for a promoted product. (eCPP = Total cost / Purchases) Use Total eCPP to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPPaidSubscriptionSignup

**Type**: Decimal

**Description**: The cost to acquire a paid subscription for sponsored products. This is calculated as total cost divided by paid subscription sign ups.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPPlayTrailer

**Type**: Decimal

**Description**: The average cost to acquire a video trailer play (eCPPT = Total cost / Play trailers)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPProductReviewPageVisit

**Type**: Decimal

**Description**: Effective (average) cost to acquire a product review page conversion for a promoted product. (eCPPRPV = Total cost / PRPV) Use Total eCPPRPV to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPRental

**Type**: Decimal

**Description**: The average cost to acquire a rental (eCPR = Total cost / Rentals)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPSkillInvocation

**Type**: Decimal

**Description**: Effective (average) cost to acquire a Skill invocation conversion for a promoted Alexa skill. (Effective cost per skill invocation = Total cost / Skill invocations)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo)

#### eCPSubscriptionSignup

**Type**: Decimal

**Description**: The cost to acquire a free trial or paid subscription for sponsored products. This is calculated as total cost divided by subscription sign ups.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPVideoAdCompletion

**Type**: Decimal

**Description**: The average cost to acquire a Video complete conversion (eCPVC = Total cost / Video complete)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPVideoDownload

**Type**: Decimal

**Description**: The average cost to acquire a video download (eCPVD = Total cost / Video downloads)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPVideoStream

**Type**: Decimal

**Description**: The average cost to acquire a video stream. (eCPVS = Total cost / Video streams)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### eCPnewSubscribeAndSave

**Type**: Decimal

**Description**: Effective (average) cost to acquire a Subscribe & Save subscription for a promoted product. (eCPSnSS = Total cost / SnSS) Use Total eCPSnSS to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### eRPM

**Type**: Decimal

**Description**: Effective (average) revenue for promoted products generated per thousand impressions. (eRPM = Sales / (Impressions / 1000)) Use Total eRPM to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### effectiveCostPerDetailPageViewPromotedClicks

**Type**: Decimal

**Description**: Effective (average) cost to acquire a click-attributed detail page view conversion for a promoted product. (eCPDPV Clicks = Total cost / DPV clicks) Use Total eCPDPV Clicks to see all conversions for the brands' products.

**Report types**: [dspAudience](guides/reporting/v3/report-types/audience)

#### effectiveCostPerPurchasePromotedClicks

**Type**: Decimal

**Description**: Effective (average) cost to acquire a click-attributed purchase for a promoted product. (eCPP Clicks = Total cost / Purchases clicks) Use Total eCPP Clicks to see all conversions for the brands' products.

**Report types**: [dspAudience](guides/reporting/v3/report-types/audience)

#### effectiveCostPerSkillInvocation

**Type**: Decimal

**Description**: Effective (average) cost to acquire a Skill invocation conversion for a promoted Alexa skill. (Effective cost per skill invocation = Total cost / Skill invocations)

**Report types**: [dspProduct](guides/reporting/v3/report-types/product), [dspAudience](guides/reporting/v3/report-types/audience)

#### einkDetailPageView

**Type**: Integer

**Description**: The number of views of Ad Details pages on Kindle E Ink devices.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### einkDetailPageViewBrandHalo

**Type**: Integer

**Description**: The number of Detail page views (Kindle) on other products from the same brands as the products tracked in the order, attributed to an ad.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### einkDetailPageViewBrandHaloClicks

**Type**: Integer

**Description**: The number of Detail page views (Kindle) attributed to ad click-throughs. These include clicks on other products from the same brands as the ASINs tracked in the order.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### einkDetailPageViewBrandHaloViews

**Type**: Integer

**Description**: The number of Detail page views (Kindle) attributed to impressions. These include clicks on other products from the same brands as the ASINs tracked in the order.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### einkDetailPageViewClicks

**Type**: Integer

**Description**: The number of views of Ad Details pages on Kindle E Ink devices attributed to clicks on ads on Kindle E Ink devices.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### einkDetailPageViewViews

**Type**: Integer

**Description**: The number of views of Ad Details pages on Kindle E Ink devices attributed to views on ads on Kindle E Ink devices

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### endDate

**Type**: String

**Description**: End date of summary period for a report in the format YYYY-MM-DD.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [spGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sdGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbAds](guides/reporting/v3/report-types/ad), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### entityId

**Type**: String

**Description**: Entity (seat) ID.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### environmentName

**Type**: String

**Description**: Break the data down by which environment the bid request came from: web or app. This dimension is connected to web and app inventory, not line item types.

**Report types**: [dspTech](guides/reporting/v3/report-types/tech)

#### featuredAsin

**Type**: String

**Description**: The Amazon Standard Identification Number (ASIN) that was set as featured in the campaign.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### freeTrialSubscriptionSignupClicks

**Type**: Integer

**Description**: The number of free trial subscriptions for sponsored products associated with a click on your ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### freeTrialSubscriptionSignupRate

**Type**: Decimal

**Description**: The ratio of how often customers signed up for a free trial for sponsored products when you ad was displayed. This is calculated as free trials divided by impressions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### freeTrialSubscriptionSignupViews

**Type**: Integer

**Description**: The number of free trial subscriptions for sponsored products associated with ad impressions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### freeTrialSubscriptionSignups

**Type**: Integer

**Description**: The number of free trial subscriptions for sponsored products associated with a click or view on your ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### frequencyAverage

**Type**: Decimal

**Description**: The number of times an ad is served to an individual/ device/ account in a period.

**Report types**: [stCampaigns](guides/reporting/v3/report-types/campaign), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspReachFrequency](guides/reporting/v3/report-types/reach#amazon-dsp)

#### frequencyGroupId

**Type**: Long

**Description**: The identifier of the frequency group.

**Report types**: [dspReachFrequency](guides/reporting/v3/report-types/reach#amazon-dsp)

#### frequencyGroupName

**Type**: String

**Description**: The name of the frequency group as entered by the advertiser.

**Report types**: [dspReachFrequency](guides/reporting/v3/report-types/reach#amazon-dsp)

#### grossClickThroughs

**Type**: Integer

**Description**: The total number of times the ad was clicked. This includes valid, potentially fraudulent, non-human, and other illegitimate clicks.

**Report types**: [spGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sdGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspGeo](guides/reporting/v3/report-types/geo)

#### grossImpressions

**Type**: Integer

**Description**: The total number of times the ad was displayed. This includes valid and invalid impressions such as potentially fraudulent, non-human, and other illegitimate impressions.

**Report types**: [spGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sdGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudioAndVideo](guides/reporting/v3/report-types/audio-and-video)

#### householdFrequencyAverage

**Type**: Decimal

**Description**: The number of times an ad is served to a household in a period.

**Report types**: [stCampaigns](guides/reporting/v3/report-types/campaign), [dspCampaign](guides/reporting/v3/report-types/campaign)

#### householdReach

**Type**: Integer

**Description**: The number of unique households an ad impression is served to in a period.

**Report types**: [stCampaigns](guides/reporting/v3/report-types/campaign), [dspCampaign](guides/reporting/v3/report-types/campaign)

#### impressionFrequencyAverage

**Type**: Decimal

**Description**: The average number of exposures per unique user for an ad

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### impressions

**Type**: Integer

**Description**: Total number of ad impressions.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [spGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sdGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbAds](guides/reporting/v3/report-types/ad), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience), [dspAudioAndVideo](guides/reporting/v3/report-types/audio-and-video), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### impressionsFrequencyAverage

**Type**: Decimal

**Description**: Average number of times unique users were exposed to an ad over the lifetime of the campaign.

**Report types**: [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### impressionsViews

**Type**: Integer

**Description**: Number of impressions that met the Media Ratings Council (MRC) viewability standard. See viewability details at https://advertising.amazon.com/library/guides/viewability.

**Report types**: [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### interactiveImpressions

**Type**: Integer

**Description**: The number of impressions from an interactive creative. This includes, interactive video, interactive audio and interactive display creatives, and more.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### intervalEnd

**Type**: String

**Description**: End date of report data.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience), [dspAudioAndVideo](guides/reporting/v3/report-types/audio-and-video), [dspReachFrequency](guides/reporting/v3/report-types/reach#amazon-dsp)

#### intervalStart

**Type**: String

**Description**: Start date of report data.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience), [dspAudioAndVideo](guides/reporting/v3/report-types/audio-and-video), [dspReachFrequency](guides/reporting/v3/report-types/reach#amazon-dsp)

#### invalidClickThroughRate

**Type**: Decimal

**Description**: The percentage of gross click-throughs that were removed by the traffic quality filter. (Invalid click-throughs rate = invalid click-throughs / gross click-throughs)

**Report types**: [spGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sdGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspGeo](guides/reporting/v3/report-types/geo)

#### invalidClickThroughs

**Type**: Integer

**Description**: Clicks that were removed by the traffic quality filter. This includes potentially fraudulent, non-human, and other illegitimate traffic.

**Report types**: [spGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sdGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspGeo](guides/reporting/v3/report-types/geo)

#### invalidImpressionRate

**Type**: Decimal

**Description**: The percentage of gross impressions that were removed by the traffic quality filter. (Invalid impression rate = invalid impressions / gross impressions)

**Report types**: [spGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sdGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspGeo](guides/reporting/v3/report-types/geo)

#### invalidImpressions

**Type**: Integer

**Description**: The number of impressions removed by a traffic quality filter. This includes potentially fraudulent, non-human, and other illegitimate traffic.

**Report types**: [spGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sdGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudioAndVideo](guides/reporting/v3/report-types/audio-and-video)

#### keyword

**Type**: String

**Description**: Text of the keyword or a representation of the targeting expression (for Sponsored Products). For Sponsored Products targeting reports, the same value is returned in the targeting metric.

**Report types**: [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [stTargeting](guides/reporting/v3/report-types/targeting)

#### keywordBid

**Type**: Decimal

**Description**: Bid associated with a keyword or targeting expression.

**Report types**: [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [stTargeting](guides/reporting/v3/report-types/targeting)

#### keywordId

**Type**: Integer

**Description**: ID associated with a keyword or targeting expression.

**Report types**: [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [stTargeting](guides/reporting/v3/report-types/targeting)

#### keywordText

**Type**: String

**Description**: Text of the keyword. 

**Report types**: [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term)

#### keywordType

**Type**: String

**Description**: Type of matching for the keyword used in bid. For keywords, one of: BROAD, PHRASE, or EXACT. For Sponsored Products reports, keywordType can also contain the type of targeting expressions, one of: TARGETING_EXPRESSION or TARGETING_EXPRESSION_PREDEFINED.

**Report types**: [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [stTargeting](guides/reporting/v3/report-types/targeting)

#### kindleDetailPageView

**Type**: String

**Description**: The number of views of Ad Details pages on Kindle E Ink devices.

**Report types**: [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### kindleDetailPageViewClicks

**Type**: Integer

**Description**: The number of views of Ad Details pages on Kindle E Ink devices attributed to clicks on ads on Kindle E Ink devices.

**Report types**: [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### kindleDetailPageViewRate

**Type**: Decimal

**Description**: The number of views of the Ad Details page relative to the number of ad impressions on Kindle E Ink devices. (DPVKR = DPVK / Impressions)

**Report types**: [dspAudience](guides/reporting/v3/report-types/audience)

#### kindleDetailPageViewViews

**Type**: Integer

**Description**: The number of views of Ad Details pages on Kindle E Ink devices attributed to views on ads on Kindle E Ink devices

**Report types**: [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### kindleEditionNormalizedPagesRead14d

**Type**: Integer

**Description**: Number of attributed Kindle edition normalized pages read within 14 days of ad click.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### kindleEditionNormalizedPagesRoyalties14d

**Type**: Decimal

**Description**: The estimated royalties of attributed estimated Kindle edition normalized pages within 14 days of ad click.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### lead

**Type**: Integer

**Description**: The number of Lead conversions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### leadCPA

**Type**: Decimal

**Description**: The average cost to acquire a Lead conversion. (Lead CPA = Total cost / Lead)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### leadCVR

**Type**: Decimal

**Description**: The number of Lead conversions relative to the number of ad impressions. (Lead CVR = Lead / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### leadClicks

**Type**: Integer

**Description**: The number of Lead conversions attributed to an ad click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### leadFormOpens

**Type**: Integer

**Description**: The number of times your lead forms were opened.

**Report types**: [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### leadValueAverage

**Type**: Decimal

**Description**: Average value associated with a Lead conversion. (Lead value average = Lead value sum / Lead)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### leadValueSum

**Type**: Decimal

**Description**: Sum of Lead conversion values

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### leadViews

**Type**: Integer

**Description**: The number of Lead conversions attributed to an ad view.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### leads

**Type**: Integer

**Description**: The number of leads collected through your lead generation campaigns.

**Report types**: [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### lineItemApprovalStatus

**Type**: String

**Description**: Approval status for the line item

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### lineItemBudget

**Type**: Decimal

**Description**: The total amount of money that be consumed by a line item.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### lineItemBudgetCap

**Type**: Decimal

**Description**: The total budget set for the line item.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### lineItemComment

**Type**: String

**Description**: Comments entered about the line item, such as optimization changes, variations in targeting, etc.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### lineItemComments

**Type**: String

**Description**: Comments entered about the line item, such as optimization changes, variations in targeting, etc.

**Report types**: [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### lineItemDeliveryRate

**Type**: Decimal

**Description**: Delivery Rate of the order. Delivery Rate = (cost/budget)/(running days/total days) or (delivery impressions/budget impressions)/(running days/total days)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### lineItemEndDate

**Type**: String

**Description**: The last date for the line item's flight.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### lineItemExternalId

**Type**: String

**Description**: Line item custom ID optionally entered by user.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### lineItemId

**Type**: String

**Description**: A unique identifier assigned to a line item.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience), [dspAudioAndVideo](guides/reporting/v3/report-types/audio-and-video)

#### lineItemLanguageTargeting

**Type**: String

**Description**: The language targeting setting for the line item.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### lineItemName

**Type**: String

**Description**: Line item name.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience), [dspAudioAndVideo](guides/reporting/v3/report-types/audio-and-video)

#### lineItemStartDate

**Type**: String

**Description**: The first date for the line item's flight.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### lineItemStatus

**Type**: String

**Description**: Status of the ad/line item

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### lineItemType

**Type**: String

**Description**: Type of the ad/line item

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspAudience](guides/reporting/v3/report-types/audience)

#### linkOuts

**Type**: Integer

**Description**: The number of times link-outs to landing page were clicked. 

**Report types**: [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### marketplace

**Type**: String

**Description**: The Amazon-owned site the product is sold on.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### masterClicks

**Type**: Integer

**Description**: A click on a master advertisement that directs the user to a destination outside of the creative.

**Report types**: [dspAudience](guides/reporting/v3/report-types/audience)

#### masterImpressions

**Type**: Integer

**Description**: The number of times the master advertisement was displayed.

**Report types**: [dspAudience](guides/reporting/v3/report-types/audience)

#### matchType

**Type**: String

**Description**: Type of matching for the keyword used in bid. For keywords, one of: BROAD, PHRASE, or EXACT. For Sponsored Products reports, keywordType can also contain the type of targeting expressions, one of: TARGETING_EXPRESSION or TARGETING_EXPRESSION_PREDEFINED.

**Report types**: [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [stTargeting](guides/reporting/v3/report-types/targeting)

#### matchedTargetAsin

**Type**: String

**Description**: The ASIN associated with the product page where a Sponsored Display ad appeared.

**Report types**: [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting)

#### measurableImpressions

**Type**: Integer

**Description**: Number of impressions that were measured for viewability. The viewability metrics are available from October 1, 2019. Selecting a date range prior to October 1, 2019 will result in incomplete or inaccurate metrics.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### measurableRate

**Type**: Decimal

**Description**: Measurable impressions / total impressions. The viewability metrics are available from October 1, 2019. Selecting a date range prior to October 1, 2019 will result in incomplete or inaccurate metrics.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### mobileAppFirstStartAverage

**Type**: Decimal

**Description**: Average value associated with a Mobile app first start conversion. (Mobile app first start value average = Mobile app first start value sum / Mobile app first start)

**Report types**: [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo)

#### mobileAppFirstStartCPA

**Type**: Decimal

**Description**: The average cost to acquire a Mobile app first start conversion. (Mobile app first start CPA = Total cost / Mobile app first starts)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### mobileAppFirstStartCVR

**Type**: Decimal

**Description**: The number of Mobile app first start conversions relative to the number of ad impressions. (Mobile app first start CVR = Mobile app first start / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### mobileAppFirstStartClicks

**Type**: Integer

**Description**: The number of Mobile app first start conversions attributed to ad click-throughs.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### mobileAppFirstStartSum

**Type**: Decimal

**Description**: Sum of Mobile app first start conversion values

**Report types**: [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo)

#### mobileAppFirstStartViews

**Type**: Integer

**Description**: The number of Mobile app first start conversions attributed to ad impressions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### mobileAppFirstStarts

**Type**: Integer

**Description**: The number of times an app for the featured product was first started.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### newSubscribeAndSave

**Type**: Integer

**Description**: Number of new Subscribe & Save subscriptions for all products, attributed to an ad view or click. This does not include replenishment subscription orders. Use Total SnSS to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### newSubscribeAndSaveBrandHalo

**Type**: Integer

**Description**: Number of new Subscribe & Save subscriptions for brand halo products, attributed to an ad view or click. This does not include replenishment subscription orders. Use Total SnSS to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### newSubscribeAndSaveBrandHaloClicks

**Type**: Integer

**Description**: Number of new Subscribe & Save subscriptions for brand halo products, attributed to an ad click. This does not include replenishment subscription orders. Use Total SnSS clicks to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### newSubscribeAndSaveBrandHaloViews

**Type**: Integer

**Description**: Number of new Subscribe & Save subscriptions for brand halo products, attributed to an ad view. This does not include replenishment subscription orders. Use Total SnSS views to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### newSubscribeAndSaveClicks

**Type**: Integer

**Description**: Number of new Subscribe & Save subscriptions for all products, attributed to ad clicks. This does not include replenishment subscription orders. Use Total SnSS to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### newSubscribeAndSaveRate

**Type**: Decimal

**Description**: The number of new Subscribe & Save subscriptions relative to the number of ad impressions

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### newSubscribeAndSaveViews

**Type**: Integer

**Description**: Number of new Subscribe & Save subscriptions for all products, attributed to ad views. This does not include replenishment subscription orders. Use Total SnSS to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### newToBrandDetailPageViewClicks

**Type**: Integer

**Description**: Number of new-to-brand detail page views for all the brands' products, attributed to an ad click. 

**Report types**: [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### newToBrandDetailPageViewRate

**Type**: Decimal

**Description**: Calculated by dividing newToBrandDetailPageViews / impressions.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sbAds](guides/reporting/v3/report-types/ad), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### newToBrandDetailPageViewViews

**Type**: Integer

**Description**: Number of new-to-brand detail page views for all the brands' products, attributed to an ad view

**Report types**: [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### newToBrandDetailPageViews

**Type**: Integer

**Description**: The number of new detail page views from shoppers who have not previously viewed a detail page with an ASIN of the same brand in past 365 days and who either clicked or viewed an ad.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### newToBrandDetailPageViewsClicks

**Type**: Integer

**Description**: The number of new detail page views from shoppers who have not previously viewed a detail page with an ASIN of the same brand in past 365 days and who clicked an ad.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad)

#### newToBrandECPDetailPageView

**Type**: Decimal

**Description**: Effective cost per new-to-brand detail page view, calculated by cost divided by new-to-brand detail page view.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sbAds](guides/reporting/v3/report-types/ad), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### newToBrandERPM

**Type**: Decimal

**Description**: Effective (average) revenue generated per thousand impressions from promoted products purchased by new-to-brand shoppers. (NTB eRPM = NTB Sales / (Impressions / 1000)) Use Total new-to-brand eRPM to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### newToBrandProductSales

**Type**: Integer

**Description**: Sales (in local currency) of promoted products to new-to-brand shoppers, attributed to an ad view or click. Use Total new-to-brand product sales to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### newToBrandPurchasePercentageBrandHalo

**Type**: Decimal

**Description**: Percent of ad-attributed purchases for brand halo products that were new to brand. Use Total percent of purchases new-to-brand to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### newToBrandPurchaseRate

**Type**: Decimal

**Description**: Rate of new-to-brand purchase conversions for promoted products relative to the number of ad impressions. (New-to-brand purchase rate = New-to-brand purchases / Impressions) Use Total new-to-brand purchase rate to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### newToBrandPurchases

**Type**: Integer

**Description**: The number of first-time orders for brand products over a one-year lookback window resulting from an ad click or view. Not available for book vendors.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### newToBrandPurchases14d

**Type**: Integer

**Description**: The number of first-time orders for brand products over a one-year lookback window. Not available for book vendors.

**Report types**: [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### newToBrandPurchasesBrandHalo

**Type**: Integer

**Description**: Number of new-to-brand purchases for brand halo products, attributed to an ad view or click. Shoppers are "new to brand" if they have not purchased from the brand in the last 365 days. Use Total new-to-brand purchases to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### newToBrandPurchasesBrandHaloClicks

**Type**: Integer

**Description**: Number of new-to-brand purchases for brand halo products, attributed to an ad click. Shoppers are "new to brand" if they have not purchased from the brand in the last 365 days. Use Total new-to-brand purchases clicks to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### newToBrandPurchasesBrandHaloViews

**Type**: Integer

**Description**: Number of new-to-brand purchases for brand halo products, attributed to an ad view. Shoppers are "new to brand" if they have not purchased from the brand in the last 365 days. Use Total new-to-brand purchases views to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### newToBrandPurchasesClicks

**Type**: Integer

**Description**: The number of first-time orders for brand products over a one-year lookback window resulting from an ad click. Not available for book vendors.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### newToBrandPurchasesPercentage

**Type**: Decimal

**Description**: The percentage of total orders that are new-to-brand orders within 14 days of an ad click. Not available for book vendors. 

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspProduct](guides/reporting/v3/report-types/product), [dspAudience](guides/reporting/v3/report-types/audience)

#### newToBrandPurchasesPercentage14d

**Type**: Decimal

**Description**: The percentage of total orders that are new-to-brand orders within 14 days of an ad click. Not available for book vendors. 

**Report types**: [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### newToBrandPurchasesRate

**Type**: Decimal

**Description**: The percentage of total orders that are new-to-brand orders. Not available for book vendors.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad)

#### newToBrandPurchasesViews

**Type**: Integer

**Description**: The number of new to branch purchases attributed to an ad view.

**Report types**: [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### newToBrandROAS

**Type**: Decimal

**Description**: Return on ad spend for new to brand sales

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### newToBrandSales

**Type**: Decimal

**Description**: Total value of new-to-brand sales occurring within 14 days of an ad click or view. Not available for book vendors.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad)

#### newToBrandSales14d

**Type**: Decimal

**Description**: Total value of new-to-brand sales occurring within 14 days of an ad click. Not available for book vendors.

**Report types**: [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### newToBrandSalesClicks

**Type**: Decimal

**Description**: Total value of new-to-brand sales occurring within 14 days of an ad click. Not available for book vendors.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad)

#### newToBrandSalesPercentage

**Type**: Decimal

**Description**: Percentage of total sales made up of new-to-brand purchases within 14 days of an ad click. Not available for book vendors. Same as newToBrandSalesPercentage14d.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad)

#### newToBrandSalesPercentage14d

**Type**: Decimal

**Description**: Percentage of total sales made up of new-to-brand purchases within 14 days of an ad click. Not available for book vendors. Same as newToBrandSalesPercentage.

**Report types**: [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### newToBrandSalesViews

**Type**: Decimal

**Description**: Total value of sales by new to brand customers occurring within 14 days of an ad view. 

**Report types**: [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting)

#### newToBrandUnitsSold

**Type**: Integer

**Description**: Total number of attributed units ordered as part of new-to-brand sales occurring within 14 days of an ad click or view. Not available for book vendors.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sbAds](guides/reporting/v3/report-types/ad), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### newToBrandUnitsSold14d

**Type**: Integer

**Description**: Total number of attributed units ordered as part of new-to-brand sales occurring within 14 days of an ad click or view. Not available for book vendors.

**Report types**: [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### newToBrandUnitsSoldBrandHalo

**Type**: Integer

**Description**: Units of brand halo products purchased by new-to-brand shoppers, attributed to an ad view or click. A new-to-brand purchase event can include multiple sold units. Use Total new-to-brand units sold to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### newToBrandUnitsSoldClicks

**Type**: Integer

**Description**: Total number of attributed units ordered as part of new-to-brand sales occurring within 14 days of an ad click. Not available for book vendors.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sbAds](guides/reporting/v3/report-types/ad)

#### newToBrandUnitsSoldPercentage

**Type**: Decimal

**Description**: Percentage of total attributed units ordered within 14 days of an ad click that are part of a new-to-brand purchase. Not available for book vendors. Same as newToBrandUnitsSoldPercentage14d.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad)

#### newToBrandUnitsSoldPercentage14d

**Type**: Decimal

**Description**: Percentage of total attributed units ordered within 14 days of an ad click that are part of a new-to-brand purchase. Not available for book vendors. Same as newToBrandUnitsSoldPercentage.

**Report types**: [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### notificationClicks

**Type**: Integer

**Description**: The number of times the link inside an email or push notification was opened.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### notificationOpens

**Type**: Integer

**Description**: The number of times an email or push notification was opened.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### offAmazonCPA

**Type**: Decimal

**Description**: The average cost to acquire for Off Amazon conversions

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### offAmazonCVR

**Type**: Decimal

**Description**: Number of off-Amazon conversions relative to the number of ad impressions. (Off-Amazon conversions / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### offAmazonClicks

**Type**: Integer

**Description**: Number of conversions that occured off Amazon attributed to an ad click. This includes all conversion types.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### offAmazonConversions

**Type**: Integer

**Description**: Number of conversions that occured off Amazon attributed to an ad view or click. This includes all conversion types.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### offAmazonECPP

**Type**: Decimal

**Description**: Effective (average) cost to acquire an off-Amazon purchase event. (Total cost / Off-Amazon purchases)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### offAmazonERPM

**Type**: Decimal

**Description**: Effective (average) revenue for sales included in off-Amazon purchases generated per thousand impressions. (Off-Amazon sales / (Impressions / 1000))

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### offAmazonProductSales

**Type**: Decimal

**Description**: Sales (in local currency) of promoted products to off Amazon shoppers, attributed to an ad view or click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### offAmazonPurchaseRate

**Type**: Decimal

**Description**: Rate of attributed off-Amazon purchase events relative to ad impressions. (Off-Amazon purchases / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### offAmazonPurchases

**Type**: Decimal

**Description**: Off Amazon Purchases

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### offAmazonPurchasesClicks

**Type**: Integer

**Description**: Number of off-Amazon purchase events attributed to an ad click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### offAmazonPurchasesViews

**Type**: Integer

**Description**: Number of off-Amazon purchase events attributed to an ad view.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### offAmazonROAS

**Type**: Decimal

**Description**: Return on ad spend for off Amazon sales

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### offAmazonUnitsSold

**Type**: Integer

**Description**: Units of product purchased off Amazon, attributed to an ad view or click. A single purchase event can include multiple sold units.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspGeo](guides/reporting/v3/report-types/geo)

#### offAmazonViews

**Type**: Integer

**Description**: Number of conversions that occured off Amazon attributed to an ad view. This includes all conversion types.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### omnichannelMetricsFee

**Type**: Decimal

**Description**: This fee is calculated based on a percentage of the supply cost

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### omsLineItemId

**Type**: String

**Description**: A unique identifier for a line item imported from the Amazon Order Management System (OMS).

**Report types**: [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### omsProposalId

**Type**: String

**Description**: A unique identifier for an order imported from the Amazon Order Management System (OMS).

**Report types**: [dspProduct](guides/reporting/v3/report-types/product), [dspAudience](guides/reporting/v3/report-types/audience)

#### onTargetCostPerImpression

**Type**: Decimal

**Description**: The total cost per thousand on-target impressions

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### onTargetImpressionCost

**Type**: Decimal

**Description**: The total amount of money spent on the campaign when delivering on-target impressions

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### onTargetImpressions

**Type**: Integer

**Description**: The number of times an advertisement was displayed within the advertiser’s target demographic segment, as measured by Nielsen Digital Ad Ratings

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### operatingSystemName

**Type**: String

**Description**: The operating systems the customers used to view the ad

**Report types**: [dspTech](guides/reporting/v3/report-types/tech)

#### orderBudget

**Type**: Decimal

**Description**: The total amount of money that be consumed by an order.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### orderCurrency

**Type**: String

**Description**: Order currency.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### orderEndDate

**Type**: String

**Description**: The last day a line item within an order is eligible to run.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### orderExternalId

**Type**: String

**Description**: Order External ID or PO number

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### orderId

**Type**: String

**Description**: A unique identifier assigned to an order.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience), [dspAudioAndVideo](guides/reporting/v3/report-types/audio-and-video)

#### orderName

**Type**: String

**Description**: Order name.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience), [dspAudioAndVideo](guides/reporting/v3/report-types/audio-and-video)

#### orderStartDate

**Type**: String

**Description**: The first day a line item within an order is eligible to run.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### orders14d

**Type**: Integer

**Description**: Number of orders within the last 14 days. Includes orders attributed to both clicks and views for vCPM campaigns, but only sales attributed to clicks for CPC campaigns.

**Report types**: [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### ordersClicks14d

**Type**: Integer

**Description**: Number of orders within the last 14 days attributed to ad clicks.

**Report types**: [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### other

**Type**: Integer

**Description**: The number of Other conversions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### otherCPA

**Type**: Decimal

**Description**: The average cost to acquire an Other conversion. (Other CPA = Total cost / Other)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### otherCVR

**Type**: Decimal

**Description**: The number of Other conversions relative to the number of ad impressions. (Other CVR = Other / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### otherClicks

**Type**: Integer

**Description**: The number of Other conversions attributed to an ad click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### otherValueAverage

**Type**: Decimal

**Description**: Average value associated with an Other conversion. (Other value average = Other value sum / Other)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### otherValueSum

**Type**: Decimal

**Description**: Sum of Other conversion values

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### otherViews

**Type**: Integer

**Description**: The number of Other conversions attributed to an ad view.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### pageViewCPA

**Type**: Decimal

**Description**: The average cost to acquire a Page view conversion. (Page view CPA = Total cost / Page view)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### pageViewCVR

**Type**: Decimal

**Description**: The number of Page view conversions relative to the number of ad impressions. (Page view CVR = Page view / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### pageViewClicks

**Type**: Integer

**Description**: The number of Page view conversions attributed to an ad click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### pageViewValueAverage

**Type**: Decimal

**Description**: Average value associated with a Page view conversion. (Page view value average = Page view value sum / Page view)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### pageViewValueSum

**Type**: Decimal

**Description**: Sum of Page view conversion values

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### pageViewViews

**Type**: Integer

**Description**: The number of Page view conversions attributed to an ad view.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### pageViews

**Type**: Integer

**Description**: The number of Page view conversions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### paidSubscriptionSignupClicks

**Type**: Integer

**Description**: The number of paid subscriptions for sponsored products associated with a click on your ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### paidSubscriptionSignupRate

**Type**: Deciimal

**Description**: The ratio of how often customers signed up for a paid subscription for sponsored products when you ad was displayed. This is calculated as paid subscriptions divided by impressions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### paidSubscriptionSignupViews

**Type**: Integer

**Description**: The number of paid subscriptions for sponsored products associated with ad impressions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### paidSubscriptionSignups

**Type**: Integer

**Description**: The number of paid subscriptions for sponsored products associated with a click or view on your ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### parentAsin

**Type**: String

**Description**: The Amazon Standard Identification Number (ASIN) that is the parent of this ASIN.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### placement

**Type**: String

**Description**: The name of the placement the campaign was run

**Report types**: [dspInventory](guides/reporting/v3/report-types/inventory)

#### placementClassification

**Type**: String

**Description**: The page location where an ad appeared.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbPlacement](guides/reporting/v3/report-types/placement)

#### placementSize

**Type**: Integer

**Description**: The dimensions of the placement in pixels.  Only available when "site" is a dimension.

**Report types**: [dspInventory](guides/reporting/v3/report-types/inventory)

#### playTrailerRate

**Type**: Decimal

**Description**: The number of video trailer plays relative to the number of impressions. (Play trailer rate = Play trailers / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### playTrailers

**Type**: Integer

**Description**: The number of times a video trailer was played for the featured product.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### playTrailersClicks

**Type**: Integer

**Description**: The number of video trailer plays attributed to ad click-throughs.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### playTrailersViews

**Type**: Integer

**Description**: The number of video trailer players attributed to ad impressions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### portfolioId

**Type**: Integer

**Description**: The portfolio the campaign is associated with.

**Report types**: [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### postalCode

**Type**: String

**Description**: Postal code.

**Report types**: [dspGeo](guides/reporting/v3/report-types/geo)

#### productCategory

**Type**: String

**Description**: The category the product is associated with on Amazon.

**Report types**: [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [dspProduct](guides/reporting/v3/report-types/product)

#### productGroup

**Type**: String

**Description**: A distinct product grouping distinguishing products like books from watches and video games from toys. Contains categories.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### productName

**Type**: String

**Description**: The name of the product.

**Report types**: [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [dspProduct](guides/reporting/v3/report-types/product)

#### productReviewPageViewBrandHalo

**Type**: Integer

**Description**: Number of times shoppers visited the product review page for a brand halo product, attributed to an ad view or click. Use Total PRPV to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### productReviewPageViewBrandHaloClicks

**Type**: Integer

**Description**: Number of times shoppers visited the product review page for a brand halo product, attributed to an ad click. Use Total PRPV clicks to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### productReviewPageViewBrandHaloViews

**Type**: Integer

**Description**: Number of times shoppers visited the product review page for a brand halo product, attributed to an ad view. Use Total PRPV views to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### productReviewPageVisitRate

**Type**: Deciaml

**Description**: Rate of product review page visits for promoted products, relative to the number of ad impressions. (ATCR = ATC / Impressions) Use Total PRPVR to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### productReviewPageVisits

**Type**: Integer

**Description**: Number of times shoppers visited the product review page for a promoted product, attributed to an ad view or click. Use Total PRPV to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### productReviewPageVisitsClicks

**Type**: Integer

**Description**: Number of times shoppers visited the product review page for a promoted product, attributed to an ad click. Use Total PRPV clicks to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### productReviewPageVisitsViews

**Type**: Integer

**Description**: Number of times shoppers visited the product review page for a promoted product, attributed to an ad view. Use Total PRPV views to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### productSubCategory

**Type**: String

**Description**: A classification for the type of product being sold which determines its place in the Amazon retail catalog.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### promotedAsin

**Type**: String

**Description**: The ASIN associated to an advertised product.

**Report types**: [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### promotedSku

**Type**: String

**Description**: The SKU being advertised. Not available for vendors.

**Report types**: [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### proposalId

**Type**: String

**Description**: A unique identifier for an order imported from the Amazon Order Management System (OMS).

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### purchaseRate

**Type**: Decimal

**Description**: Rate of ad-attributed purchase events for promoted products, relative to ad impressions. (Purchase rate = Purchases / Impressions) Use Total purchase rate to see all conversions for the brands' products.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### purchasedAsin

**Type**: String

**Description**: The ASIN of the product that was purchased.

**Report types**: [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### purchases

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 14 days of an ad click or view.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### purchases14d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 14 days of an ad click. Sponsored Products only.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### purchases1d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 1 day of an ad click.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### purchases30d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 30 days of an ad click.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### purchases7d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 7 days of an ad click.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### purchasesBrandHalo

**Type**: Integer

**Description**: Number of times any quantity of a brand halo product was included in a purchase event, attributed to an ad view or click. Purchase events include new Subscribe & Save subscriptions and video rentals. Use Total purchases to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### purchasesBrandHaloClicks

**Type**: Integer

**Description**: Number of times any quantity of a brand halo product was included in a purchase event, attributed to an ad click. Purchase events include new Subscribe & Save subscriptions and video rentals. Use Total purchases clicks to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### purchasesBrandHaloViews

**Type**: Integer

**Description**: Number of times any quantity of a brand halo product was included in a purchase event, attributed to an ad view. Purchase events include new Subscribe & Save subscriptions and video rentals. Use Total purchases views to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### purchasesClicks

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 14 days of an ad click.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### purchasesClicksRate

**Type**: Decimal

**Description**: Rate of click-attributed purchases for promoted products relative to the number of ad impressions. (Purchase Rate Clicks = Click-attributed purchases / Impressions) Use Total Purchase Click Rate to see all conversions for the brands' products.

**Report types**: [dspAudience](guides/reporting/v3/report-types/audience)

#### purchasesOtherSku14d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 14 days of an ad click where the SKU purchased was different that the advertised SKU.

**Report types**: [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### purchasesOtherSku1d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 1 day of an ad click where the SKU purchased was different that the advertised SKU.

**Report types**: [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### purchasesOtherSku30d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 30 days of an ad click where the SKU purchased was different that the advertised SKU.

**Report types**: [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### purchasesOtherSku7d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 7 days of an ad click where the SKU purchased was different that the advertised SKU.

**Report types**: [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### purchasesPromoted

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 14 days of ad click or view where the purchased SKU was the same as the SKU advertised. Same as purchasesSameSku14d.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad)

#### purchasesPromotedClicks

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 14 days of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### purchasesSameSku14d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 14 days of ad click where the purchased SKU was the same as the SKU advertised. Sponsored Products only. Same as puchasesPromoted.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### purchasesSameSku1d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 1 day of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### purchasesSameSku30d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 30 days of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### purchasesSameSku7d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 7 days of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### purchasesViews

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 14 days of an ad view.

**Report types**: [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### qualifiedBorrows

**Type**: Integer

**Description**: Number of Kindle Unlimited users who have downloaded the book due to ad exposure, during the attribution window, and have read a percent of the book within the window, triggering a royalty payment. Relevant to book advertisers only.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sbAds](guides/reporting/v3/report-types/ad), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### qualifiedBorrowsFromClicks

**Type**: Integer

**Description**: Number of Kindle Unlimited users who have downloaded the book due to ad exposure, during the attribution window, and have read a percent of the book within the window, triggering a royalty payment. Relevant to book advertisers only.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sbAds](guides/reporting/v3/report-types/ad)

#### qualifiedBorrowsFromViews

**Type**: Integer

**Description**: Number of Kindle Unlimited users who have downloaded the book due to ad exposure, during the attribution window, and have read a percent of the book within the window, triggering a royalty payment. Relevant to book advertisers only.

**Report types**: [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### reach

**Type**: Integer

**Description**: The number of unique individuals an ad impression is served to in a period.

**Report types**: [stCampaigns](guides/reporting/v3/report-types/campaign), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspReachFrequency](guides/reporting/v3/report-types/reach#amazon-dsp)

#### region

**Type**: String

**Description**: State or region.

**Report types**: [dspGeo](guides/reporting/v3/report-types/geo)

#### rentalRate

**Type**: Decimal

**Description**: The number of video rentals relative to the number of impressions. (Rental rate = Rentals / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### rentals

**Type**: Integer

**Description**: The number of times a video was rented for the featured product.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### rentalsClicks

**Type**: Integer

**Description**: The number of video rentals attributed to ad click-throughs.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### rentalsViews

**Type**: Integer

**Description**: The number of video rentals attributed to ad impressions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### reportGranularity

**Type**: String

**Description**: Report granularity.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product)

#### roas

**Type**: Decimal

**Description**: Return on ad spend.

**Report types**: [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### roasClicks14d

**Type**: Decimal

**Description**: Return on ad spend based on purchases made within 14 days of an ad click.

**Report types**: [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### roasClicks7d

**Type**: Decimal

**Description**: Return on ad spend based on purchases made within 7 days of an ad click.

**Report types**: [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### royaltyQualifiedBorrows

**Type**: Integer

**Description**: Estimated royalty payment attributed to Kindle Unlimited users downloading the book due to ad exposure, during the attribution window, and have read a percent of the book within the window, triggering a royalty payment. Relevant to book advertisers only.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sbAds](guides/reporting/v3/report-types/ad), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### royaltyQualifiedBorrowsFromClicks

**Type**: Integer

**Description**: Estimated royalty payment attributed to Kindle Unlimited users who clicked on the ad, downloaded the book due to ad exposure, during the attribution window, and have read a percent of the book within the window, triggering a royalty payment. Relevant to book advertisers only.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sbAds](guides/reporting/v3/report-types/ad)

#### royaltyQualifiedBorrowsFromViews

**Type**: Integer

**Description**: Estimated royalty payment attributed to Kindle Unlimited users who viewed the ad, downloaded the book due to ad exposure, during the attribution window, and have read a percent of the book within the window, triggering a royalty payment. Relevant to book advertisers only.

**Report types**: [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### sales

**Type**: Decimal

**Description**: Total value of sales occurring within 14 days of an ad click or view. 

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### sales14d

**Type**: Decimal

**Description**: Total value of sales occurring within 14 days of an ad click. Includes sales attributed to both clicks and views for vCPM campaigns, but only sales attributed to clicks for CPC campaigns.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### sales1d

**Type**: Decimal

**Description**: Total value of sales occurring within 1 day of an ad click.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### sales30d

**Type**: Decimal

**Description**: Total value of sales occurring within 30 days of an ad click.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### sales7d

**Type**: Decimal

**Description**: Total value of sales occurring within 7 days of an ad click.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### salesBrandHalo

**Type**: Integer

**Description**: For Sponsored Display vCPM campaigns, the total value of sales occurring within 14 days of an ad click or view where the purchased SKU was different from the SKU advertised.

**Report types**: [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### salesBrandHaloClicks

**Type**: Integer

**Description**: Total value of sales occurring within 14 days of an ad click where the purchased SKU was different from the SKU advertised.

**Report types**: [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### salesClicks

**Type**: Decimal

**Description**: Total value of sales occurring within 14 days of an ad click. 

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### salesClicks14d

**Type**: Decimal

**Description**: Total value of sales occurring within 14 days of an ad click. 

**Report types**: [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### salesOtherSku14d

**Type**: Decimal

**Description**: Total value of sales occurring within 14 days of an ad click where the purchased SKU was different from the SKU advertised.

**Report types**: [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### salesOtherSku1d

**Type**: Decimal

**Description**: Total value of sales occurring within 1 day of an ad click where the purchased SKU was different from the SKU advertised.

**Report types**: [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### salesOtherSku30d

**Type**: Decimal

**Description**: Total value of sales occurring within 30 days of an ad click where the purchased SKU was different from the SKU advertised.

**Report types**: [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### salesOtherSku7d

**Type**: Decimal

**Description**: Total value of sales occurring within 7 days of an ad click where the purchased SKU was different from the SKU advertised.

**Report types**: [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### salesPromoted

**Type**: Decimal

**Description**: Total value of sales occurring within 14 days of ad click or view where the purchased SKU was the same as the SKU advertised. Same as attributedSales14dSameSKU.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad)

#### salesPromotedClicks

**Type**: Decimal

**Description**: Total value of sales occurring within 14 days of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### salesViews

**Type**: Decimal

**Description**: Total value of sales occurring within 14 days of an ad view. 

**Report types**: [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting)

#### search

**Type**: Integer

**Description**: The number of Search conversions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### searchCPA

**Type**: Decimal

**Description**: The average cost to acquire a Search conversion. (Search CPA = Total cost / Search)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### searchCVR

**Type**: Decimal

**Description**: The number of Search conversions relative to the number of ad impressions. (Search CVR = Search / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### searchClicks

**Type**: Integer

**Description**: The number of Search conversions attributed to an ad click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### searchTerm

**Type**: String

**Description**: The search term used by the customer. Same as query.

**Report types**: [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term)

#### searchValueAverage

**Type**: Decimal

**Description**: Average value associated with a Search conversion. (Search value average = Search value sum / Search)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### searchValueSum

**Type**: Decimal

**Description**: Sum of Search conversion values

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### searchViews

**Type**: Integer

**Description**: The number of Search conversions attributed to an ad view.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### segmentClassCode

**Type**: String

**Description**: The segment's class, for example behavioral or user agent.

**Report types**: [dspAudience](guides/reporting/v3/report-types/audience), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products) 

#### segmentId

**Type**: String

**Description**: A unique identifier assigned to an audience segment.

**Report types**: [dspAudience](guides/reporting/v3/report-types/audience)

#### segmentMarketplaceId

**Type**: String

**Description**: A unique identifier assigned to the marketplace of the audience segment.

**Report types**: [dspAudience](guides/reporting/v3/report-types/audience)

#### segmentName

**Type**: String

**Description**: The name of the audience segment.

**Report types**: [dspAudience](guides/reporting/v3/report-types/audience), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### segmentSource

**Type**: String

**Description**: The segment's source, for example AAX.

**Report types**: [dspAudience](guides/reporting/v3/report-types/audience)

#### segmentType

**Type**: String

**Description**: The type of segment, for example remarketing.

**Report types**: [dspAudience](guides/reporting/v3/report-types/audience)

#### signUp

**Type**: Integer

**Description**: Number of Sign-up conversions occurring off Amazon, attributed to an ad view or click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### signUpCPA

**Type**: Decimal

**Description**: The average cost to acquire a Sign-up conversion off Amazon. (Total cost / Sign-up)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### signUpCVR

**Type**: Decimal

**Description**: Number of Sign-up conversions occurring off Amazon relative to the number of ad impressions. (Sign-up/ Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### signUpClicks

**Type**: Integer

**Description**: Number of Sign-up conversions occurring off Amazon, attributed to an ad click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### signUpValueAverage

**Type**: Decimal

**Description**: Average value associated with a Sign-up conversion. (Sign-up value average = Sign-up value sum / Sign-up)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### signUpValueSum

**Type**: Decimal

**Description**: Sum of Sign-up conversion values

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### signUpViews

**Type**: Integer

**Description**: Number of Sign-up conversions occurring off Amazon, attributed to an ad view.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### site

**Type**: String

**Description**: The site or group of sites the campaign ran on.

**Report types**: [dspInventory](guides/reporting/v3/report-types/inventory)

#### skillInvocation

**Type**: Integer

**Description**: The number of times shoppers launched a promoted Alexa skill, attributed to an ad view or click. This can include tap, touch, and voice invocations.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### skillInvocationClicks

**Type**: Integer

**Description**: The number of times shoppers launched a promoted Alexa skill, attributed to an ad click. This can include tap, touch, and voice invocations.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### skillInvocationRate

**Type**: Decimal

**Description**: Rate of Skill invocation conversions relative to the number of impressions. (Skill invocation rate = Skill invocations / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### skillInvocationViews

**Type**: Integer

**Description**: The number of times shoppers launched a promoted Alexa skill, attributed to an ad view. This can include tap, touch, and voice invocations.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### spend

**Type**: Decimal

**Description**: Total cost of ad clicks.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### startDate

**Type**: String

**Description**: Start date of summary period for a report in the format YYYY-MM-DD.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [spTargeting](guides/reporting/v3/report-types/targeting), [sbTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [spGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sdGrossandInvalids](guides/reporting/v3/report-types/gross-and-invalid-traffic), [sbAds](guides/reporting/v3/report-types/ad), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### subscribe

**Type**: Integer

**Description**: Number of Subscribe conversions occurring off Amazon, attributed to an ad view or click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### subscribeCPA

**Type**: Decimal

**Description**: Average cost to acquire a Subscribe conversion off Amazon. (Total cost / Subscribe)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### subscribeCVR

**Type**: Decimal

**Description**: Number of Subscribe conversions occurring off Amazon, relative to the number of ad impressions. (Subscribe / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### subscribeClicks

**Type**: Integer

**Description**: Number of Subscribe conversions occurring off Amazon, attributed to an ad click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### subscribeValueAverage

**Type**: Decimal

**Description**: Average value associated with a Subscribe conversion. (Subscribe value average = Subscribe value sum / Subscribe)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### subscribeValueSum

**Type**: Decimal

**Description**: Sum of Subscribe conversion values

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### subscribeViews

**Type**: Integer

**Description**: Number of Subscribe conversions occurring off Amazon, attributed to an ad view.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### subscriptionSignupClicks

**Type**: Integer

**Description**: The number of free trial and paid subscriptions for sponsored products associated with a click on your ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### subscriptionSignupRate

**Type**: Decimal

**Description**: The ratio of how often customers signed up for a free trial or paid subscription for sponsored products when you ad was displayed. This is calculated as free trials and paid subscriptions divided by impressions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### subscriptionSignupViews

**Type**: Integer

**Description**: The number of free trial and paid subscriptions for sponsored products associated with ad impressions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### subscriptionSignups

**Type**: Integer

**Description**: The number of free trial and paid subscriptions for sponsored products associated with a click or view on your ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### supplyCost

**Type**: Decimal

**Description**: The total amount of money spent on media supply.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### supplySource

**Type**: String

**Description**: The inventory the campaign ran on, for example real-time bidding exchanges or Amazon-owned sites.

**Report types**: [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudioAndVideo](guides/reporting/v3/report-types/audio-and-video), [dspReachFrequency](guides/reporting/v3/report-types/reach#amazon-dsp)

#### supplySourceId

**Type**: Integer

**Description**: The ID associated to the inventory the campaign ran on.

**Report types**: [dspReachFrequency](guides/reporting/v3/report-types/reach#amazon-dsp)

#### targetDemographic

**Type**: String

**Description**: The Nielsen demographic segment the advertiser is transacting on

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign)

#### targeting

**Type**: String

**Description**: A string representation of the expression object used in the targeting clause.

**Report types**: [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term)

#### targetingExpression

**Type**: String

**Description**: A string representation of the expression object used in the targeting clause.

**Report types**: [sbTargeting](guides/reporting/v3/report-types/targeting), [sdTargeting](guides/reporting/v3/report-types/targeting)

#### targetingId

**Type**: Integer

**Description**: The identifier of the targeting expression.

**Report types**: [sbTargeting](guides/reporting/v3/report-types/targeting), [sdTargeting](guides/reporting/v3/report-types/targeting)

#### targetingMethod

**Type**: String

**Description**: The targeting settings applied to the campaign. Examples include segments, content categories, and untargeted if no settings were applied.

**Report types**: [dspAudience](guides/reporting/v3/report-types/audience)

#### targetingText

**Type**: String

**Description**: The text used in the targeting expression.

**Report types**: [sbTargeting](guides/reporting/v3/report-types/targeting), [sdTargeting](guides/reporting/v3/report-types/targeting), [stTargeting](guides/reporting/v3/report-types/targeting)

#### targetingType

**Type**: String

**Description**: The type of targeting used in the expression.

**Report types**: [sbTargeting](guides/reporting/v3/report-types/targeting)

#### topOfSearchImpressionShare

**Type**: Decimal

**Description**: The percentage of top-of-search impressions earned out of all the top-of-search impressions that were eligible for a given date range. Various factors determine the eligibility for an impression including campaign status and targeting status.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbTargeting](guides/reporting/v3/report-types/targeting)

#### totalAddToCart

**Type**: Integer

**Description**: Number of times shoppers added the brands' products to their cart, attributed to an ad view or click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalAddToCartClicks

**Type**: Integer

**Description**: Number of times shoppers added the brands' products to their cart, attributed to an ad click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalAddToCartRate

**Type**: Decimal

**Description**: Rate of Add to Cart conversions for the brands' products relative to the number of ad impressions. (Total ATCR = Total ATC / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalAddToCartViews

**Type**: Integer

**Description**: Number of times shoppers added the brands' products to their cart, attributed to an ad view.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalAddToList

**Type**: Integer

**Description**: Number of times shoppers added the brands' products to a wish list, gift list, or registry, attributed to an ad view or click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalAddToListClicks

**Type**: Integer

**Description**: Number of times shoppers added the brands' products to a wish list, gift list, or registry, attributed to an ad click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalAddToListRate

**Type**: Decimal

**Description**: Rate of Add to List conversions for the brands' products relative to the number of impressions. (ATLR = ATL / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalAddToListViews

**Type**: Integer

**Description**: Number of times shoppers added the brands' products to a wish list, gift list or registry, attributed to an ad view.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalCost

**Type**: Decimal

**Description**: The total amount of money spent on running the campaign not including 3P fees paid by the agency.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### totalDetailPageView

**Type**: Integer

**Description**: Number of detail page views for all the brands and products attributed to an ad view or click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalDetailPageViewClicks

**Type**: Integer

**Description**: Number of detail page views for all the brands and products attributed to an ad click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalDetailPageViewRate

**Type**: Decimal

**Description**: Detail page view rate for the brands’ products, relative to the number of ad impressions. (Total DPV / Impressions = Total DPVR)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalDetailPageViewViews

**Type**: Integer

**Description**: Number of detail page views for all the brands' products, attributed to an ad view.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalECPAddToCart

**Type**: Decimal

**Description**: Effective (average) cost to acquire an Add to Cart conversion for the brands' products. (Total eCPATC = Total cost / Total ATC)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalECPAddToList

**Type**: Decimal

**Description**: Effective (average) cost to acquire an Add to List conversion for the brands' products. (Total eCPATL = Total cost / Total ATL)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalECPDetailPageView

**Type**: Decimal

**Description**: Effective (average) cost to acquire a detail page view for a product in your brand. (Total eCPDPV = Total cost / Total DPV)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalECPP

**Type**: Decimal

**Description**: Effective (average) cost to acquire a purchase conversion for the brands' products. (Total eCPP = Total cost / Total purchases)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalECPProductReviewPageVisit

**Type**: Decimal

**Description**: Effective (average) cost to acquire a product review page conversion for the brands' products. (Total eCPPRPV = Total cost / Total PRPV)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalECPSubscribeAndSave

**Type**: Decimal

**Description**: Effective (average) cost to acquire a Subscribe & Save subscription for the brands' products. (Total eCPSnSS = Total cost / Total SnSS)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalERPM

**Type**: Decimal

**Description**: Effective (average) revenue for the brands’ products generated per thousand impressions. (Total eRPM = Sales / (Impressions / 1000))

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalFee

**Type**: Decimal

**Description**: The sum of all fees.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalNewToBrandDPVClicks

**Type**: Integer

**Description**: Number of new-to-brand detail page views for all the brands' products, attributed to an ad click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### totalNewToBrandDPVRate

**Type**: Decimal

**Description**: The new-to-brand detail page view rate for the brands' products, relative to the number of ad impressions. (Total NTB - DPV / Impressions = Total DPVR)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### totalNewToBrandDPVViews

**Type**: Integer

**Description**: Number of new-to-brand detail page views for all the brands' products, attributed to an ad view.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### totalNewToBrandDPVs

**Type**: Integer

**Description**: Number of new-to-brand detail page views for all the brands' products, attributed to an ad view or click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### totalNewToBrandECPDetailPageView

**Type**: Decimal

**Description**: Effective total cost for new-to-brand detail page view, calculated by cost divided by new-to-brand detail page view

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### totalNewToBrandECPP

**Type**: Decimal

**Description**: Effective (average) cost to acquire a new-to-brand purchase conversion for the brands' products. (Total new-to-brand eCPP = Total cost / Total new-to-brand purchases)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalNewToBrandERPM

**Type**: Decimal

**Description**: Effective (average) revenue generated per thousand impressions from the brands’ products purchased by new-to-brand shoppers. (Total NTB eRPM = Total NTB Sales / (Impressions / 1000))

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalNewToBrandPurchaseRate

**Type**: Decimal

**Description**: Rate of new-to-brand purchase conversions for the brands' products relative to the number of ad impressions. (Total new-to-brand purchase rate = Total new-to-brand purchases / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalNewToBrandPurchases

**Type**: Integer

**Description**: Number of new-to-brand purchases for the brands’ products, attributed to an ad view or click. Shoppers are "new to brand" if they have not purchased from the brand in the last 365 days.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalNewToBrandPurchasesClicks

**Type**: Integer

**Description**: Number of new-to-brand purchases for the brands’ products, attributed to an ad click. Shoppers are "new to brand" if they have not purchased from the brand in the last 365 days.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalNewToBrandPurchasesPercentage

**Type**: Decimal

**Description**: Rate of new-to-brand purchase conversions for the brands' products relative to the number of ad impressions. (Total new-to-brand purchase rate = Total new-to-brand purchases / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspProduct](guides/reporting/v3/report-types/product)

#### totalNewToBrandPurchasesViews

**Type**: Integer

**Description**: Number of new-to-brand purchases for the brands’ products, attributed to an ad view. Shoppers are "new to brand" if they have not purchased from the brand in the last 365 days.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalNewToBrandROAS

**Type**: Decimal

**Description**: Return on advertising spend for new-to-brand purchases, measured as ad-attributed new-to-brand sales for the brands' products per local currency unit of ad spend. (Total new-to-brand ROAS = Total new to brand product sales / Total cost)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalNewToBrandSales

**Type**: Decimal

**Description**: Sales (in local currency) of the brands’ products purchased by new-to-brand shoppers, attributed to an ad view or click. Shoppers are "new to brand" if they have not purchased from the brand in the last 365 days.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalNewToBrandUnitsSold

**Type**: Integer

**Description**: Units of the brands' products purchased by shoppers who were new-to-brand, attributed to an ad view or click. A single new-to-brand purchase event can include multiple sold units.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalProductReviewPageVisitRate

**Type**: Decimal

**Description**: Rate of product review page visits for the brands' products, relative to the number of ad impressions. (Total ATCR = Total ATC / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalProductReviewPageVisits

**Type**: Integer

**Description**: Number of times shoppers visited a product review page for the brands' products, attributed to an ad view or click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalProductReviewPageVisitsClicks

**Type**: Integer

**Description**: Number of times shoppers visited a product review page for the brands' products, attributed to an ad click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalProductReviewPageVisitsViews

**Type**: Integer

**Description**: Number of times shoppers visited a product review page for the brands' products, attributed to an ad view.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalPurchaseRate

**Type**: Decimal

**Description**: Rate of ad-attributed purchase events for the brands' products relative to ad impressions. (Total purchase rate = Total purchases / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalPurchases

**Type**: Integer

**Description**: Number of times any quantity of a brand product was included in a purchase event, attributed to an ad view or click. Purchase events include new Subscribe & Save subscriptions and video rentals.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalPurchasesClicks

**Type**: Integer

**Description**: Number of times any quantity of a brand product was included in a purchase event, attributed to an ad click. Purchase events include new Subscribe & Save subscriptions and video rentals.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalPurchasesViews

**Type**: Integer

**Description**: Number of times any quantity of a brand product was included in a purchase event, attributed to an ad view. Purchase events include new Subscribe & Save subscriptions and video rentals.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalROAS

**Type**: Decimal

**Description**: Return on ad spend, measured as ad-attributed sales for the brands’ products per local currency unit of ad spend. (Total ROAS = Total product sales / Total cost)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalSales

**Type**: Decimal

**Description**: Sales (in local currency) of the brands’ products, attributed to an ad view or click.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalSubscribeAndSave

**Type**: Integer

**Description**: Number of new Subscribe & Save subscriptions for the brands and products, attributed to an ad view or click. This does not include replenishment subscription orders.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalSubscribeAndSaveClicks

**Type**: Integer

**Description**: Number of new Subscribe & Save subscriptions for the brands and products, attributed to an ad click. This does not include replenishment subscription orders.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalSubscribeAndSaveRate

**Type**: Decimal

**Description**: Rate of ad-attributed Subscribe & Save conversions for the brands’ products relative to ad impressions. (Total SnSSR = Total SnSS / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### totalSubscribeAndSaveViews

**Type**: Integer

**Description**: Number of new Subscribe & Save subscriptions for the brands and products, attributed to an ad view. This does not include replenishment subscription orders.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### totalUnitsSold

**Type**: Integer

**Description**: Units of the brands' products purchased, attributed to an ad view or click. A single purchase event can include multiple sold units.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### unitsSold

**Type**: Integer

**Description**: Number of attributed units sold within 14 days of an ad click or view.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### unitsSold14d

**Type**: Integer

**Description**: Number of attributed units sold within 14 days of an click or view. Only valid for Sponsored Brands version 3 campaigns, not Sponsored Brands video or multi-ad group (version 4) campaigns. Includes sales attributed to both clicks and views for vCPM campaigns, but only sales attributed to clicks for CPC campaigns.

**Report types**: [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### unitsSoldBrandHalo

**Type**: Integer

**Description**: For Sponsored Display vCPM campaigns, the total number of units ordered within 14 days of an ad click or view where the purchased SKU was different from the SKU advertised. For CPC campaigns, this value is equal to attributedUnitsOrdered14dOtherSKU.

**Report types**: [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [dspProduct](guides/reporting/v3/report-types/product)

#### unitsSoldBrandHaloClicks

**Type**: Integer

**Description**: Total number of units ordered within 14 days of an ad click where the purchased SKU was different from the SKU advertised.

**Report types**: [sdPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### unitsSoldClicks

**Type**: Integer

**Description**: Number of attributed units sold within 14 days of an ad click.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad), [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo), [sbAudiences](guides/reporting/v3/report-types/audience#sponsored-brands)

#### unitsSoldClicks14d

**Type**: Integer

**Description**: Total number of units sold within 14 days of an ad click.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [sbPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### unitsSoldClicks1d

**Type**: Integer

**Description**: Total number of units ordered within 1 day of an ad click.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### unitsSoldClicks30d

**Type**: Integer

**Description**: Total number of units ordered within 30 days of an ad click.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### unitsSoldClicks7d

**Type**: Integer

**Description**: Total number of units ordered within 7 days of an ad click.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### unitsSoldOtherSku14d

**Type**: Integer

**Description**: Total number of units ordered within 14 days of an ad click where the purchased SKU was different from the SKU advertised.

**Report types**: [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### unitsSoldOtherSku1d

**Type**: Integer

**Description**: Total number of units ordered within 1 day of an ad click where the purchased SKU was different from the SKU advertised.

**Report types**: [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### unitsSoldOtherSku30d

**Type**: Integer

**Description**: Total number of units ordered within 30 days of an ad click where the purchased SKU was different from the SKU advertised.

**Report types**: [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product)

#### unitsSoldOtherSku7d

**Type**: Integer

**Description**: Total number of units ordered within 7 days of an ad click where the purchased SKU was different from the SKU advertised.

**Report types**: [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spPurchasedProduct](guides/reporting/v3/report-types/purchased-product), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product)

#### unitsSoldPromotedClicks

**Type**: Integer

**Description**: Units of promoted products purchased, attributed to an ad click. A single click-attributed purchase event can include multiple sold units. Use Total Units Sold Clicks to see all conversions for the brands' products.

**Report types**: [dspProduct](guides/reporting/v3/report-types/product), [dspGeo](guides/reporting/v3/report-types/geo)

#### unitsSoldSameSku14d

**Type**: Integer

**Description**: Total number of units ordered within 14 days of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### unitsSoldSameSku1d

**Type**: Integer

**Description**: Total number of units ordered within 1 day of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### unitsSoldSameSku30d

**Type**: Integer

**Description**: Total number of units ordered within 30 days of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### unitsSoldSameSku7d

**Type**: Integer

**Description**: Total number of units ordered within 7 days of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [spCampaigns](guides/reporting/v3/report-types/campaign), [spTargeting](guides/reporting/v3/report-types/targeting), [spSearchTerm](guides/reporting/v3/report-types/search-term), [spAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [spGlobalAudiences](guides/reporting/v3/report-types/audience#sponsored-products)

#### unitsSoldViews

**Type**: Integer

**Description**: Number of attributed units sold within 14 days of an ad view.

**Report types**: [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting)

#### video5SecondViewRate

**Type**: Decimal

**Description**: The percentage of impressions where the customer watched the complete video or 5 seconds of the video (whichever is shorter). Sponsored Brands video-only metric.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sbAds](guides/reporting/v3/report-types/ad)

#### video5SecondViews

**Type**: Integer

**Description**: The number of impressions where the customer watched the complete video or 5 seconds (whichever is shorter). Sponsored Brands video-only metric.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sbAds](guides/reporting/v3/report-types/ad)

#### videoAdClicks

**Type**: Integer

**Description**: The number of times a video ad was clicked excluding clicks to the navigation (e.g. Play or Pause buttons).

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoAdComplete

**Type**: Integer

**Description**: The number of times a video ad played to completion. If rewind occurred, completion was calculated on the total percentage of unduplicated video viewed.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoAdCompletionRate

**Type**: Integer

**Description**: The number of video completions relative to the number of video starts. (Video completion rate = Video complete / Video start)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoAdCreativeViews

**Type**: Integer

**Description**: The number of times a portion of a video was seen.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoAdEndStateClicks

**Type**: Integer

**Description**: The number of ad clicks after a video ends.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoAdFirstQuartile

**Type**: Integer

**Description**: The number of times at least 25% of a video ad played. If rewind occurred, percent complete was calculated on the total percentage of unduplicated video viewed.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoAdImpressions

**Type**: Integer

**Description**: The number of times a video was served.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoAdMidpoint

**Type**: Integer

**Description**: The number of times at least 50% of a video ad played. If rewind occurred, percent complete was calculated on the total percentage of unduplicated video viewed.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoAdMute

**Type**: Integer

**Description**: The number of times a user muted the video ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoAdPause

**Type**: Integer

**Description**: The number of times a user paused the video ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoAdReplays

**Type**: Integer

**Description**: The number of times a video was replayed.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoAdResume

**Type**: Integer

**Description**: The number of times a user unpaused the video ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoAdSkipBacks

**Type**: Integer

**Description**: The number of times a video was skipped backwards.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoAdSkipForwards

**Type**: Integer

**Description**: The number of times an ad was skipped forward.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspProduct](guides/reporting/v3/report-types/product), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoAdStart

**Type**: Integer

**Description**: The number of times a video ad was started.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoAdThirdQuartile

**Type**: Integer

**Description**: The number of times at least 75% of a video ad played. If rewind occurred, percent complete was calculated on the total percentage of unduplicated video viewed.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoAdUnmute

**Type**: Integer

**Description**: The number of times a user unmuted the video ad.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoCompleteViews

**Type**: Integer

**Description**: The number of impressions where the video was viewed to 100%.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad)

#### videoDownloadRate

**Type**: Integer

**Description**: The number of video downloads relative to the number of impressions. (Video download rate = Video downloads / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoDownloads

**Type**: Integer

**Description**: The number of times a video was downloaded for the featured product.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoDownloadsClicks

**Type**: Integer

**Description**: The number of video downloads attributed to ad click-throughs.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoDownloadsViews

**Type**: Integer

**Description**: The number of video downloads attributed to ad impressions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoFirstQuartileViews

**Type**: Integer

**Description**: The number of impressions where the video was viewed to 25%.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad)

#### videoMidpointViews

**Type**: Integer

**Description**: The number of impressions where the video was viewed to 50%.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad)

#### videoStreams

**Type**: Integer

**Description**: The number of times a video was streamed (played without downloading).

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoStreamsClicks

**Type**: Integer

**Description**: The number of video streams attributed to ad click-throughs.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoStreamsRate

**Type**: Integer

**Description**: The number of video streams relative to the number of impressions. (Video stream rate = Video streams / Impressions)

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoStreamsViews

**Type**: Integer

**Description**: The number of video streams attributed to ad impressions.

**Report types**: [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspTech](guides/reporting/v3/report-types/tech), [dspGeo](guides/reporting/v3/report-types/geo), [dspAudience](guides/reporting/v3/report-types/audience)

#### videoThirdQuartileViews

**Type**: Integer

**Description**: The number of impressions where the video was viewed to 75%.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [stCampaigns](guides/reporting/v3/report-types/campaign), [stTargeting](guides/reporting/v3/report-types/targeting), [sbAds](guides/reporting/v3/report-types/ad)

#### videoUnmutes

**Type**: Integer

**Description**: The number of impressions where a customer unmuted the video.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sbAds](guides/reporting/v3/report-types/ad)

#### viewClickThroughRate

**Type**: Decimal

**Description**: Click-through rate for views (clicks divided by views).

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sbAds](guides/reporting/v3/report-types/ad)

#### viewabilityRate

**Type**: Decimal

**Description**: View-through rate (vtr). Views divided by impressions.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sdCampaigns](guides/reporting/v3/report-types/campaign), [sdAdGroups](guides/reporting/v3/report-types/ad-group), [sdTargeting](guides/reporting/v3/report-types/targeting), [sdAdvertisedProduct](guides/reporting/v3/report-types/advertised-product), [sbAds](guides/reporting/v3/report-types/ad), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory)

#### viewableImpressions

**Type**: Integer

**Description**: Number of impressions that met the Media Ratings Council (MRC) viewability standard. See viewability details at https://advertising.amazon.com/library/guides/viewability.

**Report types**: [sbCampaigns](guides/reporting/v3/report-types/campaign), [sbAdGroups](guides/reporting/v3/report-types/ad-group), [sbPlacement](guides/reporting/v3/report-types/placement), [sbTargeting](guides/reporting/v3/report-types/targeting), [sbSearchTerm](guides/reporting/v3/report-types/search-term), [sbAds](guides/reporting/v3/report-types/ad), [dspCampaign](guides/reporting/v3/report-types/campaign), [dspInventory](guides/reporting/v3/report-types/inventory), [dspGeo](guides/reporting/v3/report-types/geo)


