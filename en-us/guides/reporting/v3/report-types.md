---
title: Sponsored ads report types (version 3)
description: Learn what report types and metrics are available in version 3 reporting for sponsored ads using the Amazon Ads API.
type: guide
interface: api
tags:
    - Reporting
    - Sponsored Products
    - Sponsored Brands
---

# Report types

The Ads API supports a number of report types based on campaign entity structure. Depending on the ad type, campaign performance can be broken down by various dimensions including campaign, ad group, ad, keyword, target, and ASIN.

### Availability by ad type

Each type of sponsored ad supports different report types and has a separate path defined.

| Report type | Sponsored Products | Sponsored Brands | 
|-------------|--------------------|------------------|
| [Campaign](#campaign-reports) | x |  | 
| [Targeting](#targeting-reports) | x |  |
| [Search term](#search-term-reports) | x |  |
| [Advertised product](#advertised-product-reports) | x |  |
| [Purchased product](#purchased-product-reports) | x | x  |

## Campaign reports

Campaign reports contain performance data broken down at the campaign level. Campaign reports include all campaigns of the requested sponsored ad type that have performance activity for the requested days. For example, a Sponsored Products campaign report returns performance data for all Sponsored Products campaigns that received impressions on the chosen dates. Campaign reports can also be grouped by ad group and placement for more granular data. 

### Sponsored Products

#### Configurations

| Configuration | Available values |
|----------|---------|
| `reportTypeId` | `spCampaigns` |
| Maximum date range | 31 days |
| Data retention | 95 days |
| `timeUnit` | `SUMMARY` or `DAILY` |
| `groupBy` | `campaign`, `adGroup`, or `campaignPlacement` |
| `format` | `GZIP_JSON` |


#### Base metrics

[impressions](guides/reporting/v3/metrics#impressions), [clicks](guides/reporting/v3/metrics#clicks), [cost](guides/reporting/v3/metrics#cost), [purchases1d](guides/reporting/v3/metrics#purchases1d), [purchases7d](guides/reporting/v3/metrics#purchases7d), [purchases14d](guides/reporting/v3/metrics#purchases14d), [purchases30d](guides/reporting/v3/metrics#purchases30d), [purchasesSameSku1d](guides/reporting/v3/metrics#purchasesSameSku1d), [purchasesSameSku7d](guides/reporting/v3/metrics#purchasesSameSku7d), [purchasesSameSku14d](guides/reporting/v3/metrics#purchasesSameSku14d), [purchasesSameSku30d](guides/reporting/v3/metrics#purchasesSameSku30d), [unitsSoldClicks1d](guides/reporting/v3/metrics#unitsSoldClicks1d), [unitsSoldClicks7d](guides/reporting/v3/metrics#unitsSoldClicks7d), [unitsSoldClicks14d](guides/reporting/v3/metrics#unitsSoldClicks14d), [unitsSoldClicks30d](guides/reporting/v3/metrics#unitsSoldClicks30d), [sales1d](guides/reporting/v3/metrics#sales1d), [sales7d](guides/reporting/v3/metrics#sales7d), [sales14d](guides/reporting/v3/metrics#sales14d), [sales30d](guides/reporting/v3/metrics#sales30d), [attributedSalesSameSku1d](guides/reporting/v3/metrics#attributedSalesSameSku1d), [attributedSalesSameSku7d](guides/reporting/v3/metrics#attributedSalesSameSku7d), [attributedSalesSameSku14d](guides/reporting/v3/metrics#attributedSalesSameSku14d), [attributedSalesSameSku30d](guides/reporting/v3/metrics#attributedSalesSameSku30d), [unitsSoldSameSku1d](guides/reporting/v3/metrics#unitsSoldSameSku1d), [unitsSoldSameSku7d](guides/reporting/v3/metrics#unitsSoldSameSku7d), [unitsSoldSameSku14d](guides/reporting/v3/metrics#unitsSoldSameSku14d), [unitsSoldSameSku30d](guides/reporting/v3/metrics#unitsSoldSameSku30d), [kindleEditionNormalizedPagesRead14d](guides/reporting/v3/metrics#kindleEditionNormalizedPagesRead14d), [kindleEditionNormalizedPagesRoyalties14d](guides/reporting/v3/metrics#kindleEditionNormalizedPagesRoyalties14d), [date](guides/reporting/v3/metrics#date), [startDate](guides/reporting/v3/metrics#startDate), [endDate](guides/reporting/v3/metrics#endDate), [campaignBiddingStrategy](guides/reporting/v3/metrics#campaignBiddingStrategy), [costPerClick](guides/reporting/v3/metrics#costPerClick), [clickThroughRate](guides/reporting/v3/metrics#clickThroughRate), [spend](guides/reporting/v3/metrics#spend)

#### Group by `campaign`

Additional metrics: [campaignName](guides/reporting/v3/metrics#campaignName), [campaignId](guides/reporting/v3/metrics#campaignId), [campaignStatus](guides/reporting/v3/metrics#campaignStatus), [campaignBudgetAmount](guides/reporting/v3/metrics#campaignBudgetAmount), [campaignBudgetType](guides/reporting/v3/metrics#campaignBudgetType), [campaignRuleBasedBudgetAmount](guides/reporting/v3/metrics#campaignRuleBasedBudgetAmount), [campaignApplicableBudgetRuleId](guides/reporting/v3/metrics#campaignApplicableBudgetRuleId), [campaignApplicableBudgetRuleName](guides/reporting/v3/metrics#campaignApplicableBudgetRuleName), [campaignBudgetCurrencyCode](guides/reporting/v3/metrics#campaignBudgetCurrencyCode), [topOfSearchImpressionShare](guides/reporting/v3/metrics#topOfSearchImpressionShare)

Filters: 

- `campaignStatus` (values: ENABLED, PAUSED, ARCHIVED)

#### Group by `adGroup`

Additional metrics: [adGroupName](guides/reporting/v3/metrics#adGroupName), [adGroupId](guides/reporting/v3/metrics#adGroupId), [adStatus](guides/reporting/v3/metrics#adStatus)

Filters: 

- `adStatus` (values: ENABLED, PAUSED, ARCHIVED)

#### Group by `campaignPlacement`

Additional metrics: [placementClassification](guides/reporting/v3/metrics#placementClassification)

Filters: N/A

>[NOTE] You can only use a filter that is supported by **all** `groupBy` values included in a report configuration. For campaign reports, this means that filters are only supported when you include a single `groupBy` value.  

### Sample calls

#### Campaign daily report grouped by campaign and ad group

```shell
curl --location --request POST 'https://advertising-api.amazon.com/reporting/reports' \
--header 'Content-Type: application/vnd.createasyncreportrequest.v3+json' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxxx' \
--header 'Amazon-Advertising-API-Scope: xxxxxxxx' \
--header 'Authorization: Bearer Atza|xxxxxxxxx' \
--data-raw '{
    "name":"SP campaigns report 7/5-7/10",
    "startDate":"2022-07-05",
    "endDate":"2022-07-10",
    "configuration":{
        "adProduct":"SPONSORED_PRODUCTS",
        "groupBy":["campaign","adGroup"],
        "columns":["impressions","clicks","cost","campaignId","adGroupId","date"],
        "reportTypeId":"spCampaigns",
        "timeUnit":"DAILY",
        "format":"GZIP_JSON"
    }
}'
```

#### Campaign summary report grouped by campaign and placement

```shell
curl --location --request POST 'https://advertising-api.amazon.com/reporting/reports' \
--header 'Content-Type: application/vnd.createasyncreportrequest.v3+json' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxxx' \
--header 'Amazon-Advertising-API-Scope: xxxxxxxx' \
--header 'Authorization: Bearer Atza|xxxxxxxxxxx' \
 --data-raw '{
    "name":"SP campaigns report 7/5-7/10",
    "startDate":"2022-07-05",
    "endDate":"2022-07-10",
    "configuration":{
        "adProduct":"SPONSORED_PRODUCTS",
        "groupBy":["campaign","campaignPlacement"],
        "columns":["impressions","clicks","cost","campaignId","placementClassification","startDate","endDate"],
        "reportTypeId":"spCampaigns",
        "timeUnit":"SUMMARY",
        "format":"GZIP_JSON"
    }
}'
```

## Targeting reports

Targeting reports contain performance metrics broken down by both targeting expressions and keywords. To see only targeting expressions, set the `keywordType` filter to `TARGETING_EXPRESSION` and `TARGETING_EXPRESSION_PREDEFINED`. To see only keywords, set the `keywordType` filter to `BROAD`, `PHRASE`, and `EXACT`.

### Sponsored Products

#### Configurations

| Configuration | Available values |
|----------|---------|
| `reportTypeId` | `spTargeting` |
| Maximum date range | 31 days |
| Data retention | 95 days |
| `timeUnit` | `SUMMARY` or `DAILY` |
| `groupBy` | `targeting` |
| `format` | `GZIP_JSON` |

#### Base metrics

[impressions](guides/reporting/v3/metrics#impressions), [clicks](guides/reporting/v3/metrics#clicks), [costPerClick](guides/reporting/v3/metrics#costPerClick), [clickThroughRate](guides/reporting/v3/metrics#clickThroughRate), [cost](guides/reporting/v3/metrics#cost), [purchases1d](guides/reporting/v3/metrics#purchases1d), [purchases7d](guides/reporting/v3/metrics#purchases7d), [purchases14d](guides/reporting/v3/metrics#purchases14d), [purchases30d](guides/reporting/v3/metrics#purchases30d), [purchasesSameSku1d](guides/reporting/v3/metrics#purchasesSameSku1d), [purchasesSameSku7d](guides/reporting/v3/metrics#purchasesSameSku7d), [purchasesSameSku14d](guides/reporting/v3/metrics#purchasesSameSku14d), [purchasesSameSku30d](guides/reporting/v3/metrics#purchasesSameSku30d), [unitsSoldClicks1d](guides/reporting/v3/metrics#unitsSoldClicks1d), [unitsSoldClicks7d](guides/reporting/v3/metrics#unitsSoldClicks7d), [unitsSoldClicks14d](guides/reporting/v3/metrics#unitsSoldClicks14d), [unitsSoldClicks30d](guides/reporting/v3/metrics#unitsSoldClicks30d), [sales1d](guides/reporting/v3/metrics#sales1d), [sales7d](guides/reporting/v3/metrics#sales7d), [sales14d](guides/reporting/v3/metrics#sales14d), [sales30d](guides/reporting/v3/metrics#sales30d), [attributedSalesSameSku1d](guides/reporting/v3/metrics#attributedSalesSameSku1d), [attributedSalesSameSku7d](guides/reporting/v3/metrics#attributedSalesSameSku7d), [attributedSalesSameSku14d](guides/reporting/v3/metrics#attributedSalesSameSku14d), [attributedSalesSameSku30d](guides/reporting/v3/metrics#attributedSalesSameSku30d), [unitsSoldSameSku1d](guides/reporting/v3/metrics#unitsSoldSameSku1d), [unitsSoldSameSku7d](guides/reporting/v3/metrics#unitsSoldSameSku7d), [unitsSoldSameSku14d](guides/reporting/v3/metrics#unitsSoldSameSku14d), [unitsSoldSameSku30d](guides/reporting/v3/metrics#unitsSoldSameSku30d), [kindleEditionNormalizedPagesRead14d](guides/reporting/v3/metrics#kindleEditionNormalizedPagesRead14d), [kindleEditionNormalizedPagesRoyalties14d](guides/reporting/v3/metrics#kindleEditionNormalizedPagesRoyalties14d), [salesOtherSku7d](guides/reporting/v3/metrics#salesOtherSku7d), [unitsSoldOtherSku7d](guides/reporting/v3/metrics#unitsSoldOtherSku7d), [acosClicks7d](guides/reporting/v3/metrics#acosClicks7d), [acosClicks14d](guides/reporting/v3/metrics#acosClicks14d), [roasClicks7d](guides/reporting/v3/metrics#roasClicks7d), [roasClicks14d](guides/reporting/v3/metrics#roasClicks14d), [keywordId](guides/reporting/v3/metrics#keywordId), [keyword](guides/reporting/v3/metrics#keyword), [campaignBudgetCurrencyCode](guides/reporting/v3/metrics#campaignBudgetCurrencyCode), [date](guides/reporting/v3/metrics#date), [startDate](guides/reporting/v3/metrics#startDate), [endDate](guides/reporting/v3/metrics#endDate), [portfolioId](guides/reporting/v3/metrics#portfolioId), [campaignName](guides/reporting/v3/metrics#campaignName), [campaignId](guides/reporting/v3/metrics#campaignId), [campaignBudgetType](guides/reporting/v3/metrics#campaignBudgetType), [campaignBudgetAmount](guides/reporting/v3/metrics#campaignBudgetAmount), [campaignStatus](guides/reporting/v3/metrics#campaignStatus), [keywordBid](guides/reporting/v3/metrics#keywordBid), [adGroupName](guides/reporting/v3/metrics#adGroupName), [adGroupId](guides/reporting/v3/metrics#adGroupId), [keywordType](guides/reporting/v3/metrics#keywordType), [matchType](guides/reporting/v3/metrics#matchType), [targeting](guides/reporting/v3/metrics#targeting), [topOfSearchImpressionShare](guides/reporting/v3/metrics#topOfSearchImpressionShare)
 
#### Group by `targeting`

Additional metrics: [adKeywordStatus](guides/reporting/v3/metrics#adKeywordStatus)

Filters: 

- `adKeywordStatus` (values: `ENABLED`,`PAUSED`,`ARCHIVED`)
- `keywordType` (values: `BROAD`, `PHRASE`, `EXACT`, `TARGETING_EXPRESSION`, `TARGETING_EXPRESSION_PREDEFINED`)

### Sample calls

#### Targeting expressions only

 ```shell
curl --location --request POST 'https://advertising-api.amazon.com/reporting/reports' \
--header 'Content-Type: application/vnd.createasyncreportrequest.v3+json' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxxx' \
--header 'Amazon-Advertising-API-Scope: xxxxxxxx' \
--header 'Authorization: Bearer Atza|xxxxxxxxx' \
--data-raw '{
        "name":"SP targeting report 7/5-7/10",
        "startDate":"2022-07-05",
        "endDate":"2022-07-10",
        "configuration":{
            "adProduct":"SPONSORED_PRODUCTS",
            "groupBy":["targeting"],
            "columns":["adGroupId","campaignId", "targeting","keywordId","matchType","impressions", "clicks", "cost", "purchases1d", "purchases7d", "purchases14d", "purchases30d","startDate","endDate"],
            "filters": [
                {
                    "field": "keywordType",
                    "values": [
                    "TARGETING_EXPRESSION",
                    "TARGETING_EXPRESSION_PREDEFINED"
                    ]
                }
            ],
            "reportTypeId":"spTargeting",
            "timeUnit":"SUMMARY",
            "format":"GZIP_JSON"
        }
    }'
```

#### Keywords only

 ```shell
curl --location --request POST 'https://advertising-api.amazon.com/reporting/reports' \
--header 'Content-Type: application/vnd.createasyncreportrequest.v3+json' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxxx' \
--header 'Amazon-Advertising-API-Scope: xxxxxxxx' \
--header 'Authorization: Bearer Atza|xxxxxxxxx' \
--data-raw '{
        "name":"SP keywords report 7/5-7/10",
        "startDate":"2022-07-05",
        "endDate":"2022-07-10",
        "configuration":{
            "adProduct":"SPONSORED_PRODUCTS",
            "groupBy":["targeting"],
            "columns":["adGroupId","campaignId","keywordId","matchType","keyword","impressions", "clicks", "cost", "purchases1d", "purchases7d", "purchases14d", "purchases30d","startDate","endDate"],
            "filters": [
                {
                    "field": "keywordType",
                    "values": [
                    "BROAD",
                    "PHRASE",
                    "EXACT"
                    ]
                }
            ],
            "reportTypeId":"spTargeting",
            "timeUnit":"SUMMARY",
            "format":"GZIP_JSON"
        }
    }'
```

## Search term reports

Search term reports contain search term performance metrics broken down by targeting expressions and keywords. Note that search term reports only include impressions that resulted in at least one ad click. Use the `keywordType` filter to include either targeting expressions or keywords in your report.  

### Sponsored Products

#### Configurations

| Configuration | Available values |
|----------|---------|
| `reportTypeId` | `spSearchTerm` |
| Maximum date range | 31 days |
| Data retention | 95 days |
| `timeUnit` | `SUMMARY` or `DAILY` |
| `groupBy` | `searchTerm` |
| `format` | `GZIP_JSON` |

#### Base metrics

[impressions](guides/reporting/v3/metrics#impressions), [clicks](guides/reporting/v3/metrics#clicks), [costPerClick](guides/reporting/v3/metrics#costPerClick), [clickThroughRate](guides/reporting/v3/metrics#clickThroughRate), [cost](guides/reporting/v3/metrics#cost), [purchases1d](guides/reporting/v3/metrics#purchases1d), [purchases7d](guides/reporting/v3/metrics#purchases7d), [purchases14d](guides/reporting/v3/metrics#purchases14d), [purchases30d](guides/reporting/v3/metrics#purchases30d), [purchasesSameSku1d](guides/reporting/v3/metrics#purchasesSameSku1d), [purchasesSameSku7d](guides/reporting/v3/metrics#purchasesSameSku7d), [purchasesSameSku14d](guides/reporting/v3/metrics#purchasesSameSku14d), [purchasesSameSku30d](guides/reporting/v3/metrics#purchasesSameSku30d), [unitsSoldClicks1d](guides/reporting/v3/metrics#unitsSoldClicks1d), [unitsSoldClicks7d](guides/reporting/v3/metrics#unitsSoldClicks7d), [unitsSoldClicks14d](guides/reporting/v3/metrics#unitsSoldClicks14d), [unitsSoldClicks30d](guides/reporting/v3/metrics#unitsSoldClicks30d), [sales1d](guides/reporting/v3/metrics#sales1d), [sales7d](guides/reporting/v3/metrics#sales7d), [sales14d](guides/reporting/v3/metrics#sales14d), [sales30d](guides/reporting/v3/metrics#sales30d), [attributedSalesSameSku1d](guides/reporting/v3/metrics#attributedSalesSameSku1d), [attributedSalesSameSku7d](guides/reporting/v3/metrics#attributedSalesSameSku7d), [attributedSalesSameSku14d](guides/reporting/v3/metrics#attributedSalesSameSku14d), [attributedSalesSameSku30d](guides/reporting/v3/metrics#attributedSalesSameSku30d), [unitsSoldSameSku1d](guides/reporting/v3/metrics#unitsSoldSameSku1d), [unitsSoldSameSku7d](guides/reporting/v3/metrics#unitsSoldSameSku7d), [unitsSoldSameSku14d](guides/reporting/v3/metrics#unitsSoldSameSku14d), [unitsSoldSameSku30d](guides/reporting/v3/metrics#unitsSoldSameSku30d), [kindleEditionNormalizedPagesRead14d](guides/reporting/v3/metrics#kindleEditionNormalizedPagesRead14d), [kindleEditionNormalizedPagesRoyalties14d](guides/reporting/v3/metrics#kindleEditionNormalizedPagesRoyalties14d), [salesOtherSku7d](guides/reporting/v3/metrics#salesOtherSku7d), [unitsSoldOtherSku7d](guides/reporting/v3/metrics#unitsSoldOtherSku7d), [acosClicks7d](guides/reporting/v3/metrics#acosClicks7d), [acosClicks14d](guides/reporting/v3/metrics#acosClicks14d), [roasClicks7d](guides/reporting/v3/metrics#roasClicks7d), [roasClicks14d](guides/reporting/v3/metrics#roasClicks14d), [keywordId](guides/reporting/v3/metrics#keywordId), [keyword](guides/reporting/v3/metrics#keyword), [campaignBudgetCurrencyCode](guides/reporting/v3/metrics#campaignBudgetCurrencyCode), [date](guides/reporting/v3/metrics#date), [startDate](guides/reporting/v3/metrics#startDate), [endDate](guides/reporting/v3/metrics#endDate), [portfolioId](guides/reporting/v3/metrics#portfolioId), [searchTerm](guides/reporting/v3/metrics#searchTerm), [campaignName](guides/reporting/v3/metrics#campaignName), [campaignId](guides/reporting/v3/metrics#campaignId), [campaignBudgetType](guides/reporting/v3/metrics#campaignBudgetType), [campaignBudgetAmount](guides/reporting/v3/metrics#campaignBudgetAmount), [campaignStatus](guides/reporting/v3/metrics#campaignStatus), [keywordBid](guides/reporting/v3/metrics#keywordBid), [adGroupName](guides/reporting/v3/metrics#adGroupName), [adGroupId](guides/reporting/v3/metrics#adGroupId), [keywordType](guides/reporting/v3/metrics#keywordType), [matchType](guides/reporting/v3/metrics#matchType), [targeting](guides/reporting/v3/metrics#targeting), [adKeywordStatus](guides/reporting/v3/metrics#adKeywordStatus)
 

#### Group by `searchTerm`

Additional metrics: [adKeywordStatus](guides/reporting/v3/metrics#adKeywordStatus)

Filters: 

- `keywordType` (values: `BROAD`, `PHRASE`, `EXACT`, `TARGETING_EXPRESSION`, `TARGETING_EXPRESSION_PREDEFINED`)

### Sample calls

#### Targeting expressions only

```shell
curl --location --request POST 'https://advertising-api.amazon.com/reporting/reports' \
--header 'Content-Type: application/vnd.createasyncreportrequest.v3+json' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxxx' \
--header 'Amazon-Advertising-API-Scope: xxxxxxxx' \
--header 'Authorization: Bearer Atza|xxxxxxxxx' \
--data-raw '{
    "name":"SP search term report 7/5-7/10",
    "startDate":"2022-07-05",
    "endDate":"2022-07-10",
    "configuration":{
        "adProduct":"SPONSORED_PRODUCTS",
        "groupBy":["searchTerm"],
        "columns":["impressions","clicks","cost","campaignId","adGroupId","date","targeting","searchTerm","keywordType","keywordId"],
        "filters": [
            {
                "field": "keywordType",
                "values": [
                    "TARGETING_EXPRESSION",
                    "TARGETING_EXPRESSION_PREDEFINED"
                ]
            }
        ],
        "reportTypeId":"spSearchTerm",
        "timeUnit":"DAILY",
        "format":"GZIP_JSON"
    }
}'
```
    
#### Keywords only

```shell
curl --location --request POST 'https://advertising-api.amazon.com/reporting/reports' \
--header 'Content-Type: application/vnd.createasyncreportrequest.v3+json' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxxx' \
--header 'Amazon-Advertising-API-Scope: xxxxxxxx' \
--header 'Authorization: Bearer Atza|xxxxxxxxx' \
--data-raw '{
    "name":"SP search terms report 7/5-7/10",
    "startDate":"2022-07-05",
    "endDate":"2022-07-10",
    "configuration":{
        "adProduct":"SPONSORED_PRODUCTS",
        "groupBy":["searchTerm"],
        "columns":["impressions","clicks","cost","campaignId","adGroupId","startDate","endDate","keywordType","keyword","matchType","keywordId","searchTerm"],
        "filters": [
            {
                "field": "keywordType",
                "values": [
                    "BROAD",
                    "PHRASE",
                    "EXACT"
                ]
            }
        ],
        "reportTypeId":"spSearchTerm",
        "timeUnit":"SUMMARY",
        "format":"GZIP_JSON"
    }
}'
```

## Advertised product reports

Advertised product reports contain performance data for products that are advertised as part of your campaigns. 

### Sponsored Products

#### Configurations

| Configuration | Available values |
|----------|---------|
| `reportTypeId` | `spAdvertisedProduct` |
| Maximum date range | 31 days |
| Data retention | 95 days |
| `timeUnit` | `SUMMARY` or `DAILY` |
| `groupBy` | `advertiser` |
| `format` | `GZIP_JSON` |

#### Base metrics

[date](guides/reporting/v3/metrics#date), [startDate](guides/reporting/v3/metrics#startDate), [endDate](guides/reporting/v3/metrics#endDate), [campaignName](guides/reporting/v3/metrics#campaignName), [campaignId](guides/reporting/v3/metrics#campaignId), [adGroupName](guides/reporting/v3/metrics#adGroupName), [adGroupId](guides/reporting/v3/metrics#adGroupId), [adId](guides/reporting/v3/metrics#adId), [portfolioId](guides/reporting/v3/metrics#portfolioId), [impressions](guides/reporting/v3/metrics#impressions), [clicks](guides/reporting/v3/metrics#clicks), [costPerClick](guides/reporting/v3/metrics#costPerClick), [clickThroughRate](guides/reporting/v3/metrics#clickThroughRate), [cost](guides/reporting/v3/metrics#cost), [spend](guides/reporting/v3/metrics#spend), [campaignBudgetCurrencyCode](guides/reporting/v3/metrics#campaignBudgetCurrencyCode), [campaignBudgetAmount](guides/reporting/v3/metrics#campaignBudgetAmount), [campaignBudgetType](guides/reporting/v3/metrics#campaignBudgetType), [campaignStatus](guides/reporting/v3/metrics#campaignStatus), [advertisedAsin](guides/reporting/v3/metrics#advertisedAsin), [advertisedSku](guides/reporting/v3/metrics#advertisedSku), [purchases1d](guides/reporting/v3/metrics#purchases1d), [purchases7d](guides/reporting/v3/metrics#purchases7d), [purchases14d](guides/reporting/v3/metrics#purchases14d), [purchases30d](guides/reporting/v3/metrics#purchases30d), [purchasesSameSku1d](guides/reporting/v3/metrics#purchasesSameSku1d), [purchasesSameSku7d](guides/reporting/v3/metrics#purchasesSameSku7d), [purchasesSameSku14d](guides/reporting/v3/metrics#purchasesSameSku14d), [purchasesSameSku30d](guides/reporting/v3/metrics#purchasesSameSku30d), [unitsSoldClicks1d](guides/reporting/v3/metrics#unitsSoldClicks1d), [unitsSoldClicks7d](guides/reporting/v3/metrics#unitsSoldClicks7d), [unitsSoldClicks14d](guides/reporting/v3/metrics#unitsSoldClicks14d), [unitsSoldClicks30d](guides/reporting/v3/metrics#unitsSoldClicks30d), [sales1d](guides/reporting/v3/metrics#sales1d), [sales7d](guides/reporting/v3/metrics#sales7d), [sales14d](guides/reporting/v3/metrics#sales14d), [sales30d](guides/reporting/v3/metrics#sales30d), [attributedSalesSameSku1d](guides/reporting/v3/metrics#attributedSalesSameSku1d), [attributedSalesSameSku7d](guides/reporting/v3/metrics#attributedSalesSameSku7d), [attributedSalesSameSku14d](guides/reporting/v3/metrics#attributedSalesSameSku14d), [attributedSalesSameSku30d](guides/reporting/v3/metrics#attributedSalesSameSku30d), [salesOtherSku7d](guides/reporting/v3/metrics#salesOtherSku7d), [unitsSoldSameSku1d](guides/reporting/v3/metrics#unitsSoldSameSku1d), [unitsSoldSameSku7d](guides/reporting/v3/metrics#unitsSoldSameSku7d), [unitsSoldSameSku14d](guides/reporting/v3/metrics#unitsSoldSameSku14d), [unitsSoldSameSku30d](guides/reporting/v3/metrics#unitsSoldSameSku30d), [unitsSoldOtherSku7d](guides/reporting/v3/metrics#unitsSoldOtherSku7d), [kindleEditionNormalizedPagesRead14d](guides/reporting/v3/metrics#kindleEditionNormalizedPagesRead14d), [kindleEditionNormalizedPagesRoyalties14d](guides/reporting/v3/metrics#kindleEditionNormalizedPagesRoyalties14d), [acosClicks7d](guides/reporting/v3/metrics#acosClicks7d), [acosClicks14d](guides/reporting/v3/metrics#acosClicks14d), [roasClicks7d](guides/reporting/v3/metrics#roasClicks7d), [roasClicks14d](guides/reporting/v3/metrics#roasClicks14d)
 

#### Group by `advertiser`

Additional metrics: N/A

Filters: 

- `adCreativeStatus` (values: `ENABLED`,`PAUSED`,`ARCHIVED`)

### Sample call

 ```shell
curl --location --request POST 'https://advertising-api.amazon.com/reporting/reports' \
--header 'Content-Type: application/vnd.createasyncreportrequest.v3+json' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxxx' \
--header 'Amazon-Advertising-API-Scope: xxxxxxx' \
--header 'Authorization: Bearer Atza|xxxxxxxxxxxxx' \
--data-raw '{
    "name":"SP advertised product report 7/5-7/10",
    "startDate":"2022-07-05",
    "endDate":"2022-07-10",
    "configuration":{
        "adProduct":"SPONSORED_PRODUCTS",
        "groupBy":["advertiser"],
        "columns":["impressions","clicks","cost","campaignId","advertisedAsin"],
        "reportTypeId":"spAdvertisedProduct",
        "timeUnit":"SUMMARY",
        "format":"GZIP_JSON"
    }
}'
```

## Purchased product reports

### Sponsored Products

Sponsored Products purchased product reports contain performance data for products that were purchased, but were not advertised as part of a campaign. The purchased product report contains both targeting expressions and keyword IDs. After you have received your report, you can filter on [keywordType](guides/reporting/v3/metrics#keywordType) to distinguish between targeting expressions and keywords. 

#### Configurations

| Configuration | Available values |
|----------|---------|
| `reportTypeId` | `spPurchasedProduct` |
| Maximum date range | 31 days |
| Data retention | 95 days |
| `timeUnit` | `SUMMARY` or `DAILY` |
| `groupBy` | `asin` |
| `format` | `GZIP_JSON` |

#### Base metrics

[date](guides/reporting/v3/metrics#date), [startDate](guides/reporting/v3/metrics#startDate), [endDate](guides/reporting/v3/metrics#endDate), [portfolioId](guides/reporting/v3/metrics#portfolioId), [campaignName](guides/reporting/v3/metrics#campaignName), [campaignId](guides/reporting/v3/metrics#campaignId), [adGroupName](guides/reporting/v3/metrics#adGroupName), [adGroupId](guides/reporting/v3/metrics#adGroupId), [keywordId](guides/reporting/v3/metrics#keywordId), [keyword](guides/reporting/v3/metrics#keyword), [keywordType](guides/reporting/v3/metrics#keywordType), [advertisedAsin](guides/reporting/v3/metrics#advertisedAsin), [purchasedAsin](guides/reporting/v3/metrics#purchasedAsin), [advertisedSku](guides/reporting/v3/metrics#advertisedSku), [campaignBudgetCurrencyCode](guides/reporting/v3/metrics#campaignBudgetCurrencyCode), [matchType](guides/reporting/v3/metrics#matchType), [unitsSoldClicks1d](guides/reporting/v3/metrics#unitsSoldClicks1d), [unitsSoldClicks7d](guides/reporting/v3/metrics#unitsSoldClicks7d), [unitsSoldClicks14d](guides/reporting/v3/metrics#unitsSoldClicks14d), [unitsSoldClicks30d](guides/reporting/v3/metrics#unitsSoldClicks30d), [sales1d](guides/reporting/v3/metrics#sales1d), [sales7d](guides/reporting/v3/metrics#sales7d), [sales14d](guides/reporting/v3/metrics#sales14d), [sales30d](guides/reporting/v3/metrics#sales30d), [purchases1d](guides/reporting/v3/metrics#purchases1d), [purchases7d](guides/reporting/v3/metrics#purchases7d), [purchases14d](guides/reporting/v3/metrics#purchases14d), [purchases30d](guides/reporting/v3/metrics#purchases30d), [unitsSoldOtherSku1d](guides/reporting/v3/metrics#unitsSoldOtherSku1d), [unitsSoldOtherSku7d](guides/reporting/v3/metrics#unitsSoldOtherSku7d), [unitsSoldOtherSku14d](guides/reporting/v3/metrics#unitsSoldOtherSku14d), [unitsSoldOtherSku30d](guides/reporting/v3/metrics#unitsSoldOtherSku30d), [salesOtherSku1d](guides/reporting/v3/metrics#salesOtherSku1d), [salesOtherSku7d](guides/reporting/v3/metrics#salesOtherSku7d), [salesOtherSku14d](guides/reporting/v3/metrics#salesOtherSku14d), [salesOtherSku30d](guides/reporting/v3/metrics#salesOtherSku30d), [purchasesOtherSku1d](guides/reporting/v3/metrics#purchasesOtherSku1d), [purchasesOtherSku7d](guides/reporting/v3/metrics#purchasesOtherSku7d), [purchasesOtherSku14d](guides/reporting/v3/metrics#purchasesOtherSku14d), [purchasesOtherSku30d](guides/reporting/v3/metrics#purchasesOtherSku30d), [kindleEditionNormalizedPagesRead14d](guides/reporting/v3/metrics#kindleEditionNormalizedPagesRead14d), [kindleEditionNormalizedPagesRoyalties14d](guides/reporting/v3/metrics#kindleEditionNormalizedPagesRoyalties14d)
 

#### Group by `asin`

Additional metrics: N/A

Filters: N/A

### Sample call

```shell
curl --location --request POST 'https://advertising-api.amazon.com/reporting/reports' \
--header 'Content-Type: application/vnd.createasyncreportrequest.v3+json' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxx' \
--header 'Amazon-Advertising-API-Scope: xxxxxxxx' \
--header 'Authorization: Bearer Atza|xxxxxxxxxx' \
--data-raw '{
    "name":"SP purchased product report 7/5-7/10",
    "startDate":"2022-07-05",
    "endDate":"2022-07-10",
    "configuration":{
        "adProduct":"SPONSORED_PRODUCTS",
        "groupBy":["asin"],
        "columns":["purchasedAsin","advertisedAsin","adGroupName","campaignName","sales14d","campaignId","adGroupId","keywordId","keywordType","keyword"],
        "reportTypeId":"spPurchasedProduct",
        "timeUnit":"SUMMARY",
        "format":"GZIP_JSON"
    }
}'
```

### Sponsored Brands

Sponsored Brands purchased product reports contain performance data for products that were purchased as a result of your campaign.

#### Configurations

| Configuration | Available values |
|----------|---------|
| `reportTypeId` | `sbPurchasedProduct` |
| Maximum date range | 731 days |
| Data retention | 731 days |
| `timeUnit` | `SUMMARY` or `DAILY` |
| `groupBy` | `purchasedAsin` |
| `format` | `GZIP_JSON` |

#### Base metrics

[campaignId](guides/reporting/v3/metrics#campaignId), [adGroupId](guides/reporting/v3/metrics#adGroupId), [date](guides/reporting/v3/metrics#date), [startDate](guides/reporting/v3/metrics#startDate), [endDate](guides/reporting/v3/metrics#endDate), [campaignBudgetCurrencyCode](guides/reporting/v3/metrics#campaignBudgetCurrencyCode), [campaignName](guides/reporting/v3/metrics#campaignName), [adGroupName](guides/reporting/v3/metrics#adGroupName), [attributionType](guides/reporting/v3/metrics#attributionType), [purchasedAsin](guides/reporting/v3/metrics#purchasedAsin), [productName](guides/reporting/v3/metrics#productName), [productCategory](guides/reporting/v3/metrics#productCategory), [sales14d](guides/reporting/v3/metrics#sales14d), [orders14d](guides/reporting/v3/metrics#orders14d), [unitsSold14d](guides/reporting/v3/metrics#unitsSold14d), [newToBrandSales14d](guides/reporting/v3/metrics#newToBrandSales14d), [newToBrandPurchases14d](guides/reporting/v3/metrics#newToBrandPurchases14d), [newToBrandUnitsSold14d](guides/reporting/v3/metrics#newToBrandUnitsSold14d), [newToBrandSalesPercentage14d](guides/reporting/v3/metrics#newToBrandSalesPercentage14d), [newToBrandPurchasesPercentage14d](guides/reporting/v3/metrics#newToBrandPurchasesPercentage14d), [newToBrandUnitsSoldPercentage14d](guides/reporting/v3/metrics#newToBrandUnitsSoldPercentage14d)
 

#### Group by `purchasedAsin`

Additional metrics: N/A

Filters: N/A

### Sample call

```shell
curl --location --request POST 'https://advertising-api.amazon.com/reporting/reports' \
--header 'Content-Type: application/vnd.createasyncreportrequest.v3+json' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxx' \
--header 'Amazon-Advertising-API-Scope: xxxxxxxx' \
--header 'Authorization: Bearer Atza|xxxxxxxxxx' \
--data-raw '{
    "name":"SB purchased product report 7/5-7/10",
    "startDate":"2022-07-05",
    "endDate":"2022-07-10",
    "configuration":{
        "adProduct":"SPONSORED_BRANDS",
        "groupBy":["purchasedAsin"],
        "columns":["purchasedAsin","attributionType","adGroupName","campaignName","sales14d","startDate","endDate"],
        "reportTypeId":"sbPurchasedProduct",
        "timeUnit":"SUMMARY",
        "format":"GZIP_JSON"
    }
}'
```

