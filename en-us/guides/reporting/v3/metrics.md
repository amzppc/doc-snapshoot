---
title: Sponsored ads reporting metrics (version 3)
description: View descriptions for all sponsored ads version 3 reporting metrics supported by the Amazon Ads API. 
type: guide
interface: api
tags:
    - Reporting
    - Sponsored Products
    - Sponsored Brands
---

# Columns

This document describes all columns available for version 3 reporting. 

>[TIP] To understand comparisons between version 2 and version 3 metrics, see our [Migration guide](reference/migration-guides/reporting-v2-v3).

#### acosClicks14d

**Type**: Double

**Description**: Advertising cost of sales based on purchases made within 14 days of an ad click.

**Report types**: [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### acosClicks7d

**Type**: Double

**Description**: Advertising cost of sales based on purchases made within 7 days of an ad click.

**Report types**: [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### adGroupId

**Type**: Integer

**Description**: Unique numerical ID of the ad group.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### adGroupName

**Type**: String

**Description**: The name of the ad group as entered by the advertiser.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### adId

**Type**: Integer

**Description**: Unique numerical ID of the ad.

**Report types**: [Advertised product](guides/reporting/v3/report-types#advertised-product-reports)

#### adKeywordStatus

**Type**: String

**Description**: Current status of a keyword.

**Report types**: [Targeting](guides/reporting/v3/report-types#targeting-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### adStatus

**Type**: String

**Description**: Status of the ad group. 

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports)

#### advertisedAsin

**Type**: String

**Description**: The ASIN associated to an advertised product. 

**Report types**: [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### advertisedSku

**Type**: String

**Description**: The SKU being advertised. Not available for vendors.

**Report types**: [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### attributedSalesSameSku14d

**Type**: Double

**Description**: Total value of sales occurring within 14 days of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### attributedSalesSameSku1d

**Type**: Double

**Description**: Total value of sales occurring within 1 day of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### attributedSalesSameSku30d

**Type**: Double

**Description**: Total value of sales occurring within 30 days of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### attributedSalesSameSku7d

**Type**: Double

**Description**: Total value of sales occurring within 7 days of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### attributionType

**Type**: String 

**Description**: Describes whether a purchase is attributed to a promoted product or brand-halo effect. 

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### campaignApplicableBudgetRuleId

**Type**: String

**Description**: The ID associated to the active budget rule for a campaign. 

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports)

#### campaignApplicableBudgetRuleName

**Type**: String

**Description**: The name associated to the active budget rule for a campaign. 

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports)

#### campaignBiddingStrategy

**Type**: String

**Description**: The bidding strategy associated with a campaign. Possible values for Sponsored Products: `legacy` (dynamic bids - down only), `optimizeForSales` (dynamic bids - up and down), `manual` (fixed bid).  

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports)

#### campaignBudgetAmount

**Type**: Decimal

**Description**: Total budget allocated to the campaign.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### campaignBudgetCurrencyCode

**Type**: String

**Description**: The currency code associated with the campaign. 

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### campaignBudgetType

**Type**: String

**Description**: One of `daily` or `lifetime`.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### campaignId

**Type**: Integer

**Description**: The ID associated with a campaign.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### campaignName

**Type**: String 

**Description**: The name associated with a campaign.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### campaignRuleBasedBudgetAmount

**Type**: Decimal

**Description**: The value of the rule-based budget for a campaign.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports)

#### campaignStatus

**Type**: String

**Description**: The status of a campaign.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### clicks

**Type**: Integer

**Description**: Total number of ad clicks. 

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### clickThroughRate

**Type**: Decimal

**Description**: Clicks divided by impressions.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### cost

**Type**: Decimal

**Description**: Total cost of ad clicks. Same as [spend](guides/reporting/v3/metrics#spend).

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### costPerClick

**Type**: Decimal

**Description**: Total cost divided by total number of clicks.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### date

**Type**: String

**Description**: Date when the ad activity ocurred in the format YYYY-MM-DD.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### endDate

**Type**: String 

**Description**: End date of summary period for a report in the format YYYY-MM-DD.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### impressions

**Type**: Integer

**Description**: Total number of ad impressions.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### keyword

**Type**: String

**Description**: Text of the keyword or a representation of the targeting expression. For targets, the same value is returned in the [targeting](guides/reporting/v3/metrics#targeting) metric.

**Report types**: [Targeting](guides/reporting/v3/report-types#targeting-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### keywordBid

**Type**: Decimal

**Description**: Bid associated with a keyword or targeting expression.

**Report types**: [Targeting](guides/reporting/v3/report-types#targeting-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### keywordId

**Type**: Integer

**Description**: ID associated with a keyword or targeting expression.

**Report types**: [Targeting](guides/reporting/v3/report-types#targeting-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### keywordType

**Type**: String

**Description**: Type of matching for the keyword used in bid. For keywords, one of: `BROAD`, `PHRASE`, or `EXACT`. For targeting expressions, one of `TARGETING_EXPRESSION` or `TARGETING_EXPRESSION_PREDEFINED`. Same as [matchType](guides/reporting/v3/metrics#matchType)

**Report types**: [Targeting](guides/reporting/v3/report-types#targeting-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### kindleEditionNormalizedPagesRead14d

**Type**: Integer

**Description**: Number of attributed Kindle edition normalized pages read within 14 days of ad click.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### kindleEditionNormalizedPagesRoyalties14d

**Type**: Decimal

**Description**: The estimated royalties of attributed estimated Kindle edition normalized pages within 14 days of ad click.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### matchType

**Type**: String

**Description**: Type of matching for the keyword used in bid. For keywords, one of: `BROAD`, `PHRASE`, or `EXACT`. For targeting expressions, one of `TARGETING_EXPRESSION` or `TARGETING_EXPRESSION_PREDEFINED`. Same as [keywordType](guides/reporting/v3/metrics#keywordType).

**Report types**: [Targeting](guides/reporting/v3/report-types#targeting-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### newToBrandPurchases14d

**Type**: Integer

**Description**: The number of first-time orders for brand products over a one-year lookback window. Not available for book vendors.

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)


#### newToBrandPurchasesPercentage14d

**Type**: Decimal

**Description**: The percentage of total orders that are new-to-brand orders. Not available for book vendors.

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)


#### newToBrandSales14d

**Type**: Decimal

**Description**: Total value of new-to-brand sales occurring within 14 days of an ad click. Not available for book vendors.

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### newToBrandSalesPercentage14d

**Type**: Decimal

**Description**: Percentage of total sales made up of new-to-brand purchases. Not available for book vendors.

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### newToBrandUnitsSold14d

**Type**: Integer

**Description**: Total number of attributed units ordered as part of new-to-brand sales occurring within 14 days of an ad click. Not available for book vendors.

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### newToBrandUnitsSoldPercentage14d

**Type**: Decimal

**Description**: Percentage of total attributed units ordered within 14 days of an ad click that are part of a new-to-brand purchase. Not available for book vendors.

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### orders14d

**Type**: Integer

**Description**: Orders within the last 14 days.

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### placementClassification

**Type**: String

**Description**: The page location where an ad appeared. 

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports)

#### portfolioId

**Type**: Integer

**Description**: The portfolio the campaign is associated with. 

**Report types**: [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### productCategory

**Type**: String

**Description**: The category the product is associated with on Amazon. 

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### productName

**Type**: String

**Description**: The name of the product. 

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### purchasedAsin

**Type**: String 

**Description**: The ASIN of the product that was purchased.

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### purchases14d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 14 days of an ad click.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### purchases1d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 1 day of an ad click.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### purchases30d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 30 days of an ad click.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### purchases7d

**Type**: Integer 

**Description**: Number of attributed conversion events occurring within 7 days of an ad click.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### purchasesOtherSku14d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 14 days of an ad click where the SKU purchased was different that the advertised SKU. 

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### purchasesOtherSku1d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 1 day of an ad click where the SKU purchased was different that the advertised SKU. 

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### purchasesOtherSku30d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 30 days of an ad click where the SKU purchased was different that the advertised SKU. 

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### purchasesOtherSku7d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 7 days of an ad click where the SKU purchased was different that the advertised SKU. 

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### purchasesSameSku14d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 14 days of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### purchasesSameSku1d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 1 day of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### purchasesSameSku30d

**Type**: Integer

**Description**: Number of attributed conversion events occurring within 30 days of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### purchasesSameSku7d

**Type**: Integer
 
**Description**: Number of attributed conversion events occurring within 7 days of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### roasClicks14d

**Type**: Decimal 

**Description**: Return on ad spend based on purchases made within 14 days of an ad click.

**Report types**: [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### roasClicks7d

**Type**: Decimal 

**Description**: Return on ad spend based on purchases made within 7 days of an ad click.

**Report types**: [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### sales14d

**Type**: Decimal 

**Description**: Total value of sales occurring within 14 days of an ad click.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### sales1d

**Type**: Decimal 

**Description**: Total value of sales occurring within 1 day of an ad click.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### sales30d

**Type**: Decimal 

**Description**: Total value of sales occurring within 30 days of an ad click.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### sales7d

**Type**: Decimal 

**Description**: Total value of sales occurring within 7 days of an ad click.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### salesOtherSku14d

**Type**: Decimal

**Description**: Total value of sales occurring within 14 days of an ad click where the purchased SKU was different from the SKU advertised.

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### salesOtherSku1d

**Type**: Decimal

**Description**: Total value of sales occurring within 1 day of an ad click where the purchased SKU was different from the SKU advertised.

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### salesOtherSku30d

**Type**: Decimal

**Description**: Total value of sales occurring within 30 days of an ad click where the purchased SKU was different from the SKU advertised.

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### salesOtherSku7d

**Type**: Decimal 

**Description**: Total value of sales occurring within 7 days of an ad click where the purchased SKU was different from the SKU advertised.

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### searchTerm

**Type**: String

**Description**: The search term used by the customer. 

**Report types**: [Search term](guides/reporting/v3/report-types#search-term-reports)

#### spend

**Type**: Decimal

**Description**: Total cost of ad clicks. Same as [cost](guides/reporting/v3/metrics#cost).

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports)

#### startDate

**Type**: String

**Description**: Start date of summary period for a report in the format YYYY-MM-DD.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### targeting

**Type**: String

**Description**:  A string representation of the expression object used in the targeting clause. The targeting expression is also returned in [keyword](guides/reporting/v3/metrics#keyword).

**Report types**: [Targeting](guides/reporting/v3/report-types#targeting-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### topOfSearchImpressionShare

**Type**: Decimal

**Description**:  The percentage of top-of-search impressions earned out of all the top-of-search impressions that were eligible for a given date range. Various factors determine the eligibility for an impression including campaign status and targeting status.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports)

#### unitsSold14d

**Type**: Integer

**Description**: Number of attributed units sold within 14 days of click on an ad. Same as [unitsSoldClicks14d](guides/reporting/v3/metrics#unitsSoldClicks14d). Only valid for Sponsored Brands version 3 campaigns, not Sponsored Brands video or multi-ad group (version 4) campaigns. 

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### unitsSoldClicks14d

**Type**: Integer

**Description**: Total number of units ordered within 14 days of an ad click. Same as [unitsSold14d](guides/reporting/v3/metrics#unitsSold14d)

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### unitsSoldClicks1d

**Type**: Integer

**Description**: Total number of units ordered within 1 day of an ad click.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### unitsSoldClicks30d

**Type**: Integer

**Description**: Total number of units ordered within 30 days of an ad click.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### unitsSoldClicks7d

**Type**: Integer

**Description**: Total number of units ordered within 7 days of an ad click.

**Report types**: [Campaign](guides/reporting/v3/report-types#campaign-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### unitsSoldOtherSku14d

**Type**: Integer

**Description**: Total number of units ordered within 14 days of an ad click where the purchased SKU was different from the SKU advertised.

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### unitsSoldOtherSku1d

**Type**: Integer

**Description**: Total number of units ordered within 1 day of an ad click where the purchased SKU was different from the SKU advertised.

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### unitsSoldOtherSku30d

**Type**: Integer

**Description**: Total number of units ordered within 30 days of an ad click where the purchased SKU was different from the SKU advertised.

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports)

#### unitsSoldOtherSku7d

**Type**: Integer

**Description**: Total number of units ordered within 7 days of an ad click where the purchased SKU was different from the SKU advertised.

**Report types**: [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### unitsSoldSameSku14d

**Type**: Integer

**Description**: Total number of units ordered within 14 days of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### unitsSoldSameSku1d

**Type**: Integer

**Description**: Total number of units ordered within 1 day of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### unitsSoldSameSku30d

**Type**: Integer

**Description**: Total number of units ordered within 30 days of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)

#### unitsSoldSameSku7d

**Type**: Integer

**Description**: Total number of units ordered within 7 days of ad click where the purchased SKU was the same as the SKU advertised.

**Report types**: [Targeting](guides/reporting/v3/report-types#targeting-reports), [Advertised product](guides/reporting/v3/report-types#advertised-product-reports), [Purchased product](guides/reporting/v3/report-types#purchased-product-reports), [Search term](guides/reporting/v3/report-types#search-term-reports)
