---
title: Getting started with Brand Benchmarks
description: Learn how to request with brand benchmarks 
type: guide
interface: api
tags:
    - Reporting
---

# Getting started with Brand Benchmarks 

## Overview

Amazon Brand View Pro Browse Node (ABVP) is a competitive insights tool that provides Amazon sellers and advertisers with retail and media metrics to benchmark their on-Amazon brand performance against their peers. ABVP helps businesses optimize their Amazon marketing strategies by offering insights into sales growth, share of voice, traffic and conversion trends. With ABVP 3P API access, advertisers can programmatically incorporate their ABVP reporting into their own internal tools and dashboards. Users can use the API to obtain the most recent ASIN-level and brand-level performance metrics.

## Before you begin

To get started, advertisers must complete the necessary setup and authorization steps. This includes creating a Login with Amazon (LWA) application, obtaining API access approval, and configuring their environment to make authenticated calls to the API.

1. Obtain an Amazon Developer Account
    1. Sign up for a new Amazon Developer account if you don't have one already
    2. Create a new Login with Amazon (LWA) security profile
2. Gain Access to the ABVP Advertiser Entity 
    1. Ensure your Amazon Developer account has Editor access to the ABVP advertiser entity
3. Apply for Amazon Advertising API Access
    1. Submit an application to get access to the Amazon Advertising API
4. Set up your Environment for the ABVP API
    1. Obtain a refresh token using the LWA security profile
    2. Generate access token using the refresh token
5. Call the ABVP API Endpoints
    1. Fetch the latest ABVP report metadata using the allReportMetadata endpoint on the Advertising API
    2. Retrieve a specific ABVP report using the getReport endpoint and the report metadata

## Calling the Brand Benchmarks API

|Parameter / Header	| How to find it	|
|---	|---	|
| \<YOUR\_ACCESS\_TOKEN\> | Call the advertising API using your refresh token for a new access token	|
| \<YOUR\_CLIENT\_ID\> | This is your Login-with-Amazon account ID	|
| \<YOUR\_MANAGER\_ACCOUNT\_ID\> | This is the manager account ID of the ABVP entity. This can be found by chedcking https://advertising.amazon.com/am/managerAccounts	|
| \<YOUR\_ADVERTISER\_CODE\> | To find the advertiser code, please navigate to your advertiser's dashboard in ABVP. The advertiser code can be found in the URL as: "https://advertising.amazon.com/bv#/ABVP/**YOUR_ADVERTISER_CODE**/dashboard"	|
| \<REPORT\_TYPE\>  | Used only in the GET report call. Obtained from first making the report metadata call	|
| \<INDEX\_DATE\>   | Used only in the GET report call. Obtained from first making the report metadata call	|

## Create a report metadata request

### Sample request:

```
curl -X GET "https://advertising-api.amazon.com/insights/brandBenchmarks/advertisers/<YOUR_ADVERTISER_CODE>/allReportMetadata" \
-H "Authorization: Bearer <YOUR_ACCESS_TOKEN>" \
-H "Amazon-Advertising-API-ClientId: <YOUR_CLIENT_ID>" \ 
-H "Amazon-Advertising-API-Manager-Account: <YOUR_MANAGER_ACCOUNT_ID>"
```

### Sample response:

```
{
    "nextToken": null,
    "reportsMetadata": [
        {
            "advertiserId": "<YOUR_ADVERTISER_CODE>",
            "indexDate": "2024-02-04",
            "obfuscatedMarketplaceId": "ATVPDKIKX0DER",
            "reportType": "ZIP_ASIN"
        },
        {
            "advertiserId": "<YOUR_ADVERTISER_CODE>",
            "indexDate": "2024-02-09",
            "obfuscatedMarketplaceId": "ATVPDKIKX0DER",
            "reportType": "EXCEL"
        },
        {
            "advertiserId": "<YOUR_ADVERTISER_CODE>",
            "indexDate": "2024-02-09",
            "obfuscatedMarketplaceId": "ATVPDKIKX0DER",
            "reportType": "ZIP_BRAND"
        }
    ]
}
```

## Create a report download request

### Sample request:

```
curl -X GET "https://advertising-api.amazon.com/insights/brandBenchmarks/advertisers/<YOUR_ADVERTISER_CODE>/reports/<REPORT_TYPE>/indexDates/<INDEX_DATE>" \
-H "Authorization: Bearer <YOUR_ACCESS_TOKEN>" \
-H "Amazon-Advertising-API-ClientId: <YOUR_CLIENT_ID>" \
-H "Amazon-Advertising-API-Manager-Account: <YOUR_MANAGER_ACCOUNT_ID>"
```

### Sample response:

```
{
    "downloadLink": "https://brand-view.s3.amazonaws.com/output-reports-v2/..."
}
```

