---
title: DSP reporting metrics
description: View descriptions for all DSP reporting metrics supported by the Amazon Ads API. 
type: guide
interface: api
tags:
    - DSP
    - Reporting
---

# Metrics 

All metrics available for the DSP Reporting API.

## Performance metrics 

Request performance metrics to get data on your campaigns. The performance metrics available vary based on report type.

#### 3pFeeAutomotive

**Type**: Double

**Description**: A third-party fee applied to automotive data.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### 3pFeeAutomotiveAbsorbed

**Type**: Double

**Description**: CPM charge applied to impressions to track Automotive segment fees. The cost is itemized in reports, but absorbed by the user and excluded from total costs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### 3pFeeComScore

**Type**: Double

**Description**: The third-party fee applied for using ComScore.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### 3pFeeComScoreAbsorbed

**Type**: Double

**Description**: CPM charge applied to impressions to track ComScore technology fees. The cost is itemized in reports, but absorbed by the user and excluded from total costs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### 3pFeeCPM1

**Type**: Double

**Description**: A third-party fee applied.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### 3pFeeCPM1Absorbed

**Type**: Double

**Description**: CPM charge applied to impressions to track general 3P technology fees. The cost is itemized in reports, but absorbed by the user and excluded from total costs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### 3pFeeCPM2

**Type**: Double

**Description**: A third-party fee applied.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### 3pFeeCPM2Absorbed

**Type**: Double

**Description**: CPM charge applied to impressions to track general 3P technology fees. The cost is itemized in reports, but absorbed by the user and excluded from total costs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### 3pFeeCPM3

**Type**: Double

**Description**: A third-party fee applied.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### 3pFeeCPM3Absorbed

**Type**: Double

**Description**: CPM charge applied to impressions to track general 3P technology fees. The cost is itemized in reports, but absorbed by the user and excluded from total costs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### 3pFeeDoubleclickCampaignManager

**Type**: Double

**Description**: The third-party fee applied for using DoubleClick Campaign Manager.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### 3pFeeDoubleclickCampaignManagerAbsorbed

**Type**: Double

**Description**: CPM charge applied to impressions to track Doubleclick Campaign Manager technology fees. The cost is itemized in reports, but absorbed by the user and excluded from total costs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### 3pFeeDoubleVerify

**Type**: Double

**Description**: The third-party fee applied for using DoubleVerify.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### 3pFeeDoubleVerifyAbsorbed

**Type**: Double

**Description**: CPM charge applied to impressions to track DoubleVerify technology fees. The cost is itemized in reports, but absorbed by the user and excluded from total costs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### 3pFeeIntegralAdScience

**Type**: Double

**Description**: The third-party fee applied for using Integral Ad Science.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### 3pFeeIntegralAdScienceAbsorbed

**Type**: Double

**Description**: CPM charge applied to impressions to track Integral Ad Science technology fees. The cost is itemized in reports, but absorbed by the user and excluded from total costs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### 3PFees

**Type**: Double

**Description**: The total CPM charges applied for using 3P data providers.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### accept14d

**Type**: Integer

**Description**: The number of Accept pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### acceptClicks14d

**Type**: Integer

**Description**: The number of Accept pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### acceptCPA14d

**Type**: Decimal

**Description**: The average cost to acquire an Accept pixel conversion. (Accept CPA = Total cost / Accept)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### acceptCVR14d

**Type**: Percentage

**Description**: The number of Accept pixel conversions relative to the number of ad impressions. (Accept CVR = Accept / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### acceptViews14d

**Type**: Integer

**Description**: The number of Accept pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### addedToShoppingCart14d

**Type**: Integer

**Description**: The number of Add to shopping cart pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### addedToShoppingCartClicks14d

**Type**: Integer

**Description**: The number of Add to shopping cart pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### addedToShoppingCartCPA14d

**Type**: Decimal

**Description**: The average cost to acquire an Add to shopping cart pixel conversion. (ATC CPA = Total cost / ATC)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### addedToShoppingCartCVR14d

**Type**: Percentage

**Description**: The number of Add to shopping cart pixel conversions relative to the number of ad impressions. (ATSC CVR = ATSC / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### addedToShoppingCartViews14d

**Type**: Integer

**Description**: The number of Add to shopping cart pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### addToWatchlist14d

**Type**: Integer

**Description**: The number of times Add to Watchlist was clicked on a featured product.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### addToWatchlistClicks14d

**Type**: Integer

**Description**: The number of Add to Watchlist clicks attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### addToWatchlistCPA14d

**Type**: Decimal

**Description**: The average cost to acquire an Add to Watchlist click. (eCPATW = Total cost / ATW)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### addToWatchlistCVR14d

**Type**: Percentage

**Description**: The number of Add to Watchlist clicks relative to the number of impressions. (ATWR = ATW / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### addToWatchlistViews14d

**Type**: Integer

**Description**: The number of Add to Watchlist clicks attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### advertiserCountry

**Type**: String

**Description**: The country assigned to the advertiser.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### advertiserId

**Type**: Integer

**Description**: A unique identifier assigned to an advertiser.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source), [Products](reporting/dsp/report-types#products)

#### advertiserName

**Type**: String

**Description**: Adveriser name.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source), [Products](reporting/dsp/report-types#products)

#### advertiserTimezone

**Type**: String

**Description**: The time zone the advertiser uses for reporting and ad serving purposes.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### agencyFee

**Type**: Decimal

**Description**: A percentage or flat fee removed from the total budget to compensate the agency that is managing the media buy.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### amazonAudienceFee

**Type**: Decimal

**Description**: CPM charge applied to impressions that leverage Amazon's behavioral targeting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### amazonOmnichannelMetricsFee

**Type**: Decimal

**Description**: The fee calculated based on a percentage of supply cost for impressions measured by Omnichannel Metrics.

**Report types**: [Campaign](guides/reporting/dsp/report-types#campaign), [Inventory](guides/reporting/dsp/report-types#inventory), [Audience](guides/reporting/dsp/report-types#audience)

#### amazonPlatformFee

**Type**: Decimal

**Description**: The technology fee applied to the media supply costs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### amazonStandardId

**Type**: String

**Description**: A unique block of letters and/or numbers that identify all products sold on Amazon.

**Report types**: [Products](reporting/dsp/report-types#products)

#### application14d

**Type**: Integer

**Description**: The number of Application pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### applicationClicks14d

**Type**: Integer

**Description**: The number of application pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### applicationCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Application pixel conversion. (Application CPA = Total cost / Application)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### applicationCVR14d

**Type**: Percentage

**Description**: The number of Application pixel conversions relative to the number of ad impressions. (Application CVR = Application / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### applicationViews14d

**Type**: Integer

**Description**: The number of Application pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### asinConversionType

**Type**: String

**Description**: The conversion type describes whether the conversion happened on a promoted or a brand halo ASIN.

**Report types**: [Products](reporting/dsp/report-types#products)

#### atc14d

**Type**: Integer

**Description**: The number of times a promoted ASIN is added to a customer's cart.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### atcClicks14d

**Type**: Integer

**Description**: The number of add to cart conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### atcr14d

**Type**: Percentage

**Description**: The number of Add to Cart conversions relative to the number of impressions. (ATCR = ATC / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### atcViews14d

**Type**: Integer

**Description**: The number of Add to Cart conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### atl14d

**Type**: Integer

**Description**: The number of times a promoted ASIN is added to a customer's wish list, gift list or registry.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### atlClicks14d

**Type**: Integer

**Description**: The number of Add to List conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### atlr14d

**Type**: Percentage

**Description**: The number of Add to List conversions relative to the number of impressions. (ATLR = ATL / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### atlViews14d

**Type**: Integer

**Description**: The number of Add to List conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### bannerInteraction14d

**Type**: Integer

**Description**: The number of Banner interaction pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### bannerInteractionClicks14d

**Type**: Integer

**Description**: The number of Banner interaction pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### bannerInteractionCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Banner interaction pixel conversions. (Banner interaction CPA = Total cost / Banner interaction)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### bannerInteractionCVR14d

**Type**: Percentage

**Description**: The number of Banner interaction pixel conversions relative to the number of ad impressions. (Banner interaction CVR = Banner interaction / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### bannerInteractionViews14d

**Type**: Integer

**Description**: The number of Banner interaction pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandHaloAddToCart14d

**Type**: Integer

**Description**: The number of times Add to Cart was clicked on other products from the same brands as the products tracked in the order, attributed to an ad.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloAddToCartClicks14d

**Type**: Integer

**Description**: The number of Add to Cart clicks attributed to ad click-throughs. These include clicks on other products from the same brands as the ASINs tracked in the order.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloAddToCartViews14d

**Type**: Integer

**Description**: The number of Add to Cart clicks attributed to impressions. These include clicks on other products from the same brands as the ASINs tracked in the order.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloAddToList14d

**Type**: Integer

**Description**: The number of times Add to List was clicked on other products from the same brands as the products tracked in the order, attributed to an ad.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloAddToListClicks14d

**Type**: Integer

**Description**: The number of Add to List clicks attributed to ad click-throughs. These include clicks on other products from the same brands as the ASINs tracked in the order.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloAddToListViews14d

**Type**: Integer

**Description**: The number of Add to List clicks attributed to impressions. These include clicks on other products from the same brands as the ASINs tracked in the order.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloDetailPage14d

**Type**: Integer

**Description**: The number of detail page views on other products from the same brands as the products tracked in the order, attributed to an ad.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloDetailPageClicks14d

**Type**: Integer

**Description**: The number of detail page views attributed to ad click-throughs. These include views on other products from the same brands as the ASINs tracked in the order.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloDetailPageViews14d

**Type**: Integer

**Description**: The number of detail page views attributed to impressions. These include views on other products from the same brands as the ASINs tracked in the order.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloNewSubscribeAndSave14d

**Type**: Integer

**Description**: The number of Subscribe & Save subscriptions on other products from the same brands as the products tracked in the order, attributed to a view or click on an ad. This count does not include further purchases of the ASIN(s) during replenishing subscriptions.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloNewSubscribeAndSaveClicks14d

**Type**: Integer

**Description**: Number of new Subscribe & Save subscriptions for brand halo products, attributed to an ad click. This does not include replenishment subscription orders. Use Total SnSS clicks to see all conversions for the brands' products.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloNewSubscribeAndSaveViews14d

**Type**: Integer

**Description**: Number of new Subscribe & Save subscriptions for brand halo products, attributed to an ad view. This does not include replenishment subscription orders. Use Total SnSS views to see all conversions for the brands' products.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloNewToBrandPurchases14d

**Type**: Integer

**Description**: New-to-brand purchases - brand halo.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloNewToBrandPurchasesClicks14d

**Type**: Integer

**Description**: New-to-brand purchases click-through conversions - brand halo.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloNewToBrandPurchasesViews14d

**Type**: Integer

**Description**: New-to-brand purchases view-through conversions - brand halo.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloPercentOfPurchasesNewToBrand14d

**Type**: Percentage

**Description**: Percent of purchases new-to-brand - brand halo.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloProductReviewPage14d

**Type**: Integer

**Description**: The number of Product review page views for other products from the same brands as the products tracked in the order, attributed to an ad.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloProductReviewPageClicks14d

**Type**: Integer

**Description**: The number of Product review page views attributed to ad click-throughs. These include views on other products from the same brands as the ASINs tracked in the order.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloProductReviewPageViews14d

**Type**: Integer

**Description**: The number of Product review page views attributed to impressions. These include views on other products from the same brands as the ASINs tracked in the order.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloPurchases14d

**Type**: Integer

**Description**: The number of purchases on other products from the same brands as the products tracked in the order, attributed to an ad.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloPurchasesClicks14d

**Type**: Integer

**Description**: The number of purchases attributed to ad click-throughs. These include purchases on other products from the same brands as the ASINs tracked in the order.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloPurchasesViews14d

**Type**: Integer

**Description**: The number of purchases attributed to impressions. These include purchases on other products from the same brands as the ASINs tracked in the order.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloTotalNewToBrandSales14d

**Type**: Decimal

**Description**: Sales (in local currency) of brand halo products purchased by new-to-brand shoppers, attributed to an ad view or click. Shoppers are "new to brand" if they have not purchased from the brand in the last 365 days. Use Total new-to-brand product sales to see all conversions for the brands' products.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloTotalNewToBrandUnitsSold14d

**Type**: Integer

**Description**: Units of promoted products purchased by shoppers who were new-to-brand, attributed to an ad view or click. A single new-to-brand purchase event can include multiple sold units. Use Total new-to-brand units sold to see all conversions for the brands' products.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloTotalSales14d

**Type**: Decimal

**Description**: The total sales (in local currency) of brand halo ASINs purchased by customers on Amazon after delivering an ad.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandHaloTotalUnitsSold14d

**Type**: Integer

**Description**: The total quantity of brand halo products purchased by customers on Amazon after delivering an ad. A campaign can have multiple units sold in a single purchase event.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandName

**Type**: String

**Description**: The name of the brand the campaign is advertising.

**Report types**: [Products](reporting/dsp/report-types#products)

#### brandSearch14d

**Type**: Integer

**Description**: The number of times a branded keyword was searched on Amazon based on keywords generated from the featured ASINs in your campaign.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandSearchClicks14d

**Type**: Integer

**Description**: The number of branded search conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandSearchCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a branded search conversion. (Cost per branded search = Total cost / Branded searches)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandSearchRate14d

**Type**: Percentage

**Description**: The number of branded search conversions relative to the number of ad impressions. (Branded search rate = Branded searches / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandSearchViews14d

**Type**: Integer

**Description**: The number of branded search conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement1

**Type**: Integer

**Description**: The number of Brand store engagement 1 pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement1Clicks

**Type**: Integer

**Description**: The number of Brand store engagement 1 pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement1CPA

**Type**: Decimal

**Description**: The average cost to acquire a Brand store engagement 1 pixel conversions. (Brand store engagement 1 CPA = Total cost / Brand store engagement 1)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### brandStoreEngagement1CVR

**Type**: Percentage

**Description**: The number of Brand store engagement 1 pixel conversions relative to the number of ad impressions. (Brand store engagement 1 CVR = Brand store engagement 1 / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### brandStoreEngagement1Views

**Type**: Integer

**Description**: The number of Brand store engagement 1 pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement2

**Type**: Integer

**Description**: The number of Brand store engagement 2 pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement2Clicks

**Type**: Integer

**Description**: The number of Brand store engagement 2 pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement2CPA

**Type**: Decimal

**Description**: The average cost to acquire a Brand store engagement 2 pixel conversions. (Brand store engagement 2 CPA = Total cost / Brand store engagement 2)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### brandStoreEngagement2CVR

**Type**: Percentage

**Description**: The number of Brand store engagement 2 pixel conversions relative to the number of ad impressions. (Brand store engagement 2 CVR = Brand store engagement 2 / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### brandStoreEngagement2Views

**Type**: Integer

**Description**: The number of Brand store engagement 2 pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement3

**Type**: Integer

**Description**: The number of Brand store engagement 3 pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement3Clicks

**Type**: Integer

**Description**: The number of Brand store engagement 3 pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement3CPA

**Type**: Decimal

**Description**: The average cost to acquire a Brand store engagement 3 pixel conversions. (Brand store engagement 3 CPA = Total cost / Brand store engagement 3)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### brandStoreEngagement3CVR

**Type**: Percentage

**Description**: The number of Brand store engagement 3 pixel conversions relative to the number of ad impressions. (Brand store engagement 3 CVR = Brand store engagement 3 / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### brandStoreEngagement3Views

**Type**: Integer

**Description**: The number of Brand store engagement 3 pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement4

**Type**: Integer

**Description**: The number of Brand store engagement 4 pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement4Clicks

**Type**: Integer

**Description**: The number of Brand store engagement 4 pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement4CPA

**Type**: Decimal

**Description**: The average cost to acquire a Brand store engagement 4 pixel conversions. (Brand store engagement 4 CPA = Total cost / Brand store engagement 4)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### brandStoreEngagement4CVR

**Type**: Percentage

**Description**: The number of Brand store engagement 4 pixel conversions relative to the number of ad impressions. (Brand store engagement 4 CVR = Brand store engagement 4 / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### brandStoreEngagement4Views

**Type**: Integer

**Description**: The number of Brand store engagement 4 pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement5

**Type**: Integer

**Description**: The number of Brand store engagement 5 pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement5Clicks

**Type**: Integer

**Description**: The number of Brand store engagement 5 pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement5CPA

**Type**: Decimal

**Description**: The average cost to acquire a Brand store engagement 5 pixel conversions. (Brand store engagement 5 CPA = Total cost / Brand store engagement 5)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### brandStoreEngagement5CVR

**Type**: Percentage

**Description**: The number of Brand store engagement 5 pixel conversions relative to the number of ad impressions. (Brand store engagement 5 CVR = Brand store engagement 5 / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### brandStoreEngagement5Views

**Type**: Integer

**Description**: The number of Brand store engagement 5 pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement6

**Type**: Integer

**Description**: The number of Brand store engagement 6 pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement6Clicks

**Type**: Integer

**Description**: The number of Brand store engagement 6 pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement6CPA

**Type**: Decimal

**Description**: The average cost to acquire a Brand store engagement 6 pixel conversions. (Brand store engagement 6 CPA = Total cost / Brand store engagement 6)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### brandStoreEngagement6CVR

**Type**: Percentage

**Description**: The number of Brand store engagement 6 pixel conversions relative to the number of ad impressions. (Brand store engagement 6 CVR = Brand store engagement 6 / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### brandStoreEngagement6Views

**Type**: Integer

**Description**: The number of Brand store engagement 6 pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement7

**Type**: Integer

**Description**: The number of Brand store engagement 7 pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement7Clicks

**Type**: Integer

**Description**: The number of Brand store engagement 7 pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### brandStoreEngagement7CPA

**Type**: Decimal

**Description**: The average cost to acquire a Brand store engagement 7 pixel conversions. (Brand store engagement 7 CPA = Total cost / Brand store engagement 7)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### brandStoreEngagement7CVR

**Type**: Percentage

**Description**: The number of Brand store engagement 7 pixel conversions relative to the number of ad impressions. (Brand store engagement 7 CVR = Brand store engagement 7 / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### brandStoreEngagement7Views

**Type**: Integer

**Description**: The number of Brand store engagement 7 pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### clickOnRedirect14d

**Type**: Integer

**Description**: The number of Click on redirect pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### clickOnRedirectClicks14d

**Type**: Integer

**Description**: The number of Click on redirect pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### clickOnRedirectCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Click on redirect pixel conversion. (Click on redirect CPA = Total cost / Click on redirect)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### clickOnRedirectCVR14d

**Type**: Percentage

**Description**: The number of Click on redirect pixel conversions relative to the number of ad impressions. (Click on redirect CVR = Click on redirect / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### clickOnRedirectViews14d

**Type**: Integer

**Description**: The number of Click on redirect pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### clickThroughs

**Type**: Integer

**Description**: A click on an ad that directs the user to a destination outside of the creative.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### combinedECPP

**Type**: Decimal

**Description**: Effective (average) cost to acquire a purchase conversion on or off Amazon. (Total cost / Combined purchases)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### combinedERPM

**Type**: Decimal

**Description**: Effective (average) revenue for sales on and off Amazon generated per thousand impressions. (Combined Sales / (Impressions / 1000))

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### combinedProductSales

**Type**: Decimal

**Description**: Sales (in local currency) for purchases on and off Amazon, attributed to an ad view or click.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source)

#### combinedPurchaseRate

**Type**: Percentage

**Description**: Rate of attributed purchase events on and off Amazon, relative to ad impressions. (Combined purchases / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### combinedPurchases

**Type**: Integer

**Description**: Number of purchase events on and off Amazon, attributed to an ad view or click. (Off-Amazon purchases + Total purchases (Amazon))

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source)

#### combinedPurchasesClicks

**Type**: Integer

**Description**: Number of purchase events on and off Amazon, attributed to an ad click. (Off-Amazon purchases clicks + Total purchases clicks (Amazon))

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source)

#### combinedPurchasesViews

**Type**: Integer

**Description**: Number of purchase events on and off Amazon, attributed to an ad view. (Off-Amazon purchases views + Total purchases views (Amazon))

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source)

#### combinedROAS

**Type**: Decimal

**Description**: Return on advertising spend for products sold on and off Amazon, measured as ad-attributed sales per local currency unit of ad spend. (Combined product sales / Total cost)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### combinedUnitsSold

**Type**: Integer

**Description**: Units of product sold on and off Amazon, attributed to an ad view or click. A single purchase event can include multiple sold units.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source)

#### CTR

**Type**: Percentage

**Description**: The number of click-throughs relative to the number of impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### date

**Type**: String

**Description**: Date.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source), [Products](reporting/dsp/report-types#products)

#### dealType

**Type**: String

**Description**: The type of a deal.

**Report types**: [Inventory](reporting/dsp/report-types#inventory)

#### decline14d

**Type**: Integer

**Description**: The number of Decline pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### declineClicks14d

**Type**: Integer

**Description**: The number of Decline pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### declineCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Decline pixel conversion. (Decline CPA = Total cost / Decline)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### declineCVR14d

**Type**: Percentage

**Description**: The number of Decline pixel conversions relative to the number of ad impressions. (Decline CVR = Decline / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### declineViews14d

**Type**: Integer

**Description**: The number of Decline pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### downloadedVideoPlayRate14d

**Type**: Percentage

**Description**: The number of downloaded video plays relative to the number of impressions. (Downloaded video play rate = Downloaded video plays / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### downloadedVideoPlays14d

**Type**: Integer

**Description**: The number of times a video was downloaded then played for the featured product.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### downloadedVideoPlaysClicks14d

**Type**: Integer

**Description**: The number of downloaded video plays attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### downloadedVideoPlaysViews14d

**Type**: Integer

**Description**: The number of downloaded video plays attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### dpv14d

**Type**: Integer

**Description**: The number of views of the advertised product's detail pages on Amazon.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### dpvClicks14d

**Type**: Integer

**Description**: The number of detail page view conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### dpvr14d

**Type**: Percentage

**Description**: The number of detail page view conversions relative to the number of ad impressions. (DVPR = DPV / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### dpvViews14d

**Type**: Integer

**Description**: The number of detail page view conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### dropDownSelection14d

**Type**: Integer

**Description**: The number of Drop down selection pixel conversions

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### dropDownSelectionClicks14d

**Type**: Integer

**Description**: The number of Drop down selection pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### dropDownSelectionCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Drop down selection pixel conversion. (Drop down selection CPA = Total cost / Drop down selection)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### dropDownSelectionCVR14d

**Type**: Percentage

**Description**: The number of Drop down selection pixel conversions relative to the number of ad impressions. (Drop down selection CVR = Drop down selection / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### dropDownSelectionViews14d

**Type**: Integer

**Description**: The number of Drop down selection pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### eCPAtc14d

**Type**: Decimal

**Description**: The average cost to acquire an Add to Cart conversion. (eCPATC = Total cost / ATC)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### eCPAtl14d

**Type**: Decimal

**Description**: Effective (average) cost to acquire an Add to List conversion for a promoted product. (eCPATL = Total cost / ATL) Use Total eCPATL to see all conversions for the brands' products.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### eCPC

**Type**: Decimal

**Description**: The average cost paid per click-through.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### eCPDPV14d

**Type**: Decimal

**Description**: The average cost to acquire a detail page view conversion. (eCPDPV = Total cost / DPV)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### eCPDVP14d

**Type**: Decimal

**Description**: The average cost to acquire a downloaded video play (eCPDVP = Total cost / Downloaded video plays)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### eCPM

**Type**: Decimal

**Description**: The total cost per thousand impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### eCPnewSubscribeAndSave14d

**Type**: Decimal

**Description**: The average cost to acquire a Subscribe & Save subscription. (eCPSnSS = Total cost / SnSS)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### eCPP14d

**Type**: Decimal

**Description**: The average cost to acquire a purchase. (eCPP = Total cost / Purchases)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### eCPPRPV14d

**Type**: Decimal

**Description**: The average cost to acquire a product review page view conversion. (eCPPRPV = Total cost / PRPV)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### eCPPT14d

**Type**: Decimal

**Description**: The average cost to acquire a video trailer play (eCPPT = Total cost / Play trailers)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### ecpr14d

**Type**: Decimal

**Description**: The average cost to acquire a rental (eCPR = Total cost / Rentals)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### ecpvc

**Type**: Decimal

**Description**: The average cost to acquire a video complete conversion (eCPVC = Total cost / Video complete)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### ecpvd14d

**Type**: Decimal

**Description**: The average cost to acquire a video download (eCPVD = Total cost / Video downloads)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### eCPVS14d

**Type**: Decimal

**Description**: The average cost to acquire a video stream. (eCPVS = Total cost / Video streams)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### emailInteraction14d

**Type**: Integer

**Description**: The number of Email interaction pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### emailInteractionClicks14d

**Type**: Integer

**Description**: The number of Email interaction pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### emailInteractionCPA14d

**Type**: Decimal

**Description**: The average cost to acquire an Email interaction pixel conversion. (Email interaction CPA = Total cost / Email interaction)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### emailInteractionCVR14d

**Type**: Percentage

**Description**: The number of Email interaction pixel conversions relative to the number of ad impressions. (Email interaction CVR = Email interaction / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### emailInteractionViews14d

**Type**: Integer

**Description**: The number of Email interaction pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### emailLoad14d

**Type**: Integer

**Description**: The number of Email load pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### emailLoadClicks14d

**Type**: Integer

**Description**: The number of Email load pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### emailLoadCPA14d

**Type**: Decimal

**Description**: The average cost to acquire an Email load pixel conversion. (Email load CPA = Total cost / Email load)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### emailLoadCVR14d

**Type**: Percentage

**Description**: The number of Email load pixel conversions relative to the number of ad impressions. (Email load CVR = Email load / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### emailLoadViews14d

**Type**: Integer

**Description**: The number of Email load pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### entityId

**Type**: String

**Description**: Entity (seat) ID.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source), [Products](reporting/dsp/report-types#products)

#### eRPM14d

**Type**: Decimal

**Description**: The average revenue generated per thousand impressions. (eRPM = Sales / (Impressions / 1000))

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### featuredASIN

**Type**: String

**Description**: The Amazon Standard Identification Number (ASIN) that was set as featured in the campaign.

**Report types**: [Products](reporting/dsp/report-types#products)

#### gameInteraction14d

**Type**: Integer

**Description**: The number of Game interaction pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### gameInteractionClicks14d

**Type**: Integer

**Description**: The number of Game interaction pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### gameInteractionCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Game interaction pixel conversions. (Game interaction CPA = Total cost / Game interaction)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### gameInteractionCVR14d

**Type**: Percentage

**Description**: The number of Game interaction pixel conversions relative to the number of ad impressions. (Game interaction CVR = Game interaction / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### gameInteractionViews14d

**Type**: Integer

**Description**: The number of Game interaction pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### gameLoad14d

**Type**: Integer

**Description**: The number of Game load pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### gameLoadClicks14d

**Type**: Integer

**Description**: The number of Game load pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### gameLoadCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Game load pixel conversion. (Game load CPA = Total cost / Game load)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### gameLoadCVR14d

**Type**: Percentage

**Description**: The number of Game load pixel conversions relative to the number of ad impressions. (Game load CVR = Game load / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### gameLoadViews14d

**Type**: Integer

**Description**: The number of Game load pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### grossClickThroughs

**Type**: Integer

**Description**: The total number of times the ad was clicked. This includes valid, potentially fraudulent, non-human, and other illegitimate clicks. This metric is available from Jul 1, 2021. Selecting a date range before Jul 1, 2021, will lead to incomplete or inaccurate results.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### grossImpressions

**Type**: Integer

**Description**: The total number of times the ad was displayed. This includes valid and invalid impressions such as potentially fraudulent, non-human, and other illegitimate impressions. This metric is available from Jul 1, 2021. Selecting a date range before Jul 1, 2021, will lead to incomplete or inaccurate results.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### homepageVisit14d

**Type**: Integer

**Description**: The number of Homepage visit pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### homepageVisitClicks14d

**Type**: Integer

**Description**: The number of Homepage visit pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### homepageVisitCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Homepage visit pixel conversion. (Homepage visit CPA = Total cost / Homepage visit)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### homepageVisitCVR14d

**Type**: Percentage

**Description**: The number of Homepage visit pixel conversions relative to the number of ad impressions. (Homepage visit CVR = Homepage visit / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### homepageVisitViews14d

**Type**: Integer

**Description**: The number of Homepage visit pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### impressions

**Type**: Integer

**Description**: The number of times an ad was displayed.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### intervalEnd

**Type**: Integer

**Description**: End date of report data.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source), [Products](reporting/dsp/report-types#products)

#### intervalStart

**Type**: Integer

**Description**: Start date of report data.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source), [Products](reporting/dsp/report-types#products)

#### invalidClickThroughs

**Type**: Integer

**Description**: Clicks that were removed by the traffic quality filter. This includes potentially fraudulent, non-human, and other illegitimate traffic. This metric is available from Jul 1, 2021. Selecting a date range before Jul 1, 2021, will lead to incomplete or inaccurate results.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### invalidClickThroughsRate

**Type**: Percentage

**Description**: The percentage of gross click-throughs that were removed by the traffic quality filter. (Invalid click-throughs rate = invalid click-throughs / gross click-throughs) This metric is available from Jul 1, 2021. Selecting a date range before Jul 1, 2021, will lead to incomplete or inaccurate results.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### invalidImpressionRate

**Type**: Percentage

**Description**: The percentage of gross impressions that were removed by the traffic quality filter. (Invalid impression rate = invalid impressions / gross impressions) This metric is available from Jul 1, 2021. Selecting a date range before Jul 1, 2021, will lead to incomplete or inaccurate results.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### invalidImpressions

**Type**: Integer

**Description**: The number of impressions removed by a traffic quality filter. This includes potentially fraudulent, non-human, and other illegitimate traffic. This metric is available from Jul 1, 2021. Selecting a date range before Jul 1, 2021, will lead to incomplete or inaccurate results.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### marketingLandingPage14d

**Type**: Integer

**Description**: The number of Marketing landing page pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### marketingLandingPageClicks14d

**Type**: Integer

**Description**: The number of Marketing landing page pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### marketingLandingPageCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Marketing landing page pixel conversion. (Landing page CPA = Total cost / Landing page)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### marketingLandingPageCVR14d

**Type**: Percentage

**Description**: The number of Marketing landing page pixel conversions relative to the number of ad impressions. (Marketing landing page CVR = Marketing landing page / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### marketingLandingPageViews14d

**Type**: Integer

**Description**: The number of Marketing landing page pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupAddToCart14d

**Type**: Integer

**Description**: The number of Mashup Add to Cart click pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupAddToCartClickCVR14d

**Type**: Percentage

**Description**: The number of Mashup Add to Cart click pixel conversions relative to the number of ad impressions. (Mashup Add to Cart click CVR = Mashup Add to Cart click / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### mashupAddToCartClicks14d

**Type**: Integer

**Description**: The number of Mashup Add to Cart click pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupAddToCartCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Mashup Add to Cart click pixel conversion. (Mashup Add to Cart click CPA = Total cost / Ad ATC)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### mashupAddToCartViews14d

**Type**: Integer

**Description**: The number of Mashup Add to Cart click pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupAddToWishlist14d

**Type**: Integer

**Description**: The number of Mashup Add to Wishlist click conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupAddToWishlistClicks14d

**Type**: Integer

**Description**: The number of Mashup Add to Wishlist click pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupAddToWishlistCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Mashup Add to Wishlist click conversion. (Mashup Add to Wishlist click CPA = Total cost / Mashup Add to Wishlist click)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### mashupAddToWishlistCVR14d

**Type**: Percentage

**Description**: The number of Mashup Add to Wishlist click pixel conversions relative to the number of ad impressions. (Mashup Add to Wishlist click CVR = Mashup Add to Wishlist click / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### mashupAddToWishlistViews14d

**Type**: Integer

**Description**: The number of Mashup Add to Wishlist click pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupBackupImage

**Type**: Integer

**Description**: The number of Mashup backup image click pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupBackupImageClicks

**Type**: Integer

**Description**: The number of Mashup backup image click pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupBackupImageCPA

**Type**: Decimal

**Description**: The average cost to acquire a Mashup backup image click pixel conversion. (Mashup backup image CPA = Total cost / Mashup backup image)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### mashupBackupImageCVR

**Type**: Percentage

**Description**: The number of Mashup backup image click pixel conversions relative to the number of ad impressions. (Mashup backup image CVR = Mashup backup image / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### mashupBackupImageViews

**Type**: Integer

**Description**: The number of Mashup backup image click pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupClickToPage

**Type**: Integer

**Description**: The number of Mashup click to page pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupClickToPageClicks

**Type**: Integer

**Description**: The number of Mashup click to page pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupClickToPageCPA

**Type**: Decimal

**Description**: The average cost to acquire a Mashup click to page pixel conversion. (Ad click CPA = Total cost / Ad click)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### mashupClickToPageCVR

**Type**: Percentage

**Description**: The number of Mashup click to page pixel conversions relative to the number of ad impressions. (Mashup click to page CVR = Ad click / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### mashupClickToPageViews

**Type**: Integer

**Description**: The number of Mashup click to page pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupClipCouponClick14d

**Type**: Integer

**Description**: The number of Mashup Clip Coupon click pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupClipCouponClickClicks14d

**Type**: Integer

**Description**: The number of Mashup Clip Coupon click pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupClipCouponClickCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Mashup Clip Coupon click pixel conversion. (Mashup CC click CPA = Total cost / Coupon)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### mashupClipCouponClickCVR14d

**Type**: Percentage

**Description**: The number of Mashup Clip Coupon click pixel conversions relative to the number of ad impressions. (Mashup CC click CVR = Coupon / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### mashupClipCouponClickViews14d

**Type**: Integer

**Description**: The number of Mashup Clip Coupon click pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupShopNowClick14d

**Type**: Integer

**Description**: The number of Mashup Shop Now click pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupShopNowClickClicks14d

**Type**: Integer

**Description**: The number of Mashup Shop Now click pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupShopNowClickCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Mashup Shop Now click pixel conversion. (Mashup SN click CPA = Total cost / Mashup SN click)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### mashupShopNowClickCVR14d

**Type**: Percentage

**Description**: The number of Mashup Shop Now click pixel conversions relative to the number of ad impressions. (Mashup SN click CVR = Mashup SN click / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### mashupShopNowClickViews14d

**Type**: Integer

**Description**: The number of Mashup Shop Now click pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupSubscribeAndSave14d

**Type**: Integer

**Description**: The number of Mashup Subscribe and Save click pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupSubscribeAndSaveClick14d

**Type**: Integer

**Description**: The number of Mashup Subscribe and Save click pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupSubscribeAndSaveClickViews14d

**Type**: Integer

**Description**: The number of Mashup Subscribe and Save click pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mashupSubscribeAndSaveCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Mashup Subscribe and Save click pixel conversion. (Mashup SnS click CPA = Total cost / Ad SnS)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### mashupSubscribeAndSaveCVR14d

**Type**: Percentage

**Description**: The number of Mashup Subscribe and Save click pixel conversions relative to the number of ad impressions. (Mashup SnS click CVR = Ad SnS / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### measurableImpressions

**Type**: Integer

**Description**: Number of impressions that were measured for viewability. The viewability metrics are available from October 1, 2019. Selecting a date range prior to October 1, 2019 will result in incomplete or inaccurate metrics.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### measurableRate

**Type**: Percentage

**Description**: Measurable impressions / total impressions. The viewability metrics are available from October 1, 2019. Selecting a date range prior to October 1, 2019 will result in incomplete or inaccurate metrics.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### messageSent14d

**Type**: Integer

**Description**: The number of Message sent pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### messageSentClicks14d

**Type**: Integer

**Description**: The number of Message sent pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### messageSentCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Message sent pixel conversion. (Message sent CPA = Total cost / Message send)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### messageSentCVR14d

**Type**: Percentage

**Description**: The number of Message sent pixel conversions relative to the number of ad impressions. (Message sent CVR = Message sent / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### messageSentViews14d

**Type**: Integer

**Description**: The number of Message sent pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mobileAppFirstStartClicks14d

**Type**: Integer

**Description**: The number of Mobile app first start conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mobileAppFirstStartCVR14d

**Type**: Percentage

**Description**: The number of Mobile app first start conversions relative to the number of ad impressions. (Mobile app first start CVR = Mobile app first start / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### mobileAppFirstStarts14d

**Type**: Integer

**Description**: Mobile app first start view-through conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### mobileAppFirstStartsCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Mobile app first start conversion. (Mobile app first start CPA = Total cost / Mobile app first starts)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### mobileAppFirstStartViews14d

**Type**: Integer

**Description**: The number of Mobile app first start conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### newSubscribeAndSave14d

**Type**: Integer

**Description**: The number of new Subscribe & Save subscriptions attributed to a view or click on an ad. This count does not include orders of the ASIN(s) from replenishment subscription orders. New subscriptions are also included in the Purchase count.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### newSubscribeAndSaveClicks14d

**Type**: Integer

**Description**: The number of Subscribe & Save subscriptions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### newSubscribeAndSaveRate14d

**Type**: Percentage

**Description**: The number of Subscribe & Save subscriptions relative to the number of impressions. (SnSSR = SnSS / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### newSubscribeAndSaveViews14d

**Type**: Integer

**Description**: The number of Subscribe & Save subscriptions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### newToBrandECPP14d

**Type**: Decimal

**Description**: The average cost to acquire a new-to-brand purchase for promoted products (New-to-brand CPP = Total cost / New-to-brand purchases). Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### newToBrandERPM14d

**Type**: Decimal

**Description**: The average revenue generated per thousand impressions by purchasing a promoted product for the first time over a one-year lookback period. (NTB eRPM BH = NTB Sales BH / (Impressions / 1000)). Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### newToBrandProductSales14d

**Type**: Decimal

**Description**: The total sales (in local currency) of promoted products purchased for the first time over a one-year lookback window by customers on Amazon. Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### newToBrandPurchaseRate14d

**Type**: Percentage

**Description**: The number of new-to-brand purchases for promoted products relative to the number of ad impressions. (New-to-brand purchase rate = New-to-brand purchases / Impressions) Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### newToBrandPurchases14d

**Type**: Integer

**Description**: The number of first-time purchases for promoted products within the brand over a one-year lookback window. Purchases include Subscribe & Save subscriptions and video rentals. Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### newToBrandPurchasesClicks14d

**Type**: Integer

**Description**: The number of new-to-brand purchases for promoted products attributed to ad click-throughs. Purchases include Subscribe & Save subscriptions and video rentals. Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### newToBrandPurchasesViews14d

**Type**: Integer

**Description**: The number of new-to-brand purchases for promoted products attributed to ad impressions. Purchases include Subscribe & Save subscriptions and video rentals. Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### newToBrandROAS14d

**Type**: Decimal

**Description**: Ad-attributed new-to-brand purchases for promoted products per local currency unit of ad spend (New-to-brand ROAS = New-to-brand sales / Total cost). Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### newToBrandUnitsSold14d

**Type**: Integer

**Description**: The quantity of promoted products purchased for the first time within the brand over a one-year lookback period after delivering an ad. A campaign can have multiple units sold in a single purchase event. Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### offAmazonClicks14d

**Type**: Integer

**Description**: The number of conversions that occured off Amazon attributed to an ad click. This includes all conversion types.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### offAmazonCPA14d

**Type**: Decimal

**Description**: The average cost to acquire an Off-Amazon conversion. (Off-Amazon CPA = Total cost / Off-Amazon)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### offAmazonCVR14d

**Type**: Percentage

**Description**: The number of Off-Amazon conversions relative to the number of ad impressions. (Off-Amazon CVR = Off-Amazon / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### offAmazonECPP14d

**Type**: Decimal

**Description**: Effective (average) cost to acquire a purchase conversion for an Off-Amazon purchase event.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### offAmazonERPM14d

**Type**: Decimal

**Description**: Effective (average) revenue for sales included in off-Amazon purchases generated per thousand impressions. (Off-Amazon sales / (Impressions / 1000))

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### offAmazonProductSales14d

**Type**: Decimal

**Description**: Sales (in local currency) for off-Amazon purchases, attributed to an ad view or click.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Conversion source](reporting/dsp/report-types#conversion-source)

#### offAmazonPurchaseRate14d

**Type**: Percentage

**Description**: Rate of ad-attributed purchase events for products included in Off-Amazon purchase events, relative to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### offAmazonPurchases14d

**Type**: Integer

**Description**: Number of times any quantity of a product was included in an Off-Amazon purchase event, attributed to an ad view or click.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source)

#### offAmazonPurchasesClicks14d

**Type**: Integer

**Description**: Number of times any quantity of a product was included in an Off-Amazon purchase event, attributed to an ad click.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source)

#### offAmazonPurchasesViews14d

**Type**: Integer

**Description**: Number of times any quantity of a product was included in an Off-Amazon purchase event, attributed to an ad view.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source)

#### offAmazonROAS14d

**Type**: Decimal

**Description**: Return on advertising spend for products included in off-Amazon purchase events, measured as ad-attributed sales per local currency unit of ad spend. (Off-Amazon product sales / Total cost)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### offAmazonUnitsSold14d

**Type**: Integer

**Description**: Units of product purchased off Amazon, attributed to an ad view or click. A single purchase event can include multiple sold units.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Conversion source](reporting/dsp/report-types#conversion-source)

#### offAmazonViews14d

**Type**: Integer

**Description**: The number of conversions that occured off Amazon attributed to an ad view. This includes all conversion types.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source)

#### parentASIN

**Type**: String

**Description**: The Amazon Standard Identification Number (ASIN) that is the parent of this ASIN.

**Report types**: [Products](reporting/dsp/report-types#products)

#### percentOfPurchasesNewToBrand14d

**Type**: Percentage

**Description**: The percent of purchases that were first-time purchases for promoted products within the brand over a one-year lookback window. Purchases include Subscribe & Save subscriptions and video rentals. Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products)

#### placementName

**Type**: String

**Description**: The name of the placement the campaign was run. Only available when "site" is a dimension.

**Report types**: [Inventory](reporting/dsp/report-types#inventory)

#### placementSize

**Type**: String

**Description**: The dimensions of the placement in pixels.  Only available when "site" is a dimension.

**Report types**: [Inventory](reporting/dsp/report-types#inventory)

#### playerTrailersClicks14d

**Type**: Integer

**Description**: The number of video trailer plays attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### playTrailerRate14d

**Type**: Percentage

**Description**: The number of video trailer plays relative to the number of impressions. (Play trailer rate = Play trailers / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### playTrailers14d

**Type**: Integer

**Description**: The number of times a video trailer was played for the featured product.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### playTrailersViews14d

**Type**: Integer

**Description**: The number of video trailer players attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### postalCode

**Type**: String

**Description**: Postal code.

**Report types**: [Geography](reporting/dsp/report-types#geography)

#### productCategory

**Type**: String

**Description**: A classification for the type of product being sold which determines its place in the Amazon retail catalog. Contains subcategories.

**Report types**: [Products](reporting/dsp/report-types#products)

#### productGroup

**Type**: String

**Description**: A distinct product grouping distinguishing products like books from watches and video games from toys. Contains categories.

**Report types**: [Products](reporting/dsp/report-types#products)

#### productName

**Type**: String

**Description**: The name of the product sold as it appears in the Amazon retail catalog.

**Report types**: [Products](reporting/dsp/report-types#products)

#### productSubcategory

**Type**: String

**Description**: A classification for the type of product being sold which determines its place in the Amazon retail catalog.

**Report types**: [Products](reporting/dsp/report-types#products)

#### pRPV14d

**Type**: Integer

**Description**: The number of views of the advertised product's customer review pages on Amazon. This includes views on either the Read All Reviews page or individual review pages.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### pRPVClicks14d

**Type**: Integer

**Description**: The number of Product review page view conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### pRPVr14d

**Type**: Percentage

**Description**: The number of product review page views relative to the number of ad impressions. (PRPVR = PRPV / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### pRPVViews14d

**Type**: Integer

**Description**: The number of product review page view conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### purchaseButton14d

**Type**: Integer

**Description**: The number of Purchase button pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### purchaseButtonClicks14d

**Type**: Integer

**Description**: The number of Purchase button pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### purchaseButtonCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Purchase button pixel conversions. (Purchase button CPA = Total cost / Purchase button)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### purchaseButtonCVR14d

**Type**: Percentage

**Description**: The number of Purchase button pixel conversions relative to the number of ad impressions. (Purchase button CVR = Purchase button / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### purchaseButtonViews14d

**Type**: Integer

**Description**: The number of Purchase button pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### purchaseRate14d

**Type**: Percentage

**Description**: The number of purchases relative to the number of impressions. (Purchase rate = Purchases / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### purchases14d

**Type**: Integer

**Description**: The number of times any amount of a promoted product or products are included in a purchase event. Purchase events include video rentals and new Subscribe & Save subscriptions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### purchasesClicks14d

**Type**: Integer

**Description**: The number of purchases attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### purchasesViews14d

**Type**: Integer

**Description**: The number of purchases attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### referral14d

**Type**: Integer

**Description**: The number of Referral pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### referralClicks14d

**Type**: Integer

**Description**: The number of Referral pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### referralCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Referral pixel conversion. (Referral CPA = Total cost / Referral)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### referralCVR14d

**Type**: Percentage

**Description**: The number of Referral pixel conversions relative to the number of ad impressions. (Referral CVR = Referral / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### referralViews14d

**Type**: Integer

**Description**: The number of Referral pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### registrationConfirmPage14d

**Type**: Integer

**Description**: The number of Registration confirm page pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### registrationConfirmPageClicks14d

**Type**: Integer

**Description**: The number of Registration confirm page pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### registrationConfirmPageCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Registration confirm page pixel conversion. (registration confirm page CPA = Total cost / Registration confirm page)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### registrationConfirmPageCVR14d

**Type**: Percentage

**Description**: The number of Registration confirm page pixel conversions relative to the number of ad impressions. (Registration confirm page CVR = Registration confirm page / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### registrationConfirmPageViews14d

**Type**: Integer

**Description**: The number of Registration confirm page pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### registrationForm14d

**Type**: Integer

**Description**: The number of Registration form pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### registrationFormClicks14d

**Type**: Integer

**Description**: The number of Registration form pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### registrationFormCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Registration form pixel conversion. (Registration form CPA = Total cost / Registration confirm page)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### registrationFormCVR14d

**Type**: Percentage

**Description**: The number of Registration form pixel conversions relative to the number of ad impressions. (Registration form CVR = Registration form / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### registrationFormViews14d

**Type**: Integer

**Description**: The number of Registration form pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### rentalRate14d

**Type**: Percentage

**Description**: The number of video rentals relative to the number of impressions. (Rental rate = Rentals / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### rentals14d

**Type**: Integer

**Description**: The number of times a video was rented for the featured product.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### rentalsClicks14d

**Type**: Integer

**Description**: The number of video rentals attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### rentalsViews14d

**Type**: Integer

**Description**: The number of video rentals attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### reportGranularity

**Type**: String

**Description**: Granularity at ASIN or Parent ASIN level.

**Report types**: [Products](reporting/dsp/report-types#products)

#### ROAS14d

**Type**: Decimal

**Description**: Ad-attributed product sales per local currency unit of ad spend. (ROAS = Product sales / Total cost)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### sales14d

**Type**: Decimal

**Description**: The total sales (in local currency) of promoted ASINs purchased by customers on Amazon after delivering an ad.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### signUp14d

**Type**: Integer

**Description**: The number of Sign-up conversions

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### signUpClicks14d

**Type**: Integer

**Description**: The number of Sign-up conversions attributed to an ad click.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### signUpCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Sign-up conversion. (Sign-up CPA = Total cost / Sign-up)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### signUpCVR14d

**Type**: Percentage

**Description**: The number of Sign-up conversions relative to the number of ad impressions. (Sign-up CVR = Sign-up/ Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### signUpPage14d

**Type**: Integer

**Description**: The number of Sign-up page conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### signUpPageClicks14d

**Type**: Integer

**Description**: The number of Sign up page conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### signUpPageCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Sign up page conversion. (Sign up page CPA = Total cost / Sign up page)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### signUpPageCVR14d

**Type**: Percentage

**Description**: The number of Sign up page conversions relative to the number of ad impressions. (Sign up page CVR = Sign up page / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### signUpPageViews14d

**Type**: Integer

**Description**: The number of Sign up page conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### signUpViews14d

**Type**: Integer

**Description**: The number of Sign-up conversions attributed to an ad view.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### storeLocatorPage14d

**Type**: Integer

**Description**: The number of Store locator page pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### storeLocatorPageClicks14d

**Type**: Integer

**Description**: The number of Store locator page pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### storeLocatorPageCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Store locator page pixel conversion. (Store locator page CPA = Total cost / Store locator page)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### storeLocatorPageCVR14d

**Type**: Percentage

**Description**: The number of Success page pixel conversions relative to the number of ad impressions. (Success page CVR = Success page / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### storeLocatorPageViews14d

**Type**: Integer

**Description**: The number of Store locator page pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### submitButton14d

**Type**: Integer

**Description**: The number of Submit button pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### submitButtonClicks14d

**Type**: Integer

**Description**: The number of Submit button pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### submitButtonCPA14d

**Type**: Decimal

**Description**: The average cost to acquire Submit button pixel conversions. (Submit button CPA = Total cost / Submit button)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### submitButtonCVR14d

**Type**: Percentage

**Description**: The number of Submit button pixel conversions relative to the number of ad impressions. (Submit button CVR = Submit button / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### submitButtonViews14d

**Type**: Integer

**Description**: The number of Submit button pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### subscribe14d

**Type**: Integer

**Description**: The number of Subscribe conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### subscribeClicks14d

**Type**: Integer

**Description**: The number of Subscribe conversions attributed to an ad click.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### subscribeCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Subscribe conversion. (Subscribe CPA = Total cost / Subscribe)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### subscribeCVR14d

**Type**: Percentage

**Description**: The number of Subscribe conversions relative to the number of ad impressions. (Subscribe CVR = Subscribe / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### subscribeViews14d

**Type**: Integer

**Description**: The number of Subscribe conversions attributed to an ad view.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### subscriptionButton14d

**Type**: Integer

**Description**: The number of Subscription button pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### subscriptionButtonClicks14d

**Type**: Integer

**Description**: The number of Subscription button pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### subscriptionButtonCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Subscription button pixel conversion. (Subscribe CPA = Total cost / Subscribe)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### subscriptionButtonCVR14d

**Type**: Percentage

**Description**: The number of Subscription button pixel conversions relative to the number of ad impressions. (Subscription button CVR = Subscription button / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### subscriptionButtonViews14d

**Type**: Integer

**Description**: The number of Subscription button pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### successPage14d

**Type**: Integer

**Description**: The number of Success page pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### successPageClicks14d

**Type**: Integer

**Description**: The number of Subscription button pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### successPageCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Success page pixel conversions. (Success page CPA = Total cost / Success page)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### successPageCVR14d

**Type**: Percentage

**Description**: The number of Success page pixel conversions relative to the number of ad impressions. (Success page CVR = Success page / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### successPageViews14d

**Type**: Integer

**Description**: The number of Success page pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### supplyCost

**Type**: Decimal

**Description**: The total amount of money spent on media supply.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### surveyFinish14d

**Type**: Integer

**Description**: The number of Survey finish pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### surveyFinishClicks14d

**Type**: Integer

**Description**: The number of Survey finish pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### surveyFinishCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Survey finish pixel conversion. (Survey finish CPA = Total cost / Survey end)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### surveyFinishCVR14d

**Type**: Percentage

**Description**: The number of Survey finish pixel conversions relative to the number of ad impressions. (Survey finish CVR = Survey end / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### surveyFinishViews14d

**Type**: Integer

**Description**: The number of Survey finish pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### surveyStart14d

**Type**: Integer

**Description**: The number of Survey start pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### surveyStartClicks14d

**Type**: Integer

**Description**: The number of Survey start pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### surveyStartCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Survey start pixel conversion. (Survey start CPA = Total cost / Survey start)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### surveyStartCVR14d

**Type**: Percentage

**Description**: The number of Survey start pixel conversions relative to the number of ad impressions. (Survey start CVR = Survey start / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### surveyStartViews14d

**Type**: Integer

**Description**: The number of Survey start pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### targetingMethod

**Type**: String

**Description**: The targeting settings applied to the campaign. Examples include segments, content categories, and untargeted if no settings were applied.

**Report types**: [Audience](reporting/dsp/report-types#audience)

#### thankYouPage14d

**Type**: Integer

**Description**: The number of Thank you page pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### thankYouPageClicks14d

**Type**: Integer

**Description**: The number of Thank you page pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### thankYouPageCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Thank you page pixel conversions. (Thank you page CPA = Total cost / Thank you page)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### thankYouPageCVR14d

**Type**: Percentage

**Description**: The number of Thank you page pixel conversions relative to the number of ad impressions. (Thank you page CVR = Thank you page / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### thankYouPageViews14d

**Type**: Integer

**Description**: The number of Thank you page pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### totalAddToCart14d

**Type**: Integer

**Description**: The number of times an ASIN is added to a customer's cart. This counts adds for promoted products as well as products from the same brands as the products tracked in the order. (Total ATC = ATC + ATC BH)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products)

#### totalAddToCartClicks14d

**Type**: Integer

**Description**: The number of Total Add to Carts attributed to ad click-throughs. This includes adds for promoted products as well as products from the same brands as the products tracked in the order. (Total ATC clicks = ATC clicks + ATC clicks BH)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products)

#### totalAddToCartCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Total Add to Cart conversion. This includes the cost to acquire a Total Add to Cart conversion for promoted products as well as products from the same brands as the products tracked in the order. (Total eCPATC = Total cost / Total ATC)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalAddToCartCVR14d

**Type**: Percentage

**Description**: The number of Total Add to Cart conversions relative to the number of ad impressions. (Total ATC = Total ATC / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalAddToCartViews14d

**Type**: Integer

**Description**: The number of Total Add to Carts attributed to impressions. This includes adds for promoted products as well as products from the same brands as the products tracked in the order. (Total ATC views = ATC views + ATC views BH)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products)

#### totalAddToList14d

**Type**: Integer

**Description**: The number of times a promoted ASIN is added to a customer's wish list, gift list or registry. This counts adds for promoted products as well as products from the same brands as the products tracked in the order. (Total ATL = ATL + ATL BH)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products)

#### totalAddToListClicks14d

**Type**: Integer

**Description**: The number of Total Add to Lists attributed to ad click-throughs. This includes adds for promoted products as well as products from the same brands as the products tracked in the order. (Total ATL clicks = ATL clicks + ATL clicks BH)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products)

#### totalAddToListCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Total Add to List conversion. This includes the cost to acquire a Total Add to List conversion for promoted products as well as products from the same brands as the products tracked in the order. (Total eCPATL = Total cost / Total ATL)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalAddToListCVR14d

**Type**: Percentage

**Description**: The number of Total Add to List conversions relative to the number of ad impressions. (Total ATL = Total ATL / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalAddToListViews14d

**Type**: Integer

**Description**: The number of Total Add to Lists attributed to impressions. This includes adds for promoted products as well as products from the same brands as the products tracked in the order. (Total ATL views = ATL views + ATL views BH)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products)

#### totalCost

**Type**: Decimal

**Description**: The total amount of money spent on running the campaign not including 3P fees paid by the agency.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### totalDetailPageClicks14d

**Type**: Integer

**Description**: The number of Total detail page view conversions attributed to ad click-throughs. This includes click-throughs for promoted products as well as products from the same brands as the products tracked in the order. (Total DPV clicks = DPV clicks + DPV clicks BH)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products)

#### totalDetailPageViews14d

**Type**: Integer

**Description**: The number of ad-attributed detail page views on Amazon. This includes views for promoted products as well as products from the same brands as the products tracked in the order. (Total DPV = DPV + DPV BH)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products)

#### totalDetailPageViewsCVR14d

**Type**: Percentage

**Description**: The number of Total detail page view conversions relative to the number of ad impressions. (Total DPVR = Total DPV / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalDetailPageViewViews14d

**Type**: Integer

**Description**: The number of Total detail page view conversions attributed to impressions. This includes views for promoted products as well as products from the same brands as the products tracked in the order. (Total DPV views = DPV views + DPV views BH)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products)

#### totalDetaiPageViewCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Total detail page view conversion. This includes the cost to acquire a Detail page view conversion for promoted products as well as products from the same brands as the products tracked in the order. (Total eCPDPV = Total cost / Total DPV)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalECPP14d

**Type**: Decimal

**Description**: The average cost to acquire a Total purchase conversion. This includes the cost to acquire a Total purchase for promoted products as well as products from the same brands as the products tracked in the order. (Total eCPP = Total cost / Total purchases)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalECPPRPV14d

**Type**: Decimal

**Description**: The average cost to acquire a Total product review page view conversion. This includes the cost to acquire a product review page view conversion for promoted products as well as products from the same brands as the products tracked in the order. (Total eCPPRPV = Total cost / Total PRPV)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalERPM14d

**Type**: Decimal

**Description**: The average revenue generated per thousand impressions. This includes revenue for promoted products tracked in orders and products from the same brands as the products tracked in orders. (Total eRPM = Total sales / (Impressions / 1000))

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalFee

**Type**: Decimal

**Description**: The sum of all fees.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalNewToBrandECPP14d

**Type**: Decimal

**Description**: The average cost to acquire a new-to-brand purchase for both promoted and brand halo products (Total new-to-brand eCPP = Total cost / Total new-to-brand purchases). Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalNewToBrandERPM14d

**Type**: Decimal

**Description**: The average revenue generated per thousand impressions by purchasing a promoted or brand halo product for the first time over a one-year lookback period. (Total NTB eRPM = NTB Sales / (Impressions / 1000)). Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalNewToBrandProductSales14d

**Type**: Decimal

**Description**: The total sales (in local currency) of promoted and brand halo products purchased for the first time over a one-year lookback window by customers on Amazon. Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalNewToBrandPurchaseRate14d

**Type**: Percentage

**Description**: The number of new-to-brand purchases for both promoted and brand halo products relative to the number of ad impressions. (Total new-to-brand purchase rate = Total new-to-brand purchases / Impressions). Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalNewToBrandPurchases14d

**Type**: Integer

**Description**: The number of first-time purchases for promoted or brand halo products within the brand over a one-year lookback window. Purchases include Subscribe & Save subscriptions and video rentals. Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products)

#### totalNewToBrandPurchasesClicks14d

**Type**: Integer

**Description**: The number of new-to-brand purchases for promoted and brand halo products attributed to ad click-throughs. Purchases include Subscribe & Save subscriptions and video rentals. Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products)

#### totalNewToBrandPurchasesViews14d

**Type**: Integer

**Description**: The number of new-to-brand purchases for promoted and brand halo products attributed to ad impressions. Purchases include Subscribe & Save subscriptions and video rentals. Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products)

#### totalNewToBrandROAS14d

**Type**: Decimal

**Description**: Ad-attributed new-to-brand purchases per local currency unit of ad spend (Total new-to-brand ROAS = Total new-to-brand sales / Total cost). This includes purchases for both promoted and brand halo products. Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalNewToBrandUnitsSold14d

**Type**: Integer

**Description**: The quantity of promoted and brand halo products purchased for the first time within the brand over a one-year lookback period after delivering an ad. A campaign can have multiple units sold in a single purchase event. Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalPercentOfPurchasesNewToBrand14d

**Type**: Percentage

**Description**: The percent of purchases that were first-time purchases for promoted and brand halo products within the brand over a one-year lookback window. Purchases include Subscribe & Save subscriptions and video rentals. Note: New-to-brand data is available from November 1st, 2018. Selecting a date range prior to November 1st will result in incomplete or inaccurate new-to-brand reporting.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products)

#### totalPRPV14d

**Type**: Integer

**Description**: The number of ad-attributed product review page views on Amazon. This includes views on either the Read All Reviews page or individual review pages. This counts views for promoted products as well as products from the same brands as the products tracked in the order. (Total PRPV = PRPV + PRPV BH)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products)

#### totalPRPVClicks14d

**Type**: Integer

**Description**: The number of Total product review page view conversions attributed to ad click-throughs. This includes click-throughs for promoted products as well as products from the same brands as the products tracked in the order. (Total PRPV clicks = PRPV clicks + PRPV clicks BH)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products)

#### totalPRPVr14d

**Type**: Percentage

**Description**: Rate of product review page visits for promoted products, relative to the number of ad impressions. (ATCR = ATC / Impressions) Use Total PRPVR to see all conversions for the brands' products.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalPRPVViews14d

**Type**: Integer

**Description**: The number of Total product review page view conversions attributed to impressions. This includes views for promoted products as well as products from the same brands as the products tracked in the order. (Total PRPV views = PRPV views + PRPV views BH)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products)

#### totalPurchaseRate14d

**Type**: Percentage

**Description**: The number of Total purchase conversions relative to the number of ad impressions. (Total purchase rate = Total purchases / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalPurchases14d

**Type**: Integer

**Description**: The number of times any number of products are included in a single purchase event. Purchase events include Subscribe & Save subscriptions and video rentals. This counts purchases for promoted products as well as products from the same brands as the products tracked in the order. (Total purchases = Purchases + Purchases BH)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products), [Conversion source](reporting/dsp/report-types#conversion-source)

#### totalPurchasesClicks14d

**Type**: Integer

**Description**: The number of Total purchases attributed to ad click-throughs. This includes click-throughs for promoted products as well as products from the same brands as the products tracked in the order. (Total purchases clicks = Purchases clicks + Purchases clicks BH)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products), [Conversion source](reporting/dsp/report-types#conversion-source)

#### totalPurchasesViews14d

**Type**: Integer

**Description**: The number of Total purchases attributed to impressions. This includes purchases for promoted products as well as products from the same brands as the products tracked in the order. (Total purchases views = Purchases views + Purchases views BH)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products), [Conversion source](reporting/dsp/report-types#conversion-source)

#### totalROAS14d

**Type**: Decimal

**Description**: Ad-attributed product sales per local currency unit of ad spend. Includes both sales of promoted products tracked in orders and products from the same brands as the products tracked. (Total ROAS = Total Sales / Total cost)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalSales14d

**Type**: Decimal

**Description**: The total sales (in local currency) of promoted ASINs and ASINs from the same brands as promoted ASINs purchased by customers on Amazon after delivering an ad.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products), [Conversion source](reporting/dsp/report-types#conversion-source)

#### totalSubscribeAndSaveSubscriptionClicks14d

**Type**: Integer

**Description**: The number of Total Subscribe & Save subscriptions attributed to ad click-throughs. This includes subscriptions for promoted products as well as products from the same brands as the products tracked in the order. (Total SnSS clicks = SnSS clicks + SnSS clicks BH)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products)

#### totalSubscribeAndSaveSubscriptionCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Total Subscribe & Save subscriptions conversion. This includes the cost to acquire a Total Subscribe & Save subscriptions for promoted products as well as products from the same brands as the products tracked in the order. (Total eCPSnSS = Total cost / Total SnSS)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalSubscribeAndSaveSubscriptionCVR14d

**Type**: Percentage

**Description**: Rate of ad-attributed Subscribe & Save conversions for the brands’ products relative to ad impressions. (Total SnSSR = Total SnSS / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### totalSubscribeAndSaveSubscriptions14d

**Type**: Integer

**Description**: Number of new Subscribe & Save subscriptions for the brands’ products, attributed to an ad view or click. This does not include replenishment subscription orders.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products)

#### totalSubscribeAndSaveSubscriptionViews14d

**Type**: Integer

**Description**: The number of Total Subscribe & Save subscriptions attributed to impressions. This includes subscriptions for promoted products as well as products from the same brands as the products tracked in the order. (Total SnSS views = SnSS views + SnSS views BH)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products)

#### totalUnitsSold14d

**Type**: Integer

**Description**: The total quantity of promoted products and products from the same brand as promoted products purchased by customers on Amazon after delivering an ad. A campaign can have multiple units sold in a single purchase event.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products), [Conversion source](reporting/dsp/report-types#conversion-source)

#### unitsSold14d

**Type**: Integer

**Description**: The total quantity of promoted products purchased by customers on Amazon after delivering an ad. A campaign can have multiple units sold in a single purchase event.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Products](reporting/dsp/report-types#products), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoComplete

**Type**: Integer

**Description**: The number of times a video ad played to completion. If rewind occurred, completion was calculated on the total percentage of unduplicated video viewed.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoCompleted

**Type**: Integer

**Description**: The number of video completed pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoCompletedCPA

**Type**: Decimal

**Description**: The average cost to acquire a Video completed pixel conversion. (Video completed CPA = Total cost / Video completed)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### videoCompletedCVR

**Type**: Percentage

**Description**: The number of video completed pixel conversions relative to the number of ad impressions. (Video completed CVR = Video end / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### videoCompletedViews

**Type**: Integer

**Description**: The number of video completed pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoCompletionRate

**Type**: Double

**Description**: The number of video completions relative to the number of video starts. (Video completion rate = Video complete / Video start)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### videoDownloadRate14d

**Type**: Percentage

**Description**: The number of video downloads relative to the number of impressions. (Video download rate = Video downloads / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### videoDownloads14d

**Type**: Integer

**Description**: The number of times a video was downloaded for the featured product.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoDownloadsClicks14d

**Type**: Integer

**Description**: The number of video downloads attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoDownloadsViews14d

**Type**: Integer

**Description**: The number of video downloads attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoEndClicks

**Type**: Integer

**Description**: The number of video completed pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoFirstQuartile

**Type**: Integer

**Description**: The number of times at least 25% of a video ad played. If rewind occurred, percent complete was calculated on the total percentage of unduplicated video viewed.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoMidpoint

**Type**: Integer

**Description**: The number of times at least 50% of a video ad played. If rewind occurred, percent complete was calculated on the total percentage of unduplicated video viewed.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoMute

**Type**: Integer

**Description**: The number of times a user muted the video ad.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoPause

**Type**: Integer

**Description**: The number of times a user paused the video ad.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoResume

**Type**: Integer

**Description**: The number of times a user unpaused the video ad.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoStart

**Type**: Integer

**Description**: The number of times a video ad was started.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoStarted

**Type**: Integer

**Description**: The number of video started pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoStartedClicks

**Type**: Integer

**Description**: The number of video started pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoStartedCPA

**Type**: Decimal

**Description**: The average cost to acquire a video started pixel conversion. (Video started CPA = Total cost / Video started)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### videoStartedCVR

**Type**: Percentage

**Description**: The number of video started pixel conversions relative to the number of ad impressions. (Video started CVR = Video started / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### videoStartedViews

**Type**: Integer

**Description**: The number of video started pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoStreams14d

**Type**: Integer

**Description**: The number of times a video was streamed (played without downloading).

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoStreamsClicks14d

**Type**: Integer

**Description**: The number of video streams attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoStreamsRate14d

**Type**: Double

**Description**: The number of video streams relative to the number of impressions. (Video stream rate = Video streams / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### videoStreamsViews14d

**Type**: Integer

**Description**: The number of video streams attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoThirdQuartile

**Type**: Integer

**Description**: The number of times at least 75% of a video ad played. If rewind occurred, percent complete was calculated on the total percentage of unduplicated video viewed.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### videoUnmute

**Type**: Integer

**Description**: The number of times a user unmuted the video ad.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### viewabilityRate

**Type**: Percentage

**Description**: Viewable impressions / measurable impressions. The viewability metrics are available from October 1, 2019. Selecting a date range prior to October 1, 2019 will result in incomplete or inaccurate metrics.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory)

#### viewableImpressions

**Type**: Integer

**Description**: Number of impressions that met the Media Ratings Council (MRC) viewability standard. See viewability details at https://advertising.amazon.com/library/guides/viewability.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### widgetInteraction14d

**Type**: Integer

**Description**: The number of Widget interaction pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### widgetInteractionClicks14d

**Type**: Integer

**Description**: The number of Widget interaction pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### widgetInteractionCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Widget interaction pixel conversions. (Widget interaction CPA = Total cost / Widget interaction)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### widgetInteractionCVR14d

**Type**: Percentage

**Description**: The number of Widget interaction pixel conversions relative to the number of ad impressions. (Widget interaction CVR = Widget interaction / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### widgetInteractionViews14d

**Type**: Integer

**Description**: The number of Widget interaction pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### widgetLoad14d

**Type**: Integer

**Description**: The number of Widget load pixel conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### widgetLoadClicks14d

**Type**: Integer

**Description**: The number of Widget load pixel conversions attributed to ad click-throughs.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### widgetLoadCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Widget load pixel conversion. (Widget load CPA = Total cost / Widget load)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### widgetLoadCVR14d

**Type**: Percentage

**Description**: The number of Widget load pixel conversions relative to the number of ad impressions. (Widget load CVR = Widget load / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience)

#### widgetLoadViews14d

**Type**: Integer

**Description**: The number of Widget load pixel conversions attributed to ad impressions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography)

#### checkout14d

**Type**: Integer

**Description**: The number of Checkout conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### checkoutClicks14d

**Type**: Integer

**Description**: The number of Checkout conversions attributed to an ad click.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### checkoutCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Checkout conversion. (Checkout CPA = Total cost / Checkout)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### checkoutCVR14d

**Type**: Percentage

**Description**: The number of Checkout conversions relative to the number of ad impressions. (Checkout CVR = Checkout / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### checkoutViews14d

**Type**: Integer

**Description**: The number of Checkout conversions attributed to an ad view.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### contact14d

**Type**: Integer

**Description**: The number of Contact conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### contactClicks14d

**Type**: Integer

**Description**: The number of Contact conversions attributed to an ad click.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### contactCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Contact conversion. (Contact CPA = Total cost / Contact)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### contactCVR14d

**Type**: Percentage

**Description**: The number of Contact conversions relative to the number of ad impressions. (Contact CVR = Contact / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### contactViews14d

**Type**: Integer

**Description**: The number of Contact conversions attributed to an ad view.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### lead14d

**Type**: Integer

**Description**: The number of Lead conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### leadClicks14d

**Type**: Integer

**Description**: The number of Lead conversions attributed to an ad click.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### leadCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Lead conversion. (Lead CPA = Total cost / Lead)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### leadCVR14d

**Type**: Percentage

**Description**: The number of Lead conversions relative to the number of ad impressions. (Lead CVR = Lead / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### leadViews14d

**Type**: Integer

**Description**: The number of Lead conversions attributed to an ad view.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### other14d

**Type**: Integer

**Description**: The number of Other conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### otherClicks14d

**Type**: Integer

**Description**: The number of Other conversions attributed to an ad click.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### otherCPA14d

**Type**: Decimal

**Description**: The average cost to acquire an Other conversion. (Other CPA = Total cost / Other)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### otherCVR14d

**Type**: Percentage

**Description**: The number of Other conversions relative to the number of ad impressions. (Other CVR = Other / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### otherViews14d

**Type**: Integer

**Description**: The number of Other conversions attributed to an ad view.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### pageView14d

**Type**: Integer

**Description**: The number of Page view conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### pageViewClicks14d

**Type**: Integer

**Description**: The number of Page view conversions attributed to an ad click.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### pageViewCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Page view conversion. (Page view CPA = Total cost / Page view)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### pageViewCVR14d

**Type**: Percentage

**Description**: The number of Page view conversions relative to the number of ad impressions. (Page view CVR = Page view / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### pageViewViews14d

**Type**: Integer

**Description**: The number of Page view conversions attributed to an ad view.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### search14d

**Type**: Integer

**Description**: The number of Search conversions.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### searchClicks14d

**Type**: Integer

**Description**: The number of Search conversions attributed to an ad click.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### searchCPA14d

**Type**: Decimal

**Description**: The average cost to acquire a Search conversion. (Search CPA = Total cost / Search)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### searchCVR14d

**Type**: Percentage

**Description**: The number of Search conversions relative to the number of ad impressions. (Search CVR = Search / Impressions)

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

#### searchViews14d

**Type**: Integer

**Description**: The number of Search conversions attributed to an ad view.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Geography](reporting/dsp/report-types#geography), [Technology](reporting/dsp/report-types#technology)

## Dimensional metrics 

The DSP reporting API includes dimensional metrics automatically based on your selected report type and dimensions. If you include dimensional metrics in your request body, you will receive an error.

#### browser

**Type**: String

**Description**: The web browsers the customers used to view the ad.

**Report types**: [Technology](reporting/dsp/report-types#technology)

#### browserVersion

**Type**: String

**Description**: The specific browser versions the customers used to view the ad.

**Report types**: [Technology](reporting/dsp/report-types#technology)

#### city

**Type**: String

**Description**: City.

**Report types**: [Geography](reporting/dsp/report-types#geography)

#### conversionSourceAttributionType

**Type**: String

**Description**: Indicates whether the attribution method is owned by Amazon or a third party.

**Report types**: [Conversion source](reporting/dsp/report-types#conversion-source)

#### conversionSourceName

**Type**: String

**Description**: The name of the conversion source (Amazon or a third party).

**Report types**: [Conversion source](reporting/dsp/report-types#conversion-source)

#### conversionSourceOwner

**Type**: String

**Description**: Indicates whether the conversion source is Amazon or an Amazon partner.

**Report types**: [Conversion source](reporting/dsp/report-types#conversion-source)

#### conversionSourceScaled

**Type**: Boolean

**Description**: Indicates whether the data has been scaled from a smaller sample to represent a broader market measurement.

**Report types**: [Conversion source](reporting/dsp/report-types#conversion-source)

#### country

**Type**: String

**Description**: Country.

**Report types**: [Geography](reporting/dsp/report-types#geography)

#### creativeAdId

**Type**: Integer

**Description**: A unique identifier assigned to a creative. When this is different from creativeId, creativeAdId is the value that can be used in our /dsp/creatives endpoints.

**Report types**: [Campaign](reporting/dsp/report-types#campaign)

#### creativeID

**Type**: Integer

**Description**: A unique identifier assigned to a creative. When this is different from creativeAdId, creativeId cannot be used in our /dsp/creatives endpoints.

**Report types**: [Campaign](reporting/dsp/report-types#campaign)

#### creativeName

**Type**: String

**Description**: Creative name.

**Report types**: [Campaign](reporting/dsp/report-types#campaign)

#### creativeSize

**Type**: String

**Description**: The dimensions of the creative in pixels.

**Report types**: [Campaign](reporting/dsp/report-types#campaign)

#### creativeType

**Type**: String

**Description**: The type of creative (for example static image, third party, or video).

**Report types**: [Campaign](reporting/dsp/report-types#campaign)

#### deal

**Type**: String

**Description**: The name of a deal.

**Report types**: [Inventory](reporting/dsp/report-types#inventory)

#### dealID

**Type**: Integer

**Description**: The unique identifier for the deal.

**Report types**: [Inventory](reporting/dsp/report-types#inventory)

#### designatedMarketAreaCode

**Type**: String

**Description**: The Designated Market Areas® (DMA) the customers were in when they viewed the ad. DMA regions are created and defined by Nielsen.

**Report types**: [Geography](reporting/dsp/report-types#geography)

#### designatedMarketAreaName

**Type**: String

**Description**: The Designated Market Areas® (DMA) the customers were in when they viewed the ad. DMA regions are created and defined by Nielsen.

**Report types**: [Geography](reporting/dsp/report-types#geography)

#### device

**Type**: String

**Description**: The devices the customers used to view the ad.

**Report types**: [Technology](reporting/dsp/report-types#technology)

#### environmentType

**Type**: String

**Description**: Break the data down by which environment the bid request came from: web or app. This dimension is connected to web and app inventory, not line item types.

**Report types**: [Technology](reporting/dsp/report-types#technology)

#### lineItemBudget

**Type**: Decimal

**Description**: The total amount of money that be consumed by a line item.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source), [Products](reporting/dsp/report-types#products)

#### lineItemEndDate

**Type**: Integer

**Description**: The last day a line item is eligible to run.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source), [Products](reporting/dsp/report-types#products)

#### lineItemExternalId

**Type**: String

**Description**: Line item custom ID optionally entered by user.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source), [Products](reporting/dsp/report-types#products)

#### lineItemId

**Type**: Integer

**Description**: A unique identifier assigned to a line item.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source), [Products](reporting/dsp/report-types#products)

#### lineItemName

**Type**: String

**Description**: Line item name.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source), [Products](reporting/dsp/report-types#products)

#### lineItemStartDate

**Type**: Integer

**Description**: The first day a line item is eligible to run.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source), [Products](reporting/dsp/report-types#products)

#### lineitemtype

**Type**: String

**Description**: The type of line item (for example standard display, Mobile app, or Facebook).

**Report types**: [Audience](reporting/dsp/report-types#audience)

#### maxExpectedLatencyHours

**Type**: Integer

**Description**: Indicates how many hours after the event the data provider may upload conversion data.

**Report types**: [Conversion source](reporting/dsp/report-types#conversion-source)

#### operatingSystem

**Type**: String

**Description**: The operating systems the customers used to view the ad.

**Report types**: [Technology](reporting/dsp/report-types#technology)

#### orderBudget

**Type**: Decimal

**Description**: The total amount of money that be consumed by an order.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source), [Products](reporting/dsp/report-types#products)

#### orderCurrency

**Type**: String

**Description**: Order currency.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source), [Products](reporting/dsp/report-types#products)

#### orderEndDate

**Type**: Integer

**Description**: The last day a line item within an order is eligible to run.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source), [Products](reporting/dsp/report-types#products)

#### orderId

**Type**: Integer

**Description**: A unique identifier assigned to an order.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source), [Products](reporting/dsp/report-types#products)

#### orderExternalId

**Type**: String

**Description**: A user defined identifier for an order or line item.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source), [Products](reporting/dsp/report-types#products)

#### orderName

**Type**: String

**Description**: Order name.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source), [Products](reporting/dsp/report-types#products)

#### orderStartDate

**Type**: Integer

**Description**: The first day a line item within an order is eligible to run.

**Report types**: [Campaign](reporting/dsp/report-types#campaign), [Inventory](reporting/dsp/report-types#inventory), [Audience](reporting/dsp/report-types#audience), [Technology](reporting/dsp/report-types#technology), [Geography](reporting/dsp/report-types#geography), [Conversion source](reporting/dsp/report-types#conversion-source), [Products](reporting/dsp/report-types#products)

#### region

**Type**: String

**Description**: State or region.

**Report types**: [Geography](reporting/dsp/report-types#geography)

#### segment

**Type**: String

**Description**: The name of the audience segment.

**Report types**: [Audience](reporting/dsp/report-types#audience)

#### siteName

**Type**: String

**Description**: The site or group of sites the campaign ran on.

**Report types**: [Inventory](reporting/dsp/report-types#inventory)

#### supplySourceName

**Type**: String

**Description**: The inventory the campaign ran on, for example real-time bidding exchanges or Amazon-owned sites.

**Report types**: [Inventory](reporting/dsp/report-types#inventory)
