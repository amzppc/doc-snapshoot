---
title: Sponsored Products overview
description: Overview of managing Sponsored Products campaigns using the Amazon Ads API
type: guide
interface: api 
tags:
    - Sponsored Products
    - Campaign management
keywords:
    - Overview
    - Reporting
---

# Sponsored Products overview

Sponsored Products campaigns allow you to advertise your products in high-visibility placements on Amazon.com. Sponsored Products are available for vendors, sellers, and Kindle Direct Publishing authors. 

[Learn more about how Sponsored Products can help you meet your business objectives.](https://advertising.amazon.com/help?#GJUCNANNV3GQVXJZ)

You can use the Amazon Ads API to create and manage Sponsored Products campaigns, as well as retrieve campaign performance data. 

## Campaign management

>[NOTE] The latest version of the campaign management endpoints is **version 3**. All reference documentation refers to the latest version. For more information on upgrading, see the version 2 to version 3 [migration guide](reference/migration-guides/sp-v2-v3).

To learn more about creating and managing Sponsored Products campaigns using the API, see:

* [Campaign structure](guides/sponsored-products/get-started/campaign-structure)
* [Creating a manual targeting campaign](guides/sponsored-products/get-started/manual-campaigns)
* [Creating an auto targeting campaign](guides/sponsored-products/get-started/auto-campaigns)
* [Entity limits](https://advertising.amazon.com/help?#G86H2227323T8T4Q)
* [Full API reference](sponsored-products/3-0/openapi/prod)

## Reporting

The Ads API reporting functionality provides a variety of reports to help you retrieve historical impression, click, cost, and conversion data for your Sponsored Products campaigns. 

Learn more:

* [Report types](guides/reporting/v3/report-types)
* [Getting started with reports](guides/reporting/v3/get-started)
* [Full API reference](offline-report-prod-3p)

You can also access near real-time Sponsored Products traffic and conversion data using [Amazon Marketing Stream](guides/amazon-marketing-stream/overview). 

>[TIP] Try out Sponsored Products campaign management and reporting endpoints using the Amazon Ads API [Postman collection](https://github.com/amzn/ads-advanced-tools-docs/tree/main/postman).


