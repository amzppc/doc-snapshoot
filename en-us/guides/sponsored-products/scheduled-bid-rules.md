---
title: Scheduled bid rules
description: Amazon Ads API version 2 Sponsored Products scheduled bid rules
type: guide
interface: api
tags:
    - Scheduled bid rules
    - Optimization rules
    - rules
---

# Schedule Bid Rules

Schedule bid rules are rules that advertisers can set up to increase bids at specific times of the day, days of the week, and date ranges. If a rule meets the conditions set, the bid is increased. If a rule doesn’t meet the conditions set, there is no increase applied. 

For example, consider a campaign “Running Shoes for Q4,” which has the following rules set: 

1. Daily rule- Increase bids by 10% with no end date
2. Black Friday rule- Increase bids by 20% on November 27th.
3. 4 hour rule- Increase bids by 20% between 5:00PM and 9:00PM with no end date. 

On November 27th, we’ll implement a 30% daily bid increase for that day. This includes the 10% increase from the daily rule whose conditions were met, and 20% increase from the Black Friday rule. The campaign will get an additional 20% increase from the 4 hour rule increasing the bid total to 50%. After 9:00PM,  the daily bid increase will be back to 30%.

>[NOTE] Whenever a new schedule bid rule is created or an existing rule is updated, the changes will take effect within 24 hours. A maximum of 20 schedule bid rules can be specified for a campaign. Schedule bid rules are applied on top of ‘Placement adjustment’ and ‘Bidding strategies’.


## Examples

**Scenario 1:** You want to increase the budget from 5:00AM to 1:00PM on a daily basis with no end date.

**Solution:** Call the [POST /sp/rules/optimization](sponsored-products/3-0/openapi/prod#tag/Optimization-Rules/operation/CreateOptimizationRules) endpoint with `recurrence` set to `DAILY` and `timesOfDay` set with start time of 5:00 and end time of 13:00.


```json
curl --location --request POST 'https://advertising-api.amazon.com/sp/rules/optimization' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxx' \
--header 'Authorization: Bearer Atza|xxxxxxx' \
--header 'Amazon-Advertising-API-Scope;' \
--header 'Content-Type: application/json' \
--data-raw '{  
    "optimizationRules": 
    [
        { "ruleName": "increase_bids_by_15%_on_mornings",      
          "ruleCategory": "BID",      
          "ruleSubCategory": "SCHEDULE",      
          "recurrence": 
            {"type": "DAILY",        
             "timesOfDay": 
             [          
                {"startTime": "05:00",            
                 "endTime": "13:00"}
             ],        
             "duration": 
                {"startTime": "2023-08-01T00:00:00Z"}
             },      
          "action": 
                {"actionType": "ADOPT",        
                 "actionDetails": 
                    {"actionOperator": "INCREMENT",          
                     "value": 15,          
                     "actionUnit": "PERCENT"}
                 },      
          "status": "ENABLED"}
    ]
}'
```


**Scenario 2:** You want to increase the budget for the whole day, only on weekends, with no end date. 

**Solution:** Call the [POST /sp/rules/optimization](sponsored-products/3-0/openapi/prod#tag/Optimization-Rules/operation/CreateOptimizationRules) endpoint with `recurrence` set to `WEEKLY` and `daysOfWeek` set to `SATURDAY` and `SUNDAY`, with no filter on times of day.


```json
curl --location --request POST 'https://advertising-api.amazon.com/sp/rules/optimization' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxx' \
--header 'Authorization: Bearer Atza|xxxxxxx' \
--header 'Amazon-Advertising-API-Scope;' \
--header 'Content-Type: application/json' \
--data-raw '{  
    "optimizationRules": 
    [
        { "ruleName": "increase_bids_by_20%_on_weekends",      
          "ruleCategory": "BID",      
          "ruleSubCategory": "SCHEDULE",      
          "recurrence": 
            {"type": "WEEKLY", 
             "daysOfWeek":
             [
                "SATURDAY",
                "SUNDAY"
             ],
             "duration": 
                {"startTime": "2023-08-01T00:00:00Z"}
             },      
          "action": 
                {"actionType": "ADOPT",        
                 "actionDetails": 
                    {"actionOperator": "INCREMENT",          
                     "value": 20,          
                     "actionUnit": "PERCENT"}
                 },      
          "status": "ENABLED"}
    ]
}'
```

**Scenario 3:** You want to increase the budget from 10:00PM to 11:00PM, only on Wednesday, but with the rule ending in a year. 

**Solution:** Call the [POST /sp/rules/optimization](sponsored-products/3-0/openapi/prod#tag/Optimization-Rules/operation/CreateOptimizationRules) endpoint with `recurrence` set to `weekly`, `daysOfWeek` set to `WEDNESDAY`, `timesOfDay` set with start time of 22:00 and end time of 23:00, and an duration end time of 08/01/2024.


```json
curl --location --request POST 'https://advertising-api.amazon.com/sp/rules/optimization' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxx' \
--header 'Authorization: Bearer Atza|xxxxxxx' \
--header 'Amazon-Advertising-API-Scope;' \
--header 'Content-Type: application/json' \
--data-raw '{  
    "optimizationRules": 
    [
        { "ruleName": "increase_bids_by_30%_on_wednesday_night",      
          "ruleCategory": "BID",      
          "ruleSubCategory": "SCHEDULE",      
          "recurrence": 
            {"type": "WEEKLY", 
             "daysOfWeek":
             [
                "WEDNESDAY"
             ],
             "timesOfDay": 
             [          
                {"startTime": "22:00",            
                 "endTime": "23:00"}
             ], 
             "duration": 
                {"startTime": "2023-08-01T00:00:00Z",
                 "endTime": "2024-08-01T00:00:00Z"}
             },      
          "action": 
                {"actionType": "ADOPT",        
                "actionDetails": 
                    {"actionOperator": "INCREMENT",          
                     "value": 30,          
                     "actionUnit": "PERCENT"}
                 },      
          "status": "ENABLED"}
    ]
}'
```


**Scenario 4:** You want to change the already-created above rule in scenario 3 to only modulate bids by 15% instead of 30%.

**Solution:** Call the [PUT /sp/rules/optimization](sponsored-products/3-0/openapi/prod#tag/Optimization-Rules/operation/UpdateOptimizationRules) endpoint with the same fields as the scenario 3 rule, but change the `action.actionDetails.value`to 15.


```json
curl --location --request PUT 'https://advertising-api.amazon.com/sp/rules/optimization' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxx' \
--header 'Authorization: Bearer Atza|xxxxxxx' \
--header 'Amazon-Advertising-API-Scope;' \
--header 'Content-Type: application/json' \
--data-raw '{  
    "optimizationRules": 
    [
        { "optimizationRuleId" : "amzn1.ads-rule.m.xxxxxx",
          "ruleName": "increase_bids_by_30%_on_wednesday_night",      
          "ruleCategory": "BID",      
          "ruleSubCategory": "SCHEDULE",      
          "recurrence": 
            {"type": "WEEKLY", 
             "daysOfWeek":
             [
                "WEDNESDAY"
             ],
             "timesOfDay": 
             [          
                {"startTime": "22:00",            
                 "endTime": "23:00"}
             ], 
             "duration": 
                {"startTime": "2023-08-01T00:00:00Z",
                 "endTime": "2024-08-01T00:00:00Z"}
             },      
          "action": 
                {"actionType": "ADOPT",        
                "actionDetails": 
                    {"actionOperator": "INCREMENT",          
                     "value": 15,          
                     "actionUnit": "PERCENT"}
                 },      
          "status": "ENABLED"}
    ]
}'
```

