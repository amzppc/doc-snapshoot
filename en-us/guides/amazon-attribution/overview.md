---
title: Amazon Attribution API overview
description: Overview of Amazon Attribution, with information about how the API provides information about impact of advertisers' non-Amazon marketing strategies.
type: guide
interface: api
tags:
    - Amazon Attribution
keywords:
    - overview
---

# Amazon Attribution API overview

Amazon Attribution is a measurement product that enables advertisers to understand the impact of their non-Amazon marketing strategies on driving Amazon shopping activity. Advertising is measured by including attribution tags on non-Amazon media across paid and organic channels including search, social, display, video, email, third-party affiliate marketing, and more.

The Amazon Attribution API enables integrators to easily set up Amazon Attribution tags which can be used to measure media that links to an Amazon store or product detail page. The API also enables programmatic reporting on clicks and click-attributed conversion metrics so users can understand the performance and impact of their tagged media.

>[NOTE] Amazon Attribution is currently available in beta to professional seller brand owners enrolled in Amazon Brand Registry and vendors that sell products on Amazon in the following markets: US, Canada, Mexico, UK, Germany, France, Italy, Spain, and Netherlands.

## Next steps

Learn more about Amazon Attribution by visiting the [product page and support center](https://advertising.amazon.com/amazon-attribution).

Learn how to [get started](guides/amazon-attribution/get-started) with the Amazon Attribution API. 
