---
title: Campaigns dataset
description: Details and schema for the campaigns dataset.
type: guide
interface: amazon-marketing-stream
tags:
    - Reporting
    - Campaign management
    - Sponsored Display
---

# Campaigns dataset (beta)

The campaigns dataset provides a snapshot of all your campaign objects--including Sponsored Products, Sponsored Brands, and Sponsored Display. 

## Dataset ID

To subscribe to this dataset, use the ID `campaigns`.

## Schema

|Field	|Type	|Description	|
|-----|----|-------
|campaign.dataset_id	|string	|An identifier used to identify the dataset (in this case, `campaigns`).	|
|campaign.advertiser_id	|string	|Unique identifier of the advertiser (standard in Stream datasets).	|
|campaign.marketplace_id	|string	|The marketplace of the advertiser.	|
|campaign.campaignId	|string	|Unique identifier of campaign.	|
|campaign.accountId	|string	|Unique identifier of advertiser.	Also referred to as `profileId` in the API. |
|campaign.portfolioId	|string	|Unique identifier of portfolio, if the campaign belongs to one.	|
|campaign.adProduct	|string	|Product/program type of campaign. One of: SPONSORED\_PRODUCTS, SPONSORED\_BRANDS, SPONSORED\_DISPLAY.	|
|campaign.productLocation	|string	|The product location of the campaign. One of: SOLD\_ON\_AMAZON (For products sold on Amazon websites), NOT\_SOLD\_ON\_AMAZON (For products not sold on Amazon websites), SOLD\_ON\_DTC (Deprecated - For products sold on DTC websites).	|
|campaign.name	|string	|Name of the campaign (must be unique for an account).	|
|campaign.startDateTime	|datetime	|Effective from date time of campaign.	|
|campaign.endDateTime	|datetime	|Effective through date time of campaign.	|
|campaign.state	|string	|The advertiser defined state falling into one of these values: <br> Sponsored Products: ENABLED, PAUSED, ARCHIVED, USER_DELETED, ENABLING, OTHER<br><br> Sponsored Brands/Sponsored Display: ENABLED, PAUSED, ARCHIVED	|
|campaign.tags	|map<string, string>	|User defined key value pairs.	|
|campaign.targetingSettings	|string	|Targeting type of the campaign, today is used to indicate auto or manual.	|
|campaign.budget.budgetCap.monetaryBudget.amount	|double	|The budget in amount allocated to this budgetCap.	|
|campaign.budget.budgetCap.monetaryBudget.currencyCode	|string	|ISO 4217 currency code.	|
|campaign.budget.budgetCap.recurrence.recurrenceType	|string	|Whether the budget renews weekly, monthly, or lifetime e.g., DAILY, LIFETIME.	|
|campaign.bidSetting.bidStrategy	|string	|Bid strategy options. For Sponsored Products, one of: LEGACY\_FOR\_SALES, AUTO\_FOR\_SALES, MANUAL, RULE\_BASED	|
|campaign.bidSetting.placementBidAdjustment.placement	|string	|Bid strategy placement modifier<br><br> Sponsored Brands: HOME, DETAIL\_PAGE, OTHER<br>Sponsored Products: PLACEMENT\_TOP, PLACEMENT\_PRODUCT\_PAGE, PLACEMENT\_REST\_OF\_SEARCH	|
|campaign.bidSetting.placementBidAdjustment.percentage	|double	|Percentage of bid adjustment.	|
|campaign.audit.creationDateTime	|datetime	|Creation time stamp in ISO8601 format.	|
|campaign.audit.lastUpdatedDateTime	|datetime	|Last time record was updated timestamp in ISO8601 format.	|


## Sample payload

```json
{
  "dataset_id": "campaigns",
  "advertiser_id": "",
  "marketplace_id": "",
  "campaignId": "",
  "advertiser_id": "",
  "portfolioId": "",
  "adProduct": "",
  "name": "",
  "startDateTime": "",
  "endDateTime": "",
  "state": "",
  "tags": {
    "": ""
  },
  "targetingSettings": "",
  "audit": {
    "creationDateTime": "",
    "lastUpdatedDateTime": ""
  },
  "budget": {
    "budgetCap": {
      "monetaryBudget": {
        "amount": 0,
        "currencyCode": ""
      },
      "recurrence": {
        "recurrenceType": ""
      }
    },
    "bidSetting": {
      "bidStrategy": "",
      "placementBidAdjustment": [{
        "placement": "",
        "percentage": 0
      }]
    }
  }
}

```

## Resource-based IAM policy

To subscribe to this dataset, ensure the correct IAM policy is added to the SQS queue based on the marketplace.

**NA**

```json
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-east-1:570159413969:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

**EU**

```json
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:eu-west-1:834862128520:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

**FE**

```json
FE campaigns
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-west-2:527383333093:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

## Data freshness

You will receive a notification in near real time for every change in the configuration of your campaign.