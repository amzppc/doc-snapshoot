---
title: Sponsored Brands rich media dataset
description: Details and schema for the Sponsored Brands rich media (sb-rich-media) dataset.
type: guide
interface: amazon-marketing-stream
tags:
    - Reporting
    - Campaign management
    - Sponsored Brands
---

# Sponsored Brands rich media dataset (beta)

The sb-rich-media dataset contains video data related to Sponsored Brands campaigns.

>[NOTE] Amazon Marketing Stream only includes traffic deriving from Sponsored Brands version 4 (multi-ad group campaigns). Data related to any campaigns created using [POST /sb/campaigns](sponsored-brands/3-0/openapi#tag/Campaigns/operation/createCampaigns) before May 22, 2023 (in the US) or before August 10, 2023 (rest of the world) will not be sent. For more details,view the Sponsored Brands version 4 [migration guide](reference/migration-guides/sb-v3-v4).  

## Dataset ID

To subscribe to this dataset, use the `sb-rich-media` ID.

## Schema

|Field name	|Type	|Description	|
|------|-----|----------|
|idempotency\_id	|String	|An identifier than can be used to de-duplicate records.	|
|dataset\_id	|String	|An identifier used to identify the dataset (in this case, sb-rich-media).	|
|marketplace\_id	|String	|The [marketplace identifier](https://developer-docs.amazon.com/sp-api/docs/marketplace-ids) associated with the account.	|
|time\_window\_start	|String	|The start of the hour to which the performance data is attributed (ISO 8601 date time).	|
|campaign\_id	|String	|Unique numerical ID for a campaign.	|
|ad\_id	|String	|Unique numerical ID for the ad.	|
|ad\_group\_id	|String	|Unique numerical ID for an ad group.	|
|advertiser\_id	|String	|ID associated with the advertiser. You can retrieve this ID using the [GET v2/profiles](https://advertising.amazon.com/API/docs/en-us/reference/2/profiles#/Profiles/listProfiles) endpoint. This value is **not** unique and may be the same across marketplaces. Note: For non-seller accounts, this advertiser_id is set to the entity ID.	|
|keyword\_id	|String	|ID of the keyword used for a bid.	|
|keyword\_text	|String	|Text of the keyword or phrase used for a bid.	|
|keyword\_type	|String	|The type of targeting used in the expression.|
|placement\_type	|String	| The page location where an ad appeared. Possible values: Detail Page on-Amazon, Other on-Amazon, Top of Search on-Amazon.	|
|video\_5\_second\_views	|Long	|The number of impressions where the customer watched the complete video or 5 seconds (whichever is shorter).	|
|video\_complete\_views	|Long	|The number of impressions where the video was viewed to 100%.	|
|video\_first\_quartile\_views	|Long	|The number of impressions where the video was viewed to 25%.	|
|video\_midpoint\_views	|Long	|The number of impressions where the video was viewed to 50%.	|
|video\_third\_quartile\_views	|Long	|The number of impressions where the video was viewed to 75%.	|
|video\_unmutes	|Long	|The number of impressions where a customer unmuted the video.	|

## Resource-based IAM policy

To subscribe to this dataset, ensure the correct IAM policy is added to the SQS queue based on the marketplace.

**NA**

```json
{
  "Version": "2008-10-17",
  "Id": "{REPLACE_WITH_ANY_VALUE}",
  "Statement": [
    {
      "Sid": "{REPLACE_WITH_ANY_VALUE}",
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "{REPLACE WITH ARN Of SQS DESTINATION QUEUE}",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-east-1:010312603579:*"
        }
      }
    },
    {
      "Sid": "{REPLACE_WITH_ANY_VALUE}",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

**EU**

```json
{
  "Version": "2008-10-17",
  "Id": "{REPLACE_WITH_ANY_VALUE}",
  "Statement": [
    {
      "Sid": "{REPLACE_WITH_ANY_VALUE}",
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "{REPLACE WITH ARN Of SQS DESTINATION QUEUE}",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:eu-west-1:662188760626:*"
        }
      }
    },
    {
      "Sid": "{REPLACE_WITH_ANY_VALUE}",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

**FE**

```json
{
  "Version": "2008-10-17",
  "Id": "{REPLACE_WITH_ANY_VALUE}",
  "Statement": [
    {
      "Sid": "{REPLACE_WITH_ANY_VALUE}",
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "{REPLACE WITH ARN Of SQS DESTINATION QUEUE}",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-west-2:618223300352:*"
        }
      }
    },
    {
      "Sid": "{REPLACE_WITH_ANY_VALUE}",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

## Data freshness

Initial click data is available within 12 hours of the ad click. As part of the traffic validation process, clicks can be invalidated during the following 72 hours post the initial report. Amazon Marketing Stream will automatically deliver any adjustments to your queue.