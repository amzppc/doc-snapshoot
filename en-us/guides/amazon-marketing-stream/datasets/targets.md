---
title: Targets dataset
description: Details and schema for the targets dataset.
type: guide
interface: amazon-marketing-stream
tags:
    - Reporting
    - Campaign management
---

# Targets dataset (beta)

The targets dataset provides a snapshot of all your targeting and keyword objects--including those related to Sponsored Products, Sponsored Brands, and Sponsored Display campaigns. 

## Dataset ID

To subscribe to this dataset, use the `targets` ID.

## Schema

|Field	|Type	|Description	|
|---|---|---|
|target.dataset_id	|string	|An identifier used to identify the dataset (in this case `targets`).	|
|target.advertiser_id	|string	|Unique identifier of the advertiser (standard in Stream datasets).	|
|target.marketplace_id	|string	|The marketplace of the advertiser	|
|target.targetId	|string	|Unique identifier of target.	|
|target.accountId	|string	|Unique identifier of advertiser.	Also referred to as `profileId` in the API.  |
|target.adGroupId	|string	|Unique identifier of the ad group.	|
|target.campaignId	|string	|Unique identifier of the campaign.	|
|target.adProduct	|string	|Product/program type of campaign. One of: SPONSORED\_PRODUCTS, SPONSORED\_BRANDS, SPONSORED\_DISPLAY	|
|target.state	|string	|The advertiser defined state falling into one of these values:<br>Sponsored Products: ENABLED, PAUSED, ARCHIVED, USER\_DELETED, ENABLING, OTHER<br>Sponsored Brands/Sponsored Display: ENABLED, PAUSED, ARCHIVED	|
|target.targetType	|string	|The type of targeting e.g. KEYWORD, AUDIENCE, EXPRESSION, AUTO (EXPRESSION\_PREDEFINED), WEBSITE, APP, ASIN_EXPAND	|
|target.negative	|boolean	|Negative or positive targeting on the expression.	|
|target.bid	|double	|The bid used for auction. A null bid is valid, and means the target is inheriting from the ad group default bid.	|
|target.currencyCode	|string	|ISO4217 currency code.	|
|target.{targetType}.matchType	|string	|Matching type depending on target type.<br>For keywords: EXACT, PHRASE, BROAD (positive only)<br>For expressions: ASIN, CATEGORY, BRAND (negative only), DYNAMIC\_SEGMENTS (Sponsored Display only).<br>For audiences: VIEWS\_REMARKETING, PURCHASE\_REMARKETING, AMAZON\_AUDIENCES	|
|target.{targetType}.targetingClause	|string	|Targeting expression string.	|
|target.keywordTarget.keyword	|string	|Keyword value to be used for targeting.	|
|target.keywordTarget.nativeLanguageKeyword	|string	|Keyword value language.	|
|target.keywordTarget.nativeLanguageLocale	|string	|Locale of language for keyword.	|
|target.audit.creationDateTime	|datetime	|Creation time stamp in ISO8601 format.	|
|target.audit.lastUpdatedDateTime	|datetime	|Last time record was updated timestamp in ISO8601 format.	|


## Sample payload

```json
{
  "dataset_id": "targets",
  "advertiser_id": "",
  "marketplace_id": "",
  "targetId": "",
  "accountId": "",
  "adGroupId": "",
  "campaignId": "",
  "adProduct": "",
  "targetType": "",
  "negative": true,
  "bid": 0.0,
  "currencyCode": "",
  "keywordTarget": {
    "matchType": "",
    "keyword": "",
    "nativeLanguageKeyword": "",
    "nativeLanguageLocale": ""
  },
  "productTarget": {
    "matchType": "",
    "targetingClause": ""
  },
  "productAudienceTarget": {
    "matchType": "",
    "targetingClause": ""
  },
  "productCategoryTarget": {
    "matchType": "",
    "targetingClause": ""
  },
  "productCategoryAudienceTarget": {
    "matchType": "",
    "targetingClause": ""
  },
  "audienceTarget": {
    "matchType": "",
    "targetingClause": ""
  },
  "autoTarget": {
    "matchType": "",
    "targetingClause": ""
  },
  "state": "",
  "audit": {
    "creationDateTime": "",
    "lastUpdatedDateTime": ""
  }
}
```

## Resource-based IAM policy

To subscribe to this dataset, ensure the correct IAM policy is added to the SQS queue based on the marketplace.

**NA**

```json
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-east-1:644124924521:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

**EU**

```json
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:eu-west-1:503759481754:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}

```

**FE**

```json
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-west-2:248074939493:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}

```

## Data freshness

You will receive a notification in near real time for every change in the configuration of a keyword or target.