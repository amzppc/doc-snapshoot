---
title: Amazon DSP performance datasets
description: Details and schema for the Amazon DSP performance datasets.
type: guide
interface: amazon-marketing-stream
tags:
    - Reporting
    - Campaign management
    - Amazon DSP
---

# Amazon DSP performance datasets

The Amazon DSP performance datasets provide details about traffic, conversions, clickstream, and rich media for Amazon DSP campaigns.

To subscribe or manage subscriptions to these datasets, use the [/dsp/streams/subscriptions](openapi#tag/Stream-Subscription) endpoints.

>[TIP:Note about data freshness] The Amazon Marketing Stream DSP performance dataset follows [the same SLA as Amazon DSP reports](https://advertising.amazon.com/help/G2QE6RKPFG6C4KBH). Also note that compared to the Amazon DSP reporting API, Amazon Marketing Stream delivers interaction data and processes adjustments more quickly. When comparing data between Stream and API reports, we suggest waiting **five days** after the event has occurred.

## Amazon DSP traffic dataset

The adsp-traffic dataset contains click, impression, and cost data related to Amazon DSP campaigns.

### Dataset ID

To subscribe to this dataset, use the ID `adsp-traffic`.

### Schema

This dataset supports the following fields and metrics:

|Field name	|API name	|Type	|Type	|Description	|
|---	|---	|---	|---	|---	|
|dataset_id	|	N/A|String	|Dimension	|An identifier used to identify the dataset	|
|idempotency_id	|N/A	|String	|Dimension	|An identifier that can be used to de-duplicate records	|
|time\_window\_start	|N/A	|String	|Dimension	|The start of the hour at which the performance data is attributed (ISO 8601 date time). Time zone for each advertiser is indicated in the [advertiser profile](reference/2/profiles#/Profiles/listProfiles).	|
|adgroup_id	|lineItemId	|String	|Dimension	|A unique identifier assigned to a line item or ad group	|
|advertiser_country	|advertiserCountry	|String	|Dimension	|The country assigned to the advertiser	|
|advertiser_id	|advertiserId	|String	|Dimension	|The unique identifier for the advertiser	|
|advertiser_name	|advertiserName	|String	|Dimension	|The customer that has an advertising relationship with Amazon	|
|advertiser_timezone	|advertiserTimezone	|String	|Dimension	|The time zone the advertiser uses for reporting and ad serving purposes	|
|country	|country	|String	|Dimension	|Country or locale	|
|creative_id	|creativeId	|String	|Dimension	|A unique identifier assigned to a creative. When this is different from creativeAdId, creativeId cannot be used in our /dsp/creatives endpoints	|
|creative_language	|creativeLanguage	|String	|Dimension	|The primary language in the creative	|
|creative_name	|creativeName	|String	|Dimension	|Creative name	|
|creative_size	|creativeSize	|String	|Dimension	|The dimensions of the creative in pixels	|
|creative_type	|creativeType	|String	|Dimension	|The type of creative (for example static image, third party, or video)	|
|deal	|deal	|String	|Dimension	|The name of a deal	|
|deal_id	|dealID	|String	|Dimension	|The unique identifier for the deal	|
|deal_type	|dealType	|String	|Dimension	|The type of deal	|
|device	|device	|String	|Dimension	|The devices the customers used to view the ad	|
|dsp\_entity\_id	|entityId	|String	|Dimension	|Entity (seat) ID	|
|placement_name	|placementName	|String	|Dimension	|The name of the placement where the campaign ran	|
|proposal_id	|proposalId	|String	|Dimension	|A unique identifier for an order imported from the Amazon Order Management System (OMS)	|
|region	|region	|String	|Dimension	|State or region	|
|site	|siteName	|String	|Dimension	|The site or group of sites the campaign ran on	|
|supply\_source\_name	|supplySourceName	|String	|Dimension	|The inventory the campaign ran on, for example real-time bidding exchanges or Amazon-owned sites	|
|marketplace_id	|N/A	|String	|Dimension	|A unique identifier for the Amazon-owned site the product is sold on	|
|campaign_id	|orderId	|String	|Dimension	|The ID associated with a campaign	|
|flight_id	|N/A	|String	|Dimension	|Unique identifier of the campaign flight	|
|agency_fee	|agencyFee	|Currency	|Metric	|A percentage or flat fee removed from the total budget to compensate the agency that is managing the media buy	|
|amazon\_dsp\_audience_fee	|amazonAudienceFee	|Currency	|Metric	|CPM charge applied to impressions that leverage Amazon's behavioral targeting	|
|amazon\_dsp\_console\_fee	|amazonPlatformFee	|Currency	|Metric	|The technology fee applied to the media supply costs	|
|supply_cost	|supplyCost	|Currency	|Metric	|The total amount of money spent on media supply	|
|total_cost	|totalCost	|Currency	|Metric	|The total amount of money spent on running the campaign not including third-party fees paid by the agency	|
|gross_impressions	|grossImpressions	|Integer	|Metric	|The total number of times the ad was displayed. This includes valid and invalid impressions, such as potentially fraudulent, non-human, and other illegitimate impressions.	|
|impressions	|impressions	|Integer	|Metric	|Total number of ad impressions	|
|invalid_impressions	|invalidImpressions	|Integer	|Metric	|The number of impressions removed by a traffic quality filter. This includes potentially fraudulent, non-human, and other illegitimate traffic	|
|measurable_impressions	|measurableImpressions	|Integer	|Metric	|Number of impressions that were measured for viewability	|
|viewable_impressions	|viewableImpressions	|Integer	|Metric	|Number of impressions that met the Media Ratings Council's (MRC) viewability standard	|
|clicks	|clickThroughs	|Integer	|Metric	|Total number of clicks on an ad	|

### Sample Payload

```
{
    "dataset_id": "adsp-traffic",
    "idempotency_id": "string",
    "time_window_start": "string",
    "adgroup_id": "string",
    "advertiser_country": "string",
    "advertiser_id": "string",
    "advertiser_name": "string",
    "advertiser_timezone": "string",
    "country": "string",
    "creative_id": "string",
    "creative_language": "string",
    "creative_name": "string",
    "creative_size": "string",
    "creative_type": "string",
    "deal": "string",
    "deal_id": "string",
    "deal_type": "string",
    "device": "string",
    "dsp_entity_id": "string",
    "placement_name": "string",
    "proposal_id": "string",
    "region": "string",
    "site": "string",
    "supply_source_name": "string",
    "marketplace_id": "string",
    "campaign_id": "string",
    "flight_id": "string",
    "agency_fee": 0.0,
    "amazon_dsp_audience_fee": 0.0,
    "amazon_dsp_console_fee": 0.0,
    "supply_cost": 0.0,
    "total_cost": 0.0,
    "gross_impressions": 0,
    "impressions": 0,
    "invalid_impressions": 0,
    "measurable_impressions": 0,
    "viewable_impressions": 0,
    "clicks": 0
}
```

### Resource-based IAM policy

To subscribe to this dataset, ensure the correct IAM policy is added to the SQS queue based on the marketplace.

**NA**

```
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-east-1:192826442293:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```


**EU**

```
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:eu-west-1:738976022360:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```


**FE**

```
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-west-2:115810253223:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

## Amazon DSP conversion dataset

The adsp-conversion dataset contains conversion data attributed to Amazon DSP campaigns.

### Dataset ID

To subscribe to this dataset, use the ID `adsp-conversion`.

### Schema

This dataset supports the following fields and metrics:

|Field name	|API name	|Type	|Type	|Description	|
|---	|---	|---	|---	|---	|
|dataset_id	|N/A	|String	|Dimension	|An identifier used to identify the dataset	|
|idempotency_id	|N/A	|String	|Dimension	|An identifier that can be used to de-duplicate records	|
|time\_window\_start	|N/A	|String	|Dimension	|The start of the hour at which the performance data is attributed (ISO 8601 date time). Time zone for each advertiser is indicated in the [advertiser profile](reference/2/profiles#/Profiles/listProfiles).	|
|adgroup_id	|lineItemId	|String	|Dimension	|A unique identifier assigned to a line item or ad group	|
|advertiser_country	|advertiserCountry	|String	|Dimension	|The country assigned to the advertiser	|
|advertiser_id	|advertiserId	|String	|Dimension	|The unique identifier for the advertiser	|
|advertiser_name	|advertiserName	|String	|Dimension	|The customer that has an advertising relationship with Amazon	|
|advertiser_timezone	|advertiserTimezone	|String	|Dimension	|The time zone the advertiser uses for reporting and ad serving purposes	|
|country	|country	|String	|Dimension	|Country or locale	|
|creative_id	|creativeId	|String	|Dimension	|A unique identifier assigned to a creative. When this is different from creativeAdId, creativeId cannot be used in our /dsp/creatives endpoints.	|
|creative_language	|creativeLanguage	|String	|Dimension	|The primary language in the creative	|
|creative_name	|creativeName	|String	|Dimension	|Creative name	|
|creative_size	|creativeSize	|String	|Dimension	|The dimensions of the creative in pixels	|
|creative_type	|creativeType	|String	|Dimension	|The type of creative (for example static image, third party, or video)	|
|deal	|deal	|String	|Dimension	|The name of a deal	|
|deal_id	|dealID	|String	|Dimension	|The unique identifier for the deal	|
|deal_type	|dealType	|String	|Dimension	|The type of deal	|
|device	|device	|String	|Dimension	|The devices the customers used to view the ad	|
|dsp\_entity\_id	|entityId	|String	|Dimension	|Entity (seat) ID	|
|placement_name	|placementName	|String	|Dimension	|The name of the placement where the campaign ran	|
|proposal_id	|proposalId	|String	|Dimension	|A unique identifier for an order imported from the Amazon Order Management System (OMS)	|
|region	|region	|String	|Dimension	|State or region	|
|site	|siteName	|String	|Dimension	|The site or group of sites the campaign ran on	|
|supply\_source\_name	|supplySourceName	|String	|Dimension	|The inventory the campaign ran on--for example real-time bidding exchanges or Amazon-owned sites	|
|marketplace_id	|N/A	|String	|Dimension	|A unique identifier for the Amazon-owned site the product is sold on	|
|campaign_id	| sorderId	|String	|Dimension	|The ID associated with a campaign	|
|units_sold	|unitsSold14d	|Integer	|Metric	|Number of attributed units sold within 14 days of an ad click or view	|
|new\_to\_brand\_units\_sold	|newToBrandUnitsSold14d	|Integer	|Metric	|Total number of attributed units ordered as part of new-to-brand sales occurring within 14 days of an ad click or view. Not available for book vendors	|
|total\_units\_sold	|totalUnitsSold14d	|Integer	|Metric	|The total quantity of promoted products and products from the same brand as promoted products purchased by customers on Amazon after delivering an ad. A campaign can have multiple units sold in a single purchase event.	|
|total\_new\_to\_brand\_units\_sold	|totalNewToBrandUnitsSold14d	|Integer	|Metric	|The total quantity of promoted products and products from the same brand as promoted products purchased by customers on Amazon after delivering an ad. A campaign can have multiple units sold in a single purchase event.	|
|sales	|sales14d	|Currency	|Metric	|The total sales (in local currency) of promoted ASINs purchased by customers on Amazon after delivering an ad.	|
|new\_to\_brand\_product\_sales	|newToBrandProductSales14d	|Currency	|Metric	|Sales (in local currency) of products promoted to new-to-brand shoppers, attributed to an ad view or click. Use total\_new\_to\_brand\_product\_sales to see all conversions for the brand's products.	|
|total_sales	|totalSales14d	|Currency	|Metric	|Sales (in local currency) of the brand’s products, attributed to an ad view or click.	|
|total\_new\_to\_brand\_product_sales	|totalNewToBrandProductSales14d	|Currency	|Metric	|Sales (in local currency) of the brands’ products purchased by new-to-brand shoppers, attributed to an ad view or click. Shoppers are "new to brand" if they have not purchased from the brand in the last 365 days.	|
|purchases	|purchases14d	|Integer	|Metric	|Number of attributed conversion events occurring within 14 days of an ad click or view.	|
|purchases_views	|purchasesViews14d	|Integer	|Metric	|Number of attributed conversion events occurring within 14 days of an ad view.	|
|purchases_clicks	|purchasesClicks14d	|Integer	|Metric	|Number of attributed conversion events occurring within 14 days of an ad click.	|
|new\_to\_brand\_purchases	|newToBrandPurchases14d	|Integer	|Metric	|The number of first-time orders for brand products over a one-year lookback window resulting from an ad click or view. Not available for book vendors.	|
|new\_to\_brand\_purchases\_views	|newToBrandPurchasesViews14d	|Integer	|Metric	|The number of new to branch purchases attributed to an ad view.	|
|new\_to\_brand\_purchases\_clicks	|newToBrandPurchasesClicks14d	|Integer	|Metric	|The number of first-time orders for brand products over a one-year lookback window resulting from an ad click. Not available for book vendors.	|
|total_purchases	|totalPurchases14d	|Integer	|Metric	|Number of times any quantity of a brand product was included in a purchase event, attributed to an ad view or click. Purchase events include new Subscribe & Save subscriptions and video rentals.	|
|total\_purchases\_views	|totalPurchasesViews14d	|Integer	|Metric	|Number of times any quantity of a brand product was included in a purchase event, attributed to an ad view. Purchase events include new Subscribe & Save subscriptions and video rentals.	|
|total\_purchases\_clicks	|totalPurchasesClicks14d	|Integer	|Metric	|Number of times any quantity of a brand product was included in a purchase event, attributed to an ad click. Purchase events include new Subscribe & Save subscriptions and video rentals.	|
|total\_new\_to\_brand\_purchases	|totalNewToBrandPurchases14d	|Integer	|Metric	|Number of new-to-brand purchases for the brands’ products, attributed to an ad view or click. Shoppers are "new to brand" if they have not purchased from the brand in the last 365 days.	|
|total\_new\_to\_brand\_purchases\_views	|totalNewToBrandPurchasesViews14d	|Integer	|Metric	|Number of new-to-brand purchases for the brands’ products, attributed to an ad view. Shoppers are "new-to-brand" if they have not purchased from the brand in the last 365 days.	|
|total\_new\_to\_brand\_purchases\_clicks	|totalNewToBrandPurchasesClicks14d	|Integer	|Metric	|Number of new-to-brand purchases for the brands’ products, attributed to an ad click. Shoppers are "new-to-brand" if they have not purchased from the brand in the last 365 days.	|

### Sample payload

```
{
    "dataset_id": "adsp-conversion",
    "idempotency_id": "string",
    "time_window_start": "string",
    "adgroup_id": "string",
    "advertiser_country": "string",
    "advertiser_id": "string",
    "advertiser_name": "string",
    "advertiser_timezone": "string",
    "country": "string",
    "creative_id": "string",
    "creative_language": "string",
    "creative_name": "string",
    "creative_size": "string",
    "creative_type": "string",
    "deal": "string",
    "deal_id": "string",
    "deal_type": "string",
    "device": "string",
    "dsp_entity_id": "string",
    "placement_name": "string",
    "proposal_id": "string",
    "region": "string",
    "site": "string",
    "supply_source_name": "string",
    "marketplace_id": "string",
    "campaign_id": "string",
    "units_sold": 0,
    "new_to_brand_units_sold": 0,
    "total_units_sold": 0,
    "total_new_to_brand_units_sold": 0,
    "sales": 0.0,
    "new_to_brand_product_sales": 0.0,
    "total_sales": 0.0,
    "total_new_to_brand_product_sales": 0.0,
    "purchases": 0,
    "purchases_views": 0,
    "purchases_clicks": 0,
    "new_to_brand_purchases": 0,
    "new_to_brand_purchases_views": 0,
    "new_to_brand_purchases_clicks": 0,
    "total_purchases": 0,
    "total_purchases_views": 0,
    "total_purchases_clicks": 0,
    "total_new_to_brand_purchases": 0,
    "total_new_to_brand_purchases_views": 0,
    "total_new_to_brand_purchases_clicks": 0
}
```

### Resource-based IAM policy

To subscribe to this dataset, ensure the correct IAM policy is added to the SQS queue based on the marketplace.

**NA**

```
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-east-1:561176707666:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```


**EU**

```
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:eu-west-1:238549931437:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```


**FE**

```
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-west-2:063613587323:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

## Amazon DSP clickstream dataset

The adsp-clickstream dataset contains detail page views and add-to-cart metrics related to Amazon DSP campaigns.

### Dataset ID

To subscribe to this dataset, use the ID `adsp-clickstream`.

### Schema

This dataset supports the following fields and metrics:

|Field name	|API name	|Type	|Type	|Description	|
|---	|---	|---	|---	|---	|
|dataset_id	|N/A	|String	|Dimension	|An identifier used to identify the dataset	|
|idempotency_id	|N/A	|String	|Dimension	|An identifier that can be used to de-duplicate records	|
|time\_window\_start	|	|String	|Dimension	|The start of the hour at which the performance data is attributed (ISO 8601 date time). Time zone for each advertiser is indicated in the [advertiser profile](reference/2/profiles#/Profiles/listProfiles)	|
|adgroup_id	|lineItemId	|String	|Dimension	|A unique identifier assigned to a line item or ad group	|
|advertiser_country	|advertiserCountry	|String	|Dimension	|The country assigned to the advertiser	|
|advertiser_id	|advertiserId	|String	|Dimension	|The unique identifier for the advertiser	|
|advertiser_name	|advertiserName	|String	|Dimension	|The customer that has an advertising relationship with Amazon	|
|advertiser_timezone	|advertiserTimezone	|String	|Dimension	|The time zone the advertiser uses for reporting and ad serving purposes	|
|country	|country	|String	|Dimension	|Country or locale	|
|creative_id	|creativeId	|String	|Dimension	|A unique identifier assigned to a creative. When this is different from creativeAdId, creativeId cannot be used in our /dsp/creatives endpoints.	|
|creative_language	|creativeLanguage	|String	|Dimension	|The primary language in the creative	|
|creative_name	|creativeName	|String	|Dimension	|Creative name	|
|creative_size	|creativeSize	|String	|Dimension	|The dimensions of the creative in pixels	|
|creative_type	|creativeType	|String	|Dimension	|The type of creative (for example static image, third party, or video)	|
|deal	|deal	|String	|Dimension	|The name of a deal	|
|deal_id	|dealID	|String	|Dimension	|The unique identifier for the deal	|
|deal_type	|dealType	|String	|Dimension	|The type of deal	|
|device	|device	|String	|Dimension	|The devices the customers used to view the ad	|
|dsp\_entity\_id	|entityId	|String	|Dimension	|Entity (seat) ID	|
|placement_name	|placementName	|String	|Dimension	|The name of the placement where the campaign ran	|
|proposal_id	|proposalId	|String	|Dimension	|A unique identifier for an order imported from the Amazon Order Management System (OMS)	|
|region	|region	|String	|Dimension	|State or region	|
|site	|siteName	|String	|Dimension	|The site or group of sites the campaign ran on	|
|supply\_source\_name	|supplySourceName	|String	|Dimension	|The inventory the campaign ran on, for example real-time bidding exchanges or Amazon-owned sites	|
|marketplace_id	|N/A	|String	|Dimension	|A unique identifier for the Amazon-owned site the product is sold on	|
|campaign_id	| orderId	|String	|Dimension	|The ID associated with a campaign	|
|add\_to\_cart	|atc14d	|Integer	|Metric	|Number of times shoppers added a brand's products to their cart, attributed to an ad view or click	|
|add\_to\_cart\_clicks	|atcClicks14d	|Integer	|Metric	|Number of times shoppers added a brand's products to their cart, attributed to an ad click	|
|add\_to\_cart\_views	|atcViews14d	|Integer	|Metric	|Number of times shoppers added the brand's products to their cart, attributed to an ad view	|
|add\_to\_list	|atl14d	|Integer	|Metric	|Number of times shoppers added a promoted product to a wish list, gift list, or registry, attributed to an ad view or click. Use total\_add\_to\_list to see all conversions for the brand's products.	|
|add\_to\_list\_clicks	|atlClicks14d	|Integer	|Metric	|Number of times shoppers added a promoted product to a wish list, gift list, or registry, attributed to an ad click. Use total\_add\_to\_list clicks to see all conversions for the brand's products.	|
|add\_to\_list\_views	|atlViews14d	|Integer	|Metric	|Number of times shoppers added a promoted product to a wish list, gift list, or registry, attributed to an ad view. Use total\_add\_to\_list views to see all conversions for the brand's products.	|
|detail\_page\_views	|detailPageViews	|Integer	|Metric	|Number of detail page views occurring within 14 days of an ad click or view	|
|detail\_page\_view\_clicks	|detailPageViewClicks	|Integer	|Metric	|Number of detail page views occurring within 14 days of an ad click	|
|detail\_page\_view\_views	|dpvViews14d	|Integer	|Metric	|The number of times a product detail page was viewed attributed to an ad view	|
|subscribe\_and\_save	|newSubscribeAndSave14d	|Integer	|Metric	|Number of new Subscribe & Save subscriptions for all products, attributed to an ad view or click. This does not include replenishment subscription orders. Use total\_subscribe\_and\_save to see all conversions for the brand's products.	|
|subscribe\_and\_save\_clicks	|newSubscribeAndSaveClicks14d	|Integer	|Metric	|Number of new Subscribe & Save subscriptions for all products, attributed to ad clicks. This does not include replenishment subscription orders. Use total\_subscribe\_and\_save to see all conversions for the brand's products.	|
|subscribe\_and\_save\_views	|newSubscribeAndSaveViews14d	|Integer	|Metric	|Number of new Subscribe & Save subscriptions for all products, attributed to ad views. This does not include replenishment subscription orders. Use total\_subscribe\_and\_save to see all conversions for the brand's products.	|
|total\_add\_to\_cart	|totalAddToCart14d	|Integer	|Metric	|Number of times shoppers added the brand's products to their cart, attributed to an ad view or click	|
|total\_add\_to\_cart\_clicks	|totalAddToCartClicks14d	|Integer	|Metric	|Number of times shoppers added the brand's products to their cart, attributed to an ad click	|
|total\_add\_to\_cart\_views	|totalAddToCartViews14d	|Integer	|Metric	|Number of times shoppers added the brand's products to their cart, attributed to an ad view	|
|total\_add\_to\_list	|totalAddToList14d	|Integer	|Metric	|Number of times shoppers added the brand's products to a wish list, gift list, or registry, attributed to an ad view or click	|
|total\_add\_to\_list\_clicks	|totalAddToListClicks14d	|Integer	|Metric	|Number of times shoppers added the brand's products to a wish list, gift list, or registry, attributed to an ad click	|
|total\_add\_to\_list\_views	|totalAddToListViews14d	|Integer	|Metric	|Number of times shoppers added the brand's products to a wish list, gift list or registry, attributed to an ad view	|
|total\_detail\_page\_view\_clicks	|totalDetailPageClicks14d	|Integer	|Metric	|Number of detail page views for all the brands and products attributed to an ad click	|
|total\_detail\_page\_views	|totalDetailPageViews14d	|Integer	|Metric	|Number of detail page views for all the brands and products attributed to an ad view or click	|
|total\_detail\_page\_view\_views	|totalDetailPageViewViews14d	|Integer	|Metric	|Number of detail page views for all the brand's products, attributed to an ad view	|
|total\_subscribe\_and\_save\_clicks	|totalSubscribeAndSaveSubscriptionClicks14d	|Integer	|Metric	|Number of new Subscribe & Save subscriptions for the brands and products, attributed to an ad click. This does not include replenishment subscription orders.	|
|total\_subscribe\_and\_save	|totalSubscribeAndSaveSubscriptions14d	|Integer	|Metric	|Number of new Subscribe & Save subscriptions for the brands and products, attributed to an ad view or click. This does not include replenishment subscription orders.	|
|total\_subscribe\_and\_save\_views	|totalSubscribeAndSaveSubscriptionViews14d	|Integer	|Metric	|Number of new Subscribe & Save subscriptions for the brands and products, attributed to an ad view. This does not include replenishment subscription orders.	|

### Sample payload

```
{
    "dataset_id": "adsp-clickstream",
    "idempotency_id": "string",
    "time_window_start": "string",
    "adgroup_id": "string",
    "advertiser_country": "string",
    "advertiser_id": "string",
    "advertiser_name": "string",
    "advertiser_timezone": "string",
    "country": "string",
    "creative_id": "string",
    "creative_language": "string",
    "creative_name": "string",
    "creative_size": "string",
    "creative_type": "string",
    "deal": "string",
    "deal_id": "string",
    "deal_type": "string",
    "device": "string",
    "dsp_entity_id": "string",
    "placement_name": "string",
    "proposal_id": "string",
    "region": "string",
    "site": "string",
    "supply_source_name": "string",
    "marketplace_id": "string",
    "campaign_id": "string",
    "add_to_cart": 0,
    "add_to_cart_clicks": 0,
    "add_to_cart_views": 0,
    "add_to_list": 0,
    "add_to_list_clicks": 0,
    "add_to_list_views": 0,
    "detail_page_views": 0,
    "detail_page_view_clicks": 0,
    "detail_page_view_views": 0,
    "subscribe_and_save": 0,
    "subscribe_and_save_clicks": 0,
    "subscribe_and_save_views": 0,
    "total_add_to_cart": 0,
    "total_add_to_cart_clicks": 0,
    "total_add_to_cart_views": 0,
    "total_add_to_list": 0,
    "total_add_to_list_clicks": 0,
    "total_add_to_list_views": 0,
    "total_detail_page_view_clicks": 0,
    "total_detail_page_views": 0,
    "total_detail_page_view_views": 0,
    "total_subscribe_and_save_clicks": 0,
    "total_subscribe_and_save": 0,
    "total_subscribe_and_save_views": 0
}
```

### IAM policy

To subscribe to this dataset, ensure the correct IAM policy is added to the SQS queue based on the marketplace.

**NA**

```
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-east-1:740036912861:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```


**EU**

```
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:eu-west-1:475804811800:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

**FE**

```
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-west-2:201394813667:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

## Amazon DSP rich media dataset

The adsp-rich-media dataset contains video data related to Amazon DSP campaigns.

### Dataset ID

To subscribe to this dataset, use the ID `adsp-rich-media`.

### Schema

This dataset supports the following fields and metrics:

|Field name	|API name	|Type	|Type	|Description	|
|---	|---	|---	|---	|---	|
|dataset_id	|N/A	|String	|Dimension	|An identifier used to identify the dataset	|
|idempotency_id	|N/A	|String	|Dimension	|An identifier that can be used to de-duplicate records	|
|time\_window\_start	|N/A	|String	|Dimension	|The start of the hour at which the performance data is attributed (ISO 8601 date time). Time zone for each advertiser is indicated in the [advertiser profile](reference/2/profiles#/Profiles/listProfiles).	|
|adgroup_id	|lineItemId	|String	|Dimension	|A unique identifier assigned to a line item or ad group	|
|advertiser_country	|advertiserCountry	|String	|Dimension	|The country assigned to the advertiser	|
|advertiser_id	|advertiserId	|String	|Dimension	|The unique identifier for the advertiser	|
|advertiser_name	|advertiserName	|String	|Dimension	|The customer that has an advertising relationship with Amazon	|
|advertiser_timezone	|advertiserTimezone	|String	|Dimension	|The time zone the advertiser uses for reporting and ad serving purposes	|
|country	|country	|String	|Dimension	|Country or locale	|
|creative_id	|creativeId	|String	|Dimension	|A unique identifier assigned to a creative. When this is different from creativeAdId, creativeId cannot be used in our /dsp/creatives endpoints.	|
|creative_language	|creativeLanguage	|String	|Dimension	|The primary language in the creative	|
|creative_name	|creativeName	|String	|Dimension	|Creative name	|
|creative_size	|creativeSize	|String	|Dimension	|The dimensions of the creative in pixels	|
|creative_type	|creativeType	|String	|Dimension	|The type of creative (for example static image, third party, or video)	|
|deal	|deal	|String	|Dimension	|The name of a deal	|
|deal_id	|dealID	|String	|Dimension	|The unique identifier for the deal	|
|deal_type	|dealType	|String	|Dimension	|The type of deal	|
|device	|device	|String	|Dimension	|The devices the customers used to view the ad	|
|dsp\_entity\_id	|entityId	|String	|Dimension	|Entity (seat) ID	|
|placement_name	|placementName	|String	|Dimension	|The name of the placement where the campaign ran	|
|proposal_id	|proposalId	|String	|Dimension	|A unique identifier for an order imported from the Amazon Order Management System (OMS).	|
|region	|region	|String	|Dimension	|State or region.	|
|site	|siteName	|String	|Dimension	|The site or group of sites the campaign ran on.	|
|supply\_source\_name	|supplySourceName	|String	|Dimension	|The inventory the campaign ran on, for example real-time bidding exchanges or Amazon-owned sites.	|
|marketplace_id	|N/A	|String	|Dimension	|A unique identifier for the Amazon-owned site the product is sold on.	|
|campaign_id	|orderId	|String	|Dimension	|The ID associated with a campaign.	|
|audio\_ad\_companion\_banner_clicks	|audioAdCompanionBannerClicks	|Integer	|Metric	|Tracks the number of times that the audio ad companion banner was clicked.	|
|audio\_ad\_companion\_banner\_views	|audioAdCompanionBannerViews	|Integer	|Metric	|Tracks the number of times that the audio ad companion banner was displayed.	|
|audio\_ad\_completions	|audioAdCompletions	|Integer	|Metric	|Tracks the number of times the audio ad plays to the end.	|
|audio\_ad\_first\_quartile	|audioAdFirstQuartile	|Integer	|Metric	|Tracks the number of times the audio ad plays to 25% of its length.	|
|audio\_ad\_midpoint	|audioAdMidpoint	|Integer	|Metric	|Tracks the number of times the audio ad plays to 50% of its length	|
|audio\_ad\_mute	|audioAdMute	|Integer	|Metric	|Tracks the number of times a user mutes the audio ad.	|
|audio\_ad\_pause	|audioAdPause	|Integer	|Metric	|The number of times a user pauses the audio ad.	|
|audio\_ad\_progression	|audioAdProgression	|Integer	|Metric	|Tracks an optimal audio ad time marker agreed upon with the publisher	|
|audio\_ad\_resume	|audioAdResume	|Integer	|Metric	|Tracks the number of times a user resumes playback of the audio ad.	|
|audio\_ad\_rewind	|audioAdRewind	|Integer	|Metric	|Tracks the number of times a user rewinds the audio ad.	|
|audio\_ad\_skip	|audioAdSkip	|Integer	|Metric	|Tracks the number of times a user skips the audio ad.	|
|audio\_ad\_start	|audioAdStart	|Integer	|Metric	|Tracks the number of times audio ad starts playing.	|
|audio\_ad\_third\_quartile	|audioAdThirdQuartile	|Integer	|Metric	|Tracks the number of times the audio ad plays to 75% of its length.	|
|audio\_ad\_unmute	|audioAdUnmute	|Integer	|Metric	|Tracks the number of times a user skips the audio ad.	|
|audio\_ad\_views	|audioAdViews	|Integer	|Metric	|Tracks the number of times that some playback of the audio ad has occurred.	|
|video\_ad\_complete	|videoComplete	|Integer	|Metric	|The number of times a video ad played to completion. If rewind occurred, completion was calculated on the total percentage of unduplicated video viewed.	|
|video\_ad\_first\_quartile	|videoFirstQuartile	|Integer	|Metric	|The number of times at least 25% of a video ad played. If rewind occurred, percent complete was calculated on the total percentage of unduplicated video viewed.	|
|video\_ad\_midpoint	|videoMidpoint	|Integer	|Metric	|The number of times at least 50% of a video ad played. If rewind occurred, percent complete was calculated on the total percentage of unduplicated video viewed.	|
|video\_ad\_mute	|videoMute	|Integer	|Metric	|The number of times a user muted the video ad.	|
|video\_ad\_pause	|videoPause	|Integer	|Metric	|The number of times a user paused the video ad.	|
|video\_ad\_resume	|videoResume	|Integer	|Metric	|The number of times a user unpaused the video ad.	|
|video\_ad\_start	|videoStart	|Integer	|Metric	|The number of times a video ad was started.	|
|video\_ad\_third\_quartile	|videoThirdQuartile	|Integer	|Metric	|The number of times at least 75% of a video ad played. If rewind occurred, percent complete was calculated on the total percentage of unduplicated video viewed.	|
|video\_ad\_unmute	|videoUnmute	|Integer	|Metric	|The number of times a user unmuted the video ad.	|

### Sample payload

```
{
    "dataset_id": "adsp-rich-media",
    "idempotency_id": "string",
    "time_window_start": "string",
    "adgroup_id": "string",
    "advertiser_country": "string",
    "advertiser_id": "string",
    "advertiser_name": "string",
    "advertiser_timezone": "string",
    "country": "string",
    "creative_id": "string",
    "creative_language": "string",
    "creative_name": "string",
    "creative_size": "string",
    "creative_type": "string",
    "deal": "string",
    "deal_id": "string",
    "deal_type": "string",
    "device": "string",
    "dsp_entity_id": "string",
    "placement_name": "string",
    "proposal_id": "string",
    "region": "string",
    "site": "string",
    "supply_source_name": "string",
    "marketplace_id": "string",
    "campaign_id": "string",
    "audio_ad_companion_banner_clicks": 0,
    "audio_ad_companion_banner_views": 0,
    "audio_ad_completions": 0,
    "audio_ad_first_quartile": 0,
    "audio_ad_midpoint": 0,
    "audio_ad_mute": 0,
    "audio_ad_pause": 0,
    "audio_ad_progression": 0,
    "audio_ad_resume": 0,
    "audio_ad_rewind": 0,
    "audio_ad_skip": 0,
    "audio_ad_start": 0,
    "audio_ad_third_quartile": 0,
    "audio_ad_unmute": 0,
    "audio_ad_views": 0,
    "video_ad_complete": 0,
    "video_ad_first_quartile": 0,
    "video_ad_midpoint": 0,
    "video_ad_mute": 0,
    "video_ad_pause": 0,
    "video_ad_resume": 0,
    "video_ad_start": 0,
    "video_ad_third_quartile": 0,
    "video_ad_unmute": 0
}
```

### Resource-based IAM policy

To subscribe to this dataset, ensure the correct IAM policy is added to the SQS queue based on the marketplace.

**NA**

```
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-east-1:718926279583:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```


**EU**

```
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:eu-west-1:047120534722:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```


**FE**

```
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-west-2:886046350753:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```
