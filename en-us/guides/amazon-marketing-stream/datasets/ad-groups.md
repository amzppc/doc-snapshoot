---
title: Ad groups dataset
description: Details and schema for the ad groups dataset.
type: guide
interface: amazon-marketing-stream
tags:
    - Reporting
    - Campaign management
---

# Ad groups dataset (beta)

The ad groups dataset provides a snapshot of all your ad group objects--including those related to Sponsored Products, Sponsored Brands, and Sponsored Display campaigns. 

## Dataset ID 

To subscribe to this dataset, use the ID `adgroups`.

## Schema

|Field	|Type	|Description	|
|----|----|----|
|adGroup.dataset_id	|string	|An identifier used to identify the dataset (in this case, `adgroups`.)	|
|adGroup.advertiser_id	|string	|Unique identifier of the advertiser (standard in Stream datasets).	|
|adGroup.marketplace_id	|string	|The marketplace of the advertiser.	|
|adGroup.adGroupId	|string	|Unique identifier of ad group.	|
|adGroup.name	|string	|Name of the ad group.	|
|adGroup.accountId	|string	|Unique identifier of advertiser.	Also referred to as `profileId` in the API. |
|adGroup.campaignId	|string	|Unique identifier of parent campaign object.	|
|adGroup.adProduct	|string	|Product/program type of campaign. One of: SPONSORED\_PRODUCTS, SPONSORED\_BRANDS, SPONSORED\_DISPLAY	|
|adGroup.bidValue.defaultBid.value	|double	|Value of the bid in the currency specified.	|
|adGroup.bidValue.defaultBid.currencyCode	|string	|ISO4217 currency code.	|
|adGroup.state	|string	|The advertiser defined state falling into one of these values: <br><br>Sponsored Products: ENABLED, PAUSED, ARCHIVED, USER\_DELETED, ENABLING, OTHER<br>Sponsored Brands/Sponsored Display: ENABLED, PAUSED, ARCHIVED	|
|adGroup.audit.creationDateTime	|datetime	|Creation time stamp in ISO8601 format.	|
|adGroup.audit.lastUpdatedDateTime	|datetime	|Last time record was updated timestamp in ISO8601 format.	|


## Sample payload

```json
{
  "dataset_id": "adgroups",
  "advertiser_id": "",
  "marketplace_id": "",
  "adGroupId": "",
  "name": "",
  "accountId": "",
  "campaignId": "",
  "adProduct": "",
  "bidValue": {
    "defaultBid": {
      "value": 0.0,
      "currencyCode": ""
    }
  },
  "state": "",
  "audit": {
    "creationDateTime": "",
    "lastUpdatedDateTime": ""
  }
}
```

## Resource-based IAM policy

To subscribe to this dataset, ensure the correct IAM policy is added to the SQS queue based on the marketplace.

**NA**

```json
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-east-1:118846437111:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

**EU**

```json
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:eu-west-1:130948361130:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

**FE**

```json
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-west-2:668585072850:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

## Data freshness

You will receive a notification in near real time for every change in the configuration of an ad group.