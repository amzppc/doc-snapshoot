---
title: Sponsored Display traffic dataset
description: Details and schema for the Sponsored Display traffic (sd-traffic) dataset.
type: guide
interface: amazon-marketing-stream
tags:
    - Reporting
    - Campaign management
    - Sponsored Display
---

# Sponsored Display traffic dataset

The sd-traffic dataset contains click, impression, and cost data related to Sponsored Display campaigns. 

## Dataset ID

To subscribe to this dataset, use the ID `sd-traffic`.

## Schema

|Field name	|Type	|Description	|
|---	|---	|---	|
|idempotency_id	|String	|An identifier than can be used to de-duplicate records.	|
|dataset_id	|String	|An identifier used to identify the dataset (in this case, sd-traffic).	|
|marketplace_id	|String	|[The marketplace identifier associated with the account.](https://developer-docs.amazon.com/sp-api/docs/marketplace-ids)	|
|currency	|String	|The currency used for all monetary values for entities under the profile.	|
|advertiser\_id	|String	| ID associated with the advertiser. You can retrieve this ID using the [GET v2/profiles](reference/2/profiles#tag/Profiles/operation/listProfiles) endpoint. This value is **not** unique and may be the same across marketplaces. Note: For non-seller accounts, this advertiser_id is set to the entity ID.	|
|campaign_id	|String	|Unique numerical ID for a campaign.	|
|ad\_group\_id	|String	|Unique numerical ID for an ad group.	|
|ad_id	|String	|Unique numerical ID for the ad.	|
|target_id	|Integer	|The identifier of the targeting expression.	|
|targeting_text	|String	|The text used in the targeting expression.	|
|time\_window\_start	|String	|The start of the hour to which the performance data is attributed (ISO 8601 date time).	|
|clicks	|Long	|Total ad clicks.	|
|impressions	|Long	|Total ad impressions.	|
|view_impressions	|Long	|Total ad viewable impressions (for vCPM campaigns only).	|
|cost	|Double	|Total cost of all clicks (CPC) or click or view (vCPM). Expressed in local currency.	|

## Resource-based IAM policy

To subscribe to this dataset, ensure the correct IAM policy is added to the SQS queue based on the marketplace.

**NA**

```json
{
  "Version": "2008-10-17",
  "Id": "{REPLACE_WITH_ANY_VALUE}",
  "Statement": [
    {
      "Sid": "{REPLACE_WITH_ANY_VALUE}",
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "{REPLACE WITH ARN Of SQS DESTINATION QUEUE}",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-east-1:370941301809:*"
        }
      }
    },
    {
      "Sid": "{REPLACE_WITH_ANY_VALUE}",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

**EU**

```json
{
  "Version": "2008-10-17",
  "Id": "{REPLACE_WITH_ANY_VALUE}",
  "Statement": [
    {
      "Sid": "{REPLACE_WITH_ANY_VALUE}",
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "{REPLACE WITH ARN Of SQS DESTINATION QUEUE}",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:eu-west-1:947153514089:*"
        }
      }
    },
    {
      "Sid": "{REPLACE_WITH_ANY_VALUE}",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

**FE**

```json
{
  "Version": "2008-10-17",
  "Id": "{REPLACE_WITH_ANY_VALUE}",
  "Statement": [
    {
      "Sid": "{REPLACE_WITH_ANY_VALUE}",
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "{REPLACE WITH ARN Of SQS DESTINATION QUEUE}",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-west-2:310605068565:*"
        }
      }
    },
    {
      "Sid": "{REPLACE_WITH_ANY_VALUE}",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

## Data freshness

Initial click data is available within 12 hours of the ad click. As part of the traffic validation process, clicks can be invalidated during the following 72 hours post the initial report. Amazon Marketing Stream will automatically deliver any adjustments to your queue.