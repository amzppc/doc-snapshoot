---
title: Ads dataset
description: Details and schema for the ads dataset.
type: guide
interface: amazon-marketing-stream
tags:
    - Reporting
    - Campaign management
---

# Ads dataset (beta)

The ads dataset provides a snapshot of all your ad objects--including those related to Sponsored Products, Sponsored Brands, and Sponsored Display campaigns. 

## Dataset ID

To subscribe to this dataset, use `ads` as the ID.

## Schema

|Field	|Type	|Description	|
|----|----|----|
|ad.dataset_id	|string	|An identifier used to identify the dataset (in this case, `ads`).	|
|ad.advertiser_id	|string	|Unique identifier of the advertiser (standard in Stream datasets).	|
|ad.marketplace_id	|string	|The marketplace of the advertiser.	|
|ad.adId	|string	|Unique identifier of ad.	|
|ad.name	|string	|Name of ad, advertiser description.	|
|ad.accountId	|string	|Unique identifier of advertiser.	Also referred to as `profileId` in the API. |
|ad.adGroupId	|string	|Unique identifier of parent record (ad group ID).	|
|ad.campaignId	|string	|Unique identifier of campaign.	|
|ad.adProduct	|string	|Product/program type of campaign. One of: SPONSORED\_PRODUCTS, SPONSORED\_BRANDS, SPONSORED\_DISPLAY	|
|ad.customText	|string	|Custom text for KDP accounts. Sponsored Products only.	|
|ad.asin	|string	|The ASIN of the product advertised by the product ad. Relevant for Sponsored Products and Sponsored Display vendors and KDP. 	|
|ad.sku	|string	|The SKU of the product advertised by the product ad. Relevant only for sellers using Sponsored Products and Sponsored Display.	|
|ad.landingPage.url	|string	|Landing page URL for Sponsored Brands and Sponsored Display campaigns.	|
|ad.landingPage.type	|string	|Type of landing page. <br><br>Sponsored Brands: PRODUCT\_LIST, STORE, CUSTOM\_URL, DETAIL\_PAGE<br>Sponsored Display: STORE, MOMENT, OFF\_AMAZON\_LINK	|
|ad.state	|string	|The advertiser defined state.<br>Sponsored Products: ENABLED, PAUSED, ARCHIVED, USER\_DELETED, ENABLING, OTHER<br>Sponsored Brands/Sponsored Display: ENABLED, PAUSED, ARCHIVED	|
|ad.audit.creationDateTime	|datetime	|Creation time stamp in ISO8601 format.	|
|ad.audit.lastUpdatedDateTime	|datetime	|Last time record was updated timestamp in ISO8601 format.	|


## Sample payload

```json
{
  "dataset_id": "ads",
  "advertiser_id": "",
  "marketplace_id": "",
  "adId": "",
  "name": "",
  "accountId": "",
  "adGroupId": "",
  "campaignId": "",
  "adProduct": "",
  "customText": "",
  "asin": "",
  "sku": "",
  "landingPage": {
    "url": "",
    "type": ""
  },
  "state": "",
  "audit": {
    "creationDateTime": "",
    "lastUpdatedDateTime": ""
  }
}
```

## Resource-based IAM policy

To subscribe to this dataset, ensure the correct IAM policy is added to the SQS queue based on the marketplace.

**NA**

```json
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-east-1:305370293182:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

**EU**

```json
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:eu-west-1:648558082147:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}

```

**FE**

```json
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-west-2:802070757281:*"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

## Data freshness

You will receive a notification in near real time for every change in the configuration of an ad.