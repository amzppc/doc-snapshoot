---
title: Sponsored Brands clickstream dataset
description: Details and schema for the Sponsored Brands clickstream (sb-clickstream) dataset.
type: guide
interface: amazon-marketing-stream
tags:
    - Reporting
    - Campaign management
    - Sponsored Brands
---

# Sponsored Brands clickstream dataset (beta)

The sb-clickstream dataset contains detail page views and branded searches data related to Sponsored Brands campaigns.

>[NOTE] Amazon Marketing Stream only includes traffic deriving from Sponsored Brands version 4 (multi-ad group campaigns). Data related to any campaigns created using [POST /sb/campaigns](sponsored-brands/3-0/openapi#tag/Campaigns/operation/createCampaigns) before May 22, 2023 (in the US) or before August 10, 2023 (rest of the world) will not be sent. For more details,view the Sponsored Brands version 4 [migration guide](reference/migration-guides/sb-v3-v4). 

## Dataset ID

To subscribe to this dataset, use the `sb-clickstream` ID.

## Schema

|Field name	|Type	|Description	|
|------|-----|-------|
|idempotency\_id	|String	|An identifier than can be used to de-duplicate records.	|
|dataset\_id	|String	|An identifier used to identify the dataset (in this case, sb-clickstream).	|
|marketplace\_id	|String	|The [marketplace identifier](https://developer-docs.amazon.com/sp-api/docs/marketplace-ids) associated with the account.	|
|time\_window\_start	|String	|The start of the hour to which the performance data is attributed (ISO 8601 date time).	|
|campaign\_id	|String	|Unique numerical ID for a campaign.	|
|ad\_id	|String	|Unique numerical ID for the ad.	|
|ad\_group\_id	|String	|Unique numerical ID for an ad group.	|
|advertiser\_id	|String	|ID associated with the advertiser. You can retrieve this ID using the [GET v2/profiles](reference/2/profiles#/Profiles/listProfiles) endpoint. This value is **not** unique and may be the same across marketplaces. Note: For non-seller accounts, this advertiser_id is set to the entity ID.	|
|keyword\_id	|String	|ID of the keyword used for a bid.	|
|keyword\_text	|String	|Text of the keyword or phrase used for a bid.	|
|keyword\_type	|String	|The type of targeting used in the expression.	|
|placement\_type	|String	| The page location where an ad appeared. Possible values: Detail Page on-Amazon, Other on-Amazon, Top of Search on-Amazon.	|
|view\_attributed\_branded\_searches\_14d	|Long	|Total number of branded searches within 14 days of an ad view or click.	|
|attributed\_branded\_searches\_14d	|Long	|Total number of branded searches within 14 days of an ad click.	|
|view\_attributed\_detail\_page\_views\_14d	|Long	|Total number of detail page views within 14 days of an ad view or click.	|
|attributed\_detail\_page\_views\_14d	|Long	|Total number of detail page views within 14 days of an ad click.	|

## Resource-based IAM policy

To subscribe to this dataset, ensure the correct IAM policy is added to the SQS queue based on the marketplace.

**NA**

```json
{
  "Version": "2008-10-17",
  "Id": "{REPLACE_WITH_ANY_VALUE}",
  "Statement": [
    {
      "Sid": "{REPLACE_WITH_ANY_VALUE}",
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "{REPLACE WITH ARN Of SQS DESTINATION QUEUE}",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-east-1:091028706140:*"
        }
      }
    },
    {
      "Sid": "{REPLACE_WITH_ANY_VALUE}",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

**EU**

```json
{
  "Version": "2008-10-17",
  "Id": "{REPLACE_WITH_ANY_VALUE}",
  "Statement": [
    {
      "Sid": "{REPLACE_WITH_ANY_VALUE}",
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "{REPLACE WITH ARN Of SQS DESTINATION QUEUE}",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:eu-west-1:219513501272:*"
        }
      }
    },
    {
      "Sid": "{REPLACE_WITH_ANY_VALUE}",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

**FE**

```json
{
  "Version": "2008-10-17",
  "Id": "{REPLACE_WITH_ANY_VALUE}",
  "Statement": [
    {
      "Sid": "{REPLACE_WITH_ANY_VALUE}",
      "Effect": "Allow",
      "Principal": {
        "Service": "sns.amazonaws.com"
      },
      "Action": "SQS:SendMessage",
      "Resource": "{REPLACE WITH ARN Of SQS DESTINATION QUEUE}",
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": "arn:aws:sns:us-west-2:632322331982:*"
        }
      }
    },
    {
      "Sid": "{REPLACE_WITH_ANY_VALUE}",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::926844853897:role/ReviewerRole"
      },
      "Action": "SQS:GetQueueAttributes",
      "Resource": "*"
    }
  ]
}
```

## Data freshness

Conversions for Sponsored Brands campaigns are reported based on the hour when the click they are attributed to occurred. Conversion data is reported daily, weekly, and monthly, and you should expect to receive revisions to conversion data up to sixty days after the initial click. 