---
title: Documentation translations
description: An overview of available translations of the Amazon Ads API documentation.
type: guide
interface: api
tags:
    - translation
    - i18n
    - 日本語
    - 日本人
    - ja-jp
    - zh-cn
---

# Translations

Translations of advanced tools center content are available in the following languages:

- [Chinese](https://d3a0d0y2hgofx6.cloudfront.net/zh-cn/index.html)
- [Japanese](https://d3a0d0y2hgofx6.cloudfront.net/ja-jp/index.html)

The current translations reflect the status of documents in the advanced tools center as of July 2023. Some translated documents may not correlate one-to-one with the current documentation structure in the advanced tools center. 

Future updates will be noted.
