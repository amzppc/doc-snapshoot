---
title: IAS contextual targeting segments
description: A list of IAS contextual targeting segments
type: guide
interface: api
tags:
    - DSP
keywords:
    - line items
    - targeting
---

# IAS contextual targeting segments

## Full list of contextual segments

### Vertical

| Segment ID   | Segment name |
| ------------ | ------------ |
| "3006622" | "Agriculture & Farming" | 
| "3006625" | "Agriculture & Farming - Chicken Owner" | 
| "3006626" | "Agriculture & Farming - Duck Owner" | 
| "3006627" | "Agriculture & Farming - Horse Owner" | 
| "3006624" | "Agriculture & Farming - Livestock" | 
| "3006623" | "Agriculture & Farming - Organic Farming" | 
| "3008527" | "Automotive" | 
| "3006608" | "Automotive - Auto Parts & Auto Repair" | 
| "3006910" | "Automotive - Auto Racing" | 
| "3006605" | "Automotive - Auto Recalls" | 
| "3006604" | "Automotive - Auto Shows" | 
| "3005007" | "Automotive - City Car" | 
| "3006606" | "Automotive - Commercial Trucks" | 
| "3007857" | "Automotive - Electric & Hybrid Cars" | 
| "3005009" | "Automotive - Family Car" | 
| "3005022" | "Automotive - Luxury Car" | 
| "3005030" | "Automotive - Sport Car" | 
| "3005032" | "Automotive - SUV" | 
| "3006607" | "Automotive - Used Cars" | 
| "3008890" | "Automotive - Light-Duty Trucks" | 
| "3005036" | "Baby Care" | 
| "3005037" | "Baby Care - Baby Food" | 
| "3005044" | "Consumer Electronics - Apple/iPhone" | 
| "3005045" | "Consumer Electronics - Samsung" | 
| "3005038" | "Consumer Electronics - Gaming Consoles" | 
| "3005039" | "Consumer Electronics - Laptops, Desktops, PCs" | 
| "3005040" | "Consumer Electronics - Smartphones, Tablets" | 
| "3005041" | "Consumer Electronics - TV, Smart TV" | 
| "3005042" | "Consumer Electronics - Wearables" | 
| "3005043" | "Consumer Electronics - Appliances" | 
| "3007356" | "Education" | 
| "3006364" | "Energy" | 
| "3006365" | "Energy - Oil" | 
| "3006366" | "Energy - Coal" | 
| "3006367" | "Energy - Electricity" | 
| "3006368" | "Energy - Nuclear Energy" | 
| "3006369" | "Energy - Green Energy" | 
| "3006370" | "Energy - Natural Gas" | 
| "3005055" | "Entertainment - Music Events" | 
| "3005057" | "Entertainment - Pay-per-view Services" | 
| "3005061" | "Entertainment - Sports Events" | 
| "3008367" | "Entertainment - Women's Sports" | 
| "3005063" | "Entertainment - Theater Events" | 
| "3005053" | "Entertainment - Horror Movies & Books" | 
| "3005059" | "Entertainment - Romance Movies & Books" | 
| "3005046" | "Entertainment - Action/Adventure Movies & Books" | 
| "3005047" | "Entertainment - Anime/Animation Movies & Books" | 
| "3005048" | "Entertainment - Children/Family Movies & Books" | 
| "3005049" | "Entertainment - Brand Classic Movies & Books" | 
| "3005051" | "Entertainment - Crime Movies & Books" | 
| "3005050" | "Entertainment - Comedy Movies & Books" | 
| "3005054" | "Entertainment - LGBTQ+ Movies & Books" | 
| "3005065" | "Entertainment - Western Movies & Books" | 
| "3005064" | "Entertainment - War/Military Movies & Books" | 
| "3005058" | "Entertainment - Political Movies & Books" | 
| "3005060" | "Entertainment - Sci-fi/Fantasy Movies & Books" | 
| "3005062" | "Entertainment - Brand Sports Movies & Books" | 
| "3005052" | "Entertainment - Documentary" | 
| "3008574" | "Entertainment - Spanish & Latin Music" | 
| "3008509" | "Entertainment - Rap & Hip Hop Music" | 
| "3005066" | "Fashion - Kid's Apparel" | 
| "3005068" | "Fashion - Men's Fashion" | 
| "3005067" | "Fashion - Luxury" | 
| "3005069" | "Fashion - Women's Fashion" | 
| "3005070" | "Financial - Bank Account" | 
| "3005071" | "Financial - Business Bank Account" | 
| "3005072" | "Financial - Family Bank Account" | 
| "3005073" | "Financial - Credit Cards" | 
| "3005074" | "Financial - Loans & Mortgages" | 
| "3005075" | "Financial - Personal/Family Investments" | 
| "3005076" | "Food & Beverage - Beer" | 
| "3005077" | "Food & Beverage - Biscuits & Cereal" | 
| "3005078" | "Food & Beverage - Gourmet Food" | 
| "3005079" | "Food & Beverage - Healthy Food" | 
| "3007488" | "Food & Beverage - Meat" | 
| "3005080" | "Food & Beverage - Ready-to-eat Meal" | 
| "3005081" | "Food & Beverage - Salty Snacks" | 
| "3005082" | "Food & Beverage - Soft Drinks" | 
| "3005083" | "Food & Beverage - Spirits" | 
| "3005084" | "Food & Beverage - Sweet Snacks" | 
| "3005085" | "Food & Beverage - Vegetarian/Vegan" | 
| "3005086" | "Food & Beverage - Wine" | 
| "3005087" | "Gambling - Online Betting" | 
| "3005088" | "Gambling - Online Gambling" | 
| "3005089" | "Games & Toys - Children's Toys" | 
| "3005090" | "Games & Toys - Video & Online Games" | 
| "3005091" | "House Care - Cleaning Appliances" | 
| "3005092" | "House Care - Home Cleaning Products" | 
| "3005093" | "House Care - Washing Soap/Powder" | 
| "3005094" | "Insurance - Car Insurance" | 
| "3005095" | "Insurance - Health Insurance" | 
| "3005096" | "Insurance - Home Insurance" | 
| "3006616" | "Logistics & Transportation" | 
| "3006617" | "Logistics & Transportation - Air Freight" | 
| "3006618" | "Logistics & Transportation - Cargo Ship" | 
| "3006619" | "Logistics & Transportation - Freight Train" | 
| "3006620" | "Logistics & Transportation - Road Cargo" | 
| "3008524" | "Logistics & Transportation - Stock Shortage" | 
| "3005097" | "Personal Care - Body Care" | 
| "3005099" | "Personal Care - Makeup" | 
| "3005100" | "Personal Care - Men's Care" | 
| "3005102" | "Personal Care - Women's Hairstyling" | 
| "3005098" | "Personal Care - Hair Loss" | 
| "3005101" | "Personal Care - Plastic Surgery" | 
| "3005103" | "Pharma - Flu" | 
| "3005105" | "Pharma - Hay Fever" | 
| "3005106" | "Pharma - Headache & Migraine" | 
| "3005107" | "Pharma - Painkillers" | 
| "3005108" | "Pharma - Vitamins & Supplements" | 
| "3005119" | "Retail - Supermarkets & Department Stores" | 
| "3005112" | "Retail - Food" | 
| "3005111" | "Retail - Fashion" | 
| "3005109" | "Retail - Art & Entertainment" | 
| "3005110" | "Retail - Consumer Electronics" | 
| "3005115" | "Retail - Office Furniture" | 
| "3005117" | "Retail - Pharmaceuticals" | 
| "3005113" | "Retail - Home & Garden" | 
| "3005118" | "Retail - Sports Equipment" | 
| "3005114" | "Retail - Objects for Children" | 
| "3005116" | "Retail - Pet Food" | 
| "3005120" | "Society - Career Change" | 
| "3005122" | "Society - Donors & Volunteers/Philanthropy/Medical Fund" | 
| "3005121" | "Society - Outdoor Activities" | 
| "3005123" | "Society - Starting a Family" | 
| "3005124" | "Society - Sweet Sixteen" | 
| "3005125" | "Telco - Business Connectivity" | 
| "3005126" | "Telco - Family Voice" | 
| "3005127" | "Telco - Mobile" | 
| "3005128" | "Travel - Airline Tickets" | 
| "3006597" | "Travel - Asia" | 
| "3005129" | "Travel - Budget Travel" | 
| "3005130" | "Travel - Casino Vacations" | 
| "3006595" | "Travel - Central & South America" | 
| "3006596" | "Travel - Europe" | 
| "3005131" | "Travel - Family Holidays" | 
| "3005132" | "Travel - Luxury Holidays" | 
| "3006594" | "Travel - North America" | 
| "3005133" | "Travel - Senior Travel" | 
| "3005134" | "Travel - Traveling with Pets" | 
| "3005135" | "Wellness/Healthy Living - Diet" | 
| "3005138" | "Wellness/Healthy Living - Weight Loss" | 
| "3005137" | "Wellness/Healthy Living - Healthy Living" | 
| "3005136" | "Wellness/Healthy Living - Fitness" |
| "3009532" | "Automotive - Motorcycles" |
| "3010097" | "Consumer Electronics - Dell" |
| "3010098" | "Consumer Electronics - Acer" |
| "3010099" | "Consumer Electronics - Asus" |
| "3010096" | "Consumer Electronics -Lenovo" |
| "3009681" | "Food & Beverage - Japanese Food" |
| "3010239" | "Language - English" |
| "3010238" | "Language - Arabic" |
| "3010240" | "Language - Bulgarian" |
| "3010241" | "Language - Catalan" |
| "3010327" | "Language - Chinese Simplified" |
| "3010329" | "Language - Chinese Traditional" |
| "3010243" | "Language - Croatian" |
| "3010244" | "Language - Czech" |
| "3010245" | "Language - Danish" |
| "3010246" | "Language - Dutch" |
| "3010247" | "Language - Finnish" |
| "3010248" | "Language - French" |
| "3010249" | "Language - German" |
| "3010250" | "Language - Hungarian" |
| "3010251" | "Language - Hindi" |
| "3010252" | "Language - Indonesian" |
| "3010253" | "Language - Italian" |
| "3010254" | "Language - Japanese" |
| "3010255" | "Language - Korean" |
| "3010256" | "Language - Norwegian" |
| "3010257" | "Language - Polish" |
| "3010258" | "Language - Portuguese" |
| "3010259" | "Language - Romanian" |
| "3010260" | "Language - Russian" |
| "3010261" | "Language - Serbian" |
| "3010262" | "Language - Slovak" |
| "3010263" | "Language - Slovenian" |
| "3010264" | "Language - Spanish" |
| "3010265" | "Language - Swedish" |
| "3010266" | "Language - Thai" |
| "3010267" | "Language - Turkish" |
| "3010268" | "Language - Vietnamese" |
| "3010495" | "Language - Bengali" |
| "3010496" | "Language - Gujarati" |
| "3010497" | "Language - Marathi" |
| "3010498" | "Language - Punjabi" |
| "3010499" | "Language - Tamil" |
| "3010500" | "Language - Telugu" |
| "3010501" | "Language - Malayalam" |
| "3010502" | "Language - Kannada" |
| "3010503" | "Language - Urdu" |
| "3010504" | "Language - Estonian" |
| "3010505" | "Language - Greek" |
| "3010506" | "Language - Latvian" |
| "3010507" | "Language - Lithuanian" |
| "3010885" | "Language - Malay" |
| "3010886" | "Language - Persian" |
| "3010887" | "Language - Ukrainian" |
| "3010888" | "Language - Hebrew" |
| "3010889" | "Language - Belarusian" |
| "3012760" | "Language- Tagalog" |
| "3012757" | "Language - Filipino" |
| "3010449" | "Language - English - Positive and Neutral Content" |
| "3010450" | "Language - Arabic - Positive and Neutral Content" |
| "3010451" | "Language - Bulgarian - Positive and Neutral Content" |
| "3010452" | "Language - Catalan - Positive and Neutral Content" |
| "3010453" | "Language - Chinese Simplified - Positive and Neutral Content" |
| "3010454" | "Language - Chinese Traditional - Positive and Neutral Content" |
| "3010455" | "Language - Croatian - Positive and Neutral Content" |
| "3010456" | "Language - Czech - Positive and Neutral Content" |
| "3010457" | "Language - Danish - Positive and Neutral Content" |
| "3010458" | "Language - Dutch - Positive and Neutral Content" |
| "3010459" | "Language - Finnish - Positive and Neutral Content" |
| "3010460" | "Language - French - Positive and Neutral Content" |
| "3010461" | "Language - German - Positive and Neutral Content" |
| "3010462" | "Language - Hungarian - Positive and Neutral Content" |
| "3010463" | "Language - Hindi - Positive and Neutral Content" |
| "3010464" | "Language - Indonesian - Positive and Neutral Content" |
| "3010465" | "Language - Italian - Positive and Neutral Content" |
| "3010466" | "Language - Japanese - Positive and Neutral Content" |
| "3010467" | "Language - Korean - Positive and Neutral Content" |
| "3010468" | "Language - Norwegian - Positive and Neutral Content" |
| "3010469" | "Language - Polish - Positive and Neutral Content" |
| "3010470" | "Language - Portuguese - Positive and Neutral Content" |
| "3010471" | "Language - Romanian - Positive and Neutral Content" |
| "3010472" | "Language - Russian - Positive and Neutral Content" |
| "3010473" | "Language - Serbian - Positive and Neutral Content" |
| "3010474" | "Language - Slovak - Positive and Neutral Content" |
| "3010475" | "Language - Slovenian - Positive and Neutral Content" |
| "3010476" | "Language - Spanish - Positive and Neutral Content" |
| "3010477" | "Language - Swedish - Positive and Neutral Content" |
| "3010478" | "Language - Thai - Positive and Neutral Content" |
| "3010479" | "Language - Turkish - Positive and Neutral Content" |
| "3010480" | "Language - Vietnamese - Positive and Neutral Content" |
| "3010481" | "Language - Bengali - Positive and Neutral Content" |
| "3010482" | "Language - Gujarati - Positive and Neutral Content" |
| "3010483" | "Language - Marathi - Positive and Neutral Content" |
| "3010484" | "Language - Punjabi - Positive and Neutral Content" |
| "3010485" | "Language - Tamil - Positive and Neutral Content" |
| "3010486" | "Language - Telugu - Positive and Neutral Content" |
| "3010487" | "Language - Malayalam - Positive and Neutral Content" |
| "3010488" | "Language - Kannada - Positive and Neutral Content" |
| "3010489" | "Language - Urdu - Positive and Neutral Content" |
| "3010490" | "Language - Estonian - Positive and Neutral Content" |
| "3010491" | "Language - Greek - Positive and Neutral Content" |
| "3010492" | "Language - Latvian - Positive and Neutral Content" |
| "3010493" | "Language - Lithuanian - Positive and Neutral Content" |
| "3010895" | "Language - Malay - Positive and Neutral Content" |
| "3010896" | "Language - Persian - Positive and Neutral Content" |
| "3010897" | "Language - Ukrainian - Positive and Neutral Content" |
| "3010898" | "Language - Hebrew - Positive and Neutral Content" |
| "3010899" | "Language - Belarusian - Positive and Neutral Content" |
| "3012762" | "Language- Tagalog- Positive and Neutral Content " |
| "3012759" | "Language- Filipino - Positive and Neutral Content " |
| "3009653" | "Society - Retirement Planning" |

### Topical

| Segment ID   | Segment name |
| ------------ | ------------ |
| "3005141" | "Infectious Diseases & Outbreaks - Negative Sentiment" | 
| "3005139" | "Infectious Diseases & Outbreaks - Positive Sentiment" | 
| "3005140" | "Remote Working" | 
| "3006302" | "Diversity Inclusion" | 
| "3006362" | "Pollution" | 
| "3006363" | "Pollution - Positive Sentiment" | 
| "3006610" | "B2B" | 
| "3007987" | "Positive Content" | 
| "3008351" | "Luxury Goods" | 
| "3008644" | "Construction & Engineering" | 
| "3008629" | "Love" | 
| "3008630" | "Love & Romance" | 
| "3008791" | "True Crime" |
| "3009527" | "DIY & Home Improvement" |
| "3009821" | "Arts and Crafts DIY" |
| "3011902" | "News Homepages" |
| "3013168" | "News Opinion Sections" |
| "3012557" | "Culture & Community - Multicultural" |
| "3012519" | "Culture & Community - AAPI" |
| "3012514" | "Culture & Community - Latinx" |
| "3012493" | "Culture & Community - LGBTQIA+" |
| "3012679" | "Culture & Community - Black" |
| "3005200" | "WINTER - Valentine's Day" |
| "3005163" | "WINTER - Hanukkah" |
| "3005189" | "WINTER - Super Bowl" |
| "3005193" | "WINTER - The Grammys" |
| "3005195" | "WINTER - The Oscars" |
| "3005161" | "WINTER - Green Monday" |
| "3005150" | "WINTER - Consumer Electronics Show" |
| "3005157" | "WINTER - Fitness in the New Year" |
| "3005204" | "WINTER - Winter Getaway Travel" |
| "3005203" | "WINTER - Winter Apparel" |
| "3005205" | "WINTER - Winter Sports Equipment" |
| "3008526" | "WINTER - Lunar New Year" |
| "3008596" | "WINTER - Boxing Day" |
| "3008675" | "WINTER - FIFA Men's World Cup" |
| "3008680" | "WINTER - New Year's Resolutions" |
| "3005169" | "SPRING - Mother's Day" |
| "3005154" | "SPRING - Easter" |
| "3005186" | "SPRING - Spring Break" |
| "3005187" | "SPRING - Spring Fashion" |
| "3005188" | "SPRING - Spring Getaway" |
| "3005155" | "SPRING - Father's Day" |
| "3005159" | "SPRING - French Open" |
| "3005198" | "SPRING - UEFA Europa League Final" |
| "3005197" | "SPRING - UEFA Champions League Final" |
| "3005168" | "SPRING - Memorial Day" |
| "3005190" | "SPRING - Tax Day" |
| "3005153" | "SPRING - Earth Day" |
| "3005148" | "SPRING - Cinco de Mayo" |
| "3005158" | "SPRING - Free Comic Book Day" |
| "3005167" | "SPRING - Masters Tournament" |
| "3005173" | "SPRING - NBA Playoffs" |
| "3005174" | "SPRING - NCAA Basketball Playoffs" |
| "3005177" | "SPRING - NHL Playoffs" |
| "3005178" | "SPRING - MLB Opening Day" |
| "3005156" | "SUMMER - FIFA Women's World Cup" |
| "3005202" | "SUMMER - Wimbledon Grand Slam" |
| "3005164" | "SUMMER - ICC Cricket World Cup" |
| "3005194" | "SUMMER - The Open Championship" |
| "3005192" | "SUMMER - The Ashes" |
| "3005165" | "SUMMER - Independence Day" |
| "3005181" | "SUMMER - Ramadan" |
| "3005145" | "SUMMER - Beach Fashion/Swimwear" |
| "3005183" | "SUMMER - Seaside Vacations" |
| "3005182" | "SUMMER - Rugby World Cup" |
| "3005180" | "SUMMER - Premier League Opening Weekend" |
| "3005185" | "SUMMER - South by Southwest (SXSW)" |
| "3005170" | "SUMMER - Music Festivals (UK)" |
| "3005171" | "SUMMER - Music Festivals (US)" |
| "3005179" | "SUMMER - PGA Championship" |
| "3005201" | "SUMMER - The Vuelta a España" |
| "3005196" | "SUMMER - Tour de France" |
| "3005160" | "SUMMER - Giro d'Italia" |
| "3005172" | "SUMMER - Music Festivals (IT)" |
| "3006456" | "SUMMER - Olympics" |
| "3007631" | "SUMMER - UEFA European Championship" |
| "3008578" | "SUMMER - Australia Day" |
| "3005143" | "FALL - Back to School - Elementary and High School" |
| "3005144" | "FALL - Back to School - College and University" |
| "3005199" | "FALL - US Open" |
| "3005176" | "FALL - NFL Kickoff" |
| "3005162" | "FALL - Halloween" |
| "3005152" | "FALL - The Día de Muertos" |
| "3005191" | "FALL - Thanksgiving" |
| "3005149" | "FALL - Columbus Day" |
| "3005166" | "FALL - Labor Day" |
| "3005146" | "FALL - Black Friday" |
| "3005184" | "FALL - Small Business Saturday" |
| "3005151" | "FALL - Cyber Monday" |
| "3005147" | "FALL - Christmas" |
| "3005175" | "FALL - New York Fashion Week" |
| "3005142" | "FALL - Autumn Fashion" |
| "3008292" | "FALL - Diwali and Festive Season" |
| "3008293" | "FALL - Indian Wedding Season" |
| "3008366" | "FALL - 11.11 E-Commerce Sales" |
| "3006632" | "AUDIENCE PROXY - Starting a Family" |
| "3007499" | "AUDIENCE PROXY - Expecting Mothers" |
| "3006633" | "AUDIENCE PROXY - Marriage" |
| "3006634" | "AUDIENCE PROXY - New Career" |
| "3006635" | "AUDIENCE PROXY - Car Ownership" |
| "3006636" | "AUDIENCE PROXY - Home Ownership" |
| "3006637" | "AUDIENCE PROXY - Holiday Shoppers" |
| "3006638" | "AUDIENCE PROXY - Business Travelers" |
| "3006639" | "AUDIENCE PROXY - Small Business Owners" |
| "3006642" | "AUDIENCE PROXY - Self-Employed" |
| "3006643" | "AUDIENCE PROXY - Car Enthusiasts" |
| "3006644" | "AUDIENCE PROXY - Sports Enthusiasts" |
| "3006645" | "AUDIENCE PROXY - Fitness Enthusiasts" |
| "3006646" | "AUDIENCE PROXY - Music Enthusiasts" |
| "3006647" | "AUDIENCE PROXY - Tech Enthusiasts" |
| "3006819" | "AUDIENCE PROXY - Software and IT Professionals" |
| "3006893" | "AUDIENCE PROXY - Pet Owners" |
| "3007008" | "AUDIENCE PROXY - Sustainability & Environmentally Conscious" |
| "3007500" | "AUDIENCE PROXY - Gluten & Dairy Free" |
| "3007896" | "AUDIENCE PROXY - Smoking Cessation" |
| "3008357" | "AUDIENCE PROXY - Martial Arts Enthusiasts" |
| "3008475" | "AUDIENCE PROXY - Travel Enthusiasts" |
| "3008617" | "AUDIENCE PROXY - Movie & TV Enthusiasts" |
| "3009742" | "AUDIENCE PROXY - Social Media Enthusiasts" |
| "3013931" | "User Generated Content" |

### Contextual avoidance 

| Segment ID   | Segment name |
| ------------ | ------------ |
| "1500195" | "Infectious Diseases & Outbreaks - Negative Sentiment" | 
| "1500078" | "Infectious Diseases & Outbreaks" | 
| "1500671" | "Protests & Demonstrations" | 
| "1500672" | "Protests & Demonstrations - Negative Emotions" | 
| "1500693" | "Cyber Security" | 
| "1500691" | "Natural Disasters" | 
| "1500690" | "Corporate Culture & Labor Relations" | 
| "1500692" | "Sensitive Social Issues" | 
| "1500860" | "Japan Emperor" | 
| "1500902" | "Politics" | 
| "1500903" | "Politics - Negative Content" | 
| "1505819" | "Discrimination Avoidance" | 
| "1506278" | "Pollution" | 
| "1506279" | "Pollution - Negative Sentiment" | 
| "1506447" | "Thai Monarchy" | 
| "1506453" | "Olympics" | 
| "1507473" | "Pop Culture" | 
| "1506436" | "Pop Culture - Negative Content" | 
| "1507358" | "Smoking" | 
| "1507394" | "Animal Cruelty" | 
| "1507589" | "Conspiracy Theories" | 
| "1507610" | "Dating" | 
| "1507653" | "Death, Injury or Military Conflict" | 
| "1507654" | "Crime" | 
| "1507655" | "Sexual Content" | 
| "1507657" | "Arms & Ammunition" | 
| "1507658" | "Obscenities" | 
| "1507659" | "Online Piracy" | 
| "1507660" | "Terrorism" | 
| "1507661" | "Incitement of Hatred" | 
| "1507663" | "Illegal and Recreational Drugs" | 
| "1507080" | "JICDAQ Brand Safety Japan Specific" | 
| "1508646" | "British Royal Family" | 
| "1508815" | "Chinese Party Politics" | 
| "1508964" | "Arms and Ammunition - High Risk" | 
| "1508965" | "Arms and Ammunition - Medium Risk" | 
| "1508966" | "Crime - High Risk" | 
| "1508967" | "Crime - Medium Risk" | 
| "1508981" | "Cyber Security - High Risk" | 
| "1508982" | "Cyber Security - Medium Risk" | 
| "1508968" | "Death, Injury or Military Conflict - High Risk" | 
| "1508970" | "Death, Injury or Military Conflict - Medium Risk" | 
| "1508979" | "Illegal and Recreational Drugs - High Risk" | 
| "1508980" | "Illegal and Recreational Drugs - Medium Risk" | 
| "1508975" | "Incitement of Hatred - High Risk" | 
| "1508976" | "Incitement of Hatred - Medium Risk" | 
| "1508977" | "Obscenities - High Risk" | 
| "1508978" | "Obscenities - Medium Risk" | 
| "1508973" | "Online Piracy - High Risk" | 
| "1508974" | "Online Piracy - Medium Risk" | 
| "1508985" | "Sensitive Social Issues - High Risk" | 
| "1508986" | "Sensitive Social Issues - Medium Risk" | 
| "1508962" | "Sexual Content - High Risk" | 
| "1508963" | "Sexual Content - Medium Risk" | 
| "1508983" | "Terrorism - High Risk" | 
| "1508984" | "Terrorism - Medium Risk" |
| "1510542" | "British Royal Family" |
| "1509531" | "Anti-vaccine" |
| "1509884" | "Cost of Living" |
| "1509974" | "Arms and Ammunition - Medium Risk - Entertainment" |
| "1509975" | "Arms and Ammunition - Medium Risk - News" |
| "1509976" | "Arms and Ammunition - Medium Risk - Video Gaming" |
| "1509977" | "Crime - Medium Risk - Entertainment" |
| "1509978" | "Crime - Medium Risk - News" |
| "1509979" | "Crime - Medium Risk - Video Gaming" |
| "1509998" | "Cyber Security - Medium Risk - Entertainment" |
| "1509999" | "Cyber Security - Medium Risk - News" |
| "1510000" | "Cyber Security - Medium Risk - Video Gaming" |
| "1509980" | "Death, Injury or Military Conflict - Medium Risk - Entertainment" |
| "1509981" | "Death, Injury or Military Conflict - Medium Risk - News" |
| "1509982" | "Death, Injury or Military Conflict - Medium Risk - Video Gaming" |
| "1509983" | "Illegal and Recreational Drugs - Medium Risk - Entertainment" |
| "1509984" | "Illegal and Recreational Drugs - Medium Risk - News" |
| "1509985" | "Illegal and Recreational Drugs - Medium Risk - Video Gaming" |
| "1509986" | "Incitement of Hatred - Medium Risk - Entertainment" |
| "1509987" | "Incitement of Hatred - Medium Risk - News" |
| "1509988" | "Incitement of Hatred - Medium Risk - Video Gaming" |
| "1509989" | "Obscenities - Medium Risk - Entertainment" |
| "1509990" | "Obscenities - Medium Risk - News" |
| "1509991" | "Obscenities - Medium Risk - Video Gaming" |
| "1509992" | "Online Piracy - Medium Risk - Entertainment" |
| "1509993" | "Online Piracy - Medium Risk - News" |
| "1509994" | "Online Piracy - Medium Risk - Video Gaming" |
| "1509995" | "Sensitive Social Issues - Medium Risk - Entertainment" |
| "1509996" | "Sensitive Social Issues - Medium Risk - News" |
| "1509997" | "Sensitive Social Issues - Medium Risk - Video Gaming" |
| "1509971" | "Sexual Content - Medium Risk - Entertainment" |
| "1509972" | "Sexual Content - Medium Risk - News" |
| "1509973" | "Sexual Content - Medium Risk - Video Gaming" |
| "1510001" | "Terrorism - Medium Risk - Entertainment" |
| "1510002" | "Terrorism - Medium Risk - News" |
| "1510003" | "Terrorism - Medium Risk - Video Gaming" |
| "1510669" | "Misinformation" |
| "1510711" | "Qatar World Cup" |
| "1510878" | "Kids Content" |
| "1510727" | "Misinformation - High Risk" |
| "1510678" | "Misinformation - Medium Risk" |
| "1510679" | "Misinformation - Medium Risk - Entertainment" |
| "1510680" | "Misinformation - Medium Risk - News" |
| "1510681" | "Misinformation - Medium Risk - Video Gaming" |
| "1511375" | "Politics - Democratic Party " |
| "1511377" | "Politics - GOP" |
| "1512790" | "News Homepages" |
| "1512396" | "News Opinion Sections" |
| "1506621" | "Agriculture & Farming" |
| "1500079" | "Auto" |
| "1500080" | "Baby Products" |
| "1500096" | "Baby Products - Baby Care" |
| "1500097" | "Baby Products - Baby Food" |
| "1500081" | "Consumer Electronics" |
| "1500098" | "Consumer Electronics - Gaming Consoles" |
| "1500099" | "Consumer Electronics - Laptop, Desktop, PC" |
| "1500100" | "Consumer Electronics - Smartphone, Tablet" |
| "1500101" | "Consumer Electronics - TV, Smart TV" |
| "1500102" | "Consumer Electronics - Wearables" |
| "1500103" | "Consumer Electronics - Appliances" |
| "1507355" | "Education" |
| "1506280" | "Energy" |
| "1506282" | "Energy - Nuclear Energy" |
| "1506283" | "Energy - Green Energy" |
| "1506284" | "Energy - Natural Gas" |
| "1506285" | "Energy - Oil" |
| "1506286" | "Energy - Coal" |
| "1506287" | "Energy - Electricity" |
| "1500082" | "Entertainment" |
| "1500104" | "Entertainment - Music Events" |
| "1500105" | "Entertainment - Pay-per-view Services" |
| "1500106" | "Entertainment - Sports Events" |
| "1500107" | "Entertainment - Theatre Events" |
| "1500083" | "Fashion" |
| "1500108" | "Fashion - Kid's Apparel" |
| "1500111" | "Fashion - Luxury" |
| "1500109" | "Fashion - Men's Fashion" |
| "1500110" | "Fashion - Women's Fashion" |
| "1500084" | "Finance" |
| "1500113" | "Finance - Bank Account, Business" |
| "1500114" | "Finance - Bank Account, Family" |
| "1500112" | "Finance - Banking" |
| "1500115" | "Finance - Credit Cards" |
| "1510207" | "Finance - Cryptocurrency" |
| "1510208" | "Finance - Cryptocurrency - Negative Content" |
| "1500116" | "Finance - Loans & Mortgages" |
| "1500117" | "Finance - Personal/Family Investing" |
| "1500085" | "Food and Beverage" |
| "1500118" | "Food and Beverage - Beer" |
| "1500119" | "Food and Beverage - Biscuits, Cereals" |
| "1500120" | "Food and Beverage - Gourmet" |
| "1500121" | "Food and Beverage - Healthy Food" |
| "1500122" | "Food and Beverage - Ready-to-eat Meals" |
| "1500123" | "Food and Beverage - Salty Snacks" |
| "1500124" | "Food and Beverage - Soft Drinks" |
| "1500125" | "Food and Beverage - Spirits" |
| "1500126" | "Food and Beverage - Sweet Snacks" |
| "1500127" | "Food and Beverage - Vegan/Vegetarian" |
| "1500128" | "Food and Beverage - Wine" |
| "1500086" | "Gambling" |
| "1500129" | "Gambling - Online Betting" |
| "1500130" | "Gambling - Online Gambling" |
| "1500087" | "Games and Toys" |
| "1500131" | "Games and Toys - Children's Toys" |
| "1500132" | "Games and Toys - Video Games, Online Games" |
| "1500088" | "House Care" |
| "1500133" | "House Care - Cleaning Appliances" |
| "1500134" | "House Care - Household Cleaning Products" |
| "1500135" | "House Care - Washing Soap, Powder" |
| "1500089" | "Insurance" |
| "1500136" | "Insurance - Auto, Motor Insurance" |
| "1500137" | "Insurance - Health" |
| "1500138" | "Insurance - Home" |
| "1510285" | "Language - English" |
| "1510286" | "Language - Arabic" |
| "1510287" | "Language - Bulgarian" |
| "1510288" | "Language - Catalan" |
| "1510328" | "Language - Chinese Simplified" |
| "1510332" | "Language - Chinese Traditional" |
| "1510290" | "Language - Croatian" |
| "1510291" | "Language - Czech" |
| "1510284" | "Language - Danish" |
| "1510292" | "Language - Dutch" |
| "1510293" | "Language - Finnish" |
| "1510294" | "Language - French" |
| "1510295" | "Language - German" |
| "1510296" | "Language - Hungarian" |
| "1510297" | "Language - Hindi" |
| "1510298" | "Language - Indonesian" |
| "1510299" | "Language - Italian" |
| "1510300" | "Language - Japanese" |
| "1510301" | "Language - Korean" |
| "1510302" | "Language - Norwegian" |
| "1510303" | "Language - Polish" |
| "1510304" | "Language - Portuguese" |
| "1510305" | "Language - Romanian" |
| "1510306" | "Language - Russian" |
| "1510307" | "Language - Serbian" |
| "1510308" | "Language - Slovak" |
| "1510309" | "Language - Slovenian" |
| "1510310" | "Language - Spanish" |
| "1510311" | "Language - Swedish" |
| "1510312" | "Language - Thai" |
| "1510313" | "Language - Turkish" |
| "1510314" | "Language - Vietnamese" |
| "1510508" | "Language - Bengali" |
| "1510509" | "Language - Gujarati" |
| "1510510" | "Language - Marathi" |
| "1510511" | "Language - Punjabi" |
| "1510512" | "Language - Tamil" |
| "1510513" | "Language - Telugu" |
| "1510514" | "Language - Malayalam" |
| "1510515" | "Language - Kannada" |
| "1510516" | "Language - Urdu" |
| "1510517" | "Language - Estonian" |
| "1510518" | "Language - Greek" |
| "1510519" | "Language - Latvian" |
| "1510520" | "Language - Lithuanian" |
| "1510890" | "Language - Malay" |
| "1510891" | "Language - Persian" |
| "1510892" | "Language - Ukrainian" |
| "1510893" | "Language - Hebrew" |
| "1510894" | "Language - Belarusian" |
| "1512761" | "Language- Tagalog" |
| "1512758" | "Language - Filipino" |
| "1506611" | "Logistics & Transportation" |
| "1506612" | "Logistics & Transportation - Air Freight" |
| "1506613" | "Logistics & Transportation - Cargo Ship" |
| "1507359" | "Logistics & Transportation - Freight Train" |
| "1506615" | "Logistics & Transportation - Road Cargo" |
| "1500090" | "Personal Care" |
| "1500139" | "Personal Care - Body Care" |
| "1500140" | "Personal Care - Hair Loss" |
| "1500141" | "Personal Care - Makeup" |
| "1500142" | "Personal Care - Men's Care" |
| "1500143" | "Personal Care - Plastic Surgery" |
| "1500144" | "Personal Care - Women's Hair Styling" |
| "1500091" | "Pharma" |
| "1500092" | "Retail" |
| "1500151" | "Retail - Arts & Entertainment" |
| "1500152" | "Retail - Consumer Electronics" |
| "1500153" | "Retail - Fashion" |
| "1500154" | "Retail - Food" |
| "1500155" | "Retail - Home & Garden" |
| "1500156" | "Retail - Children's Toys & Objects" |
| "1500157" | "Retail - Office Furniture & Supplies" |
| "1500158" | "Retail - Pet Food" |
| "1500159" | "Retail - Pharmacies" |
| "1500160" | "Retail - Sports Equipment" |
| "1500161" | "Retail - Supermarkets & Department Stores" |
| "1500162" | "Career Change" |
| "1500166" | "Outdoor Activities " |
| "1500163" | "Philanthropy" |
| "1500164" | "Starting a Family" |
| "1500093" | "Telco" |
| "1500167" | "Telco - Business Connectivity" |
| "1500168" | "Telco - Mobile" |
| "1500094" | "Travel" |
| "1500169" | "Travel - Airline Tickets" |
| "1500170" | "Travel - Casino Vacations" |
| "1500171" | "Travel - Family Holidays" |
| "1500172" | "Travel - Luxury" |
| "1500173" | "Travel - Senior" |
| "1500174" | "Travel - Traveling with Pets" |
| "1500095" | "Wellness & Healthy Living" |
| "1500176" | "Wellness & Healthy Living - Diet" |
| "1500178" | "Wellness & Healthy Living - Fitness" |
| "1500175" | "Wellness & Healthy Living - Weight Loss" |
| "1513949" | "Made for Advertising Sites" |
| "1513948" | "Made for Advertising Sites and Sites with Ad Clutter" |
| "1513741" | "Brand Safety Vietnam Specific" |
| "1515904" | "Misinformation - Politics" |
| "1513584" | "User Generated Content" |

## Example

To use IAS contextual segment targeting, specify the relevant segment IDs in your line item's `target` object while creating or updating your line item.

The three available categories are vertical targeting segments, topical targeting segments, and contextual avoidance segments. See part of an example request below that makes use of all three categories.


```
...
"integralAdScience":
    "contextualTargeting": {
        "verticalSegments": [
            "3006622"
        ],
        "topicalSegments": [
            "3005140"
        ],
    },
    "contextualAvoidance": {
        "avoidanceSegments": [
            "1500195"
        ],
    }
...
```