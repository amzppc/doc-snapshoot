---
title: Prerequisites for DSP API tutorials
description: Prerequisites for DSP API tutorials, including registration and required authorization headers.
type: guide
interface: api
tags:
    - DSP
    - Authorization
---

# Prerequisites for DSP API tutorials

Each of the tutorials in this section walk you through the steps to create, read, and update resources using the DSP API. Before you can begin with any of the tutorials, you must have completed all of the prerequisites.

## Register for the DSP API

If you haven't already, you must [register for the DSP API](guides/onboarding/overview) and wait for an email confirmation with instructions to complete the onboarding process. If you have registered, make note of your client identifier and client secret.

## Get your security identifier values

The Amazon Ads APIs uses the Amazon Identity Provider to authenticate and authorize requests. This security scheme is consistent across all Amazon Ads products.

The Amazon Ad API requires three mandatory headers for each request:

* **Amazon-Advertising-API-ClientId**
* **Amazon-Advertising-API-Scope**
* **Authorization**

The `Amazon-Advertising-API-ClientId` header contains the client identifer that you registered with the Amazon Ads API platform. It represents a unique identity for your application. This value should not be shared with anyone else since it represents entitlement to your Amazon Ads API.

The `Amazon-Advertising-API-Scope` header contains the profile identifier of your entity. You can retrieve this identifier using the [profiles resource](reference/2/profiles#tag/Profiles).

The `Authorization` header is an industry-standard header that includes an OAuth access token. It follows the format: `Authorization: Bearer <access_token>`.

To learn more about creating an authorization token, see [creating API authorization and refresh tokens](guides/get-started/retrieve-access-token).

## Get your Amazon DSP API Advertiser identifier

Several of the DSP API resources in this set of tutorials require an Advertiser identifier as a parameter. To get a list of Advertiser identifiers associated with your Amazon DSP API account, retrieve the [Advertiser resource](dsp-advertiser/#tag/Advertiser/paths/~1dsp~1advertisers~1%7BadvertiserId%7D/get). For example:

```bash
GET /dsp/advertisers
```

An example response resembles:

```JSON
{
  "totalResults": 1100,
  "response": [
    {
      "advertiserId": "00000000000000",
      "name": "DSP Public API Advertiser",
      "currency": "USD",
      "url": "www.example.com",
      "country": "US",
      "timezone": "string"
    }
  ]
}
```

The Advertiser identifier is the value of the `advertiserId` field.

## Next steps

Now that you've completed all of the prerequisites, you can complete the following tutorials:

* [How to create and update an order](guides/dsp/tutorials/create-and-update-order)
* [How to create and update a line item](guides/dsp/tutorials/create-and-update-line-item)
* [How to add Creatives to and remove Creatives from Line Items](dsp/add-remove-creatives)

