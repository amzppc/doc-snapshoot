---
title: How to create and update a line item
description: Guide to creating and updating line items
type: guide
interface: api
tags:
    - DSP
keywords:
    - line items
---

# How to create and update a line item

## Prerequisites

If you haven't already, you must [complete the prerequisites for this tutorial](guides/dsp/tutorials/prerequisites).

For this tutorial, you'll require:

* Your Amazon Ads API client identifier.
* Your Amazon Ads API scope.
* An authorization token.
* Your Advertiser identifier.

Each Line Item must be associated with an order. When you create a Line Item, you specify the Order identifier to which the created Line Item is associated. You'll use the [Order resource](dsp-campaigns/#tag/Order) to retrieve this identifier. 

You'll also require values associated with product category identifiers because a line item must be associated with one or more product categories. Product category identifiers are required becasuse they're passed to advertising exchanges and Amazon uses them to determine whether a campaign is eligible to run. You'll use the [product categories resource](dsp-campaigns/#tag/Discovery/operation/getProductCategories) to retrieve product category identifiers.

There's also a set of optional fields that you can include when creating a line item:

### Supply source identifiers

The supply source identifier specifies the **inventory** that runs this line item. By default, the line item will run on all available real-time bidding exchanges, Amazon-owned sites, and Amazon Publisher Services inventory.

You'll use the [supply sources resource](dsp-campaigns/#tag/Discovery/operation/getSupplySources) to retrieve a list of supply source identifiers. 

Note that `lineItemType` and `supplySource` type are **required** parameters. When the supply source type is set to  `deal`, a value for `orderId` is required. The list of deal supply sources in the response is filtered to include those valid for the advertiser that owns the order and are running during the order dates only.

### Geo location tagging identifiers

You use geo location tagging identifiers to include or exclude users associated with a location. Location is based on each user's Amazon billing address postal code. If a postal code is not available, each user's IP address is used instead. 

You'll use the [geo location resource](dsp-campaigns/#tag/Discovery/operation/getGeoLocations) to retrieve identifiers that are used for line item targeting. 

The geo location resource enables you to retrieve identifiers by identifier or text query such as city name, zip code, or other address text.

### Audience identifiers

You can retrieve a list of possible audience segment identifiers that are used to specify targeting during line item creation.

You'll use the [audiences resource](dsp-campaigns/#tag/Discovery) to retrieve the list. 

> [NOTE] A targeting fee is applied to a line item if any in-market, lifestyle, and/or automotive segments are chosen. Amazon recommends setting up separate line items when combining segments with different fees, as the highest fee will apply to the entire line item.

### Third-party pre-bid targeting

You can retrieve a list of possible custom contextual segments associated with an account that has an existing link to a third-party data provider. 

You'll use the [pre-bid targeting resource](dsp-campaigns/#tag/Discovery) to retrieve the list of possible custom contextual segments. Currently, the supported providers are `doubleVerify`, `pixalate`, `integralAdScience`, and `oracleDataCloud`. 

For `integralAdScience`, there are three kinds of contextual segments you can use for targeting. For a full list of all segments, see [here](guides/dsp/tutorials/contextual-segments).

### IAB content categories

You can also target site according to their subject matter by scanning the target site's content. This ensures that ads deliver to relevant sites and not next to inappropriate content.

You'll use the [IAB content categories resource](dsp-campaigns/#tag/Discovery/operation/getIabContentCategories) to retrieve a list of IAB content category identifiers. 

> [NOTE] This setting doesn’t apply to Amazon owned-and-operated sites and these sites will ignore content categories selected. For video line items, using this targeting will block delivery to third-party mobile apps. Set up separate line items without this targeting if you want to run ads on mobile apps.

### Domain list identifiers

You can use the [domain lists resource](dsp-campaigns/#tag/Discovery/operation/getDomains) to get a list of domain resource identifiers that can be used for targeting during line item creation. 

## Creating a Line Item

You'll use the `POST` operation of the [Line Item resource](dsp-campaigns/#tag/LineItem) to create a line item. The following parameters are required when creating a Line Item:

* `lineItemType`: Determines where the line item will run. 
* `name`: A name for the line item.
* `orderId` The order identifier the created Line Item is associated with. This is the value you retrieved using the [Order resource](dsp-campaigns/#tag/Order) in the prerequisites. 
* `startDateTime` and `endDateTime`: The Line Item start and end date. The dates must be within the order’s flight date range.
* `lineItemClassification`
  * `productCategories`: The product category identifiers you retrieved using the [product categories resource](dsp-campaigns/#tag/Discovery) in the prerequisites.
* `bidding`
  * `baseSupplyBid`: Specifies the ideal amount you want to pay per thousand impressions for ad inventory. The value you set isn't guaranteed, but Amazon optimizes the bidding strategy to attempt to meet this goal.
* `frequencyCap`: Choose whether to set a limit on the number of times the same user can see any creatives in this line item within a given time period. The combined frequency of all line items will never exceed the order’s frequency, regardless of line item settings. It is highly recommended to set a frequency cap. An unlimited frequency could result in overexposure, increased costs per unique user device, and a poor user experience.
* `optimization`
  * `budgetOptimization`: Optimization can be enabled for a line item only if optimization is also enabled for the Order to which the Line Item is associated. Set to `true` to enable automatic budget allocation across eligible Line Items in the order.

An example Line Item request resembles:

```JSON
[
    {
        "lineItemType": "STANDARD_DISPLAY",
        "name": "This is my user friendly LineItem Name",
        "orderId": 5155*****0601,
        "externalId": "This is my external Id to uniquely identify this resource",
        "startDateTime": "2021-12-11 05:00:00 UTC",
        "endDateTime": "2021-12-19 02:59:00 UTC",
        "comments": "Additional Comments",
        "lineItemClassification": {
            "productCategories": [
                "3512**********2141",
                "3506**********6463",
                "3156**********1919"            
             ]
        },
        "frequencyCap": {
            "type": "CUSTOM",
            "maxImpressions": 3,
            "timeUnitCount": 5,
            "timeUnit": "DAYS"
        },
        "targeting": {
            "standardDisplayTargeting": {
                "userLocationTargeting": "US",
                "segmentTargeting": {
                    "segmentGroups": [
                        {
                            "segments": [
                                {
                                    "segmentId": "4116**********8920"
                                },
                                {
                                    "segmentId": "3945**********8988"
                                }
                            ],
                            "intraOperator": "AND",
                            "interOperator": "ALL"
                        },
                        {
                            "segments": [
                                {
                                    "segmentId": "4307**********8675"
                                },
                                {
                                    "segmentId": "3670**********5734"
                                }
                            ],
                            "intraOperator": "OR",
                            "interOperator": "ANY"
                        }
                    ]
                },
                "amazonViewabilityTargeting": {
                    "viewabilityTier": "VIEWABILITY_TIER_GT_60",
                    "includeUnmeasurableImpressions": true
                },
                "thirdPartyPreBidTargeting": {
                    "doubleVerify": {
                        "brandSafety": {
                            "highSeverityContent": [
                                "ADULT_CONTENT",
                                "DRUGS_SUBSTANCES",
                                "EXTREME_GRAPHICS_VIOLENCE_WEAPONS",
                                "HATE_SPEECH_PROFANITY",
                                "ILLEGAL_ACTIVITIES",
                                "INCENTIVIZED_MALWARE_CLUTTER",
                                "PIRACY_COPYRIGHT_INFRINGEMENT"
                            ],
                            "mediumSeverityContent": [
                                "AD_SERVER",
                                "ADULT_CONTENT",
                                "CELEBRITY_GOSSIP",
                                "CULTS_SURVIVALISM",
                                "DISASTER_AVIATION",
                                "DISASTER_MAN_MADE",
                                "DISASTER_NATURAL",
                                "DISASTER_TERRORISTS_EVENTS",
                                "DISASTER_VEHICLE",
                                "DRUGS_ALCOHOL",
                                "DRUGS_SMOKING",
                                "GAMBLING",
                                "INFLAMMATORY_POLITICS_NEWS",
                                "NEGATIVE_NEWS_FINANCIAL",
                                "NEGATIVE_NEWS_PHARMACEUTICAL",
                                "NON_STANDARD_CONTENT_NON_ENGLISH",
                                "NON_STANDARD_CONTENT_PARKING_PAGE",
                                "OCCULT",
                                "SEX_EDUCATION",
                                "UNMODERATED_UGC_FORUMS_IMAGES_VIDEO"
                            ],
                            "unknownContent": true
                        },
                        "fraudInvalidTraffic": {
                            "excludeImpressions": true,
                            "excludeAppsAndSites": "FRAUD_TRAFFIC_LEVEL_GTE_50",
                            "blockAppAndSites": true
                        },
                        "authenticBrandSafety": {
                            "doubleVerifySegmentId": "514**434"
                        },
                        "viewability": {
                            "mrcViewabilityTargeting": "MRC_VIEWABILITY_GTE_80",
                            "brandExposureViewabilityTargeting": "BRAND_EXPOSURE_VIEWABILITY_GTE_15_SEC_AVG_DURATION",
                            "includeUnmeasurableImpressions": true
                        },
                        "customContextualSegmentId": "520**712"
                    },
                    "oracleDataCloud": {
                        "brandSafety": {
                            "targetingOption": "ESSENTIAL_PROTECTION",
                            "essentialProtection": [
                                "ADULT",
                                "ARMS",
                                "CRIME",
                                "INJURY",
                                "PIRACY",
                                "DRUGS",
                                "HATE_SPEECH",
                                "MILITARY",
                                "OBSCENITY",
                                "TERRORISM",
                                "TOBACCO"
                            ]
                        },
                        "fraudInvalidTraffic": "FRAUD_INVALID_TRAFFIC_MAXIMUM_PROTECTION",
                        "customSegmentId": "255**888",
                        "contextualPredictsSegmentId": "259**212"
                    },
                    "integralAdScience": {
                        "brandSafety": {
                            "iasBrandSafetyAdult": "BRAND_SAFETY_EXCLUE_HIGH_RISK",
                            "iasBrandSafetyAlcohol": "ALLOW_ALL",
                            "iasBrandSafetyGambling": "ALLOW_ALL",
                            "iasBrandSafetyHateSpeech": "BRAND_SAFETY_EXCLUE_HIGH_AND_MODERATE_RISK",
                            "iasBrandSafetyIllegalDownloads": "ALLOW_ALL",
                            "iasBrandSafetyIllegalDrugs": "ALLOW_ALL",
                            "iasBrandSafetyOffensiveLanguage": "ALLOW_ALL",
                            "iasBrandSafetyViolence": "ALLOW_ALL",
                            "excludeContent": true
                        },
                        "fraudInvalidTraffic": "FRAUD_INVALID_TRAFFIC_EXCLUDE_HIGH_RISK",
                        "viewability": {
                            "standard": "PUBLICIS",
                            "viewabilityTargeting": "VIEWABILITY_TIER_GT_60"
                        }
                    }
                },
                "supplyTargeting": {
                    "supplySourceTargeting": {
                        "supplySources": [
                            "3156**********8896",
                            "2997**********6584",
                            "3576**********8825",
                            "3497**********8740",
                            "3242**********7683"
                        ]
                    }
                },
                "geoLocationTargeting": {
                    "locationTargetingBy": "IPADDRESS_POSTALCODE",
                    "inclusions": [
                        "XHvCjcKHXmJ7woVowo7CjmvCln5swozCncKawrLCrsKoZQ=="
                    ],
                    "exclusions": [
                        "XHvCjcKHXsKUwotpwonCkg=="
                    ]
                },
                "dayPartTargeting": {
                    "timeZonePreference": "AD_SERVER_TIMEZONE",
                    "dayParts": [
                        {
                            "hourSlots": [
                                0,
                                1,
                                2,
                                3,
                                4,
                                5,
                                6,
                                7,
                                8,
                                9,
                                10,
                                11,
                                12,
                                13,
                                14,
                                16,
                                17,
                                18,
                                19,
                                20,
                                21,
                                22,
                                23
                            ],
                            "dayOfWeek": "SUNDAY"
                        },
                        {
                            "hourSlots": [
                                0,
                                1,
                                2,
                                3,
                                5,
                                6,
                                7,
                                8,
                                9,
                                10,
                                11,
                                12,
                                14,
                                15,
                                16,
                                17,
                                18,
                                19,
                                21,
                                22,
                                23
                            ],
                            "dayOfWeek": "TUESDAY"
                        },
                        {
                            "hourSlots": [
                                0,
                                1,
                                2,
                                3,
                                4,
                                5,
                                6,
                                7,
                                8,
                                9,
                                10,
                                11,
                                12,
                                13,
                                14,
                                15,
                                16,
                                17,
                                18,
                                19,
                                20,
                                21,
                                22,
                                23
                            ],
                            "dayOfWeek": "WEDNESDAY"
                        },
                        {
                            "hourSlots": [
                                0,
                                1,
                                3,
                                4,
                                5,
                                6,
                                7,
                                8,
                                9,
                                10,
                                12,
                                13,
                                14,
                                15,
                                16,
                                18,
                                19,
                                20,
                                21,
                                22,
                                23
                            ],
                            "dayOfWeek": "THURSDAY"
                        },
                        {
                            "hourSlots": [
                                0,
                                1,
                                2,
                                3,
                                4,
                                5,
                                6,
                                7,
                                8,
                                9,
                                10,
                                11,
                                12,
                                13,
                                14,
                                15,
                                16,
                                17,
                                18,
                                19,
                                20,
                                21,
                                22,
                                23
                            ],
                            "dayOfWeek": "FRIDAY"
                        },
                        {
                            "hourSlots": [
                                0,
                                1,
                                2,
                                3,
                                4,
                                5,
                                6,
                                8,
                                9,
                                10,
                                11,
                                12,
                                13,
                                14,
                                15,
                                16,
                                17,
                                18,
                                19,
                                20,
                                21,
                                22,
                                23
                            ],
                            "dayOfWeek": "SATURDAY"
                        }
                    ]
                },
                "domainListTargeting": {
                    "inheritFromAdvertiser": true
                },
                "deviceTypeTargeting": "DESKTOP_AND_MOBILE",
                "siteLanguageTargeting": "ES",
                "contentTargeting": [
                    "3487**********2680",
                    "3099**********1246",
                    "3221**********5754",
                    "2895**********2704",
                    "3151**********2995"
                ],
                "contextualTargeting": false
            }
        },
        "budget": {
            "budgetCaps": [
                {
                    "amount": 1.0,
                    "recurrenceTimePeriod": "DAILY"
                },
                {
                    "amount": 13.0,
                    "recurrenceTimePeriod": "MONTHLY"
                }
            ],
            "pacing": {
                "deliveryProfile": "EVENLY",
                "catchUpBoost": "CATCH_UP_BOOST_2X"
            }
        },
        "appliedFees": {
            "thirdPartyFees": [
                {
                    "providerName": "INTEGRAL_AD_SCIENCE",
                    "feeAmount": 0.4,
                    "feeAllocation": "ABSORB_WITH_AGENCY_FEE"
                },
                {
                    "providerName": "DOUBLE_VERIFY",
                    "feeAmount": 0.5,
                    "feeAllocation": "PASS_TO_ADVERTISER"
                },
                {
                    "providerName": "DOUBLE_CLICK_CAMPAIGN_MANAGER",
                    "feeAmount": 0.6,
                    "feeAllocation": "ABSORB_WITH_AGENCY_FEE"
                },
                {
                    "providerName": "COM_SCORE",
                    "feeAmount": 0.7,
                    "feeAllocation": "PASS_TO_ADVERTISER"
                },
                {
                    "providerName": "CPM_1",
                    "feeAmount": 0.8,
                    "feeAllocation": "ABSORB_WITH_AGENCY_FEE"
                },
                {
                    "providerName": "CPM_2",
                    "feeAmount": 0.9,
                    "feeAllocation": "ABSORB_WITH_AGENCY_FEE"
                },
                {
                    "providerName": "CPM_3",
                    "feeAmount": 0.2,
                    "feeAllocation": "ABSORB_WITH_AGENCY_FEE"
                }
            ]
        },
        "bidding": {
            "baseSupplyBid": 1.5,
            "maxSupplyBid": 5.0
        },
        "optimization": {
            "budgetOptimization": true
        }
    }
]
```

## Updating a Line Item

You'll use the PUT operation of the [Line Item resource](dsp-campaigns/#tag/LineItem) to update the values associated with a Line Item that you specify by identifier. You must submit a complete Line Item resource with updated field values for the fields you wish to update, and the remaining fields unchanged. 

To begin, retrieve the Line Item you want to update using the GET operation of the [Line Item resource](dsp-campaigns/#tag/LineItem). 

Next, update the individual field values you wish to update. Finally, update the Line Item using the PUT operation of the [Line Item resource](dsp-campaigns/#tag/LineItem).