---
title: How to create and update an order
description: A tutorial demonstrating how to create and update an Order using the Amazon DSP API
type: guide
interface: api
tags:
    - DSP
keywords:
    - orders
---

# How to create and update an order

## Prerequisites

If you haven't already, you must [complete the prerequisites for this tutorial](guides/dsp/tutorials/prerequisites).

For this tutorial, you'll require:

* Your Amazon Ads API client identifier.
* Your Amazon Ads API scope.
* An authorization token.
* Your Advertiser identifier.

You'll also require values associated with an optimization goal and key performance indicator (KPI). You'll use the [Goal Configurations resource](dsp-campaigns/#tag/Discovery/operation/getGoalConfigurations) to retrieve these values. 

## Creating an order

You'll use the `POST` operation of the [Order resource](dsp-campaigns/#tag/Order) to create an order. You can specify values for your Order such as flight dates, budget, optimization goals and other settings you’d like your campaign to use. The following parameters are required when creating an Order:

* `advertiserId`: The advertiser identifier under which the order will be created. You retrieved the value for this field in the [prerequisites](guides/dsp/tutorials/prerequisites). 
* `name`: A name for the order. Note that this value can’t be changed after the Order ends.
* `budget`: The budget and flight dates for the order. 
  * `startDateTime` and `endDateTime`: This sets a limit on the date range the Order's associated Line Items can run. Note that the start date can't be updated once the Order is running. The end date can be updated up to five days after the order has ended.
  * `amount`: The budget amount.
* `frequencyCap`: Sets the frequency cap type. You can set the frequency cap type to `UNCAPPED` or define a `CUSTOM` type by specifying the number of impressions per time unit. 
* `optimization`: Sets `productLocation`, `goal`, and `goalKpi`. You retrieved these values using the [Goal Configurations resource](dsp-campaigns/#tag/Discovery/operation/getGoalConfigurations) in the prerequisites section.

An example order request resembles:

```JSON
[{
    "advertiserId": "170xxxxxxxx01",
    "name": "This is my user friendly Order Name",
    "externalId": "This is my external Id to uniquely identify this resource",
    "comments": "Additional comments",
    "budget": {
        "budgetCaps": [{
            "amount": 2,
            "recurrenceTimePeriod": "DAILY"
        }],
        "flights": [{
            "startDateTime": "2021-12-11 05:00:00 UTC",
            "endDateTime": "2021-12-19 02:59:00 UTC",
            "amount": 120,
        }]
    },
    "agencyFee": {
        "feePercentage": 2
    },
    "frequencyCap": {
        "type": "CUSTOM",
        "maxImpressions": 3,
        "timeUnitCount": 5,
        "timeUnit": "DAYS"
    },
    "optimization": {
        "productLocation": "SOLD_ON_AMAZON",
        "goal": "AWARENESS",
        "goalKpi": "CLICK_THROUGH_RATE",
        "autoOptimizations": [
            "BUDGET"
        ],
        "biddingStrategy": "SPEND_BUDGET_IN_FULL"
    }
}]
```

## Updating an order

You'll use the PUT operation of the [Order resource](dsp-campaigns/#tag/Order) to update the values associated with an Order that you specify by identifier. You must submit a complete Order resource with updated field values for the fields you wish to update, and the remaining fields unchanged. 

To begin, retrieve the Order you want to update using the GET operation of the [Order resource](dsp-campaigns/#tag/Order).

Next, update the individual field values you wish to update. 

Finally, update the Order using the PUT operation of the [Order resource](dsp-campaigns/#tag/Order).