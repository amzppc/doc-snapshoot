---
title: How to add update product tracking using productFile
description: How to add update product tracking using productFile
type: guide
interface: api
tags:
    - DSP
keywords:
    - product tracking
---

# How to add update product tracking 

## Prerequisites

If you haven't already, you must [complete the prerequisites for this tutorial](https://advertising.amazon.com/API/docs/en-us/guides/dsp/tutorials/prerequisites).

## Update product tracking using a CSV file

You can update product tracking using a CSV file. The format of the CSV file must be:

| ASIN | Featured | Domain |
|------|----------|--------|
| Your ASIN | | Domain to match |

You can use the following CSV template to create your own CSV file:

```bash
ASIN,Featured,Domain,,*** Instructions ***
<Enter ASIN here>,,<Enter Domain here>,,Please note that the header row is required to ensure all ASINs are uploaded
,,,,"Enter ASIN list using the following guidelines for ""Featured"" and ""Domain"" columns:"
,,,,"1) Use ""X"" to mark as a featured ASIN, leave it empty otherwise (note: Out of Stock alerts will not be sent for orders with more than 100 featured ASINs)"
,,,,2) Match each ASIN to one of the following Amazon domains in NA:
,,,,amazon.com
,,,,amazon.ca
,,,,Prime Now US
,,,,Prime Now CA
,,,,Prime Video ROW-NA
,,,,Whole Foods Market US
```

Once you've created your CSV file, you'll first use the [/dsp/fileUploads/policy/](dsp-campaigns/#tag/FileUploads/operation/createFileUploadsPolicy) resource to create an [AWS S3 policy](https://docs.aws.amazon.com/AmazonS3/latest/API/sigv4-authentication-HTTPPOST.html). 

For example:

```
{
    "url": "https://amazon-dsp-api-368306839028-us-east-1.s3.amazonaws.com/",
    "fields": {
        "key": "product/2021/5/24/b27d75a9-b012-4c25-9122-5714537b7b0f.csv",
        "x-amz-algorithm": "AWS4-HMAC-SHA256",
        "x-amz-credential": "ASIAVLQGMLX2BUFP5NQX/20210524/us-east-1/s3/aws4_request",
        "x-amz-date": "20210524T050135Z",
        "x-amz-security-token": "YOUR-TOKEN-HERE",
        "policy": "YOUR-POLICY-HERE",
        "x-amz-signature": "YOUR-SIGNATURE-HERE"
    }
}
```

Once you've successfully created the AWS S3 policy, you'll upload the CSV to Amazon S3.

For example, you can use [python request](https://boto3.amazonaws.com/v1/documentation/api/1.11.4/guide/s3-presigned-urls.html#generating-a-presigned-url-to-upload-a-file) or [javascript request](https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/modules/_aws_sdk_s3_presigned_post.html#post-file-using-formdata-in-nodejs)

You can use any programming language that supports HTTP POST operations.

Once the file has been successfully uploaded to Amazon S3, you can update product tracking using the `PUT` operation [/dsp/orders/{orderId}/conversionTracking/products](dsp-campaigns/#tag/Order/paths/~1dsp~1orders~1%7BorderId%7D~1conversionTracking~1products/put) resource with the Amazon S3 path in the `productFile` parameter. For example:

```JSON
{ 
  "productFile": "product/2021/5/24/b27d75a9-b012-4c25-9122-5714537b7b0f.csv" 
}
```

Note that if the product tracking update is successful, you will receive an `HTTP 204 - No content` response. 

## Update product tracking using a JSON list

To update product tracking using a JSON list, you'll use the `PUT` operation of the [/dsp/orders/{orderId}/conversionTracking/products](dsp-campaigns/#tag/Order/paths/~1dsp~1orders~1%7BorderId%7D~1conversionTracking~1products/put) resource. It takes the following parameters:

* `productId`: Also known as ASIN. A product's ASIN can be found in the product details section of the product's detail page. Separate ASINs with a space, comma, tab, or new line.
* `productAssociation`: A featured ASIN is a specific product that’s promoted in creatives within this order. Tracking non-featured ASINs or variations, in addition to featured and parent ASINs, will ensure proper conversion metrics and prevent data loss that is not recoverable once a campaign starts.
* `domain`: Amazon domain.

For example:

```JSON
{
    "productList": [
        {
            "productId": "B004VB9OVK",
            "productAssociation": "FEATURED",
            "domain": "AMAZON_US"
        },
        {
            "productId": "B07HJW9HKL",
            "productAssociation": "FEATURED",
            "domain": "AMAZON_US"
        }
    ]
}
```

Note that if the product tracking update is successful, you will receive an `HTTP 204 - No content` response. 