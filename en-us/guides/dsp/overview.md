---
title: Amazon DSP API overview
description: An overview of the Amazon DSP API
type: guide
interface: api
tags:
    - DSP
---

# Amazon DSP API overview

> [WARNING] The current DSP Campaign Management API, version 3, is on a deprecation path. Please keep an eye on the [release notes](release-notes/index) for the new API.

Amazon DSP is a demand-side platform (DSP) that enables advertisers to programmatically buy display, video, and audio ads both on and off Amazon. Using Amazon's DSP, you can reach audiences across the web on both Amazon sites and apps as well as through our publishing partners and third-party exchanges. Amazon DSP is available to both advertisers who sell products on Amazon and those who do not. 
 
The Amazon DSP API provides programmatic access to Amazon's DSP for self-service customers. API functionality for Campaign Management is currently available for display and video ads in beta in the US, Canada, Mexico, and Brazil. API functionality for Reporting is Generally Available for customers in the US, CA, MX, DE, ES, FR, IT, ES, NL, UK, AU, IN, JP, UAE, SE, DK, FI, NO, and SA.
 
Advertisers, agencies, and integrators with active DSP self-serve entities or who are working on behalf of an advertiser with an active DSP self-serve entity can request access to Amazon DSP API [here](https://advertising.amazon.com/about-api). If you already have access to Sponsored Ads API, you do not need to request access again. If you are interested in obtaining a DSP self-serve entity, contact your Amazon Ads representative or submit a request [here](https://advertising.amazon.com/contact-sales).

> [NOTE] The [DSP reporting API](dsp-reports-beta-3p) no longer requires the `Amazon-Advertising-API-Scope header` and instead includes the `accountId` URL parameter. The `accountid` parameter takes either a DSP entity id, an advertiser identifier or, in the future, a manager account identifier. 

## Next steps

Once you have access to the DSP API, learn about the [prerequisites](guides/dsp/tutorials/prerequisites) before moving on to the tutorials:

* [How to create and update an order](guides/dsp/tutorials/create-and-update-order)
* [How to create and update a line item](guides/dsp/tutorials/create-and-update-line-item)
* [How to add Creatives to and remove Creatives from Line Items](guides/dsp/tutorials/add-remove-creatives)
