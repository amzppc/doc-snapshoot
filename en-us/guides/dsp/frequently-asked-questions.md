---
title: Amazon DSP API Frequently Asked Questions
description: Frequently asked questions and answers for using the Amazon DSP API
type: guide
interface: api
tags:
    - DSP
keywords:
    - frequently asked questions
    - FAQ
---

# Amazon DSP API frequently asked questions

This FAQ includes general questions about the Amazon DSP API, questions about generating reports using the Amazon DSP API, and questions about the Amazon DSP Campaign Management API open beta.

## General 

**Who can request access to Amazon DSP functionality via the Amazon Ads API?**

Advertisers, agencies, and integrators with active DSP self-serve entities or who are working on behalf of an advertiser with an active DSP self-serve entity can request access to Amazon DSP API [here](https://advertising.amazon.com/about-api). If you are interested in obtaining a DSP self-serve entity, contact your Amazon Ads representative or submit a request [here](https://advertising.amazon.com/contact-sales).

**If I already have access to Sponsored Ads API or other Amazon Ads APIs, do I need to do anything additional for DSP API access?**

No, your access to Amazon Ads’s API covers all API functionalities. You can use the same client ID and Login with Amazon account for all API functions. 

**How do I request access to Amazon DSP API functionality?**

You can apply for API access [here](https://advertising.amazon.com/about-api) by selecting **Amazon DSP API** when asked “What do you plan to manage via the API?”

**How do I get started with Amazon DSP API functionality?**

To get started, follow the [Amazon Ads account setup steps](https://advertising.amazon.com/API/docs/en-us/guides/onboarding/overview).

**Will I be given technical support to integrate with the Amazon DSP API?**

The Amazon DSP API is a self-serve product. We provide reference documentation needed to successfully integrate with the API on this site. Once approved for API access, you will be granted access to [JIRA](https://amzn-clicks.atlassian.net/servicedesk/customer/portal/2/group/2/create/6) where limited technical support is available.

**Can I grant my agency or my other partner’s access to the Amazon DSP API? Can they integrate the API for me?**

Yes, you can invite your own development team or a trusted third party to access your entity information through the API. For steps to invite others to your entity, please follow the process outlined [here](https://advertising.amazon.com/API/docs/en-us).

## Campaign Management API open beta

**What are these Amazon DSP APIs for?**

Using Amazon DSP, you can: 

1. Create, read, and update Orders and Line Items.
2. Add and remove creatives to and from Line Items, including updating creative start/ends dates and creative weighting.
3. Read domain lists, audiences, IAB content categories, product categories, supply sources, pixels, and creatives. This enables you to automate campaign set-up and optimization and reduce the time you spend on manual operations. Additionally, you can include Amazon DSP campaign management in a centralized tool management platform where you manage all of your advertising channels. 

**What is available?** 

The Amazon DSP API open beta supports campaign management for Display campaigns only. Support for other ad products such as Video will be added in future expansions. 

**Where is the Amazon DSP API open beta available?**

The open beta is currently available in the US only. Support for other marketplaces will be added in 2021.

**How do I use these APIs?**

* [How to create and update an order](guides/dsp/tutorials/create-and-update-order)
* [How to create and update a line item](guides/dsp/tutorials/create-and-update-line-item)
* [How to add Creatives to and remove Creatives from Line Items](dsp/add-remove-creatives)

**Are Campaign Management APIs idempotent?**

Resources that support the `GET` operation are idempotent. Resources that support `POST` and `PUT` are not yet idempotent. If the response includes an HTTP response status between 501 and 599, verify that either the resource was **not** created or the update to a resource **did not** persist before retrying the operation.

**Are there rate limit rules specific to Amazon DSP campaign management APIs that we should be aware of?**

With the Open Beta launch, we are imposing request limits at an API integration (client) level to protect the Amazon DSP infrastructure and protect all customers from being impacted by a single partner’s actions. Our current guidance is the following, subject to change based on usage data and scale over time, for Amazon DSP campaign management APIs:

| Operation	| Description | Per Second | Per Day |
|-----|-----|-----|-----|
| READ | GET operations	| 1	| 80,000 |
| CREATE/DELETE	| POST operations | 1 | 36,000 |
| UPDATE | PUT operations | 1 | 48,000	|

For best practices to avoid being rate limited, see the **rate limiting** section in the Amazon Ads API [developer notes](https://advertising.amazon.com/API/docs/en-us/reference/concepts/developer-notes).