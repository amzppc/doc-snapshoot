---
title: Sponsored TV overview
description: Sponsored TV overview with details on how to get started with Sponsored TV.
type: guide
interface: api
keywords:
    - Sponsored TV
---

# Sponsored TV overview

Sponsored TV is a self-service, programmatic ad solution that helps advertisers build their brand in front of new audiences across streaming TV services like Freevee, Twitch, and other Streaming TV publishers. Brands can easily amplify their full-funnel marketing mix with Sponsored TV, discovering, reaching, and inspiring audiences as they watch their favorite shows, streamers and movies.

>[NOTE] Sponsored TV is currently in open beta and available for the BR, CA, MX, UK, and US marketplaces.

## Get started

To learn how to get started using Sponsored TV, see [Sponsored TV eligibility requirements](https://advertising.amazon.com/help/GC2CYZXA7CU2NVPG).

## Campaign management

To learn more about creating and managing Sponsored TV campaigns using the API, see:

* [Campaign structure](guides/sponsored-tv/campaigns/campaign-structure)
* [Creating a campaign](guides/sponsored-tv/campaigns/get-started)
* [Managing campaigns](guides/sponsored-tv/campaigns/manage-campaigns)
* [Full API reference](sponsored-tv-open-beta)

## Creatives

To learn more about creating and managing Sponsored TV creatives, see the creatives [overview](guides/sponsored-tv/creatives/overview).


## Reporting
The Ads API reporting functionality provides a variety of reports to help you retrieve historical impression, click, cost, and conversion data for your Sponsored TV campaigns.

Learn more:

* [Report types](guides/reporting/v3/report-types/overview)
* [Getting started with reports](guides/reporting/v3/get-started)