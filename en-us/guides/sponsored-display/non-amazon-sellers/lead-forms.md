---
title: Sponsored Display lead forms (beta) guide
description: Understand how to create lead forms and associate them with Sponsored Display campaigns.
type: guide
interface: api 
tags:
    - Sponsored Display
---

# Get started with lead forms

>[WARNING] By accessing and using the [lead form API](sponsored-display/3-0/lead-forms), you agree that you've read and accepted the [Amazon Lead Generation Ads and Leads Manager Agreement](https://advertising.amazon.com/leadgenerationterms). This includes use of the API through a third-party product.

The lead form API allows you to create lead forms to collect information from customers who are interested in your ad. 

Lead forms currently supports the following inputs:

- FULL_NAME
- FIRST_NAME
- LAST_NAME
- EMAIL
- PHONE NUMBER

>[NOTE] Lead forms are currently only available for advertisers who **do not** sell on Amazon. [Learn more](guides/sponsored-display/non-amazon-sellers/get-started) about getting started with Sponsored Display as a non-Amazon seller.

## Creating lead forms

To create a lead form, add a name, description, the types of questions you want to ask, and a privacy policy URL.

>[TIP] If you want to see what the lead form will look like before creation, see [Previewing lead forms](guides/sponsored-display/non-amazon-sellers/lead-forms#previewing-lead-forms).

Note that once a lead form has been created, it can't be updated unless is it rejected by moderation. 

The example below creates a lead form that takes customer full name and email address:

```bash
curl --location 'https://advertising-api.amazon.com/leadForms' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxxx' \
--header 'Authorization: Bearer Atza| xxxxxxxxxx' \
--header 'Amazon-Advertising-API-Scope: xxxxxxxxxxx' \
--header 'Content-Type: application/vnd.leadform.v1+json' \
--data '[
    {
        "leadFormName": "Test lead form",
        "properties": {
            "description": "This is a test lead form.",
            "questions": [
                {
                    "type": "FULL_NAME"
                },
                {
                    "type": "EMAIL"
                }
            ],
            "privacyPolicyUrl": "https://www.test.com"
        }
    }
]'
```

A successful request returns a 200 response and the leadFormId:

```json
[
    {
        "leadFormId": "amzn1.lead-form.v1.xxxxxxxxxxx",
        "code": "SUCCESS"
    }
]
```

## Associating lead forms to creatives

Once you create a lead form, you can associate it to a Sponsored Display creative as a call to action. 

>[NOTE] To use a lead form in a creative, the associated ad group must have a `bidOptimization` of `leads` and `creativeType` of `IMAGE`. 

After you've created a lead form, use [POST /sd/creatives](sponsored-display/3-0/openapi#tag/Creatives/operation/createCreatives) to build a creative that uses the `SIGN_UP` call to action, with `leadFormId` as the property.

```bash
curl --location 'https://advertising-api.amazon.com/sd/creatives' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxxxx' \
--header 'Amazon-Advertising-API-Scope: xxxxxxx' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer Atza|xxxxxxxx' \
--data '[
    {
        "adGroupId": 12345678,
        "creativeType": "IMAGE",
        "properties": {
            "headline": "Test Headline (input custom headline)",
            "brandLogo": {
                "assetId": "amzn1.assetlibrary.asset1.xxxxxxx1",
                "assetVersion": "version_v1",
                "croppingCoordinates": {
                    "top": 0,
                    "left": 0,
                    "width": 600,
                    "height": 100
                }
            },
            "callToActions": [
                {
                    "callToActionType": "SIGN_UP",
                    "properties": {
                        "leadFormId": "amzn1.lead-form.v1.xxxxxxxxxx2"
                    }
                }
            ]
        }
    }
]'

```

## Checking approval status

All lead forms must be reviewed by Amazon moderation. You can check the status of the review using [GET /leadForms/approvalStatuses](sponsored-display/3-0/lead-forms#tag/Lead-Forms/operation/listLeadFormApprovalStatuses).

```bash
curl --location 'https://advertising-api.amazon.com/leadForms/approvalStatuses?leadFormIdFilter=amzn1.lead-form.v1.xxxxxxx&language=en-US' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxxx' \
--header 'Authorization: Bearer Atza| xxxxxxxxx' \
--header 'Amazon-Advertising-API-Scope: xxxxxxxx' \
```

A successful response returns the overall status and details on any policy violations.

```json
{
    "approvalStatuses": [
        {
            "leadFormId": "1234",
            "securityScanStatus": "APPROVED",
            "moderationStatus": "APPROVED",
            "policyViolations": []
        }
    ],
    "nextToken": "eyJzdGFydEluZGV4IjoxMDB9"
}
```

## Updating lead forms

You can use [PUT /leadForms](sponsored-display/3-0/lead-forms#tag/Locations/operation/archiveLocations) to update lead form configurations, including privacy policy URL, questions, and descriptions.

>[NOTE] Lead forms in PENDING_REVIEW or APPROVED status cannot be updated. Lead forms can only be updated if they have been REJECTED. 

After any update to a lead form, the moderation status will be reset.

```json
PUT /leadForms
[
    {
        "leadFormId": "amzn1.lead-form.v1.86fc8f71-4e9c-4de4-8647-4e1092d3f686",
        "idempotencyToken": "TEST",
        "properties": {
            "description": "description",
            "privacyPolicyUrl": "https://anotherexample.com",
            "questions": [
                {
                    "type": "FULL_NAME"
                },
                {
                    "type": "EMAIL"
                }
            ]
        }
    }
]
```

## Previewing lead forms

If you want to visualize your lead form prior to creation, you can use the [POST /leadForms/preview](sponsored-display/3-0/lead-forms#tag/Lead-Forms/operation/createLeadForms) API.

If you want to include a brand logo asset ID in your preview, upload an image and create an asset ID using the [asset library API](guides/creative-asset/creating-assets).

```bash

curl --location 'https://advertising-api.amazon.com/leadForms/preview' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxxxx' \
--header 'Authorization: Bearer Atza| xxxxxxxx' \
--header 'Amazon-Advertising-API-Scope: 3678286220383777' \
--header 'Content-Type: application/vnd.leadform.v1+json' \
--data '{
    "leadFormPreviewConfiguration": {
        "previewMode": "LEAD_FORM_MAIN"
    },
    "leadForm": {
        "properties": {
            "learnMoreUrl": "https://example.com",
            "description": "test",
            "privacyPolicyUrl": "hello",
            "questions": [
                {
                    "type": "PHONE_NUMBER"
                }
            ]
        }
    }
}
'
```

If successful, you a 200 response containing the raw HTML so you can preview the experience.

## Listing lead forms

You can use [GET /leadForms](sponsored-display/3-0/lead-forms#tag/Lead-Forms/operation/listLeadForms) to query your created lead forms.

**Examples**

GET /leadForms?leadFormIdFilter={{leadFormIds}}

GET /leadForms?leadFormNameFilter={{leadFormNames}}

```json
{
    "leadForms": [
        {
            "leadFormId": "1234",
            "leadFormName": "testLeadForm",
            "properties": {
                "description": "test description",
                "questions": [
                    {
                        "type": "FULL_NAME"
                    },
                    {
                        "type": "EMAIL"
                    },
                    {
                        "type": "PHONE_NUMBER"
                    },
                    {
                        "type": "ZIP_CODE"
                    },
                    {
                        "type": "CITY"
                    }
                ],
                "privacyPolicyUrl": "testPolicyUrl"
            },
            "moderationStatus": "APPROVED",
            "securityScanStatus": "APPROVED",
            "createdTimestamp": "2024-06-10T18:13:42.108Z"
        }
    ]
}

```

## Creating reports with lead forms metrics

To view lead generation ads campaign performance data, you can use the [version 3 reports](offline-report-prod-3p#tag/Asynchronous-Reports/operation/getAsyncReport). This tutorial outlines how to asynchronously request and download reports for campaigns.

Version 3 reports support a variety of report types, time periods, and metrics. For more information on what configurations are supported, see [Report types](guides/reporting/v3/report-types/overview).

- All reports requests use a single endpoint: `POST /reporting/reports`. See the [API reference](offline-report-prod-3p#tag/Asynchronous-Reports/operation/getAsyncReport) for details on parameters.
- `adProduct` is valid input for adProduct param. Set it to "SPONSORED_DISPLAY".
- All report requests require the `groupBy` parameter in the report configuration. `groupBy` determines the level of granularity for the report.
- `timeUnit` can be set to "DAILY" or "SUMMARY". If you set `timeUnit` to "DAILY", you should include date in your column list.
- If you set `timeUnit` to "SUMMARY" you can include `startDate` and `endDate` in your column list.

Advertisers can see lead generation metrics in any of the supported Sponsored Display reports; "sdCampaigns", "sdTargeting", "sdAdvertisedProduct" and "sdAdGroup". Set `reportTypeId` to "sdCampaigns" in the example request.

There will be additional metrics (columns) unique to lead generation campaigns: linkOuts, leadFormOpens, leads. Those metrics will only be available in sdCampaigns, sdTargeting, sdAdvertisedProduct and sdAdGroup reports.

- [linkOuts](guides/reporting/v3/columns#linkouts):  The number of times link-outs to landing page were clicked. 
- [leadFormOpens](guides/reporting/v3/columns#leadformopens):  The number of times your lead forms were opened. 
- [leads](guides/reporting/v3/columns#leads):  The number of leads collected through your lead generation campaigns. 

```bash

curl --location --request POST 'https://advertising-api.amazon.com/reporting/reports' \ 
--header 'Content-Type: application/vnd.createasyncreportrequest.v3+json' \ 
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxxxxx' \ 
--header 'Amazon-Advertising-API-Scope: xxxxxxxxxx' \ 
--header 'Authorization: Bearer Atza|xxxxxx' \ 
--data-raw 
'{
    "name":"SD campaigns report 5/5-5/10",
    "startDate":"2024-05-05",
     "endDate":"2024-05-10",
    "configuration":{
        "adProduct":"SPONSORED_DISPLAY",
        "groupBy":["campaign"],
        "columns":["impressions","clicks","cost","campaignId","startDate","endDate","linkOuts","leadFormOpens","leads"],
        "reportTypeId":"sdCampaigns",
        "timeUnit":"SUMMARY",
        "format":"GZIP_JSON",
         "filters": [
            {
                "field": "campaignStatus",
                "values": ["ENABLED","PAUSED"]
            }
        ]
    }
}'
```

A successful request results in a 200 response that includes the requested report configuration, a reportId, and a status. Make note of the reportId for use in the next step.

```json
{
    "approvalStatuses": [
        {
            "leadFormId": "1234",
            "securityScanStatus": "APPROVED",
            "moderationStatus": "APPROVED",
            "policyViolations": []
        }
    ],
    "nextToken": "eyJzdGFydEluZGV4IjoxMDB9"
}
```

## Downloading reports

Once your report is ready, the url field of a GET `reporting/reports/{reportId}` contains a link to the S3 bucket where you can download your report. You can make a `GET` call using cURL or enter the URL in your browser.

```bash

curl --location --request GET 'https://advertising-api.amazon.com/reporting/reports/{reportId}' \ 
--header 'Content-Type: application/vnd.createasyncreportrequest.v3+json' \ 
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxxxxx' \ 
--header 'Amazon-Advertising-API-Scope: xxxxxxx' \ 
--header 'Authorization: Bearer Atza|xxxxxxxxxxx' \
```

Sample output of requested report.

```json
{
[
    {
        "cost":14.5,
        "endDate":"2024-05-10",
        "campaignId":158410630682987,
        "clicks":13,
        "impressions":2216,
        "adGroupId":72320882861500,
        "linkOuts":1,
        "leadFormOpens":1,
        "leads":1,
        "startDate":"2024-05-05",
    },
    {
        "cost":9.45,
        "endDate":"2024-05-10",
        "campaignId":158410630682987,
        "clicks":10,
        "impressions":3721,
        "linkOuts":1,
        "leadFormOpens":1,
        "leads":1,
        "adGroupId":55720282058882,
        "startDate":"2024-05-05",
    }
]
}
```