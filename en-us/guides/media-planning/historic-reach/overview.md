---
title: Historic Reach APIs Overview
description: Overview of the Historic Reach APIs.
type: guide
interface: api
tags:
  - Historic Reach
keywords:
  - reach
  - historic
  - curve
  - curves
---

# Historic Reach APIs

The Historic Reach API provides visibility into the historic reach of Amazon’s supply sources for media planning purposes. The Historic Reach API allows you to pass input parameters, such as supply package, audience, demographic and date range and return back a historic reach curve that matches the input criteria.  

## Currently supported geographies

- **North America**: United States, Canada, Mexico
- **South America**: Brazil
- **Europe**: Great Britain, Germany, France, Italy, Spain, Austria
- **Asia Pacific**: Australia, Japan

## FAQ

<details>
<summary><strong>Is there a cost associated with using the Historic Reach APIs?</strong></summary>
  There is no cost to use these APIs.
</details>

<details>
<summary><strong>Who can access the Historic Reach APIs?</strong></summary>
  The Historic Reach API is available for use to anyone who has a valid client ID, access token, and profile ID. Please use these [instructions](guides/media-planning/historic-reach/get-started#before-you-begin) to get set up.
</details>


## Next steps

To make your first call to the Historic Reach API, please navigate to the [Get Started](guides/media-planning/historic-reach/get-started) guide.