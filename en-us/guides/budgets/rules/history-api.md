---
title: Viewing historical changes to daily budgets for campaigns with budget rules 
description: Learn how to get a list of Amazon Ads budget rule changes over time for a campaign.
type: guide
interface: api 
tags:
    - Sponsored Brands
    - Sponsored Products
    - Sponsored Display
    - Budget Rules
keywords:
    - budget rule history
    - budgets
---

# Viewing historical changes to daily budgets for campaigns with budget rules 

You can use the budget rule history resource to get historical values of a rule budget. When requesting the resource, you can specify a time range for rule budget changes. There is a separate endpoint for each ad type.

| Ad type | Endpoint|
|-----|------|
| Sponsored Products |[GET /sp/campaigns/{campaignId}/budgetRules/budgetHistory](sponsored-products/3-0/openapi/prod#tag/BudgetRules/operation/getRuleBasedBudgetHistoryForSPCampaigns) |
| Sponsored Display |[GET /sd/campaigns/{campaignId}/budgetRules/budgetHistory](sponsored-display/3-0/openapi/prod#tag/BudgetRules/operation/getRuleBasedBudgetHistoryForSDCampaigns) |
| Sponsored Brands |[GET /sb/campaigns/{campaignId}/budgetRules/budgetHistory](sponsored-brands/3-0/openapi/prod#tag/BudgetRules/operation/getRuleBasedBudgetHistoryForSBCampaigns) | 

>[WARNING] These endpoints will be deprecated on August 31, 2023. 

> [NOTE] The maximum range allowed is 90 days. 

The request response includes a list of rule budget changes for the specified campaign. The request response list is paginated, so you must include a page size in the request and include the `nextToken` with the value in the previously returned response.  

## Example

This example shows the process for Sponsored Brands; it is the same for the other ad types.

First, request the list of history resources:

```bash
GET /sb/campaigns/{campaignId}/budgetRules/budgetHistory?pageSize=1&startDate=20200706&endDate=20200707
```

The response may resemble:

```json
{
    "history": [
    {
      "appliedRule": {
        "ruleDetails": {
          "budgetIncreaseBy": {
            "type": "PERCENT",
            "value": 20.0
          },
          "duration": {
            "dateRangeTypeRuleDuration": {
              "endDate": "20200707",
              "startDate": "20200706"
            }
          },
          "name": "SAMPLE_BUDGET_RULE",
          "performanceMeasureCondition”: {
            "metricName": "ACOS",
            "threshold”: 20.0,
            "comparison Operator”: "LESS_THAN_OR_EQUAL_TO"
          },
          “recurrence” : {
            “type”: “DAILY"
          }
        },
        "ruleType": “PERFORMANCE”: { 
          "ruleId": "10c204e9-ccc2-4ed5-903e-99363dad6851",
          "ruleState": "ACTIVE"
		},
        "dailyBudgetValue": 10.0,
        "executionTime": 1594128659918,
        "ruleBasedBudgetValue": 12.0,
        “performanceMetric”: {
            “metricName”: “ACOS”,
            “value”: 10
        }
	}],
	"nextToken": "tqcN6nsiYnVkZ2V0U3RhcnRUaW1lc3RhbXAiOnsib”
}
```

Note that in this response, the `nextToken` field is not null. To return the next set of history resources, the request resembles:

```bash
GET /sb/campaigns/{campaignId}/budgetRules/budgetHistory?pageSize=1&startDate=20200706&endDate=20200707
&nextToken=tqcN6nsiYnVkZ2V0U3RhcnRUaW1lc3RhbXAiOnsib
```
