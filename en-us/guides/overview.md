---
title: Developer guides overview
description: Learn how developers can get started using the Amazon Ads API and other advanced tools.  
type: guide
interface: api
---

# Developer guides overview

Amazon Ads provides a number of advanced tools that give developers the ability to programatically create and manage campaigns, as well as retrieve performance data.

>[TIP:Documentation translations]Translations of these developer guides are available in select languages. For a list of available languages, see [Translations](guides/translations).

## Use cases

### Onboarding

To use all developer tools, you must first complete the onboarding process which includes registering an application and applying for access to the API. 

[Get started with the onboarding process.](guides/onboarding/overview)

### Reporting & measurement

Developers can use the Amazon Ads API and Amazon Marketing Stream to access performance metrics for Amazon Ads campaigns.

The Ads API supports asynchronous report requests, while Amazon Marketing Stream provides near real-time access to metrics through your AWS account.

- [Request a report using the API](guides/reporting/v3/get-started)
- [Get started with Amazon Marketing Stream](guides/amazon-marketing-stream/onboarding)

### Campaign management

You can also use the Amazon Ads API to create and manage sponsored ads and Amazon DSP campaigns. 

- [Create a Sponsored Products campaign](guides/sponsored-products/get-started/manual-campaigns)
- [Create a Sponsored Brands campaign](guides/sponsored-brands/campaigns/get-started-with-campaigns)
- [Create a Sponsored Display campaign (Amazon sellers or vendors)](guides/sponsored-display/contextual-targeting)
- [Create a Sponsored Display campaign (advertisers that don't sell on Amazon)](guides/sponsored-display/non-amazon-sellers/get-started)

## More information

- [Full API reference](reference/api-overview)
- [API best practices](reference/concepts/overview)
- [Release notes](release-notes/index)
- [Support](support/overview)



