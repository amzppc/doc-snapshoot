---
title: Brand Store feature availability by marketplace
description: Learn which Amazon Advertising API for Brand Store features are supporting in each Amazon marketplace.
type: guide
interface: api 
tags:
    - Brand Store
keywords:
    - marketplace
    - features
    - brand store
---

# Brand Stores feature availability by marketplace

This document lists descriptions and marketplace availability for all Brand Stores features.


## Store insights

Store insights provide brands with analytics about the Brand store performance and customer behavior. For more information, see [Brand Store insights metrics.](guides/brand-store/insight-metrics)

**Relevant resource:**

* [/stores/{brandEntityId}/insights](stores/open-api#tag/Stores-Analytics/operation/getInsightsForStoreAPI)

**Marketplace availability:** All marketplaces.


## Store ASIN metrics

Store ASIN metrics provides information about your store ASIN performance, including rendered impressions, viewed impressions, clicks and sales. For more information, see [Brand Store insights metrics.](guides/brand-store/insight-metrics)

**Relevant resource:**

* [/stores/{brandEntityId}/asinMetrics](stores/open-api#tag/Stores-Analytics/operation/getAsinEngagementForStore)

**Marketplace availability**: All marketplaces
