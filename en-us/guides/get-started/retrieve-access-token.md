---
title: Getting started - Generate access and refresh tokens
description: How to generate access tokens through Login with Amazon for use with the Amazon Ads API
type: guide
interface: api
tags:
    - Onboarding
    - Authorization
keywords:
    - token expiration
    - unauthorized
    - Authorization header
    - invalid parameter code
    - new code
    - new token
---

# Step 2: Generate access and refresh tokens

<div class="breadcrumb-top" style="display: block; font-size: .9em; margin: 0px; margin: -10px 0 20px 0; padding: 10px; background: #f6f6f6; border-left: 8px solid #4f7cb1;">Getting started : <span style="white-space: nowrap;">[Overview](guides/get-started/overview) </span> | <span style="white-space: nowrap;">[1\. Create auth grant](guides/get-started/create-authorization-grant) | [**2\. Generate access token**](guides/get-started/retrieve-access-token) | [3\. Retrieve profiles](guides/get-started/retrieve-profiles)</span></div>

As described in the ["Getting Started" overview](guides/get-started/overview), an approved *client application* may make calls to the Amazon Ads API on behalf of an Amazon *user account* with access to Amazon Ads accounts.

In [step 1 of the "Getting started" walkthrough](guides/get-started/create-authorization-grant), you created an authorization grant representing a user account's agreement to enable access for your client application. To represent that agreement, Login with Amazon created an *authorization code*, which you will use to retrieve an *access token* for calling the API.

- For a conceptual overview of access and refresh tokens in the Amazon Ads API, see [Access Tokens](guides/account-management/authorization/access-tokens).

>[TIP:Manage tokens with our Postman collection]The Amazon Ads API Postman collection makes it easier to retrieve and manage access and refresh tokens. [Learn more about the Amazon Ads API Postman collection.](guides/get-started/using-postman-collection)

To create access and refresh tokens, follow these steps:

## Retrieve your client ID and client secret

Sign in to [Amazon Developer](https://developer.amazon.com) with the Amazon account you used to [create your Login with Amazon client](guides/onboarding/create-lwa-app). Navigate to the Login with Amazon console and locate the **client ID** and **client secret** for the security profile to which you [assigned API access in an earlier step](guides/onboarding/assign-api-access).

- For information on locating your client ID and client secret, see [Create a Login with Amazon Application](guides/onboarding/create-lwa-app#retrieve-your-security-credentials).

## Call the authorization URL to request access and refresh tokens

To retrieve access and refresh tokens, select the authorization URL for your region:

| Region | Authorization URL |
|--------|-----|
| North America (NA) | `https://api.amazon.com/auth/o2/token` |
| Europe (EU) | `https://api.amazon.co.uk/auth/o2/token` |
| Far East (FE) | `https://api.amazon.co.jp/auth/o2/token` |

Next, construct a `POST` request to retrieve access and refresh tokens. This request has the following query parameters:

| Parameter | Description |
|-----------|-------------|
| grant_type | Must be `authorization_code`.  |
| code | The authorization code retrieved in [step 1](guides/get-started/create-authorization-grant). Note this code expires after 5 minutes. A new code may be generated by repeating the same steps. |
| redirect_uri | One of the values in the **Allowed Return URLs** field in your Login with Amazon account. |
| client_id | The **Client ID** of your Login with Amazon account. |
| client_secret | The **Client Secret** of your Login with Amazon account. |

For example, to retrieve access and refresh tokens using cURL, substitute your values in the following request:

```shell
curl  \
    -X POST \
    --data "grant_type=authorization_code&code=AUTH_CODE&redirect_uri=YOUR_RETURN_URL&client_id=YOUR_CLIENT_ID&client_secret=YOUR_SECRET_KEY" \
    https://api.amazon.com/auth/o2/token  
```

## Access token response

The response to a successful token request is a JSON object with the following schema:

| Field | Description |
|-------|-------------|
| access_token | The access token. |
| token_type | The type of OAuth 2.0 token. Always set to `bearer`. |
| expires_in | The length of time until the access token expires, in seconds.|
| refresh_token | The refresh token. |

For example:

```JSON
{
    "access_token": "Atza|IQEBLjAsAhRmHjNgHpi0U-Dme37rR6CuUpSR...",
    "token_type": "bearer",
    "expires_in": 3600,
    "refresh_token": "Atzr|IQEBLzAtAhRPpMJxdwVz2Nn6f2y-tpJX2DeX..."
}
```

Note that access tokens are valid for 60 minutes. A new access token can be generated at any time using the refresh token. Refresh tokens do not expire.

 Access tokens begin with the characters `Atza|`, and refresh tokens begin with `Atzr|`. These characters are part of the token and should be included wherever the token is used.

>[NOTE]An unsuccessful request may return a `400` response with the following `error_description`: *"The request has an invalid parameter : code"*<br /> <br />This may indicate that the authorization code has expired, as codes expire after 5 minutes. A new code can be generated by repeating [step 1 of this walkthrough](guides/get-started/create-authorization-grant).<br /><br />For more information about LwA error responses, see [Access Token Errors](https://developer.amazon.com/docs/login-with-amazon/authorization-code-grant.html#access-token-errors).

## Using refresh tokens

Once tokens have been retrieved using the authorization code, a new access token can be retrieved at any time using the refresh token. A call using the refresh token can be made to the same URL determined above, but will include different parameters.

- For more information, see [Generating an access token using a refresh token](guides/account-management/authorization/access-tokens#generating-an-access-token-using-a-refresh-token).

## Next steps

You now have the two essential credentials for a successful request to the Amazon Ads API:

- The **client ID** of your Login with Amazon *client application*
- The **access token** that enables your client to access advertising data and services for a particular *user account*

However, as described in the ["Getting Started" overview](guides/get-started/overview), the majority of requests to the API require using [profiles](guides/account-management/authorization/profiles) to access data and services in a specific marketplace. 

To retrieve a list of profiles for an account, continue to the next step in the onboarding process: [Retrieve a profile ID](guides/get-started/retrieve-profiles).

>[TOPIC:Technical support] If you have difficulty connecting to the Amazon Ads API, please visit our [Technical Support page](support/overview) for assistance.
