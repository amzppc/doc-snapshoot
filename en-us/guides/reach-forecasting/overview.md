---
title: Reach forecasting API overview
description: Introduction to and explanation of the reach forecasting API
type: guide
interface: api
tags:
  - Reach forecasting
keywords:
  - overview
  - marketplace
  - FAQ
---

# Reach Forecasting API

The Reach Forecasting API allows integrators to generate reach forecasts against inventory that is available to buy, while taking into account brand and campaign parameters.


## Where is the reach forecasting API available?

- **North America:** United States, Canada, Mexico
- **South America**: Brazil
- **Europe:** Germany, Spain, France, Italy, Netherlands, United Kingdom, Sweden, Turkey
- **Middle East:** Saudi Arabia, United Arab Emirates
- **Asia Pacific:** Australia, India, Japan

## FAQ
<details>
<summary><strong>Is there a cost associated with using the reach forecasting API?</strong></summary>
 The reach forecasting API is available at no cost.
</details>

<details>
<summary><strong>Who can access the reach forecasting API?</strong></summary>
 The reach forecasting API is accessible to anyone who has valid access token and profile ID. Please visit this [instruction](guides/reach-forecasting/get-started#before-you-begin) to learn how to obtain your access token and profile ID.
</details>

<details>
<summary><strong>How many calls can I make to the reach forecasting API at the same time?</strong></summary>
 You can make up to 1 concurrent call.
</details>

## Next steps

To make your first call to the reach forecasting API, see [Get started](guides/reach-forecasting/get-started).
