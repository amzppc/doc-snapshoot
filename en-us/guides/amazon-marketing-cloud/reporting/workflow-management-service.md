---
title: Workflow management service
description: How to create and manage workflows in AMC
type: guide
interface: api
---

# Workflow management service

<div class="breadcrumb-top" style="display: block; font-size: .9em; margin: 0px; margin: -10px 0 20px 0; padding: 10px; background: #f6f6f6; border-left: 8px solid #4f7cb1;"><span>[1\. Manage a workflow](guides/amazon-marketing-cloud/reporting/create-workflow) | [2\. Manage a workflow schedule](guides/amazon-marketing-cloud/reporting/schedule-workflow) | [3\. Manage workflow executions](guides/amazon-marketing-cloud/reporting/execute-workflow)  |  [4\. Get your workflow results](guides/amazon-marketing-cloud/reporting/get-your-results)</span></div>

## Overview 

AMC's workflow management service provides you the ability to create, store, and execute parameterized workflows. Workflows can be executed "on-demand" or can be scheduled to run at a specific time via AMC's built-in scheduler.

Learn how to:

- **[Manage a workflow](guides/amazon-marketing-cloud/reporting/create-workflow)**
- **[Manage a workflow schedule](guides/amazon-marketing-cloud/reporting/schedule-workflow)**
- **[Manage workflow executions](guides/amazon-marketing-cloud/reporting/execute-workflow)**

After you execute your workflows, learn [how to get your query results](guides/amazon-marketing-cloud/reporting/get-your-results). 

If you want to create a workflow on-demand and execute it, see [Create and execute an ad hoc workflow](guides/amazon-marketing-cloud/reporting/ad-hoc-workflow).


