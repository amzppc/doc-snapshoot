---
title: AMC migration
description: How to migrate
type: guide
interface: api
---
# Migrating from instance-based APIs to AMC APIs on Amazon Ads API

The launch of AMC APIs on the Ads API represents the next generation API for programmatic access to Amazon features for AMC. All new development is occurring in the Ads API, including enhanced data protection and security features. This change ensures developers are using the latest technology and features for the benefit of both partners and customers.

> [WARNING] Starting in May 2024, AMC will begin the process of deprecating the AMC instance-based APIs. We will not offer technical support for these APIs after May. We recommend that you begin the process of migrating from the instance-based APIs to the Amazon Ads APIs as soon as you are able.

### What changes?

- **Base URL**: Previously, API requests used the AMC Instance API URL, a unique URL for each instance. Now, API requests use a common [URL prefix](guides/get-started/first-call#url-prefixes) depending on the region.
- **Authorization**: Authorization is now based on OAuth2 instead of AWS IAM based permissions. An AWS Account is not a mandatory requirement to access AMC via APIs.
- **API endpoints**: API endpoints that are provided on a per-AMC-instance basis will be replaced by an instanceId parameter included in the Ads.
- **Request headers**: The request header now requires the following mandatory parameters: Amazon-Advertising-API-MarketplaceId (the AMC Account ID) and Amazon-Advertising-API-MarketplaceId in addition to the headers for accessing other Amazon Ads APIs.
- **Instance-level actions**: Previous instance level APIs contained only instance level actions. Additional cross-AMC instance APIs are available on the Ads API. These APIs include `/amc/instances/` and `/amc/accounts/`.

To learn more about Amazon Ads API, visit the Amazon Ads Advanced Tool Center: [https://advertising.amazon.com/API/docs/en-us/index](https://advertising.amazon.com/API/docs/en-us/index)

### What’s the same?

All AMC API actions will be supported on the Ads API remain the same. For example, if you were making API calls to the AMC workflows API, an equivalent API is available on the Amazon Ads API, as listed below.

### Endpoint equivalencies


| Category                   | Resource            | AMC v1 endpoints                                                                | AMC APIs on Amazon Ads endpoints                                                               |
| ---------------------------- | --------------------- | --------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------ |
| **Reporting**              | Workflow Executions | POST {unique-amc-url}/prod/workflowExecutions                                   | POST {api_url}/amc/reporting/{instanceId}/workflowExecutions                                   |
|                            |                     | PUT {unique-amc-url}/prod/workflowExecutions/{workflowExecutionId}              | PUT {api_url}/amc/reporting/{instanceId}/workflowExecutions/{workflowExecutionId}              |
|                            |                     | GET {unique-amc-url}/prod/workflowExecutions/{workflowExecutionId}/downloadUrls | GET {api_url}/amc/reporting/{instanceId}/workflowExecutions/{workflowExecutionId}/downloadUrls |
|                            |                     | GET {unique-amc-url}/prod/workflowExecutions                                    | GET {api_url}/amc/reporting/{instanceId}/workflowExecutions                                    |
|                            |                     | GET {unique-amc-url}/prod/workflowExecutions/{workflowExecutionId}              | GET {api_url}/amc/reporting/{instanceId}/workflowExecutions/{workflowExecutionId}              |
|                            |                     | DELETE {unique-amc-url}/prod/workflowExecutions/{workflowExecutionId}           | DELETE {api_url}/amc/reporting/{instanceId}/workflowExecutions/{workflowExecutionId}           |
| **Reporting**              | Workflows           | GET {unique-amc-url}/prod/workflows/{workflowId}                                | GET {api_url}/amc/reporting/{instanceId}/workflows/{workflowId}                                |
|                            |                     | DELETE {unique-amc-url}/prod/workflows/{workflowId}                             | DELETE {api_url} /amc/reporting/{instanceId}/workflows/{workflowId}                            |
|                            |                     | PUT {unique-amc-url}/prod/workflows/{workflowId}                                | PUT {api_url}/amc/reporting/{instanceId}/workflows/{workflowId}                                |
|                            |                     | POST {unique-amc-url}/prod/workflows                                            | POST {api_url}/amc/reporting/{instanceId}/workflows                                            |
|                            |                     | GET {unique-amc-url}/prod/workflows                                             | GET {api_url}/amc/reporting/{instanceId}/workflows                                             |
| **Reporting**              | Schedules           | POST {unique-amc-url}/prod/schedules                                            | POST {api_url}/amc/reporting/{instanceId}/schedules                                            |
|                            |                     | GET {unique-amc-url}/prod/schedules                                             | GET {api_url}/amc/reporting/{instanceId}/schedules                                             |
|                            |                     | GET {unique-amc-url}/prod/schedules{scheduleId}                                 | GET {api_url}/amc/reporting/{instanceId}/schedules/{scheduleId}                                |
|                            |                     | DELETE {unique-amc-url}/prod/schedules{scheduleId}                              | DELETE {api_url}/amc/reporting/{instanceId}/schedules/{scheduleId}                             |
|                            |                     | PUT {unique-amc-url}/prod/schedules{scheduleId}                                 | PUT {api_url}/amc/reporting/{instanceId}/schedules/{scheduleId}                                |
| **Reporting**              | Datasources         | GET {unique-amc-url}/prod/{instance_id}/dataSources                             | GET {api_url}/amc/reporting/{instance_id}/dataSources                                          |
| **Audiences**              | Rule Based Audience | GET {rba\_api\_url}/amc/audiences/query                                         | GET {api_url}/amc/audiences/query                                                              |
|                            |                     | GET {rba\_api\_url}/amc/audiences/query                                         | POST {api_url}/amc/audiences/query                                                             |
|                            |                     | GET {rba\_api\_url}/amc/audiences/query                                         | GET {api_url} /amc/audiences/query/{audienceExecutionId}                                       |
| **Advertiser data upload** | Data sets           | POST {unique-amc-url}/dataSets                                                  | POST {api_url}/amc/advertiserData/{instanceId}/dataSets                                        |
|                            |                     | PUT {unique-amc-url}/dataSets/{datasetID}                                       | PUT {api_url}/amc/advertiserData/{instanceId}/dataSets/{dataSetId}                             |
|                            |                     | GET {unique-amc-url}/dataSets                                                   | POST {api_url}/amc/advertiserData/{instanceId}/dataSets/list                                   |
|                            |                     | GET {unique-amc-url}/dataSets/{datasetID}                                       | GET {api_url}/amc/advertiserData/{instanceId}/dataSets/{dataSetId}                             |
|                            | Uploads             | POST {unique-amc-url}/data/{datasetID}/uploads                                  | POST {api_url}/amc/advertiserData/{instanceId}/uploads/list                                    |
|                            |                     | POST {unique-amc-url}/data/{datasetID}/uploads                                  | POST {api_url}/amc/advertiserData/{instanceId}/uploads/{dataSetId}                             |
|                            |                     | GET {unique-amc-url}/data/{dataSetId}/uploads/{uploadId}                        | GET {api_url}/amc/advertiserData/{instanceId}/uploads/{dataSetId}/{uploadId}                   |

### New endpoints


| Category                   | Resource                              | Endpoints                                                              |
| :--------------------------- | :-------------------------------------- | :----------------------------------------------------------------------- |
| **Administration**         | Accounts                              | GET {api_url}/amc/accounts                                             |
|                            | Instances                             | POST {api_url}/amc/instances/{instanceId}/updateCustomerAwsAccount     |
|                            |                                       | GET {api_url}/amc/instances                                            |
|                            |                                       | GET {api_url}/amc/instances/{instanceId}/advertisers                   |
|                            |                                       | GET {api_url}/amc/instances/{instanceId}                               |
| **Audiences**              | Advertiser audiences - connection     | POST {api_url} /amc/audiences/connections                              |
|                            |                                       | GET {api_url}/amc/audiences/connections                                |
|                            |                                       | DELETE {api_url}/amc/audiences/connections?connectionId={connectionId} |
|                            | Advertiser audience creation metadata | POST {api_url}/amc/audiences/metadata/                                 |
|                            |                                       | PUT {api_url}/amc/audiences/metadata/{audienceId}                      |
|                            |                                       | GET {api_url}/amc/audiences/metadata/{audienceId}                      |
|                            | Advertiser audience creation          | POST {api_url}/amc/audiences/records                                   |
|                            | AMC terms                             | GET {api_url} /amc/audiences/connections/terms                         |
|                            |                                       | PATCH {api_url} /amc/audiences/connections/terms                       |
| **Audiences**              | Rule-based lookalike audiences        | POST {api_url} /amc/audiences/lookalike                                |
| **Advertiser data upload** | Data sets                             | DELETE {api_url}/amc/advertiserData/{instanceId}/dataSets/{dataSetId}  |
|                            | Data set column                       | POST {api_url}/amc/advertiserData/{instanceId}/dataSets/{dataSetId}    |
|                            |                                       | PUT {api_url}/amc/advertiserData/{instanceId}/dataSets/{dataSetId}     |
|                            |                                       | DELETE {api_url}/amc/advertiserData/{instanceId}/dataSets/{dataSetId}  |

### Deprecated Endpoint


| Category               | AMC v1 endpoint                                  | Description   |
| :----------------------- | :--------------------------------------------------- | :-------------- |
| Advertiser data upload | POST {unique-amc-url}/data/{datsetId}/batchUploads | Batch uploads |

### Region limitations

> [WARNING] You must use the Amazon Ads North America (NA) API endpoint (https://advertising-api.amazon.com) to access AMC Data Upload APIs on Amazon Ads API. Additionally, only AMC instances hosted in the **us-east-1** region are supported by this API. For **eu-west-1** instances, please contact amc-support@amazon.com.

### Migrating from Advertiser Data Upload V1 to Advertiser Data Upload APIs on Amazon Ads

If you are migrating from Advertiser Data Upload v1 to AMC Advertiser Data Upload APIs on Amazon Ads (v2) and want to use the same datasets you uploaded using v1, you will need to:

1. Recreate the datasets using Advertiser Data Upload APIs on Amazon Ads (`POST /amc/advertiserData/{instanceId}/dataSets`). Refer to [Create dataset](guides/amazon-marketing-cloud/advertiser-data-upload/advertiser-data-sets). 

>[NOTE] You must use a unique `dataSetID` for when creating the new datasets. For example, if the datasets created using v1 had `"dataSetId": "myfirstdataset"`, you must create the new dataset with a different name (for example, `"dataSetId": "myfirstdatasetv2"`).

2. Upload the data to the newly created datasets (`POST {api_url}/amc/advertiserData/{instanceId}/uploads/{dataSetId}`). Refer to [Upload data to datasets](guides/amazon-marketing-cloud/advertiser-data-upload/adu-uploads). 

>[NOTE] You will still retain access to the datasets created using v1 in the AMC UI and AMC reporting APIs, but you will not be able to upload to, read, or modify those datasets using the v2 AMC Data Upload APIs. To edit dataSets created using the v1 AMC Data Upload APIs, you can continue to use the v1 API paths.

### New or updated parameters for workflow management services

The following two new parameters have been included in the schedules and workflowExecutions of Sandbox instances.

- **requireSyntheticData**
- **disableAggregationControls**

For details, refer to [AMC sandbox](guides/amazon-marketing-cloud/amc-sandbox)

### New or updated parameters for advertiser data upload resources

**Update strategy**

This is a new parameter that allows you to set how new data will be merged with existing data in the instance.

**Dataset period**

Dataset period options have changed to:

- P1D (daily partition)
- PT1H (hourly partition)
- P1M (monthly partition)

> [NOTE] There is no longer the option to partition per-minute or per-week. Also, uploaded files do not have to be partitioned before uploading, though you can choose to partition files and upload individually if desired.

> [WARNING] The data upload S3 bucket must be in the same AWS Account as the "Connected AWS Account ID" displayed in the **Instance info** page in the AMC console.

Start your [onboarding journey to the Ads API here](guides/onboarding/overview).

### Contact Us

AMC Support: amc-support@amazon.com
