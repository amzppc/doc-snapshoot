---
title: Amazon Marketing Cloud - AWS Clean Rooms
description: AMC - AWS Clean Rooms
type: guide
interface: api
---
# Set up AMC-AWS Clean Rooms

Setting up AMC-AWS Clean Rooms requires you to:

- [Perform set up tasks before uploading 1P data](guides/amazon-marketing-cloud/acr/3_setup_requirements)
- [Accept and join a collaboration](guides/amazon-marketing-cloud/acr/4_setup_collab)
- [Know the essentials for data upload](guides/amazon-marketing-cloud/acr/5_data_prep)
- [Upload event data](guides/amazon-marketing-cloud/acr/6_upload_event)
- [Upload identity data](guides/amazon-marketing-cloud/acr/7_upload_identity)

After your data is uploaded, you can leverage AMC to generate insights and discover new audiences using your 1P data. 

The following diagram illustrates the steps to set up AMC-AWS Clean Rooms.

![ACR Setup](/_images/amazon-marketing-cloud/acr/ACR_setup_workflow.png)
