---
title: Amazon Prime Video Channel Insights
description: Amazon Prime Video Channel Insights
type: guide
interface: api
---
# Amazon Prime Video Channel Insights 

Amazon Prime Video Channel Insights is a collection of two AMC data views that represent Prime Video Channel subscriptions and streaming signals. Amazon Prime Video Channel Insights is a standalone AMC Paid Features resource that is available for trial and subscription enrollments within the AMC Paid Features suite of insight expansion options, powered by Amazon Advertising. This resource is available to Amazon Advertisers that operate Prime Video Channels within the supported AMC Paid Features account marketplaces (US/CA/JP/AU/FR/IT/ES/UK/DE).

The AMC datasets associated with Prime Video Channel Insights are listed below:

- `amazon_pvc_enrollments`
- `amazon_pvc_streaming_events_feed`

## amazon\_pvc\_enrollments

Amazon Prime Video Channels enrollment records presented per benefit id. The `pv_subscription_id` column can be converted to metrics via COUNT() or COUNT(distinct) SQL aggregate functions.

| Field Category | Name | Data Type | Metric/Dimension | Description | Aggregation Threshold |
|----------------|------|-----------|------------------|-------------|---------------------|
| Prime Video Channel Insights | marketplace\_id | LONG | Dimension | ID of the marketplace where the Amazon Prime Video Channel enrollment event occurred\. | INTERNAL |
| Prime Video Channel Insights | marketplace\_name | STRING | Dimension | The marketplace associated with the Amazon Prime Video Channel record | LOW |
| Prime Video Channel Insights | no\_3p\_trackers | BOOLEAN | Dimension | Indicates if this item prohibits third\-party tracking | LOW |
| Prime Video Channel Insights | pv\_benefit\_id | STRING | Dimension | Amazon Prime Video subscription benefit identifier | INTERNAL |
| Prime Video Channel Insights | pv\_benefit\_name | STRING | Dimension | Amazon Prime Video subscription benefit name | LOW |
| Prime Video Channel Insights | pv\_billing\_type | STRING | Dimension | Amazon Prime Video billing type for the enrollment record | MEDIUM |
| Prime Video Channel Insights | pv\_end\_date | DATE | Dimension | Interval\-specific end date associated with the PVC enrollment record | LOW |
| Prime Video Channel Insights | pv\_enrollment\_status | STRING | Dimension | Status for the PVC enrollment record | LOW |
| Prime Video Channel Insights | pv\_is\_latest\_record | BOOLEAN | Dimension | Indicator of whether the PVC enrollment record is the most recent record within the table | INTERNAL |
| Prime Video Channel Insights | pv\_is\_plan\_conversion | BOOLEAN | Dimension | Indicates whether the PVC enrollment record has converted from Free Trial to a subscription\. This is often associated with a change in the billing type for the PVC subscription | LOW |
| Prime Video Channel Insights | pv\_is\_plan\_start | BOOLEAN | Dimension | Indicates whether the PVC enrollment record is the start of a enrollment\. This is often the opening of a free trial or the first subscription record for the PV subscription ID | LOW |
| Prime Video Channel Insights | pv\_is\_promo | BOOLEAN | Dimension | Indicator of whether the PVC enrollment record is associated with a promotional offer | LOW |
| Prime Video Channel Insights | pv\_offer\_name | STRING | Dimension | Amazon Prime Video subscription offer name | HIGH |
| Prime Video Channel Insights | pv\_start\_date | DATE | Dimension | Interval\-specific start date associated with the PVC enrollment record | LOW |
| Prime Video Channel Insights | pv\_sub\_event\_primary\_key | STRING | Dimension | Unique identifier for the PVC enrollment event | INTERNAL |
| Prime Video Channel Insights | pv\_subscription\_id | STRING | Dimension | Amazon Prime Video subscription id | INTERNAL |
| Prime Video Channel Insights | pv\_subscription\_name | STRING | Dimension | Amazon Prime Video subscription name | LOW |
| Prime Video Channel Insights | pv\_subscription\_product\_id | STRING | Dimension | Amazon Prime Video subscription product identifier | INTERNAL |
| Prime Video Channel Insights | pv\_unit\_price | DECIMAL | Dimension | Unit price for the PVC enrollment record | NONE |
| Prime Video Channel Insights | user\_id | STRING | Dimension | User ID of the customer | VERY\_HIGH |
| Prime Video Channel Insights | user\_id\_type | STRING | Dimension | Type of user ID | LOW |



## amazon\_pvc\_streaming\_events\_feed

Amazon Prime Video Channel Streaming and engagement events. Provides Amazon Prime Video Channel engagement metrics including content metadata, request context and duration. Events are session based and presented at the PVC benefit-level. The `pv_session_id` columns can be converted to metrics via COUNT() or COUNT(distinct) SQL aggregate functions.

| Field Category | Name | Data Type | Metric/Dimension | Description | Aggregation Threshold |
|----------------|------|-----------|------------------|-------------|---------------------|
| Prime Video Channel Insights | marketplace\_id | LONG | Dimension | Marketplace ID of the event | INTERNAL |
| Prime Video Channel Insights | marketplace\_name | STRING | Dimension | Marketplace name of the event | LOW |
| Prime Video Channel Insights | no\_3p\_trackers | BOOLEAN | Dimension | Indicates if item prohibits third\-party tracking | LOW |
| Prime Video Channel Insights | pv\_access\_type | STRING | Dimension | Content access type \(e\.g\., free, prime subscription, rental, purchase\) | LOW |
| Prime Video Channel Insights | pv\_gti\_cast | ARRAY | Dimension | Content cast members | LOW |
| Prime Video Channel Insights | pv\_gti\_content\_entity\_type | STRING | Dimension | Content entity type \(e\.g\., Short Film, Educational\) | LOW |
| Prime Video Channel Insights | pv\_gti\_content\_rating | STRING | Dimension | Content rating | LOW |
| Prime Video Channel Insights | pv\_gti\_content\_type | STRING | Dimension | Content type \(e\.g\., TV Episode, Movie, Promotion\) | LOW |
| Prime Video Channel Insights | pv\_gti\_director | ARRAY | Dimension | Content directors | LOW |
| Prime Video Channel Insights | pv\_gti\_episode | STRING | Dimension | Series episode | LOW |
| Prime Video Channel Insights | pv\_gti\_event\_context | STRING | Dimension | Content event context | LOW |
| Prime Video Channel Insights | pv\_gti\_event\_item | STRING | Dimension | Content event item | LOW |
| Prime Video Channel Insights | pv\_gti\_event\_league | STRING | Dimension | Content event league | LOW |
| Prime Video Channel Insights | pv\_gti\_event\_name | STRING | Dimension | Content event name | LOW |
| Prime Video Channel Insights | pv\_gti\_event\_sport | STRING | Dimension | Content event sport | LOW |
| Prime Video Channel Insights | pv\_gti\_genre | ARRAY | Dimension | Content genre | LOW |
| Prime Video Channel Insights | pv\_gti\_is\_live | BOOLEAN | Dimension | Indicates if content is live | LOW |
| Prime Video Channel Insights | pv\_gti\_release\_date | DATE | Dimension | Content release date | LOW |
| Prime Video Channel Insights | pv\_gti\_season | STRING | Dimension | Series season | LOW |
| Prime Video Channel Insights | pv\_gti\_series\_or\_movie\_name | STRING | Dimension | Series or movie name | LOW |
| Prime Video Channel Insights | pv\_gti\_studio | STRING | Dimension | Content studio | LOW |
| Prime Video Channel Insights | pv\_gti\_title\_name | STRING | Dimension | Content title \(e\.g\., episode name\) | LOW |
| Prime Video Channel Insights | pv\_is\_avod | BOOLEAN | Dimension | Indicates if content is advertiser\-supported VOD | LOW |
| Prime Video Channel Insights | pv\_is\_channels | BOOLEAN | Dimension | Indicates if content is a channel | LOW |
| Prime Video Channel Insights | pv\_is\_hh\_share | BOOLEAN | Dimension | Indicates if content is shared within household | LOW |
| Prime Video Channel Insights | pv\_is\_svod | BOOLEAN | Dimension | Indicates if content is subscription VOD | LOW |
| Prime Video Channel Insights | pv\_is\_user\_initiated | BOOLEAN | Dimension | Indicates if content playback was user\-initiated | LOW |
| Prime Video Channel Insights | pv\_material\_type | STRING | Dimension | Video material type \(e\.g\., full, live, promo, trailer\) | LOW |
| Prime Video Channel Insights | pv\_offer\_group | STRING | Dimension | Content offer group \(e\.g\., free, prime, rental, purchase\) | LOW |
| Prime Video Channel Insights | pv\_playback\_date\_utc | DATE | Dimension | Date of the event in UTC | LOW |
| Prime Video Channel Insights | pv\_playback\_dt\_utc | TIMESTAMP | Dimension | Timestamp of the event in UTC | MEDIUM |
| Prime Video Channel Insights | pv\_seconds\_viewed | DECIMAL | Metric | Seconds of view time associated with streaming event | MEDIUM |
| Prime Video Channel Insights | pv\_session\_id | STRING | Dimension | Unique identifier for streaming session | VERY\_HIGH |
| Prime Video Channel Insights | pv\_stream\_type | STRING | Dimension | Type of stream content \(e\.g\., linear TV, live\-event, VOD\) | LOW |
| Prime Video Channel Insights | pv\_streaming\_asin | STRING | Dimension | Amazon Standard Identification Number \(ASIN\) | LOW |
| Prime Video Channel Insights | pv\_streaming\_geo\_country | STRING | Dimension | Country where event was accessed | LOW |
| Prime Video Channel Insights | pv\_streaming\_gti | STRING | Dimension | Global title information | LOW |
| Prime Video Channel Insights | pv\_streaming\_language | STRING | Dimension | Language of the event | LOW |
| Prime Video Channel Insights | user\_id | STRING | Dimension | User ID that performed the event | VERY\_HIGH |
| Prime Video Channel Insights | user\_id\_type | STRING | Dimension | Type of User ID | LOW |

