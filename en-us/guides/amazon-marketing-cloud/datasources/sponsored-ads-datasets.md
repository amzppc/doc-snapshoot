---
title: Sponsored ads datasets
description: The sponsored ads datasets
type: guide
interface: api
---
# Onboarding sponsored ads to AMC

In order to onboard onto Sponsored Products and Sponsored Display, you must be able to provide entity ids for each advertiser they wish to add. Generally, your Ad Tech Representative can help identify those if you are unable to. Once you have the entity ids, send request to the AMC team to either create your instance or update your existing instance with those ids.

### Sponsored Products and Sponsored Display Introduction

The `amazon_attributed_conversions `views and the `sponsored_ads_traffic `include the `ad_product_type `column, which contains a string to denote a general description of the ad product. For example, it contains `sponsored_products` to denote Sponsored Products attributed conversions or traffic. The `ad_product_type `is only populated for sponsored ads and is NULL for DSP campaigns.

Finally, there are a few things to keep in mind regarding time:

- Columns names and the default time zone in AMC and the console will differ and
- Specific to conversions, atribution intervals (conversion windows) and conversion vs traffic time need to be specified in AMC.

#### Column Mappings

Columns differ between Sponsored Display and Sponsored Products, or Sponsored Brands. Note that **14d** (14 Day) can be replaced with any interval of \_1d, \_7d, \_14d (see [Attribution Interval](guides/amazon-marketing-cloud/datasources/overview#arbitrary-attribution-intervals)).


| Sponsored Products                    | User facing title                 | AMC attributed conversion column            |
| ------------------------------------- | --------------------------------- | ------------------------------------------- |
| **attributedConversions14d**          | 14 Day Total Orders (#)           | total\_purchases (OR total\_purchases\_clicks) |
| **attributedConversions14dSameSKU**   | 14 Day Advertised ASIN Orders (#) | purchases (OR purchases\_clicks)             |
| **attributedUnitsOrdered14d**         | 14 Day Total Units (#)            | total\_units\_sold[\_clicks]                   |
| **attributedUnitsOrdered14dSameSKU**  | 14 Day Advertised ASIN Units (#)  | units\_sold [\_clicks]                       |
| **attributedUnitsOrdered14dOtherSKU** | 14 Day Brand Halo ASIN Units (#)  | brand\_halo\_units\_sold [\_clicks]           |
| **attributedSales14d**                | 14 Day Total Sales                | total\_product\_sales [\_clicks]              |
| **attributedSales14dSameSKU**         | 14 Day Advertised ASIN Sales      | product\_sales[\_clicks]                      |
| **attributedSales14dOtherSKU**        | 14 Day Brand Halo ASIN Sales      | brand\_halo\_product\_sales[\_clicks]         |


### Sponsored ads traffic table

For the  detailed schema of the **sponsored\_ads\_traffic**, see [sponsored\_ads\_traffic](guides/amazon-marketing-cloud/datasources/sponsored_ads_traffic)

#### Sponsored ads Instructional Queries

Search the Instructional Query Library for 'Sponsored Brands', 'Sponsored Display' and 'Sponsored Products' to find more examples of use cases.
