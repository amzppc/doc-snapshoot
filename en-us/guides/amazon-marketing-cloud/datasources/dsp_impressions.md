---
title: Amazon DSP impressions table
description: The Amazon DSP impressions table
type: guide
interface: api
---

# Amazon DSP impressions table

## dsp\_impressions

Records of all impressions delivered to viewers. This table contains the most granular record of every impression delivered, allowing you to query the all viewers reached through an Amazon DSP campaign.

Field Category | Name | Data Type | Metric / Dimension | Description | Aggregation Threshold 
---|---|---|---|---|---
 Advertiser Setup | advertiser | STRING | Dimension | Advertiser name\. Example: “Example Company” | LOW 
 Advertiser Setup | advertiser\_id | LONG | Dimension | Customer\-facing ID of advertiser\. Example: 8443640090301 | LOW 
 Advertiser Setup | advertiser\_timezone | STRING | Dimension | Advertiser time zone\. Example: “America/New\_York” | LOW 
 Costs | audience\_fee | LONG | Metric | The fee (in microcents) that Amazon DSP charges to customers to utilize Amazon audiences. | NONE 
 Bidding | bid\_price | LONG | Metric | Bid price in micro\-cents | MEDIUM 
 Impression Delivery Device Info | browser\_family | STRING | Dimension | Family of browser used\. Example: “Chrome” | LOW 
 Campaign Setup | campaign | STRING | Dimension | Name of campaign\(called “order” in DSP Console\) | LOW 
 Campaign Setup | campaign\_budget\_amount | LONG | Dimension | Amount of budget for the campaign in millicents | LOW 
 Campaign Setup | campaign\_end\_date | TIMESTAMP | Dimension | End date of campaign in advertiser time zone | LOW 
 Campaign Setup | campaign\_end\_date\_utc | TIMESTAMP | Dimension | End date of campaign in UTC | LOW 
 Campaign Setup | campaign\_id | LONG | Dimension | The ID of the Amazon DSP campaign responsible for the impression event. In this table, campaign\_id and campaign\_id\_string fields will show the same value for a given event. | LOW 
 Campaign Setup | campaign\_id\_string | STRING | DIMENSION | The ID of the Amazon DSP campaign responsible for the impression event. In this table, campaign\_id and campaign\_id\_string fields will show the same value for a given event\. | LOW 
 Campaign Setup | campaign\_sales\_type | STRING | Dimension | Campaign Sales Type | LOW 
 Campaign Setup | campaign\_source | STRING | Dimension | Campaign Source | LOW 
 Campaign Setup | campaign\_start\_date | TIMESTAMP | Dimension | Start date of campaign in advertiser time zone | LOW 
 Campaign Setup | campaign\_start\_date\_utc | TIMESTAMP | Dimension | Start of campaign in UTC | LOW 
 Creative Setup | creative | STRING | Dimension | Name of creative\. Example: “Example Creative Name” | LOW 
 Creative Setup | creative\_id | LONG | Dimension | Customer\-facing creative ID\. Example: 4301568770201 | LOW 
 Creative Setup | creative\_size | STRING | Dimension | Size in pixels of slot in which ad is shown\. Format: YYYxZZZ | LOW 
 Creative Setup | creative\_type | STRING | Dimension | Type of creative\. Example: “Shazam HTML – AAP” | LOW 
 Costs | currency\_iso\_code | STRING | Dimension | Currency ISO Code | LOW 
 Costs | currency\_name | STRING | Dimension | Currency Name | LOW 
 Supply | deal\_id | STRING | Dimension | Customer facing deal ID | LOW 
 Supply | deal\_name | STRING | Dimension | Customer facing deal name | LOW 
 Impression Delivery Device Info | device\_id | STRING | Dimension | Device ID | VERY\_HIGH 
 Impression Delivery Device Info | device\_type | STRING | Dimension | Device type upon which the ad is displayed | LOW 
 Geography | dma\_code | STRING | Dimension | \(US only\) Nielsen Designated Market Area within which the ad was displayed | LOW 
 Advertiser Setup | entity\_id | STRING | Dimension | Customer\-facing ID of entity – can be found in the URL within the DSP Console\. Example: “ENTITYA6I16E0BHHHY” | LOW 
 Impression Delivery Info | impression\_cost | LONG | Metric | Impression cost in millicents | NONE 
 Impression Delivery Info | impression\_date | DATE | Dimension | MM/DD/YYYY\(advertiser time zone\) | LOW 
 Impression Delivery Info | impression\_date\_utc | DATE | Dimension | MM/DD/YYYY\(UTC\) | LOW 
 Impression Delivery Info | impression\_day | INTEGER | Dimension | 01\- 31 \(advertiser time zone\) | LOW 
 Impression Delivery Info | impression\_day\_utc | INTEGER | Dimension | 01\- 31 \(UTC\) | LOW 
 Impression Delivery Info | impression\_dt | TIMESTAMP | Dimension | Timestamp of impression in advertiser time zone\. Example: 2019\-06\-18T03:45:55\.000 | MEDIUM 
 Impression Delivery Info | impression\_dt\_hour | TIMESTAMP | Dimension | Timestamp of impression in advertiser time zone truncated to hour\. Example: 2019\-06\-18T03:00:00\.000 | LOW 
 Impression Delivery Info | impression\_dt\_hour\_utc | TIMESTAMP | Dimension | Timestamp of impression in UTC truncated to hour\. Example: 2019\-06\-18T08:00:00\.000Z | LOW 
 Impression Delivery Info | impression\_dt\_utc | TIMESTAMP | Dimension | Timestamp of impression in UTC\. Example: 2019\-06\-18T03:45:55\.000 | MEDIUM 
 Impression Delivery Info | impression\_hour | INTEGER | Dimension | 00 – 23\(advertiser time zone\) | LOW 
 Impression Delivery Info | impression\_hour\_utc | INTEGER | Dimension | 00 – 23\(UTC\) | LOW 
 Impression Delivery Info | impressions | LONG | Metric | Count of each impression\. For each impression event, this value will be 1, but the value can be summed, averaged, and so on across various dimensions | NONE 
 Supply | is\_amazon\_owned | BOOLEAN | Dimension | Is Amazon Owned | LOW 
 Supply | is\_mobile | BOOLEAN | Dimension | Is Mobile | LOW 
 Geography | iso\_country\_code | STRING | Dimension | ISO 3166 Alpha\-2 country code based on the user's IP address\. Example: “US”\. | LOW 
 Geography | iso\_state\_province\_code | STRING | Dimension | Alpha\-2 state or province code\. Example: “CO” | LOW 
 Line item setup | line\_item | STRING | Dimension | Name of line item\(ad\) | LOW 
 Line item setup | line\_item\_budget\_amount | LONG | Dimension | Line item budget amount in millicents | LOW 
 Line item setup | line\_item\_end\_date | TIMESTAMP | Dimension | End date of line item in advertiser time zone | LOW 
 Line item setup | line\_item\_end\_date\_utc | TIMESTAMP | Dimension | End date of line item in UTC | LOW 
 Line item setup | line\_item\_id | LONG | Dimension | Customer\-facing line item ID | LOW 
 Line item setup | line\_item\_price\_type | STRING | Dimension | Line Item Price Type | LOW 
 Line item setup | line\_item\_start\_date | TIMESTAMP | Dimension | Start date of line item in advertiser time zone | LOW 
 Line item setup | line\_item\_start\_date\_utc | TIMESTAMP | Dimension | Start date of line item in UTC | LOW 
 Line item setup | line\_item\_status | STRING | Dimension | Status of line item\. Example: “ENDED” | LOW 
 Line item setup | line\_item\_type | STRING | Dimension | Type of line item\. Example “CLASS\_I\_WEB” | LOW 
 Advertiser Setup | merchant\_id | LONG | Dimension | Merchant id | LOW 
 Impression Delivery Device Info | operating\_system | STRING | Dimension | OS used | LOW 
 View Information | placement\_is\_view\_aware | BOOLEAN | Dimension | True if the placement is view aware\. | LOW 
 View Information | placement\_view\_rate | DECIMAL | Dimension | The view rate of the placement\(0\.0 \- 1\.0\) | LOW 
 Costs | platform\_fee | LONG | Metric | The fee (in microcents) that Amazon DSP charges customers to access and use Amazon DSP. The platform fee (also known as the technology fee or console fee in other contexts) is calculated as a percentage of the supply cost. | LOW 
 Geography | postal\_code | STRING | Dimension | Postal code\(also referred to as "zip code" in the US\)\. The postal code is looked up from a user's IP address, with the iso\_country\_code prepended\. Example: "US\-10118"\. If the country is known, but the postal code is unknown, only the country code will be populated in postal\_code\. Example: "US" | HIGH 
 Campaign Setup | product\_line | STRING | Dimension | Product line for campaign\. Note that this value is optional at campaign setup, and may not match categories used in targeting\. Example: “Consumables – Grocery” | LOW 
 Impression Delivery Info | request\_dt | TIMESTAMP | Dimension | DEPRECATED\- Returns null\. Instead use impression\_dt | MEDIUM 
 Impression Delivery Info | request\_dt\_hour | TIMESTAMP | Dimension | DEPRECATED\- Returns null\. Instead use impression\_dt\_hour | LOW 
 Impression Delivery Info | request\_dt\_hour\_utc | TIMESTAMP | Dimension | DEPRECATED\- Returns null\. Instead use impression\_dt\_hour\_utc | LOW 
 Impression Delivery Info | request\_dt\_utc | TIMESTAMP | Dimension | DEPRECATED\- Returns null\. Instead use impression\_dt\_utc | MEDIUM 
 Impression Delivery Info | request\_tag | STRING | Dimension | An ID related to the impression event\. For example, an impression served will have a request\_tag of X\. The click event associated with the the impression and any conversion events attributed to the impression will also have a request tag of X\. While this occurs infrequently, there are some duplicate request\_tag values for impression and click events\. As a result, it is not recommended to use request\_tag as a JOIN key without first grouping by it; this approach is only recommended for joining the segment data sources to the attributed data sources\. See the example query 'conversions\_by\_segment\_with\_impressions\.sql'\. It is not recommended to use request\_tag to JOIN impressions, clicks, and attributed data sources\. Rather, it is a best practice to first consider using UNION ALL in a query instead of joining based on the request\_tag to combine data sources\. See the example query clicks\_and\_impressions\_UNION\_ALL\.sql\. Insights that involve user\-level ad exposure across data sources should not use request\_tag and should instead join based on user\_id\. See the various overlap and path\-to\-conversion queries in the 'example queries' folder\. | VERY\_HIGH 
 Supply | site | STRING | Dimension | Domain part of the URL of the inventory being auctioned\. Note that some sites are masked due to contractual obligations\. Example: “amazon\.com” | LOW 
 Costs | supply\_cost | LONG | Metric |The cost (in microcents) that Amazon DSP pays a publisher or publisher ad tech platform (such as an SSP or exchange) for an impression.  | LOW 
 Supply | supply\_source | STRING | Dimension | Name of supply source\(exchange\)\. Example: “Amazon Publisher Services” | LOW 
 Supply | supply\_source\_is\_view\_aware | BOOLEAN | Dimension | True if the supply source is view aware\. | LOW 
 Supply | supply\_source\_view\_rate | DECIMAL | Dimension | The view rate of the supply source\(0\.0 \- 1\.0\) | LOW 
 Costs | third\_party\_fees | LONG | Metric | The sum of all third\-party fees charged for the impression (in microcents). | NONE 
 Costs | total\_cost | LONG | Metric | Total cost (in millicents). | NONE 
 Impression Delivery Info | user\_id | STRING | Dimension | User ID | VERY\_HIGH 
 Bidding | winning\_bid\_cost | LONG | Metric | SOFT DEPRECATED\- not deprecated now and will continue to function, but it's not recommended\. Winning Bid Cost \(Microcents\) | MEDIUM 
 Segment Info | matched\_behavior\_segment\_ids | ARRAY | Metric | Contains the IDs of the behavior segments the user belongs to that were also targeted by the ad\. | LOW 
 Segment Info | segment\_marketplace\_id | INTEGER | Metric | Contains the segment marketplace ID\. | LOW 
 Segment Info | user\_behavior\_segment\_ids | ARRAY | Metric | Contains the IDs of the behavior segments the user belongs to\. | LOW 
 Advertiser Setup | advertiser\_country | STRING | Dimension | DSP advertiser object country attribute | LOW 
 Creative Setup | creative\_category | STRING | Dimension | the DSP type of ad: display, video, or other\. | LOW 
 Creative Setup | creative\_duration | INTEGER | Dimension | DSP creative object duration attribute\(common for video format\) | LOW 
 Creative Setup | creative\_is\_link\_in | STRING | Dimension | DSP creative object, clickthrough type attribute\(link in versus link out\)\. | LOW 
 Campaign Setup | campaign\_primary\_goal | STRING | Dimension | The DSP object goal attribute | LOW 
 Campaign Setup | campaign\_insertion\_order\_id | STRING | Dimension | The DSP object IO/DSM attribute from the DSP | LOW 
 Campaign Setup | campaign\_status | STRING | Dimension | Status of the DSP campaign object | LOW 
 Campaign Setup | campaign\_flight\_id | LONG | Dimension | DSP campaign flight identifier | LOW 
 Supply | ad\_slot\_size | STRING | Dimension | The inventory ad slot size associated with the DSP impression event | LOW 
 Impression Delivery Device Info | environment\_type | STRING | Dimension | the environment type associated with the DSP auction\(site versus app objects\) | LOW 
 Impression Delivery Device Info | device\_make | STRING | Dimension | the user\-agent based device make attribute associated with the DSP auction\. | LOW 
 Impression Delivery Device Info | device\_model | STRING | Dimension | the user\-agent based device model attribute associated with the DSP auction\. | LOW 
 Geography | city\_name | STRING | Dimension | the geography city name attribute associated with the ip\-to\-geo lookup for the DSP auction\. | HIGH 
 Impression Delivery Device Info | os\_version | STRING | Dimension | the user\-agent based device OS version attribute associated with the DSP auction\. | LOW 
 Supply | supply\_source\_id | LONG | Dimension | the identifier for the supply source name associated with the DSP impression | LOW 
 Supply | publisher\_id | STRING | Dimension | the publisher ID associated with the DSP impression | LOW 
 Supply | app\_bundle | STRING | Dimension | the app bundle id associated with the DSP impression, often appstore specific formating and data structure\. | LOW 
 Supply | page\_type | STRING | Dimension | the type of page associated with the DSP impression\(e\.g\. detail, thank you, etc\.\) | LOW 
 Supply | slot\_position | STRING | Dimension | the ad position associated with the DSP impression\(e\.g\. ATF, BTF\) | LOW 
 Costs | managed\_service\_fee | LONG | Metric | The fee (in microcents) that Amazon DSP charges to customers for campaign management services provided by Amazon’s in-house service team. The managed service fee is calculated as a percentage of the supply cost. | NONE 
 Costs | ocm\_fee | LONG | Metric | The fee (in microcents) that Amazon DSP charges for the use of omnichannel measurement studies. The omnichannel metrics fee is calculated as a percentage of the supply cost. | LOW 