---
title: Amazon DSP views table
description: The Amazon DSP views table
type: guide
interface: api
---
# Amazon DSP views table

## dsp\_views

Represents all view events and all measurable events. Measurable events are from impression that were able to be measured. Viewable events represent events that met a viewable standard. The metric `viewable_impressions` is the count of viewable impressions using the IAB standard (50% of the impression's pixels were on screen for at least 1 second). Note the `dsp_views` table records a single impression more than one time if it is both measurable and viewable according to IAB's standards. An impression that is both measurable and viewable according to IAB's standards is recorded in one row as a measurable event and in another row as a viewable event. Note that while all impression events from `dsp_views` are recorded in `dsp_impressions`, not all impression events from `dsp_impressions` are recorded in `dsp_views` because not all impression events can be categorized as viewable, measurable or unmeasurable.

| Field Category | Name  | Data Type  | Metric / Dimension  | Description  | Aggregation Threshold |
|---|---|---|---|---|---|
| Advertiser Setup | advertiser | STRING | Dimension | Advertiser name\. Example: “Example Company” | LOW |
| Advertiser Setup | advertiser\_id | LONG | Dimension | Customer\-facing ID of advertiser\. Example: 8443640090301 | LOW |
| Advertiser Setup | advertiser\_timezone | STRING | Dimension | Advertiser time zone\. Example: “America/New\_York” | LOW |
| Campaign Setup | campaign | STRING | Dimension | Name of campaign \(called “order” in DSP Console\) | LOW |
| Campaign Setup | campaign\_end\_dt | TIMESTAMP | Dimension | End date of campaign in advertiser time zone | LOW |
| Campaign Setup | campaign\_end\_dt\_utc | TIMESTAMP | Dimension | End date of campaign in UTC | LOW |
| Campaign Setup | campaign\_id | LONG | Dimension | The ID of the Amazon DSP campaign responsible for the impression event. In this table, campaign\_id and campaign\_id\_string fields will show the same value for a given event. | LOW 
| Campaign Setup | campaign\_id\_string | STRING | DIMENSION | The ID of the Amazon DSP campaign responsible for the impression event. In this table, campaign\_id and campaign\_id\_string fields will show the same value for a given event\. | LOW 
| Campaign Setup | campaign\_sales\_type | STRING | Dimension | Campaign Sales Type | LOW |
| Campaign Setup | campaign\_source | STRING | Dimension | Campaign Source | LOW |
| Campaign Setup | campaign\_start\_dt | TIMESTAMP | Dimension | Start date of campaign in advertiser time zone | LOW |
| Campaign Setup | campaign\_start\_dt\_utc | TIMESTAMP | Dimension | Start of campaign in UTC | LOW |
| Creative Setup | creative | STRING | Dimension | Name of creative\. Example: “Example Creative Name” | LOW |
| Creative Setup | creative\_id | LONG | Dimension | Customer\-facing creative ID\. Example: 4301568770201 | LOW |
| Creative Setup | creative\_type | STRING | Dimension | Type of creative\. Example: “Shazam HTML – AAP” | LOW |
| Advertiser Setup | entity\_id | STRING | Dimension | Customer\-facing ID of entity – can be found in the URL within the DSP Console\. Example: “ENTITYA6I16E0BHHHY” | LOW |
| Event Delivery Info | event\_date | DATE | Dimension | Event Date \(Advertiser Timezone\) | MEDIUM |
| Event Delivery Info | event\_date\_utc | DATE | Dimension | Event Date \(UTC\) | MEDIUM |
| Event Delivery Info | event\_dt | TIMESTAMP | Dimension | Event Timestamp \(Advertiser Timezone\) | MEDIUM |
| Event Delivery Info | event\_dt\_hour | TIMESTAMP | Dimension | Event Timestamp \(Advertiser Timezone\) Truncated to Hour | LOW |
| Event Delivery Info | event\_dt\_hour\_utc | TIMESTAMP | Dimension | Event Timestamp \(UTC\) Truncated to Hour | LOW |
| Event Delivery Info | event\_dt\_utc | TIMESTAMP | Dimension | Event Timestamp \(UTC\)\. Example: 2019\-06\- 18T03:45:55\.000 | MEDIUM |
| Event Delivery Info | event\_type | STRING | Dimension | Event Type | LOW |
| Event Delivery Info | events | LONG | Metric | Event Count | NONE |
| Impression Delivery Info | impression\_id | STRING | Dimension | Join key to display\_impressions\.request\_tag\. The impression\_id and the request\_tag are identical\. See the defintion of request\_tag in the data source display\_impressions\. | VERY\_HIGH |
| Line item setup | line\_item | STRING | Dimension | Name of line item \(ad\) | LOW |
| Line item setup | line\_item\_end\_dt | TIMESTAMP | Dimension | End date of line item in advertiser time zone | LOW |
| Line item setup | line\_item\_end\_dt\_utc | TIMESTAMP | Dimension | End date of line item in UTC | LOW |
| Line item setup | line\_item\_id | LONG | Dimension | Customer\-facing line item ID | LOW |
| Line item setup | line\_item\_price\_type | STRING | Dimension | Line Item Price Type | LOW |
| Line item setup | line\_item\_start\_dt | TIMESTAMP | Dimension | Start date of line item in advertiser time zone | LOW |
| Line item setup | line\_item\_start\_dt\_utc | TIMESTAMP | Dimension | Start date of line item in UTC | LOW |
| Line item setup | line\_item\_status | STRING | Dimension | Status of line item\. Example: “ENDED” | LOW |
| Line item setup | line\_item\_type | STRING | Dimension | Type of line item\. Example “CLASS\_I\_WEB” | LOW |
| Impression Viewability | measurable\_impressions | LONG | Metric | Measurable impression count\. Note that a single impression that is measurable could also be viewable, but not all measurable impressions are viewable\. If an impression is both measurable and viewable, each event for the single impression is listed as a separate row\. | LOW |
| Advertiser Setup | merchant\_id | LONG | Dimension | Merchant id | LOW |
| Impression Viewability | unmeasurable\_viewable\_impressions | LONG | METRIC | Unmeasurable viewable impression count\. These represent impressions at a normally view\-aware supply source / placement that could not be measured due to special circumstances \(such as using an uncommon browser\) but were estimated to be viewable based on the usual view rate of the supply source / placement\. Note that impressions which are served to a non\-view\-aware supply source / placement will NOT show up in this data source \- use the display\_impressions data source to query these impressions\. | LOW |
| Impression Delivery Info | user\_id | STRING | Dimension | User ID | VERY\_HIGH |
| Impression Viewability | view\_definition | LONG | METRIC | View Type \(For Viewable Impressions\) | LOW |
|Impression Viewability|viewable_impressions|	LONG	|METRIC |	Viewable impression count (IAB standard). Note that a single impression that is viewable has two events: a viewable impression and a measurable impression. Each event for the single impression is listed as a separate row.	| LOW |
|Bidding	| winning\_bid\_amount	|LONG	|Metric	|Price for the impression in micro\-cents	| MEDIUM|
| Advertiser Setup | advertiser\_country | STRING | Dimension | DSP advertiser object country attribute | LOW |
| Creative Setup | creative\_category | STRING | Dimension | The DSP type of ad: display, video, or other\. | LOW |
| Creative Setup | creative\_duration | INTEGER | Dimension | DSP creative object duration attribute \(common for video format\) | LOW |
| Creative Setup | creative\_is\_link\_in | STRING | Dimension | DSP creative object, clickthrough type attribute \(link in versus link out\)\. | LOW |
| Campaign Setup | campaign\_primary\_goal | STRING | Dimension | The DSP object goal attribute | LOW |
| Campaign Setup | campaign\_insertion\_order\_id | STRING | Dimension | The DSP object IO/DSM attribute from the DSP | LOW |
| Campaign Setup | campaign\_status | STRING | Dimension | status of the DSP campaign object | LOW |
| Campaign Setup | campaign\_flight\_id | LONG | Dimension | DSP campaign flight identifier | LOW |
| Supply | ad\_slot\_size | STRING | Dimension | The inventory ad slot size associated with the DSP impression event | LOW |
| Impression Delivery Device Info | environment\_type | STRING | Dimension | The environment type associated with the DSP auction \(site versus app objects\) | LOW |
| Impression Delivery Device Info | device\_make | STRING | Dimension | The user\-agent based device make attribute associated with the DSP auction\. | LOW |
| Impression Delivery Device Info | device\_model | STRING | Dimension | The user\-agent based device model attribute associated with the DSP auction\. | LOW |
| Geography | city\_name | STRING | Dimension | The geography city name attribute associated with the ip\-to\-geo lookup for the DSP auction\. | HIGH |
| Impression Delivery Device Info | os\_version | STRING | Dimension | The user\-agent based device OS version attribute associated with the DSP auction\. | LOW |
| Supply | supply\_source\_id | LONG | Dimension | The identifier for the supply source name associated with the DSP impression | LOW |
| Supply | publisher\_id | STRING | Dimension | The publisher ID associated with the DSP impression | LOW |
| Supply | app\_bundle | STRING | Dimension | The app bundle id associated with the DSP impression, often appstore specific formating and data structure\. | LOW |
| Supply | page\_type | STRING | Dimension | The type of page associated with the DSP impression \(e\.g\. detail, thank you, etc\.\) | LOW |
| Supply | slot\_position | STRING | Dimension | The ad position associated with the DSP impression \(e\.g\. ATF, BTF\) | LOW |
|Impression Delivery Device Info |operating\_system | STRING | Dimension | Operating system | LOW |

