---
title: Amazon DSP video events table
description: The Amazon DSP video events table
type: guide
interface: api
---
# Amazon DSP video events table

## dsp\_video\_events\_feed

> [WARNING] Workflows that use these tables will time out when run over extended periods of time.

This table has the exact same basic structure as [dsp_impressions](guides/amazon-marketing-cloud/datasources/dsp_impressions) table but in addition to that, the table provides video metrics for each of the video creative events triggered by the video player and associated with the impression event.

The following table shows columns **in addition** to the ones listed for `dsp_impressions`.

| Field Category | Name                 | Data Type | Metric / Dimension | Description                                                  | Aggregation Threshold |
| -------------- | -------------------- | --------- | ------------------ | ------------------------------------------------------------ | --------------------- |
| Video          | video\_impression     | LONG      | Metric             | The first frame of the ad was shown. (only shown once)       | NONE                  |
| Video          | video\_start          | LONG      | Metric             | Video was started                                            | NONE                  |
| Video          | video\_creative\_view  | LONG      | Metric             | The first frame of the creative in the ad was shown (synonymoustostart) | NONE                  |
| Video          | video\_first\_quartile | LONG      | Metric             | Video play reached 25% of total content                      | NONE                  |
| Video          | video\_midpoint       | LONG      | Metric             | Video play reached 50% of total content                      | NONE                  |
| Video          | video\_third_quartile | LONG      | Metric             | Video play reached 75% of total content                      | NONE                  |
| Video          | video\_complete       | LONG      | Metric             | Video play reached 100% of total content                     | NONE                  |
| Video          | video\_pause          | LONG      | Metric             | Video was paused                                             | NONE                  |
| Video          | video\_resume         | LONG      | Metric             | Video was resumed after a pause                              | NONE                  |
| Video          | video\_mute           | LONG      | Metric             | Video was muted                                              | NONE                  |
| Video          | video\_unmute         | LONG      | Metric             | Video was unmuted                                            | NONE                  |
| Video          | video\_click          | LONG      | Metric             | Video clicked by user                                        | NONE                  |
| Video          | video\_replay         | LONG      | Metric             | Video was started after it was completed                     | NONE                  |
| Video          | video\_skip\_backward  | LONG      | Metric             | Video playback jumped backwards in time                      | NONE                  |
| Video          | video\_skip\_forward   | LONG      | Metric             | Video playback jumped forward in time                        | NONE                  |

