---
title: Amazon DSP impressions by segments table
description: The Amazon DSP impressions by segments table
type: guide
interface: api
---

# Amazon DSP impressions tables

## dsp\_impressions\_by\_matched\_segments
## dsp\_impressions\_by\_user\_segments


>[WARNING] Workflows that use these tables will time out when run over extended periods of time.

These two tables have exactly the same basic structure as `dsp_impressions` table but in addition to that they provide segment level information for each of the segments that was either targeted by the ad or included the user that received the impression. This means that each impression appears multiple times in these tables. `dsp_impressions_by_matched_segments` shows only the segments that included the user and were targeted by the ad at the time of the impression. `dsp_impressions_by_user_segments` shows all segments that include the user, `behavior_segment_matched` is set to "1" if the segment was targeted by the ad.
The following table shows columns in addition to the ones listed for `dsp_impressions`.

The reference here shows only columns in addition to the ones listed for `dsp_impressions`.




| Field   Category | Name                         | Data Type | Metric / Dimension | Description                                                  | Aggregation Threshold |
| ---------------- | ---------------------------- | --------- | ------------------ | ------------------------------------------------------------ | --------------------- |
| Segments         | behavior\_segment\_description | STRING    | Dimension          | User behavior segment description                            | LOW                   |
| Segments         | behavior\_segment\_id          | INTEGER   | Dimension          | User behavior segment identifier                             | LOW                   |
| Segments         | behavior\_segment\_matched     | LONG      | Metric             | 1 if the user behavior segment was targeted by the ad that received the   impression, 0 otherwise | LOW                   |
| Segments         | behavior\_segment\_name        | STRING    | Dimension          | User behavior segment name                                   | LOW                   |