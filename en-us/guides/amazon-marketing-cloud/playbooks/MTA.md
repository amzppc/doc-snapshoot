---
title: Amazon Marketing Cloud - Custom attribution model
description: AMC Custom attribution model playbook
type: guide
interface: api
---
# Custom attribution - Markov chain

This playbook provides a custom attribution solution using a Markov process, from AMC query to audience creation. The scope includes querying signals from AMC, generating a custom attribution model  in SageMaker, analyzing results, and creating
an audience that can be exported for activation in Amazon demand-side platform (Amazon DSP). While this playbook covers a specific solution; further customization is encouraged.

## Defining custom attribution

Attribution is a marketing process that considers  a set of a customer's touchpoints on the journey to purchase products on Amazon. It credits each touchpoint with a certain percentage of the total sale. Unlike linear, time-based, and last touch attribution methods,  the custom attribution model (CAM) supports the specific needs of the advertiser to reflect that each touchpoint in a customer journey ultimately drives a customer towards conversion. In this playbook, it leverages the Markov process statistical model to describe the sequence of advertising touchpoints based on the probability that they have led to a conversion.

The custom attribution model unlocks insights such as Return-on-ad-spend (ROAS) by touchpoint, customer lifecycle analysis, and spend optimization for action; and coupled with AMC's Audiences feature, you can create and activate specific audiences with Amazon DSP. Audiences can be based on respective customer journeys so that they receive the right messages at the right time while keeping customer signals safe.

## Benefits

Through the guidance in this playbook, you will get answers to the following questions:

- How to identify common customer journeys using AMC?
  - How to pull aggregated signals around customers journeys & touchpoints from AMC for analysis?
  - Which touchpoints are most important for conversion on a customer's journey?
- How to run an CAM on aggregated signals?
- What ad should I send to customers who are familiar with my brand?
- How to interpret and utilize the custom attribution results for marketing actions?
- How do I create the "Added to Cart but never checked out" audience using the AMC Audience API?
- How to create and export an audience into Amazon DSP?

### Sample chart showing how attributed revenue varies across touchpoints

![sample-top5roas](/_images/amazon-marketing-cloud/playbooks/MTA_1_top-5-revenue-attributed-touchpointvsspend.png)

## Prerequisites

To effectively use this playbook, following are the concepts we recommend you know:

### Technical concepts

- **AMC concepts**: Specifically concepts such as data aggregation thresholds, privacy limitations, and other restrictions listed in the [AMC documentation](guides/amazon-marketing-cloud/overview).
- **Knowledge of [AMC SQL (link only available for AMC UI users)](https://advertising.amazon.com/marketing-cloud/instructional-queries/f53b1ad1359ef811fcdccdc8c6e69422b83bf06a163da53fddf79b6ca729edb6)**: The process outlined here relies on altering SQL code to match your purposes.
- **Basic Python**: This playbook is built using Python, and may require some scripting to adapt it to your use case.
- **Basic AWS Knowledge** [Amazon SageMaker](https://docs.aws.amazon.com/sagemaker/latest/dg/whatis.html), and [Amazon Athena](https://docs.aws.amazon.com/athena/latest/ug/what-is.html) in specific. You will need to know how to set up an Amazon SageMaker Notebook in accordance with your company's standards.

### Marketing concepts

- **Attribution Time Window**: Knowledge of the appropriate lookback window for customer journeys as it relates to your specific use  case. The default is 14 days in AMC.
- **Level of Aggregation**: If the level of aggregation for your touchpoints is too granular, you may hit aggregation thresholds in AMC. For instance, measuring at a creative level vs a line-item level where the line item might contain too many distinct possibilities.

See [Basics of Amazon Attribution](https://advertising.amazon.com/library/guides/basics-of-amazon-attribution) for more information.

### Recommended

We also recommend that you deploy [Amazon Marketing Cloud Insights on AWS](https://aws.amazon.com/advertising-marketing/solutions/amc-insights/) or a similar custom solution. The AMC insights on AWS provides several solutions:

- **Workflow manager:** Service to create and schedule AMC query executions.
- **Data Lake**: Service to take signals from the source bucket, transform into parquet and catalogue in Athena. This allows for signals to pulled via Athena. This is a standard offering with AWS insights product.

### Playbook steps

The playbook is structured in the following order:

1. [Query signals from AMC for attribution analysis](#query-signals-from-amc-for-attribution-analysis)
2. [Transform and prepare generated data](#transform-and-prepare-generated-data)
3. [Create attribution model](#create-attribution-model)
4. [Validate data](#validate-data)
5. [Analyze and create KPIs](#analysis)
6. [Visualizations and insights](#visualizations-and-insights)
7. [Create and activate audiences](#create-and-activate-audiences)

## Query signals from AMC for attribution analysis

The script below will require you to make several decisions on how you want your attribution model to work. These decisions will depend on your domain knowledge and the objectives you intend to achieve.

At a high level the SQL script below will do the following:

- Query ad signal from source tables in AMC: Amazon DSP, Sponsored Display and Sponsored Product.
- Query conversion signal and metrics from the table conversions in AMC.
- Combine signals at a user-level to capture touchpoint order and conversion signals.

> [NOTE] User signals will be aggregated along touchpoint journeys.

A simplified version of the signal flow diagram is:

![image](/_images/amazon-marketing-cloud/playbooks/MTA_2_signal-flow-diagram.png)

To know more, refer to the [Custom Attribution Instructional Query (link only available for AMC UI users)](https://advertising.amazon.com/marketing-cloud/instructional-queries/72313993a488c7c1821e67c6fdd7a366cd434ef65ea14a2aae4d96fd16a2e627#2.3).

### User input and decisions

- **Level of aggregation**: You will need to decide the level of aggregation with your marketing team. Common levels include audience, product and/or campaign. The default for this playbook is at the creative level.
- **SQL level of aggregation**
  The default for this example script is at the "creative" level; however, you can choose your level of aggregation by replacing "creative" in the SQL script below.
- **Size of attribution window:** This value will determine how much data will be pulled. The common value and the value in the script below is 14 days.

### Custom attribution SQL script

Copy and paste the script below into the Query editor of your instance in the [AMC UI](https://advertising.amazon.com/marketing-cloud) to run it.

> [NOTE] You can also use the `POST` method of the `/amc/reporting/{instanceId}/workflowExecutions` endpoint to create an [ad hoc workflow execution](guides/amazon-marketing-cloud/get-started/make-your-first-call#on-demand-workflow-execution) with the SQL script below as single-line query.

<details class="details-bar">
  <summary>Click to see: Custom attribution SQL script</summary>

```
WITH display AS ( 
    SELECT user_id,
        impression_dt_utc AS event_time,
        creative AS level_of_agg,
        SUM(impressions) AS impressions
-- use this if you need to extend your impression window to include pre study period impressions (currently it's set at 14 days extended):TABLE(EXTEND_TIME_WINDOW('display_impressions', 'P14D', 'P0D'))
    FROM dsp_impressions
    GROUP BY 1,2,3
        ), 
-- include below if you have sponsored ads (Sponsored Products and/or Sponsored Brands) traffic
sp AS ( 
    SELECT  user_id, 
        ad_product_type, 
        event_dt_utc AS event_time, 
        creative AS level_of_agg, 
        SUM(impressions) AS impressions 
-- if you extended impression window for display_impressions AND if you have data going back that far, use the same extended window. 
    FROM sponsored_ads_traffic
    WHERE ad_product_type = 'sponsored_products' 
    and impressions > 0 
    and creative is not null
-- apply your own filter(s) for SP campaigns 
    GROUP BY 1,2,3,4 
    ), 
-- include below if you have sponsored ads (SP and/or SB) traffic 
sd AS ( 
    SELECT  user_id, 
        event_dt_utc AS event_time, 
        creative AS level_of_agg, 
        SUM(impressions) AS impressions 
    -- if you extended impression window for display_impressions AND if you have data going back that far, use the same extended window. 
    FROM sponsored_ads_traffic
    WHERE ad_product_type = 'sponsored_display' 
    and impressions > 0 
    and creative is not null
    -- apply your own filter(s) for SP campaigns 
    GROUP BY 1,2,3 
        ), 
-- Combining both DSP and SP 
all_media AS ( 
    SELECT user_id, 
       event_time, 
       level_of_agg, 
       impressions 
    FROM display 
    UNION ALL 
    SELECT user_id, 
       event_time, 
       level_of_agg, 
       impressions 
    FROM sp 
    UNION ALL 
    SELECT user_id, 
       event_time, 
       level_of_agg, 
       impressions 
    FROM sd 
    ),
  
convs AS ( 
    SELECT 
        user_id, 
        MIN(event_dt_utc) AS event_time, -- DO NOT CHANGE event_dt_utc to event_dt 
        SUM(total_product_sales) AS total_product_sales
-- use this if you need to extend your conversion window to include post study period conversions (currently it's set at 14 days after): TABLE(EXTEND_TIME_WINDOW('conversions', 'P0D', 'P14D')) 
        FROM conversions
        WHERE event_subtype = 'order'  -- event_type_description in ('Product purchased') -- replace with your conversion type(s), e.g. Product purchased 
-- for endemic conversions, please filter track_asin below to only include brand ASins. 
        GROUP BY 1 
        ),
      
convert_prepathing AS ( 
    SELECT  m.user_id, 
        m.event_time, 
        m.level_of_agg, 
        m.impressions, 
        1 AS convert_flag, 
        c.total_product_sales
    FROM all_media m 
    INNER JOIN convs c 
    ON m.user_id = c.user_id 
    WHERE m.event_time < c.event_time 
        ), 
nonconvert_prepathing AS ( 
    SELECT  m.user_id, 
        m.event_time, 
        m.level_of_agg, 
        m.impressions, 
        0 AS convert_flag, 
        c.total_product_sales
    FROM all_media m 
    LEFT JOIN convs c 
    ON m.user_id = c.user_id 
    WHERE c.user_id is null 
    ),
  
    prepathing as (
    SELECT  user_id, 
        event_time, 
        CONCAT(CASE WHEN ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY event_time ASC) > 9 THEN CAST( ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY event_time ASC) AS VARCHAR) ELSE CAST(CONCAT( '0', ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY event_time ASC)) AS VARCHAR) END ,'&',level_of_agg) level_of_agg1,
        convert_flag, 
        impressions, 
        total_product_sales
    FROM convert_prepathing 
    UNION ALL 
    SELECT  user_id, 
        event_time, 
        CONCAT(CASE WHEN ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY event_time ASC) > 9 THEN CAST( ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY event_time ASC) AS VARCHAR) ELSE CAST(CONCAT( '0', ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY event_time ASC)) AS VARCHAR) END,'&',level_of_agg) level_of_agg1, 
        convert_flag, 
        impressions, 
        total_product_sales
    FROM nonconvert_prepathing 
    ),
  
ranked AS ( 
    SELECT  NAMED_ROW( 'level_of_agg', level_of_agg1) AS group2, 
        user_id, 
        convert_flag, 
        SUM(impressions) AS impressions, 
        MAX(total_product_sales) AS total_product_sales
    FROM prepathing 
    GROUP BY 1,2,3 
        ),
      
paths_assembling AS ( 
    SELECT  user_id, 
        convert_flag, 
        ARRAY_SORT(COLLECT(ranked.group2.level_of_agg)) AS path, 
        SUM(impressions) AS impressions, 
        MAX(total_product_sales) AS total_product_sales
    FROM ranked 
    GROUP BY 1,2 
        ), 
      
paths_assembled AS ( 
    SELECT  user_id, 
        convert_flag, 
        ARRAY_TO_STRING(path, '@') as touchpoint_path, 
        SUM(impressions) AS impressions, 
        MAX(total_product_sales)  AS   total_product_sales
    FROM paths_assembling 
    GROUP BY 1,2,3 
        ), 
pathing AS ( 
    SELECT  touchpoint_path, 
        SUM(case when convert_flag = 1 then 1 else 0 end) converters, 
        SUM(case when convert_flag = 0 then 1 else 0 end) nonconverters, 
        COUNT(DISTINCT user_id) AS reach, 
        SUM(impressions) AS impressions, 
        SUM(total_product_sales) AS total_product_sales
    FROM paths_assembled 
    GROUP BY 1 
        )  
      
SELECT  touchpoint_path as touchpoint_ordered_path, 
    converters, 
    nonconverters, 
    SUM(impressions) AS impressions, 
    SUM(total_product_sales) AS total_product_sales
FROM pathing 
WHERE (converters != 1 and nonconverters != 1) 
GROUP BY 1,2,3 

```

</details>

### Custom attribution SQL script output

After the script has run successfully, download the output .csv file from the **Submitted queries** section below the Query editor.
Or, if you ran an API call for your script, [download the results](guides/amazon-marketing-cloud/get-started/get-your-results#download-a-csv-file-containing-query-results).

The output of this SQL statement will be a .csv that contains:

- **touchpoint\_ordered\_path**: List of touchpoints in order seen - by default aggregated to creative
- **converters**: Users who saw ads and converted
- **nonconverters**: Users who saw ads and did not convert
- **metrics**: impressions, total product sales

#### Sample output

![touchpoint](/_images/amazon-marketing-cloud/playbooks/MTA_3_script_output.png)

Now, upload the .csv file to your Amazon SageMaker instance or you can pull it in via Amazon Athena if datalake is configured.

To upload your .csv file, click on the "upload" arrow in the top right of the Amazon SageMaker web UI.
Browse to locate and to select the file to upload.

## Transform and prepare generated data

Data transformation focuses around using the output of your query from AMC and reformatting it to be in the correct format for ingestion by the attribution models.

### Import libraries and packages

If your Amazon SageMaker instance does not have the `ChannelAttribution` package, then install the following packages using the script below:

`ChannelAttribution` is package that is publicly available to be downloaded. In Amazon SageMaker, prepending the pip command with a "!" installs the package to your instance.

These packages are required for manipulating data, creating attribution models, and for visualizations.

<details class="details-bar">
  <summary>Click to see: Script to import library and packages</summary>

```
# #Uncomment here to install packages, only required to run once.
# !pip install -q --upgrade setuptools
# !pip install -q Cython
# !pip install -q ChannelAttribution

#import required libraries
import pandas as pd
import re
from ChannelAttribution import *

#import plotly for visualizations
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import plotly.io as pio
```

</details>

### Import and clean data

The following code will read your data set and then remove empty rows or rows returned as not applicable (NA).

> [NOTE] The empty rows represent redacted data from AMC where there aren\'t enough individuals within the subset to return a result.

Ensure that the file path points to where your data files are stored for upload. The file path provided is for the sample data.

<details class="details-bar">
  <summary> Click to see: Script to read data and remove empty rows</summary>

```
# Read in Sample Data - Note this data was randomly generated, please use only as an example
# Test with sample data or repoint to your local data
df = pd.read_csv('Data/Attribution_Sample_Data_Generated.csv')
df = df.dropna(subset= ['touchpoint_ordered_path'])
validation_df = df.copy()

#Clean up pathing data
pathing = df.copy()
pathing['touchpoint_ordered_path_raw'] = pathing['touchpoint_ordered_path'].str.replace('O&O','OO')

#Assign funnel same value
pathing['funnel'] = pathing['touchpoint_ordered_path_raw']
pathing.drop('touchpoint_ordered_path', axis=1, inplace=True)
pathing

```

</details>

> [NOTE] You can also import data using Amazon Athena, which is also the approach [\"Amazon Marketing Cloud Insights on AWS\" ](https://aws.amazon.com/advertising-marketing/solutions/amc-insights/)recommends.

Listed below is code for pulling signals values from Amazon Athena. This code pulls in only records from the most recently placed file from the Amazon Athena table.

Some of this is specific for the formatting and partitioning used by the AWS solution, this is only in the "file\_last\_modified" column which is one of the partitions in Amazon Athena and can be used to query.

<details class="details-bar">
  <summary>Click to see: Script to import files from Amazon Athena</summary>

```
### Import files from Amazon Athena
### These will be created via Workflow Manager using AWS Insights

# !pip install PyAthena
from pyathena import connect
from pyathena.pandas.util import as_pandas

database = "amc_insights_db"

#Workflow #1 - CAM Touchpoint data
table = "insights_cam_touchpoint_chain_adhoc"


cursor = connect(
                work_group = 'primary',
                 region_name="us-east-1").cursor()
cursor.execute(f"SELECT * FROM  {database}.{table} WHERE file_last_modified = (SELECT MAX(file_last_modified) FROM {database}.{table})")

df = as_pandas(cursor)
df.head()

#Only needed for Athena
df = df[df['touchpoint_ordered_path'] != 'nan']

#Workflow #2 - CAM Spend data
table = "insights_cam_spend_per_touchpoint_adhoc"

cursor = connect(
                work_group = 'primary',
                 region_name="us-east-1").cursor()
cursor.execute(f"SELECT * FROM  {database}.{table} WHERE file_last_modified = (SELECT MAX(file_last_modified) FROM {database}.{table})")

spend = as_pandas(cursor)
spend.head()

```

</details>

#### Sample output

![import_data](/_images/amazon-marketing-cloud/playbooks/MTA_4_SQLScript_output.png)

## Calculate pathing data

The code below ensures data is correctly formatted and ordered. The formatting applied here allows the data to be ingested by heuristic models we will use. See [Attribution Models.](#create-attribution-model)

<details class="details-bar">
  <summary>Click to see: Script to calculate pathing data </summary>

```
# This regex function removes the & and the numbers
# Example: 01&DSP_SampleTouchpoint becomes -> DSP_SampleTouchpoint

pattern = re.compile('.*(\s*)&(\s*)')
def apply_pattern(s):
    out_list = []
    for item in s:
        out_list.append(pattern.sub('\\1\\2', item))
    return out_list

# Split the string on the symbol @

pathing['split_string'] = pathing['touchpoint_ordered_path_raw'].apply(lambda x: x.split('@'))

#Order touchpoints by the numeric value 01, 02, etc.

pathing['sorted_touchpoint_list'] = pathing['split_string'].apply(lambda x: 
                            sorted(x,key=lambda y: int(str(y)[0]) if y[1]=='&' else int(str(y)[0:2])))

# remove numeric values and '&' signs from touchpoints

pathing['touchpoint_list_clean'] = pathing['sorted_touchpoint_list'].apply(apply_pattern)

# Transform list back into a string with '@' symbols as the seperator

pathing['touchpoint_ordered_path_clean'] = pathing['touchpoint_list_clean'].apply(lambda x: '@'.join(x))

# Calculate last touch and path length

pathing['path_length'] = [len(s.split('@')) for s in pathing['touchpoint_ordered_path_clean'].tolist()]
pathing['last_touch_str'] = [s.split('@')[-1] for s in pathing['touchpoint_ordered_path_clean'].tolist()]

# Drop intermediate calculation columns

pathing.drop(columns = ['split_string', 'sorted_touchpoint_list', 'touchpoint_list_clean'], inplace = True)
pathing.head()
```

</details>

### Sample output

![pathingdata](/_images/amazon-marketing-cloud/playbooks/MTA_5_Python1_output.png)

## Create attribution model

Next, we will generate heuristic models (First Touch, Last Touch, and Linear Touch Attribution Models) as well as the Markov Model for the custom attribution.

### Heuristic model

The heuristic model covers the following attribution scenarios:

- First Touch Attribution: The first touchpoint gets credit.
- Last Touch Attribution: The last touchpoint gets credit.
- Linear Touch Attribution: All touchpoints receive equal credit.

Touchpoint path, converters, and sales are passed into the model to determine conversion and revenue attribution for each scenario.

<details class="details-bar">
  <summary>Click to see: Heuristic model definition </summary>

```
heumod_revenue = heuristic_models(pathing,
var_path='touchpoint_ordered_path_clean',
var_conv = 'converters', 
var_value = 'total_product_sales', sep='@')
heumod_revenue.head()               

```

</details>

### Sample output

![heuristicmodel](/_images/amazon-marketing-cloud/playbooks/MTA_6_heuristic_model_output.png)

### Markov Model

Find the minimum Markov Model order that gives a good representation of customers' behavior for data considered. It requires paths that do not lead to conversion as input.

Suggested order is the number of touchpoints back in the Markov chain the model uses for calculation, this number will vary based on the data provided.

<details class="details-bar">
  <summary>Click to see: Markov model definition</summary>

```
order = choose_order(pathing, var_path = "touchpoint_ordered_path_clean",
var_conv = "converters",
var_null = "nonconverters",
max_order = 10,
sep = "@",
ncore = 4,
plot=True)
Suggested order: 7
```

</details>

The Markov model below will take time to converge after a given number of simulations. To decrease the time the model takes, you can simplify it by reducing the order (and therefore complexity), or by removing outliers.

<details class="details-bar">
  <summary>Click to see: Script to estimate a k-order Markov model</summary>

```
# Estimate a k-order Markov model from customer journey data. Differently from markov_model, this function iterates estimation until a desidered convergence is reached and enables multiprocessing.

markovmod_revenue = markov_model(
pathing, var_path='touchpoint_ordered_path_clean',
var_conv='converters',
var_null='nonconverters',
var_value = 'total_product_sales',
order=order[2],
out_more =True,
sep='@',
seed=424,
ncore=4,
flg_adv= False)

```

</details>

- **Number of simulations:** 100000 - Convergence reached: 4.26% \< 5.00%
- **Percentage of simulated paths that successfully end before maximum number of steps (54) is reached:** 99.94%

## Joining data

The following code block joins the output of the Markov Model, and the Heuristic Model together to create one data frame. This is the data frame used to interpret results.

<details class="details-bar">
  <summary>Click to see: Script to clean Markov output and join data from Heuristic output </summary>

```
#clean up markov output
markov_revenue = markovmod_revenue['result'].rename(columns=
{'total_conversion_value': 'attribution_revenue', 'total_conversions': 'markov'})

#clean up removal effect
removal_revenue = pd.DataFrame.from_dict(markovmod_revenue['removal_effects'].rename(columns 
={'removal_effects_conversion': 'removal_effects_conversion_revenue', 
'removal_effects_conversion_value': 'removal_effects_conversion_value_revenue'}))

# Join data from the hueristic data set
result = markov_revenue.join(heumod_revenue.set_index('channel_name'), on='channel_name')

# Join data from the removal data set
result = result.join(removal_revenue.set_index('channel_name'), on='channel_name')
```

</details>

## Spend query

To get ROAS and other spend-related metrics after your CAM analysis, you need to pull the spend data at the aggregation level specified earlier. In this example we used creative.
Run the following query in the Query editor of your instance in the AMC UI .

<details class="details-bar">
  <summary>Click to see: Script for spend-related metrics</summary>

```
WITH display AS (
    SELECT 
        creative,
-- Divide by 100,000 as total_cost is returned in millicents, divide by 1,000 to get to cents,then convert to dollars, divide by 100: total_cost/1,000/100 = total_cost/100,000
        SUM(total_cost/100000) AS spend
-- use this if you need to extend your impression window to include pre study period impressions (currently it's set at 14 days extended):TABLE(EXTEND_TIME_WINDOW('display_impressions', 'P0D', 'P14D'))
    FROM TABLE(EXTEND_TIME_WINDOW('dsp_impressions', 'P0D', 'P14D'))
    GROUP BY 1
        ),
-- include below if you have sponsored ads (Sponsered Products and/or Sponsored Brands) traffic
sp AS (
    SELECT
        creative,
-- Divide by 100,000,000 as total_cost is returned in mircrocents, divide by 1,000,000 to get to cents,then convert to dollars, divide by 100: total_cost/1,000,000/100 = total_cost/100,000,000
        SUM(spend/100000000) AS spend
-- if you extended impression window for display_impressions AND if you have data going back that far, use the same extended window.
    FROM TABLE(EXTEND_TIME_WINDOW('sponsored_ads_traffic', 'P0D', 'P14D'))
    WHERE ad_product_type = 'sponsored_products'
-- apply your own filter(s) for SP campaigns
    GROUP BY 1
    ),
-- include below if you have sponsored ads (SP and/or SB) traffic
sd AS (
    SELECT  
        creative,
-- Divide by 100,000,000 as total_cost is returned in Microicents, divide by 1,000,000 to get to cents,then converted to dollars, divide by 100: total_cost/1,000,000/100 = total_cost/100,000,000
        SUM(spend/100000000) AS spend
    -- if you extended impression window for display_impressions AND if you have data going back that far, use the same extended window.
    FROM TABLE(EXTEND_TIME_WINDOW('sponsored_ads_traffic', 'P0D', 'P14D'))
    WHERE ad_product_type = 'sponsored_display'
    GROUP BY 1
        ),
-- Combining both DSP and SP
all_media AS (
    SELECT creative,
       spend
    FROM display
    UNION ALL
    SELECT creative,
       spend
    FROM sp
    UNION ALL
    SELECT creative,
       spend
    FROM sd
    )
SELECT  creative as channel_name, SUM(spend) as Spend
FROM all_media
GROUP BY creative

```

</details>

Download the output of the query (in the.csv format) and upload the .csv file to your Amazon SageMaker notebook.

## Incorporate spend data

To incorporate the spend data, import and perform a join on the data with your CAM results. This will allow you to generate key KPIs such as ROAS on your chosen level of aggregation.

<details class="details-bar">
  <summary>Click to see: Script to incorporate spend data</summary>

```
#Adjust the filepath below to point to your spend data set

spend = pd.read_csv('Data/sample_data_spend.csv')
result = result.merge(spend.set_index('channel_name'), on='channel_name')
result.head(5)

```

</details>

![spenddata](/_images/amazon-marketing-cloud/playbooks/MTA_7_spend_data.png)

## Validate data

The purpose of this step is to ensure that data has not been lost or duplicated throughout the process.

<details class="details-bar">
  <summary>Click to see: Script to validate output data</summary>

```
# Create data frame of summation of values from just after outlier removal
# Change column names to allow for a join
validation_df.rename(columns = {'converters': 'markov', 'total_product_sales': 'attribution_revenue',
                                'spend': 'Spend'}, inplace = True)
validation_sum = validation_df.sum().rename('value').to_frame().reset_index()

#Create data frame of summation of values from final result
values_df = result.sum().rename('value').to_frame().reset_index()
#Combine validation data frames
df_validate = pd.merge(values_df, validation_sum,
                       how = 'inner', left_on= 'index', right_on = 'index',  
                       suffixes = ('_validation', '_result'), validate = 'one_to_one')

#calculate difference between validation dataframe and result dataframe
df_validate['difference'] = df_validate['value_validation'] - df_validate['value_result']
df_validate['percent_difference'] = df_validate['difference']/(df_validate['value_result'])

# Raise exception is value difference is above .5%
# This threshold is to account for minor changes and rounding errors
def difference_threshold_validation(x):
    # if difference greater than .5%
    if x > .005:
        display(df_validate.style.format({'value_validation': '{:,.0f}', 'value_result': '{:,.0f}', 
                          'difference': '{:,.0f}', 'percent_difference': '{:.2%}'}))
        raise Exception('Validation failure. Please check for duplication in joins or data pulled at different times')
      
# Apply exception to each row in "percent difference"
df_validate['percent_difference'].apply(difference_threshold_validation)
# Display output 
display(df_validate.style.format({'value_validation': '{:,.0f}', 'value_result': '{:,.0f}', 
                          'difference': '{:,.0f}', 'percent_difference': '{:.2%}'}))

```

</details>

| - | index        | value\_validation | value\_result | difference | percent\_difference |
| - | ------------ | ----------------- | ------------- | ---------- | ------------------- |
| 0 | markov       | 4,404,510         | 4,404,510     | 0          | 0\.00%              |
| 1 | attribution\_revenue | 3,190,408         | 3,190,408     | 0          | 0\.00%              |
| 2 | Spend        | 2,598,742         | 2,623,971     | \-25,229   | \-0\.96%            |
|   |              |                   |               |            |                     |

**Optional**: After incorporating spend and validating the data, you can export your data by uncommenting and running the following command to export your results for analysis in your favorite BI Tool. You can download the file locally by right clicking on the object and selecting \"download\".

```
\# result.to_csv(\'Data/result_export.csv\', index = False)
```

## Analyze and calculate KPIs

The calculations below are utilized for further visualizations.

- **ROAS**: Return on Ad Spend - Ratio of revenue to spend.
- **Revenue Per Markov**: Revenue per Markov conversion.

<details class="details-bar">
  <summary>Click to see: Script to calculate KPIs </summary>

```
### Calculation tab
#Revenue per Markov Calc
result['revenue_per_markov'] = result['attribution_revenue']/result['markov']
#Roas 
result['attribution_roas'] = result['attribution_revenue']/result['Spend']
result['last_touch_roas'] = result['last_touch_value']/result['Spend']
result['linear_touch_roas'] = result['linear_touch_value']/result['Spend']
result.head(10)
```

</details>

![revenuemarkov](/_images/amazon-marketing-cloud/playbooks/MTA_8_rev_markov.png)

## Visualizations and insights

- [Revenue vs spend](#revenue-vs-spend)
- [Conversions vs spend](#conversions-vs-spend)
- [ROAS vs spend](#roas-vs-spend)
   

### Revenue vs spend

The chart below compares touchpoint revenue between different attribution models to answer:

* What are the highest value touchpoints?
* What are the major differences in revenue attribution between model types?

<details class="details-bar">
  <summary>Click to see: Visualization for Revenue vs spend </summary>

```

fig = make_subplots(specs=[[{"secondary_y": True}]])
viz_df = result.sort_values(by= 'attribution_revenue', ascending= False).head(15).copy()
pio.renderers.default = 'iframe'
fig.add_trace(
    go.Bar(x=viz_df['channel_name'], y=viz_df['attribution_revenue'], name="Attribution Revenue (Left Axis)"),
    secondary_y=False
)   
fig.add_trace(
    go.Bar(x=viz_df['channel_name'], y=viz_df['last_touch_value'], name="Last Touch Revenue (Left Axis)"),
    secondary_y=False
)  
fig.add_trace(
    go.Bar(x=viz_df['channel_name'], y=viz_df['linear_touch_value'], name="Linear Touch Revenue (Left Axis)"),
    secondary_y=False
)  
fig.add_trace(
    go.Scatter(x=viz_df['channel_name'], y=viz_df['Spend'], name='Spend (Right Axis)', mode="lines", marker = {'color' : 'green'}),
    secondary_y=False)

# Set y-axes titles
fig.update_yaxes(title_text='Revenue', secondary_y=False)
fig.update_yaxes(title_text='Spend', secondary_y=True, showgrid=False, color = 'green')

fig.update_yaxes(secondary_y=True, tickformat = "$,.0f")
fig.update_yaxes(secondary_y=False, tickformat = "$,.0f")

fig.update_layout(title_text= 'Top 15 Revenue Attributed to Touchpoints vs Spend')

fig.update_layout(legend=dict(
    orientation="v",
    yanchor="top",
    y=1.2,
    xanchor="left",
    x=1.2   
))
fig.show()

```

</details>

![top5revtouchpointvsspend](/_images/amazon-marketing-cloud/playbooks/MTA_1_top-5-revenue-attributed-touchpointvsspend.png)

The chart illustrates that touchpoints on the left generate more revenue than the touchpoints on the right. The graph shows us that the models attribute revenue differently to each touchpoint; however, which model is best for your use case depends on the industry and the marketing strategy you decide on. A clearer view on profitability can be viewed on the next chart:

- **According to the CAM**: Touchpoint test\_creative\_Thorium generates the most revenue while Touchpoint "test\_creative\_Thorium Mobile" generates the least.
- **According to Last Touch**: test\_creative\_Thorium is significantly closer to the second place test\_creative\_ThoriumUniversal.

### Conversions vs spend

Conversions help tell the story of quantity vs quality. If the goal is to get higher numbers of customers to convert, then this is the chart to tell the story. This can tell us where conversions were attributed historically using last touch, and where they are attributed now. This chart can help you determine what paths you'd like your users to take if your goal is simply conversion.

<details class="details-bar">
  <summary>Click to see: Visualization for conversion vs spend</summary>

```

fig = make_subplots(specs=[[{"secondary_y": True}]])
viz_df = result.sort_values(by= 'markov', ascending= False).head(15).copy()
fig.add_trace(
    go.Bar(x=viz_df['channel_name'], y=viz_df['markov'], name="Attribution Conversions (Left Axis)"))   
fig.add_trace(
    go.Bar(x=viz_df['channel_name'], y=viz_df['last_touch_conversions'], name="Last Touch Conversions (Left Axis)")
)  
fig.add_trace(
    go.Scatter(x=viz_df['channel_name'], y=viz_df['Spend'], name='Spend (Right Axis)', mode="lines", marker = {'color' : 'green'}),
    secondary_y=True)

# Set y-axes titles
fig.update_yaxes(title_text='Conversions', secondary_y=False)
fig.update_yaxes(title_text='Spend', secondary_y=True, showgrid=False, color = 'green')

fig.update_yaxes(secondary_y=True, tickformat = "$,.0f")

fig.update_layout(title_text= "Top 5 Attributed Conversions vs Spend",
    legend=dict(orientation="v", yanchor="top", y=1.2,xanchor="left", x=1))
fig.show()

```

</details>

 ![convvsspend](//_images/amazon-marketing-cloud/playbooks/MTA_9_attribute_conversionVsspend.png)

 

### ROAS vs spend

ROAS is a popular metric for attribution analysis because it enables a more complete picture on ads profitability. **Any ROAS value below 1.0 indicates negative profitability**. The recommendation is to further evaluate the touchpoint and its performance towards the desired metric (loyalty, conversion, consideration, or awareness).

<details class="details-bar">
  <summary>Click to see: Visualization for ROAS vs spend</summary>

```
fig = make_subplots(specs=[[{"secondary_y": True}]])
viz_df = result.sort_values(by= 'attribution_roas', ascending= False).head(15).copy()
fig.add_trace(
    go.Bar(x=viz_df['channel_name'], y=viz_df['attribution_roas'], name="Attribution Roas (Left Axis)"),
    secondary_y=False
)   
fig.add_trace(
    go.Bar(x=viz_df['channel_name'], y=viz_df['last_touch_roas'], name="Last Touch Roas (Left Axis)"),
    secondary_y=False
)  
fig.add_trace(
    go.Scatter(x=viz_df['channel_name'], y=viz_df['Spend'], name='Spend (Right Axis)', mode="lines", marker = {'color' : 'green'}),
    secondary_y=True)

# Set y-axes titles
fig.update_yaxes(title_text='ROAS', secondary_y=False)
fig.update_yaxes(title_text='Spend', secondary_y=True, showgrid=False, color = 'green')

fig.update_yaxes(secondary_y=True, tickformat = "$,.0f")

fig.update_layout(title_text= " Top 15 Attributed ROAS vs Spend")
fig.update_layout(legend=dict(
    orientation="v",
    yanchor="top",
    y=1.2,
    xanchor="left",
    x=1   
))
fig.show()

```

</details>
 
![roas_spend](/_images/amazon-marketing-cloud/playbooks/MTA_9_attributed_roasVSspend.png)

Touchpoint test\_creative\_Thorium has a high spend and a strong ROAS, but "test\_creative\_Thorium Mobile" has strong ROAS with low spend, and could benefit from increased funding.

Potential actions to take:

- Decrease funding to non-profitable ROAS touchpoints.
- Increase funding to high ROAS touchpoints.
   

## Create and activate audiences

You can build an SQL query to select your audience by using any combination of parameters or fields available in AMC.

Here we attempt to shift spending from our high spend ads to our high ROAS touchpoints to see if we can increase conversion. To do this, we would like to reach a segment of users who have added to cart and did not convert. Utilizing the AMC Audiences feature, submit the following query to create a unique audience to reach using Amazon DSP.

1. Log in to the AMC UI and navigate to the **Audiences** tab.
2. Click **Create audience**, and then select **Create new query** to view the Audiences query editor.
3. Copy the query below and paste the code in the Audiences query editor.
4. Identify the creative touchpoint you would like to send to the audience.
5. Log in to Amazon DSP to remarket the custom audience with the creative.

<details class="details-bar">
  <summary>Click to see: Script to create audiences</summary>

```

-- List of distinct user ids where the user ordered in the attribution window
WITH orders AS (
    SELECT
    DISTINCT user_id
    FROM TABLE(EXTEND_TIME_WINDOW('conversions',  'P0D', 'P0D')) 
    WHERE event_subtype = 'order' 
), 
-- List of distinct user ids where the user added to cart in the attribution window – starting 7 days before
cart as (
SELECT
    DISTINCT cart.user_id
    FROM TABLE(EXTEND_TIME_WINDOW('conversions',  'P7D', 'P0D')) cart
    WHERE cart.event_subtype = 'shoppingCart' 
),
-- List of distinct user ids where the user added to cart but did not order
comb as (
    SELECT cart.USER_ID
    FROM cart
    LEFT JOIN orders
    on cart.user_id = orders.user_id
    WHERE
    -- Filter to just records where user_id never ordered
    orders.user_id is null
)
SELECT DISTINCT USER_ID
FROM comb

```

</details>

## Troubleshooting

Listed below are some common issues with CAM analysis and tips to troubleshoot those.

### Excessive redaction due to each line not having enough unique users.

The following are the options to work around this issue:

#### Add a lookback limit on touchpoints.

- This can be done by adding in a ROW_NUMBER value, similar to the logic already used in the current query, as a new column change the
  ordering to be DESC. This will now provide an order of touchpoints with #1 being most recent. A filter can be added to limit only the last X touchpoints. A suggested value is 10. The result is that the longest chain will be 10. Allowing for much more user density along each path.
- Additional lookback window can also help as there will be more users to populate the different journeys. Use this with #1.

**If custom attribution is built out in a fully automated environment** the compute typically cannot be done in Amazon SageMaker as Amazon Sagemaker is typically more adhoc, and the custom attribution package is too large to load into a normal lambda function.

A suggested solution is to create a docker image with the necessary packages and runtime already installed, then use [AWS Lambda with that docker container](https://docs.aws.amazon.com/lambda/latest/dg/images-create.html).
