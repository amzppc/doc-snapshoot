---
title: Amazon Marketing Cloud - Playbooks Overview
description: AMC Playbooks overview
type: guide
interface: api
---
# What are AMC Playbooks?

Playbooks are intended to accelerate specific marketing use cases leveraging Amazon Marketing Cloud (AMC) data. The examples provided in the playbooks are based on a real-life use case, but are illustrative in nature. Each playbook lists the strategical and tactical perspective to achieve desired results.

These playbooks include a combination of adtech strategy, code (SQL & Python), and data visualizations to intimately describe how and why each use case creates value-generating actions from AMC.

### Intended audience

- Data analyst: You are experienced in SQL and use SQL to build and customize queries for specific advertiser needs.
- Data engineer: You can build data flow pipelines that run the automation of segment measurement into activation.
- Data scientist: You have an understanding of the scientific methodologies and assumptions required to conduct and apply the test results listed here.

Get started with the following playbooks:

* [Custom attribution model](guides/amazon-marketing-cloud/playbooks/MTA)
* [Customer journey analytics](guides/amazon-marketing-cloud/playbooks/CJA)
* [Customer long-term value](guides/amazon-marketing-cloud/playbooks/CLTV)
* [Programmatic audience framework](guides/amazon-marketing-cloud/playbooks/programmatic_audience_framework)
* [Off-Amazon conversions](guides/amazon-marketing-cloud/playbooks/off-amazon-conversions)
* [Optimal frequency analysis](guides/amazon-marketing-cloud/playbooks/optimal-frequency-analysis)
* [AMC lookalike audiences for promotional events](guides/amazon-marketing-cloud/playbooks/promotional_events)
* [Media performance optimization](guides/amazon-marketing-cloud/playbooks/media_performance_optimization)
* [Enhanced scoring for overlapping AMC audiences'](guides/amazon-marketing-cloud/playbooks/enhanced_scoring_for_overlapping_audiences)