---
title: Advertiser data upload - overview
description: Upload data
type: guide
interface: api
---
# Process overview

The high-level process to upload data to an AMC instance is described below. These steps are covered in greater detail in the following
sections.

**Step 1 – [Prepare data](#prepare-data)**

Prepare data in CSV, JSON, or Parquet format.

- Data containing hashed identifiers (email, first name, last name, address, etc.) must be [standardized](#standardization-rules) and [hashed](#hash-method--sha256) before it is uploaded to the AMC instance.
- Third party identifiers (such as MAIDs) should not be hashed.
- The only accepted hash method is SHA-256.
- Files should be compressed, although it is not required. AMC supports file compressions with algorithms BZIP, GZIP, ZIP, SNAPPY, or no compressions with NONE.
- Large files should be separated for optimal upload.  We recommend partitioning files larger than 1GB into multiple files for upload. 

**Step 2 – Create S3 bucket**

Create S3 bucket for storing data files. We recommend using a bucket separate from one created for AMC API report storage. The data upload S3 bucket must be within the **Connected AWS Account** associated to the AMC instance you will be uploading data to.

- To create an AWS account, refer to: [https://aws.amazon.com/free/](https://aws.amazon.com/free/)
- Set permissions for S3 bucket so AMC instance can read data files. See [S3 Bucket Creation](#create-s3-bucket) for further details.

**Step 3 – Create datasets using AMC APIs**

[Perform a dataset creation API call](docs/en-us/amc-advertiser-data-upload#tag/Data-Set/operation/CreateDataSet). 

This will create new empty dataset in the AMC instance. 

   - Column names specified when creating a dataset must match those in the data being uploaded.

**Step 4 - Upload datasets using AMC APIs**

[Perform a data upload API call](docs/en-us/amc-advertiser-data-upload#tag/Upload/operation/GetUpload). 
This API call requires the user to specify the S3 bucket (**sourceS3Bucket**) where data is stored, the specific file(s) to upload to AMC, and the dataset (schema) to which the data will be loaded. File(s) to be uploaded may be specified with one of the following:

   - **sourceS3Prefix** - Allows for uploading all S3 objects rooted at a single S3 prefix
   - **sourceFileS3Key** - Allows for uploading a single S3 object
   - **sourceManifestS3Key** - Allows for uploading multiple, distinct S3 object

>[NOTE] When uploading, we recommend that individual files do not exceed 1GB.. 

3. Optionally, use the Data Set Column API to modify columns in a dataset.

All [supported identifiers](#id-resolution-and-supported-identifiers) will be resolved to an AMC identity only when Amazon recognizes the individual. Failure to recognize an identity will result in a null AMC identity being stored.
Joining advertiser uploaded data and AMC campaign data can only be done via the **user_id** column. The original identifiers (e.g., hashed emails) cannot be used for joining or querying in AMC.

## Prepare data

### File format requirements

AMC accepts CSV, JSON, and Parquet files for upload. All options have specific formatting requirements for the upload files.

**CSV file requirements**

CSV files must be **UTF-8 encoded**.

The CSV format allows for any **fieldDelimiter**. Tab-separated values ("\t"), and pipe-separated values ("|") are also common.

```
"fileFormat": {
"csvDataFormat": {
"recordDelimiter": "\n",
"quoteEscapeCharacter": "\"\"",
"quoteCharacter": "\"",
"fieldDelimiter": ",",
"commentCharacter": "#"
}
} 
```

**JSON file requirements**

JSON files must contain one object per row of data. An example of the accepted JSON format can be seen below.

```
{"name": "Product A", "sku": 11352987, "quantity": 2, "pur_time": "2022-06-23T19:53:58Z"}
{"name": "Product B", "sku": 18467234, "quantity": 2, "pur_time": "2022-06-24T19:53:58Z"}
{"name": "Product C", "sku": 27264393, "quantity": 2, "pur_time": "2022-06-25T19:53:58Z"}
{"name": "Product A", "sku": 48572094, "quantity": 2, "pur_time": "2022-06-25T19:53:58Z"}
{"name": "Product B", "sku": 18278476, "quantity": 1, "pur_time": "2022-06-26T13:33:58Z"}
```

>[NOTE] Each row is a top-level object. No parent object or array is present in the JSON file.

**Parquet file requirements**

While there are no specific requirements for preparing parquet files, we recommend that you look up instructions to help you get started, depending on what you are using:
- [Python/Pandas](https://pandas.pydata.org/pandas-docs/version/1.1/reference/api/pandas.DataFrame.to_parquet.html)
- [Spark SQL](https://spark.apache.org/docs/latest/sql-data-sources-parquet.html)


**Data types, timestamp, and date formats**

Dataset columns can be defined with the following data types. Pay close attention to the accepted formats for **TIMESTAMP** and **DATE** columns. If values in CSV / JSON data do not meet the accepted format, the upload may fail.

Ensure all values in CSV / JSON data confirm the specified data type and format before uploading. Where possible, string values will be coerced to the corresponding numerical type and vice-versa, but no guarantees are made on the casting process.
The table below lists the format of accepted data types:


| Data Type            | Format                                             | Example              |
| -------------------- | -------------------------------------------------- | ---------------------|
| STRING               | UTF-8 encoded character data                       | My string data       |
| DECIMAL              | Numerical with two floating point level precision. | 123.45               |
| INTEGER (int 32-bit) | 32-bit numerical, no floating points               | 12345                |
| LONG (int 64-bit)    | 64-bit numerical, no floating points               | 1233454565875646     |
| TIMESTAMP (must be in UTC)           | yyyy-MM-ddThh:mm:ssZ                               | 2022-08-02T08:00:00Z |
| DATE                 | yyyy-MM-dd                                         | 2022-08-02           |

### Fact vs dimension datasets

Before data can be uploaded, a dataset must be created to store that data. As mentioned in the section on [DataSets API](guides/amazon-marketing-cloud/advertiser-data-upload/advertiser-data-sets), AMC supports two types of tables (also referred to datasets): Fact and Dimension. 

Queries run on fact data will only evaluate against events that fall within the query's time range. Example: Uploading purchase events, each with a TIMESTAMP type column, then querying for purchases that occurred on Tuesday.

Queries run on DIMENSION data will evaluate against the current set of records that have been uploaded at the time the query executes. Example: Uploading a customer list, which is a fixed set of records, with no time component, then querying for the count of customers aggregated by zip code.

Dimension datasets **must not specify** a main event time (`mainEventTime`) column. If you define a dataset where no column is marked with the `mainEventTime`: true parameter, it will be considered a Dimension dataset.


### ID resolution and supported identifiers

In order to join advertiser uploaded data with Amazon data at the user level, uploaded data should contain one or more of the supported
identifier columns.

When uploading data that contains one or more of the supported identifiers, each record is processed, and if successful will be matched with an existing Amazon user record. Records with a matched Amazon user can then be joined with Amazon data, enabling advertisers to attribute off-Amazon events to Amazon media events within AMC.
 When a match is found, a value for `user_id `(an anonymous user ID corresponding to an Amazon user) will be inserted in the row. The original identifiers are discarded and will not be made available to query in AMC. Records that contain a value for `user_id` can then be joined with Amazon data, enabling advertisers to attribute off-Amazon events to Amazon media events within AMC.

#### Hashed identifier column options

All hashed identifier columns must be standardized and hashed before uploading to AMC. See [File Preparation (Partitioning, Standardization, & Hashing).](#prepare-files-partitioning-standardization-and-hashing)

>[NOTE] If identifiers are not hashed, the upload will be rejected.

#### External identifier column options

##### Mobile Ad ID requirements

When a dataset is defined with a Mobile AdID (MAID) column, AMC will attempt to resolve those IDs to an Amazon ID. The Amazon ID will be used to join with existing datasets.

>[NOTE] MAID values are treated as case-insensitive.

### Prepare files (Partitioning, Standardization, and Hashing)

#### [Optional] Selecting a Partition Scheme (Period)

The partition size is specified on the dataset and is referred to as the **dataset period**. The following options are supported:

- PT1H (hourly partition)
- P1D (daily partition) - this is the default option
- P1M (monthly partition)

>[NOTE] For optimal query performance, select a partition scheme that satisfies the granularity of the queries you want to run, but also does not impact query performance.

Some examples:

If you specify your dataset period to be monthly (P1M), and upload your records then your queries to obtain analytics corresponding to last Thursday  will involve reading a month's worth of data. This is because Thursday is stored in a single partition with all other data for the month.
If you specify the dataset as hourly (PT1H), and upload records that take place over the span of a year and query analytics for last Thursday, then the queries will read data within 24 hourly partitions that correspond to Thursday, causing this query to be more efficient.

So, if you have a small volume of events and frequently query for analytics across long time windows, then hourly partitioning could cause queries to run more slowly. For example, if there are only a handful of events per day, and data are partitioned hourly (PT1H), and queries run once a quarter, many small partitions are read, when fewer, larger partitions would be more efficient.

#### Standardization Rules

Raw values in a hashed identifier column must be standardized before they go through the hashing process. Standardization refers to performing a set of transformations to the raw data, for example converting to lowercase. For the majority of fields, the standardization process is simple. Certain fields like phone number and address require additional transformations. The full set of rules for hashed data, including all conventions for street post and prefixes [can be found here.](https://advertising.amazon.com/dsp/help/ss/en/audiences/advertiser-audiences/advertiser-hashed-audience/#GA6BC9BW52YFXBNE)


#### Hash Method – SHA256

Once the data has been standardized, all hashed identifier columns will need to be hashed via the **SHA-256** hash method. An example of how to hash an email value in Python 3 can be seen below.

```
\>\>\> import hashlib

\>\>\> hashlib.sha256(b"abc@example.com").hexdigest()
'9eceb13483d7f187ec014fd6d4854d1420cfc634328af85f51d0323ba8622e21'
```

### KMS Encryption Key Usage

AMC provides the ability to encrypt customer datasets with encryption keys created in [AWS Key Management Service (KMS).](https://aws.amazon.com/kms/) This step is optional. If an encryption key is not provided, AMC will perform default encryption on behalf of the customer. 
The benefit to using a customer generated encryption key is the ability to revoke AMC’s access to previously uploaded data at any point. In addition, customers can monitor encryption key access via AWS CloudTrail event logs.

> [WARNING] To use AMC Data Upload with a customer-specified KMS key, you must create a [multi-region key](https://docs.aws.amazon.com/kms/latest/developerguide/multi-region-keys-overview.html). Additionally, changing a KMS key is not supported.

In order to enable server-side encryption of customer datasets with AWS Key Management Service, consider the following process:

1. Create symmetric encryption key in AWS KMS.

   - During the key creation process, grant usage permissions to the AMC instance to perform decryption and encryption operations with the key.

2. Apply default encryption to S3 bucket where datasets are stored via the key created in step 1. See [Enable default encryption with AWS Key Management Service (KMS)](#optional-enable-default-encryption-with-aws-key-management-service-kms).

3. When creating a new dataset (table schema) in the AMC instance, include the key ARN for AMC to know the key used to encrypt/decrypt original data files during data ingestion and query computation. See [Encrypt the Dataset with AWS Key Management Service (KMS)](#create-encryption-key-with-aws-key-management-service-kms).

#### Create Encryption Key with AWS Key Management Service (KMS)

1. In the AWS console, navigate to Key Management Service (KMS) by searching for **KMS** in the global search bar.
2. From the KMS service homepage, click on the create key button in the top right.
3. Under **Step 1 \> Configure Key**, select **Symmetric** for **Key Type.** In Advanced options, leave **KMS** selected and select **Multi-Region key**. Click Next.
4. Under **Step 2 \> Add Labels**, add the key alias which will serve as the display name. Complete the remaining optional fields as necessary. Click next.
5. Under **Step 3 \> Define key administrative permissions,** select the users and/or roles who can administer the key. Click next.
6. Under **Step 4 \> Define key usage permissions,** add the AMC Instance AWS accounts to the **Other AWS accounts** section. For each AMC instance that will be using the KMS key, [key access](https://docs.aws.amazon.com/kms/latest/developerguide/key-policy-modifying-external-accounts.html) will need to be provided to the **Data upload AWS account ID**. This account ID can be located in the **Instance info** page in the AMC console or use the GET operation of the `/amc/instances` resource to get the Data Upload AWS account ID.

>[WARNING] If the key does not exist in **us-east-1**, then you need to create a replica key in the **us-east-1** region.  The replica key also needs to grant access to the data upload account.
You also must provide the us-east-1 arn in the `customerEncryptionKeyArn` field when creating the dataset.

7. Click Next. On the last page (Step 5) review the key details and click **Finish** to complete the key creation process.

## Create S3 bucket

>[WARNING] Create a new S3 bucket for data upload. Your bucket must be in the same AWS Account as the "Connected AWS Account ID" displayed in the **Instance info** page in the AMC console.

The process below covers creating an S3 bucket and applying permissions so that AMC can read data files from the bucket.

1. Navigate to the S3 console and click "Create Bucket".
2. Under "General configuration" give the bucket a name, e.g., "data-upload-tutorial" and select the AWS region. The remaining settings can be left to their default options. 
3. Click "Create bucket".

#### Optional: Enable default encryption with AWS Key Management Service (KMS)**

This step is optional and assumes a KMS encryption key has already been created. See [Create Encryption Key with AWS Key Management Service (KMS)](#create-encryption-key-with-aws-key-management-service-kms) for guidance on creating a KMS key.

Use the following settings:

- **Server-side encryption**: Select Enable
- **Key type**: AWS Key Management Service key
- **AWS KMS key:** Select from created KMS keys or provide key ARN. 

>[NOTE] The key region must match the bucket region. For details, see [Configuring default encrytion](https://docs.aws.amazon.com/AmazonS3/latest/userguide/default-bucket-encryption.html) 

3. Click into the newly created S3 bucket, and navigate to the **Permissions** tab.
4. Within the **Permissions** tab, edit the bucket policy using the below permissions object.

   - Replace **{Data upload AWS account ID}** with your AMC instance AWS account ID.

>[NOTE] If you are using the instance-level Advertising Data Upload APIs, you will see a legacy account ID to include. You may include both if you use both APIs for uploads.  The **Data upload AWS account ID** will be provided by your Amazon AdTech executive. You can also email <amc-support@amazon.com> to get this ID.

- Replace **{bucket_name}** with the name of the newly created bucket

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam:{Data upload AWS account ID}:root"
            },
            "Action": [
                "s3:GetObject",
                "s3:GetObjectVersion",
                "s3:ListBucket",
                "s3:GetObjectTagging",
                "s3:GetBucketTagging"
            ],
            "Resource": [
                "arn:aws:s3:::api-data-upload-tutorial/*",
                "arn:aws:s3:::api-data-upload-tutorial"
            ]
        }
    ]
}

```

Add the permissions object under the bucket permissions: **Bucket Policy \> Edit**. 
Be sure to insert the correct `Data upload AWS account ID` and S3 `bucket name` (under the **Resource** element).

While defining your bucket policy, note that you included "tagging options" to the **Action** element. These options ensure that your S3 bucket and/or the objects within the selected bucket are tagged to the correct instance to which data is being uploaded from. After you include the `s3:GetObjectTagging` and `s3:GetBucketTagging` options, you will need to define tags for either the bucket or the object to associate them with your instance.

>[NOTE]  If you are tagging instances to objects, you must add `s3:GetObjectTagging` and `s3:GetBucketTagging` options under the Action element. However, if you are tagging your instance at the bucket level, include only the  `s3:GetBucketTagging` under the Action element.

To define a tag for your instance, perform the following steps:

1. Navigate to your Amazon S3 console and click on the bucket name you want to associate tags to. 
2. Click **Properties** and scroll to the **Tags** section. Click **Edit**. 
3. Click **Add tag** to define a key for the tag and provide a value for the key. For our purpose, define the key as “instanceId” and type the identifier of the instance you use to upload data as the value for the key. 
4. Click **Save**. 

>[NOTE] There is a limit of 100 unique buckets per upload. This applies to manifest uploads where you can choose your uploads from any of your defined buckets.

To tag an instance to an object, perform the following steps:

1. Navigate to your Amazon S3 console and click on the bucket name to view the objects associated with the bucket. 
2. Click the specific object you want to associate your instance with.  
3. The object details page appears, listing all properties associated with the object.

>[NOTE] You can select multiple objects and associate those with the same tag (instance). 

4. Scroll to the **Tags** section. Click **Edit**. 
5. Click **Add tag** to define a key for the tag and provide a value for the **Key**. For our purpose, define the key as “instanceId” and type the identifier of the instance you use to upload data as the value for the key. 
6. Be sure to click **Save**. 

The S3 bucket is ready for use, and you can start creating datasets upload your data.

Three primary resources will be used in conjunction with the AMC API to perform data upload:

- [Dataset](guides/amazon-marketing-cloud/advertiser-data-upload/advertiser-data-sets) To define the schema for uploaded data.
- [Upload](guides/amazon-marketing-cloud/advertiser-data-upload/adu-uploads): To transfer data to the dataset (schema) for a given time window.
- [Dataset Column](guides/amazon-marketing-cloud/advertiser-data-upload/advertiser-data-column):** To add, modify, or delete columns from a dataset.
