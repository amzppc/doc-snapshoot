---
title: Data uploads
description: How to use data uploads in AMC
type: guide
interface: api
---
# Upload data to datasets

The Upload API is used to manage the transfer of data to the dataset (schema).

- Perform uploads containing a single partition of event-level data (e.g. one day)
- Perform uploads of dimensional data (non-time-based data) such as CRM lists

### Supported Methods


| Methods                                                                      | Description                                          |
|------------------------------------------------------------------------------|------------------------------------------------------|
| POST {api_url}/amc/advertiserData/{instanceId}/uploads/{dataSetId}           | Create new upload                                    |
| POST {api_url}/amc/advertiserData/{instanceId}/uploads/list                  | Retrieve a paginated list of uploads for an instance |
| GET {api_url}/amc/advertiserData/{instanceId}/uploads/{dataSetId}/{uploadId} | View status and other details of an upload operation |

### Upload resource definition

```
POST {api_url}/amc/advertiserData/{instanceId}/uploads/{dataSetId}
```

**Sample request body**

```
POST {api_url}/amc/advertiserData/{instanceId}/uploads/{dataSetId}
{
  "updateStategy": "ADDITIVE",
  "compressionFormat": "GZIP",
  "dataSource":   
	{
            "sourceS3Bucket": "S3Bucket",
            "sourceFileS3Key": "S3Key"
        },
  "fileFormat": {
    "csvDataFormat": {
      "headerIncluded": true,
      "fieldDelimiter": ","
    }
  }
}

```

The `updateStategy` parameter allows you to specify how the new data will be merged with existing data in the dataset.	The valid options are:


| Update Strategy | Description                            |
|-----------------|----------------------------------------|
| ADDITIVE        | Adds new records to those that are already present (if any), in each time-based partition. The set being uploaded may overlap with content already in the table. These overlaps are partition overlaps. Let's say with time-based data, partitioned on month. If there is currently data registered for months January, February, March, and new data is being added for March, April, May. There are records that will fall within the March partition, which will overlap with a partition where data already exists.|
| FULL\_REPLACE    | Any and all content previously registered to the table is removed in favor of the new content.| 
| OVERLAP\_REPLACE | **This strategy applies for uploads to fact datasets only.** All new data will be added to table partitions. Any overlapping partitions will have their prior content removed. When the upload overlaps with any partitions that already have data, the content of the overlapping partition(s) are **replaced**, and those in the upload are ignored.  For example, previously uploaded monthly data, falling into February, March, and April partitions. Data is being uploaded that falls into April and May. With this option, any prior events in April will be **removed**, in favor of those corresponding to April in the new upload, and May will be added directly, as there is no overlap with existing data.|
| OVERLAP\_KEEP    | **This strategy applies for uploads to fact datasets only.** All new data for non-overlapping partitions will be added to table. Any overlapping partitions will retain their prior content. When the upload overlaps with any partitions that already have data, the content of the overlapping partition(s) are RETAINED, and those in the upload are ignored"  Example, dataset has previously uploaded monthly data, falling into February, March, and April partitions. Data is being uploaded that falls into April and May. With this option, any prior events in April will be retained, and those corresponding to April in the new upload will be **ignored**. May will be added directly, as there is no overlap with existing data.  |

A successful call will return the status of the upload and an `uploadId`.

**Sample response**

```
{
    "status": "PENDING",
    "uploadId": "5f384873-8a50-4a9b-b557-3738f7eb74ae"
}
```

#### Sample API Call Response – View all uploads

Returns the details of all uploads.

```

POST /amc/advertiserData/{instanceId}/uploads/list

```

```
{
  "uploads": [
    {
      "createdAt": "2023-09-29T17:36:32Z",
      "metrics": {
        "OutputRowCount": 1782332
      },
      "status": "COMPLETED",
      "updatedAt": "2023-09-29T17:51:30Z",
      "uploadId": "2dd474b2-4029-4ede-b0b7-eafe9ccc216c"
    },
    {
      "createdAt": "2023-09-29T17:58:45Z",
      "message": "Exception in User Class: java.io.FileNotFoundException : No such file or directory 's3://myS3bucket'",
      "metrics": {},
      "status": "FAILED",
      "updatedAt": "2023-09-29T18:04:26Z",
      "uploadId": "c83cd17b-d38e-454d-b459-db6424aab1c3"
    }
  ]

}  

```

#### Sample API Call Response – View a specific upload

To get details about a specific upload status for a dataset, use the following operation:

```
GET {api_url}/amc/advertiserData/{instanceId}/uploads/{dataSetId}/{uploadId}
```

```
{
  "upload": {
    "createdAt": "2023-09-29T17:36:32Z",
    "metrics": {
      "OutputRowCount": 1782332
    },
    "status": "COMPLETED",
    "updatedAt": "2023-09-29T17:51:30Z",
    "uploadId": "2dd474b2-4029-4ede-b0b7-eafe9ccc216c"
  }
}

```

