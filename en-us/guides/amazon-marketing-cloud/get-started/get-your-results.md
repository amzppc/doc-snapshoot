---
title: Get your results
description: How to get results of AMC queries
type: guide
interface: api
---

# Get your query results

In the previous section, we ran an ad hoc workflow, which executed a query that aggregates total cost, impressions, and reach at the advertiser and campaign level. Next, we will access the CSV file containing the results of the query.

You can retrieve your results by [generating a download URL](guides/amazon-marketing-cloud/get-started/get-your-results#download-a-csv-file-containing-query-results) or if you have set up an Amazon S3 bucket for your instance, you can [retrieve reports via S3](guides/amazon-marketing-cloud/get-started/get-your-results#view-workflow-output-in-the-amazon-s3-bucket). 

> [NOTE] If you have not set up an S3 bucket, reports can only be retrieved via through a download URL.

## Download a CSV file containing query results

You will need to copy the workflowExecutionId from the response of the ad hoc execution that we previously ran. We will use that with the `/workflowExecutions/{workflowExecutionId}/downloadUrls` endpoint to retrieve a link for the results.

Let's create a new request to download the results of our query:

1. Copy the workflowExecutionId from the response returned in the previous section. In our example, the workflowExecutionId is: `a111111c-111f-1111-b1f1-11d111f1c1d1`

2. Create a new request and select the GET method.

> [NOTE] Workflow executions are run asynchronously, and often take at least 15 minutes to execute.

3. Enter the request URL, inserting the workflowExecutionId from the previous step into the following URL:
`<https://advertising-api.amazon.com/amc/reporting>/{instanceId}/workflowExecutions/{workflowExecutionId}/downloadUrls`

4. This request will return a URL where your report can be directly downloaded. Copy the URL and enter it into a browser to download the resulting CSV file.

It may take some time for the report to generate. If your report is not ready, you will receive the following response:
`"Execution with ID a111111c-111f-1111-b1f1-11d111f1c1d1 is unavailable for download".`

If you receive that message, wait for 15 minutes and try the request again.

We've just run our first ad hoc query by creating a workflow execution. Then, we created a request to generate a URL to download the CSV file containing the query results.

We will now navigate to our S3 bucket where the output of the query is saved.

## View workflow output in the Amazon S3 bucket

You've run ad hoc workflows, created workflows and run them using the workflow execution resource, and scheduled workflows. Now, we will explore the output of the queries you have run using the AMC workflow APIs.

The output from the workflow executions and the scheduled workflows is stored in CSV format in the S3 bucket associated with the AMC instance. Let's access the S3 bucket and locate the CSV file for the workflow execution that we just ran.

> [NOTE] You can also use the AMC API to [retrieve a download link](#download-a-csv-file-containing-query-results) to view the output for a specific workflow execution.

To access the Amazon S3 bucket:

1. Log in to the AMC UI and navigate to the **Instance info** page.

2. Locate the S3 bucket name. This will be the name of the S3 bucket created in the connected AWS account.

3. Log in to the connected AWS account and navigate to the S3 Buckets page. For more details, see  [Methods for accessing a bucket](https://docs.aws.amazon.com/AmazonS3/latest/userguide/access-bucket-intro.html).

4. Open the AMC S3 bucket. Each folder maps 1:1 to a workflow. In other words, there is a folder for each individual SQL query.

5. Search for the folder with named with the workflowID that was generated (in our example, the workflowId is `transient_c3393afa-5996-4e0e-829e-5da4718eb9e3`).

6. Within the folder, you will see sub-folders based on any schedules or workflow executions that have been created. For more details, see [Understand the S3 Folder Path.](#understand-the-s3-folder-path)

The following depicts a workflow that was run weekly as well as ad hoc:


> ![S3Output](/_images/amazon-marketing-cloud/amc_workflow_s3output.png)


If you run an ad hoc workflow (that is, a workflow execution without first saving the workflow), you'll see folders with randomly generated names beginning with "transient"

7. Open one of the workflow folders. Within the folder is a CSV file and another folder containing metadata. You can download the CSV   file and open it to view the results of the query. You can also download the metadata to view additional information about the   workflow that was run, including the SQL query used.

## Understand the S3 folder path

AMC creates unique folder paths within the connected S3 bucket to organize different AMC reports. The folder path for a report written to S3 looks like this:

`amc-myinstance-2ofoqzvk/workflow=my_report_daily/schedule=daily/2023-06-03-my_report_daily-ver1.csv`

The first part of the path (`amc-myinstance-2ofoqzvk`) contains the `amc-` prefix, your AMC instance name, and a hash value. The hash (in this example `2ofoqzvk`) ensures that the full S3 bucket name `amc-myinstance-2ofoqzvk` is globally unique.

The second part of the path (`workflow=my_report_daily`) provides the name of the **workflow** that generated the report file.

The third part (`schedule=daily`) is the schedule, either "daily", weekly", or "adhoc". Scheduled reports are sent to the "daily" or "weekly" paths based on the value for aggregationPeriod set within the schedule.

Finally, the file name (`2023-06-03-my_report_daily-ver1.csv`) is based off the date the report was generated, the workflow that generated it and a version number. Reports are generated in Comma Separated Value (.csv) format.