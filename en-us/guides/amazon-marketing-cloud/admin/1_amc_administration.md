---
title: Amazon Marketing Cloud - API administration overview
description: AMC API administration overview
type: guide
interface: api
---
# Overview

As the administrator of an AMC account, you can programmatically perform tasks like managing accounts, instances, and users in your organization. You can also manage and invite users in your organization to your account and elevate their role to an administrator.

> [NOTE] This section details the administrator tasks specific to managing your AMC account and instances. See [Manager Accounts](guides/account-management/authorization/manager-accounts) for information on working with Manager Accounts and [Billing and Payment](guides/account-management/billing/getting-started) for details on how to access and manage your billings.

The developer guide details the following areas that an AMC administrator manages:

- [Accounts](guides/amazon-marketing-cloud/admin/account-management): An AMC account represents an advertiser or partner organization, is managed by an account administrator, who can invite other users to access or administer the account.
- [Users](guides/amazon-marketing-cloud/admin/account-management#manage-users): AMC has two primary types of users, "Admins" responsible for managing AMC accounts and instances, and "Viewers" - users with the ability to view and use the accounts.
- [Instances](guides/amazon-marketing-cloud/admin/instance-management): An instance is the actual AMC "clean room" that is set up for a given advertiser or company. Event-level signals for a given set of advertiser IDs start to accumulate in the instances and made available for querying. A single AMC account can contain more than one "instance". 
