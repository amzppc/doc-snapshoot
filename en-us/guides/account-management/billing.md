---
title: Billing API overview
description: Explains what permissions are necessary to view billing data
type: guide
interface: api
keywords:
    - advertising invoices
    - billing permissions
    - billing API
---

# Billing API overview

The [billing API](invoices) enables you to view advertising invoices that include the total amount due, the amount of taxes due, and other billing-related information.

To use the API, the user that granted permission for your application must have the permissions specified in the [Amazon Ads API permissions reference](guides/account-management/permissions).

## Determining Billing permission using the Profiles resource

You can determine which accounts to which an advertiser has permission to view billing information using the [profiles resource](reference/2/profiles). Use the [GET operation](reference/2/profiles#tag/Profiles/operation/listProfiles) and include the `apiProgram` query parameter set to `billing`. 

## Payment registration, eligibility, and execution

The billing API provides the following endpoints related to payment registration, eligibility, and execution:

### [POST /billing/paymentMethods/list](billing#tag/Get-Payment-Methods)

This API returns a list of payment methods that an advertiser is eligible to register. Clients can directly use the payment methods returned from this API to create payment agreements and register these payment methods to advertising accounts. 

Currently credit card eligibility is not supported through this API. 

### [POST /billing/paymentAgreements](billing#tag/Create-Payment-Agreement)

This is a batch API that supports creating payment agreements for a given advertiser. You can use this API to update or add a payment method to one or many advertising accounts. This API lets you specify a list of payment methods in a payment profile and specify priorities for how they want these payment methods to be used during payment execution. If one payment method fails, we will attempt payment on the next lowest priority payment method. 

### [POST /billing/paymentAgreements/list](billing#tag/Get-Payment-Agreements)

This API allows clients to retrieve payment agreements that are currently active for a given advertising entity. These payment agreements include the current payment agreement that is being used to automatically try and collect on the advertisers invoices. 

Currently, this API only returns one payment agreement for the advertiser that the client is trying to access. In the future, this API will support global advertisers retrieving all payment agreements that are active for their sub-accounts. 

### [POST /billing/invoices/pay](billing#tag/Pay-Invoices)

This API allows clients to initiate payments on their currently unpaid or open invoices. The API returns details regarding which invoices it failed to collect payment on, a human readable reason for the failure, and list of invoices for which it successfully collected payment. 

### [POST /billing/paymentProfiles](billing#tag/Create-Payment-Profiles/operation/CreatePaymentProfiles)

* This is a batch API that will support creating payment profiles for a given advertiser. 
* Clients can specify a list of payment methods as part of the payment profile and specify priorities for how they want these payment methods to be used during payment execution. If one payment method fails, we will attempt payment on the next lowest priority payment method.
* Clients can additionally specify `defaultFor` when creating a payment profile. Currently clients can only specify their global advertising account as the `defaultFor`. Setting a payment profile as the `defaultFor` for a global advertising account will mean that all advertising accounts that get created under this global account (as the global account expands to new marketplaces) will use the same payment profile and payment setup is not needed separately for the advertising accounts.
* Clients can also specify eligibleEntities for the payment profile. eligibleEntities controls which all accounts are eligible to use and update a given payment profile. Currently only the global advertising account can be set as an eligible entity, i.e., a payment profile can only be used and updated by different advertising accounts belonging to a global advertising account.
* Currently, credit card cannot be provided as one of the payment methods in the payment profile.

### [POST /billing/paymentAgreements/list](billing#tag/Get-Payment-Agreements/operation/GetPaymentAgreements)

* The existing paymentAgreement APIs have been updated to reflect payment profile as an independent resource.
* When creating payment agreements, client can either provide a payment profileId to refer to an already created payment profile, or provide the body using which a payment profile should be created. If doing the latter, both a payment profile and payment agreement will be created for the client. Note that either the `paymentProfileId` or the payment profile should be provided when creating payment agreements, but not both.