---
title: Apply for Amazon Ads API access
description: How to apply for Amazon Ads API access by completing the application form
type: guide
interface: api
tags:
    - Onboarding
keywords:
    - partner
    - direct advertiser
    - register
    - email
    - technical support
---

# Step 2: Apply for Amazon Ads API access

<div class="breadcrumb-top" style="display: block; font-size: .9em; margin: 0px; margin: -10px 0 20px 0; padding: 10px; background: #f6f6f6; border-left: 8px solid #4f7cb1;">Onboarding: <span style="white-space: nowrap;">[Overview](guides/onboarding/overview)</span> | <span style="white-space: nowrap;">[1\. Create LwA client](guides/onboarding/create-lwa-app) | [**2\. Apply for API access**](guides/onboarding/apply-for-access) | [3\. Assign access](guides/onboarding/assign-api-access)</span></div>

Now that you have [created an Amazon Developer account and a Login with Amazon application](guides/onboarding/create-lwa-app), the next step is to apply for access to the Amazon Ads API.

How you apply for access will depend upon how your company intends to use the API. First, determine which of these categories best describes your business:

- **Partner** : Businesses that build applications and software solutions to automate and help optimize advertising on behalf of others can access the Amazon Ads API as a third party.
- **Direct Advertiser** : Advertisers that use the Amazon Ads API to automate, scale, and optimize advertising activities and reporting.

>*To apply for access as a Direct Advertiser, scroll down on this page or [click here to continue](#To-apply-for-API-Access-as-a-Direct-Advertiser).*

## To apply for API access as a Partner

Partners will apply for API access through the [Amazon Ads Partner Network](https://advertising.amazon.com/partners/network), a self-service hub for agencies and tool providers to manage their business relationships with Amazon Ads. If your company has not registered for the Amazon Ads Partner Network, please do so by following the link above.

>[NOTE] Registration for API access is just one aspect of the Amazon Ads Partner Network. Please register using an email address that can be managed by the appropriate members of your organization, which may or may not be the address used to create an Amazon Developer account and Login with Amazon client in [step 1](guides/onboarding/create-lwa-app). Approved partners will be able to assign their API access to an LwA client in a later step.

| [IMAGE] |   |
| ---- | ---- |
| Once registered, log in to the Amazon Advertising Partner Network console. Select **Advertising API** from the menu at center left, then click **Manage Advertising API access**. | ![Screenshot of Partner Network console](/_images/setting-up/partner-network-manage.png) |


| [IMAGE] |   |
| ---- | ---- |
| Next, from the **Advertising API** tab, click the **Request API access** button in the message box at top right. <br><br>Complete the application form. Select **Submit for review**. | ![Screenshot of Partner Network "Advertising API" tab](/_images/setting-up/partner-network-request.png) |

>*Scroll down on this page or [click here for next steps](#Next-steps).*

## To apply for API access as a Direct Advertiser

Direct advertisers may go directly to the application form by clicking the link below:

- **[Apply for API access as a Direct Advertiser](https://advertising.amazon.com/partner-network/register-api?ref_=a20m_us_api_drctad)**

>Alternatively, go to the [Amazon Ads API web page](https://advertising.amazon.com/about-api) and select **Request API Access**, then choose **Direct Advertiser**.

Sign in using the email address you used to create the Amazon Developer account.

>[WARNING] If you are already logged in to *any* Amazon account, you will immediately be redirected to the application form. Please ensure that the correct profile is displayed in the top right corner of the screen. If it is not, please log out and log back in with the correct account.<br> <br>You must log in with the same email address that was used to create the Amazon Developer account in [step 1](guides/onboarding/create-lwa-app). 

Next, complete the application form. Select **Submit for review**.

## Next steps

You should receive an email **at the address you used to log in above** to confirm receipt of your application.

Review of your application may take up to 1 business day. Once your application has been reviewed, you will receive an email detailing your application status.

If your application has been approved, read about the next step in the onboarding process: [Assign API access to your LwA application](guides/onboarding/assign-api-access). If your application is not approved, you will receive information about how to resolve the issue.

>[WARNING]Please read [Assign API access to your LwA application](guides/onboarding/assign-api-access) before following the links in the application status email.<br> <br>Your LwA developer registration will be associated to your Amazon Ads API permissions in the next step of the process. **This association cannot be changed once it is set**, so we strongly recommended that all individuals with access to the application email address be made aware of this.

### Additional steps for the data provider API

If you are setting up an account for the [data provider API](data-provider/openapi), there is an [additional step to complete](guides/onboarding/data-provider-api). 

>[TOPIC:Technical support] If you have difficulty connecting to the Amazon Ads API, please visit our [Technical Support page](support/overview) for information.
