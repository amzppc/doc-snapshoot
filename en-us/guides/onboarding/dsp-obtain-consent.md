---
title: Obtain consent to access an Amazon DSP User's Account
description: Describes the steps for granting management access to one or more specific entities.
type: guide
interface: api
tags:
    - Onboarding
    - DSP
keywords:
    - entity
    - interface
    - DSP console
---

# Obtain consent to access an Amazon DSP user's account

Obtaining consent to an Amazon DSP user's account requires that users be invited to access an Amazon DSP account. The users must then accept the invitation. 

>[TIP  Getting access to AU and JP entities] <br>
Please follow these steps to get access to Far East profiles: Go to the Amazon Japan retail site, https://amazon.co.jp, and register for an account there using a different email than the email you registered at developer.amazon.com. For example, if your email at developer.amazon.com is joe@company.com, you can use joe+jp@company.com for the Amazon Japan retail site, and both will go to the same inbox. If you don’t read Japanese, registering can be a little tricky, but the layout and workflow is the same as the Amazon US retail site, so it can be helpful to compare the two side by side while registering. <br><br> Then when you get to step 2 on [this page](guides/get-started/retrieve-access-token), log in with this new Japan email address and complete the oAuth process. 

## Inviting a user

To invite a user to an entity, the Amazon DSP user goes through the following process. In this example, the API development team is invited to have access to the Demo: CA entity.

In the Amazon DSP web interface, select the **Administration** menu.

![Administration "gear" icon](/_images/dsp/administration-icon.png)

-----

From the Administration menu, select **Account access & settings** to enter the Entity admin panel.

![Account access & settings option](/_images/dsp/account-access.png)

-----

Next, click **Invite user**:

![Invite user button](/_images/dsp/invite-user.png)

-----

Enter a friendly name for the user, and their development team’s email address, created as a prerequisite for this onboarding process. Then, click **Save**:

![Invite user form](/_images/dsp/invite-user-2.png)

-----

Then wait for the success message:

![Invite success message](/_images/dsp/success-message.png)

-----

Finally, the development team will have access to the entity once the invitation is accepted:

![Screenshot of email](/_images/dsp/accept-invitation.png)

## Deleting a user from an entity

To delete a user from an entity and prevent the user from accessing the entity, open the **Users** tab and locate **Delete**:

![Delete user option](/_images/dsp/delete-user.png)

Note that both inviting and deleting a user from an entity occur immediately. As soon as they are invited the entity’s profile will be accessible, and as soon as access is denied they no longer have permission to access the entity.

## Next steps

Now that you've obtained consent to access a DSP user's account, you can make API calls to manage those entities. To begin, learn how to [create API authorization tokens](guides/get-started/retrieve-access-token).