---
title: Amazon Ads API Onboarding Overview
description: Overview and explanation of the onboarding process for the Amazon Ads API
type: guide
interface: api
tags:
    - Onboarding
    - Authorization
keywords:
    - register
    - approval
    - access
    - email
    - technical support
---

# Amazon Ads API onboarding overview

<div class="breadcrumb-top" style="display: block; font-size: .9em; margin: 0px; margin: -10px 0 20px 0; padding: 10px; background: #f6f6f6; border-left: 8px solid #4f7cb1;">Onboarding: <span style="white-space: nowrap;">[**Overview**](guides/onboarding/overview)</span> | <span style="white-space: nowrap;">[1\. Create LwA client](guides/onboarding/create-lwa-app) | [2\. Apply for API access](guides/onboarding/apply-for-access) | [3\. Assign access](guides/onboarding/assign-api-access)</span></div>

The Amazon Ads API enables you to manage the advertising resources associated with an Amazon advertiser using a REST API.

To use the Amazon Ads API, you must apply for access and be approved. Direct advertisers, partners, and integrators are all eligible to apply.

Approved applicants can establish a *client application* through [Login with Amazon (LwA)](https://developer.amazon.com/docs/login-with-amazon/conceptual-overview.html) that serves as a credential for requests to the Amazon Ads API.

## Establishing a client application

This process does not require specific technical expertise. It includes three steps: 

1. **Create a Login with Amazon application.** Requests to the Amazon Ads API are made by a client application administered by Login with Amazon. The client application is free to create, and no approval is required for this step.

2. **Apply for permission to access the API.** The application form includes questions about how your business intends to use the Amazon Ads API, as well as information about the [Amazon Ads API License Agreement](https://advertising.amazon.com/API/docs/license-agreement) and the [Data Protection Policy](https://advertising.amazon.com/API/docs/policy/en_US).

    >[NOTE]Application approval may take up to 1 business day.

3. **Assign API access to your LwA application.** Once your application is approved, you will enable your LwA client application to make requests to the API.

## First steps

If you're ready to apply for access to the Amazon Ads API, start with step 1 of the onboarding process: [Create a Login with Amazon application](guides/onboarding/create-lwa-app).

>[TOPIC:Technical support] If you have difficulty connecting to the Amazon Ads API, visit our [technical support page](support/overview) for information about assistance.
