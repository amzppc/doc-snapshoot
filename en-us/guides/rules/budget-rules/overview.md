---
title: Budget Rules overview
description: Learn how to set campaign budgets in advance using rule-based functionality.
type: guide
interface: api 
tags:
    - Sponsored Brands
    - Sponsored Products
    - Sponsored Display
    - Budget Rules
keywords:
    - budgets
---

# Sponsored ads budget rules overview 

Advertisers using sponsored ads can set campaign budgets in advance using budget rules in the Amazon Ads API. Budget rules help reduce manual effort spent on adjusting budgets. You can set budget rules for specific dates or based on performance goals. 

Schedule based budget rules can be set to increase daily budgets for any date range or recommended events, such as Black Friday or Cyber Monday. During shopping events, we will provide recommended budgets based on historical shopping activities. Performance-based budget rules let you set rules to increase budgets based on campaign performance metrics. With budget rules, advertisers can reduce the likelihood of campaigns running out of budgets, especially during peak days or holiday events. 

Benefits of using budget rules:

* Reduces the likelihood of campaigns going out of budget preventing missed opportunities.
* Provides budget recommendations for special events.
* Gives more control over budget.
* Reduces manual effort and time spent adjusting budget.

There are two types of rules you can create to increase the daily budget for your campaigns:

1. Schedule-based rules enable you to set a rule to increase daily budget based on a scheduled events such as Black Friday or a custom time period. This type of rule temporarily increases campaign budgets by the percentage specified in the rule. The budget is increased for the time range specified as part of rule.
2. Performance-based rules enable you to set rules to increase campaign budgets when your campaigns meet a certain performance threshold. The thresholds are measured using campaign performance metrics such as ACOS, Click-through Rate (CTR), Conversion Rate (CVR), and Return on Ad Spend (ROAS). These metrics are calculated using data from the previous 7 days for sellers and 14 days for vendors.

## Learn more

Learn about [creating schedule-based rules](guides/rules/budget-rules/schedule-based) or [creating performance-based rules](guides/rules/budget-rules/performance-based).
