---
title: Asset library overview
description: An overview for the asset library API
type: guide
interface: api
tags: 
    - Sponsored Display
    - Sponsored Brands
    - Stores
    - DSP
keywords:
    - Asset library overview
    - Creative specification

---

# Asset library overview

The creative asset library is used to manage creatives across ad programs, helping you organize creatives more efficiently. The library currently supports video and image for Sponsored Brands, Sponsored Display, Amazon DSP, and Stores. 

You can also view, manage, and upload assets using the [Asset library UI](https://advertising.amazon.com/creative-assets).  

## Creative specifications and guidelines

For creative specifications for each ad program, see:

* [DSP](https://advertising.amazon.com/resources/ad-specs/dsp)
* [Sponsored ads](https://advertising.amazon.com/resources/ad-policy/sponsored-ads-policies) (including Sponsored Brands and Sponsored Display)
* [Stores](https://advertising.amazon.com/resources/ad-specs/stores)

## Additional resources

To learn more about the creative assets library, visit the resources below:

* [Creating assets](guides/creative-asset/creating-assets)
* [Managing assets](guides/creative-asset/managing-assets)
* [API reference](creative-asset-library)
