---
title: Partner opportunities overview
description: An overview of the Partner Opportunities API, which provides insights to Partner Network members for all of the partner's linked clients.
type: guide
interface: api
tags:
    - Partner opportunities
    - Campaign management
    - Reporting
keywords:
    - high-potential ASINs
    - retail readiness
    - Partner Network
---

# Partner Opportunities overview

As a partner of the Amazon Ads Partner Network, you can access Partner Opportunities** to receive actionable insights for all your linked clients through a single API. 

Example insights may include:

* high potential ASINs and excess inventory ASINs to consider advertising, 
* retail readiness scores below a certain threshold, and 
* ASINs without branded search terms. 

In the future, we'll launch a new feature to help you prioritize actionable opportunities and make you aware as new information becomes available. 

The Partner Opportunities API allows you to:

* **Simplify your API call process:** Rather than calling multiple APIs for each advertising product, you can use the Partner Opportunities API to receive valuable insights for all advertising products.
* **Save time on managing advertising activities:** By reducing the number of calls required, you can save time and resources. The Partner Opportunities API returns actionable recommendations for all advertising campaigns associated with all of your clients. 
* **Better support your clients’ campaigns:** The Partner Opportunities API allows you to receive custom insights aimed at improving overall campaign performance, helping you prioritize and view all client accounts in a single dashboard.

## Next steps

- [Get started with the Partner Opportunities API](guides/recommendations/partner-opportunities/getting-started)
- [Check out the current catalog to understand available opportunities](guides/recommendations/partner-opportunities/catalog )  
- [View frequently asked questions on Partner Network](https://advertising.amazon.com/partner-network/help/GEFTRSVWUGJ3EFQC)