---
title: Getting started with the Partner Opportunities API
description: How to get started with the Partner Opportunities API, including how to retrieve information for the additional required authorization header.
type: guide
interface: api
tags:
    - Partner opportunities
    - Account management
    - Authorization
keywords:
    - manager accounts
    - Partner Network
    - Amazon-Advertising-API-Manager-Account
---

# Getting started with the Partner Opportunities API

## Prerequisites

To get started with the Partner Opportunities API, there are two prerequisites:

1. Access to the Amazon Ads API. If you don’t already have access, you must complete the [API onboarding steps](guides/onboarding/overview).
2. An Amazon user account with **Admin** or **Custom - Editor** permissions for a Partner Network account. For more information on these permissions, see the [Amazon Ads Support Center documentation on managing Partner Network users](https://advertising.amazon.com/help/G2WDQSK87WSDWPQC).

## Authorization

Unlike most Amazon Ads APIs, the Partner Opportunities API accesses partner data (i.e., your data). Partner data includes:

1. Data for advertisers in your book of business.
2. Partner-specific data, e.g. users of your Partner Network account or the certifications granted to you in the Partner Network. 

The process for authorizing a Partner Opportunities API call is similar to what is described in “[Getting started with the Amazon Ads API](guides/get-started/overview).” However, because the Partner Opportunities API accesses partner data, the process differs in some ways.

While following the “Getting started” process for the Partner Opportunities API, please ensure that you take care of following:

1. When completing [the authorization grant procedure](guides/get-started/create-authorization-grant), log in and grant access to the same LwA application that you'll be using to call the Partner Opportunities API.
1. While [generating an authorization code](guides/get-started/create-authorization-grant#create-an-authorization-url):
    - Use `advertising::campaign_management` as the scope when creating the authorization code URL.
    - Follow the section “[To grant access to your *own* Amazon Ads data](guides/get-started/create-authorization-grant#to-grant-access-to-your-own-amazon-ads-data),” as you are granting *your* LwA app permission to access your own data ("partner data" as defined above). Before pasting the authorization URL (step 1 of that section), ensure that you are signed out of all Amazon accounts *or* paste the authorization URL in an ‘incognito’/‘private’ window of your browser. Sign in using an Amazon user account that is associated with your LwA application. Ensure that this user has the **Admin** role in your Partner Network account. 

>[NOTE] You may skip “[Step 3: Retrieve a profile ID](guides/get-started/retrieve-profiles),” as the Partner Opportunities API does not require this parameter.

## Partner Network account ID

Calls to the Partner Opportunities API require an additional header, `Amazon-Advertising-API-Manager-Account`. To retrieve the value for this header, use the Amazon Ads console. First, select the “account” icon in the upper right corner of the interface, then copy the value of the **Partner ID** field.

![Partner Account Id](/_images/partner-opportunities/partner-account-id.png)

Pass this value as the `Amazon-Advertising-API-Manager-Account` header in any requests to the Partner Opportunities API.

## Partner Opportunities API resources

The Partner Opportunities API includes three resources:

1. `/partnerOpportunities/summary:` This resource retrieves aggregated information across all opportunities available for your business. Aggregated information includes: Unique advertiser count, Available ad products, Number of available opportunities, Number of opportunities with data, Available audiences, Available objective types.
2. `/partnerOpportunities`: This resource retrieves the list of all opportunities. To find the ones which are available for your account, introspect on the value of the `opportunities[].dataMetadata.rowCount` field (API version 1.1 onwards). If the value is greater than zero, data is available and a data file can be downloaded.
3. `/partnerOpportunities/{partnerOpportunityId}/file`: This resource retrieves the download URL of a CSV file that has data to perform the action recommended by the opportunity.

[View the technical specifications for the Partner Opportunities API](partner-opportunities).

## Next steps

Learn [how to use the Partner Opportunities API](guides/recommendations/partner-opportunities/how-to).
