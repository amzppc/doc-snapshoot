---
title: How to use the Partner Opportunities API
description: Example requests to the Partner Opportunities API, along with information on error codes and messages.
type: guide
interface: api
tags:
    - Partner opportunities
    - Reporting
    - Account management
keywords:
    - opportunity data file
    - excess inventory ASINs
    - Amazon-Advertising-API-Manager-Account
    - application/vnd.partneropportunity.v1+json
---

# How to use the Partner Opportunities API 

The following examples demonstrate how to call the Partner Opportunities API using [cURL](https://github.com/curl/curl).

## partnerOpportunities resource

[View the technical specification for the `partnerOpportunities` resource.](partner-opportunities#tag/Partner-Opportunities/operation/partnerOpportunitiesListOpportunities)

**Request**

```
GET https://advertising-api.amazon.com/partnerOpportunities
```

**Example cURL command**

>[NOTE] This example request uses `v1` per the `Accept` header. The example response also represents `v1`. For information about other versions, see the [technical specification](partner-opportunities).

```
curl 'https://advertising-api.amazon.com/partnerOpportunities' \
 -H 'Accept: application/vnd.partneropportunity.v1+json' \
 -H 'Amazon-Advertising-API-ClientId: <LWA_APP_CLIENT_ID>' \
 -H 'Amazon-Advertising-API-Manager-Account: <PARTNER_NETWORK_ACCOUNT_ID>' \
 -H 'Authorization: Bearer <ACCESS_TOKEN>'
```

**Example response**

```
{
  "opportunities": [
    {
      "partnerOpportunityId": "opportunity-1-id",
      "title": "Title for opportunity-1",
      "description": "Description for opportunity-1",
      "callToAction": "Action that opportunity-1 recommends you to take",
      "product": "Amazon advertising product that opportunity-1 corresponds to",
      "objective": "Objective that opportunity-1 intends to improve e.g. brand engagement",
      "createdDate": "2022-05-15T23:42:33.823Z",
      "updatedDate": "2022-05-16T23:43:54.867Z",
      "dataUrl": "/partnerOpportunities/opportunity-1-id/file"
    },
    {
      "partnerOpportunityId": "opportunity-2-id",
      "title": "Title for opportunity-2",
      "description": "Description for opportunity-2",
      "callToAction": "Action that opportunity-2 recommends you to take",
      "product": "Amazon advertising product that opportunity-2 corresponds to",
      "objective": "Objective that opportunity-2 intends to improve e.g. sales",
      "createdDate": "2022-05-14T23:42:33.823Z",
      "updatedDate": "2022-05-15T23:43:54.867Z",
      "dataUrl": "/partnerOpportunities/opportunity-2-id/file"
    },
    {
      "partnerOpportunityId": "opportunity-3-id",
      "title": "Title for opportunity-3",
      "description": "Description for opportunity-3",
      "callToAction": "Action that opportunity-3 recommends you to take",
      "product": "Amazon advertising product that opportunity-3 corresponds to",
      "objective": "Objective that opportunity-3 intends to improve e.g. awareness",
      "createdDate": "2022-05-13T23:42:33.823Z",
      "updatedDate": "2022-05-14T23:43:54.867Z",
      "dataUrl": "/partnerOpportunities/opportunity-3-id/file"
    },
    {
      "partnerOpportunityId": "opportunity-4-id",
      "title": "Title for opportunity-4",
      "description": "Description for opportunity-4",
      "callToAction": "Action that opportunity-4 recommends you to take",
      "product": "Amazon advertising product that opportunity-4 corresponds to",
      "objective": "Objective that opportunity-4 intends to improve e.g. retention",
      "createdDate": "2022-05-15T23:42:33.823Z",
      "updatedDate": "2022-05-16T23:43:54.867Z",
      "dataUrl": "/partnerOpportunities/opportunity-4-id/file"
    }
  ]
}
```

## file resource

[View the technical specification for the file resource.](partner-opportunities#tag/Partner-Opportunities/operation/partnerOpportunitiesGetOpportunityFile)

**Request**

```
GET https://advertising-api.amazon.com/partnerOpportunities/<partnerOpportunityId>/file
```

**Example curl command**

```
curl 'https://advertising-api.amazon.com/partnerOpportunities/<partnerOpportunityId>/file' \
 -H 'Accept: application/vnd.partneropportunity.v1+json' \
 -H 'Amazon-Advertising-API-ClientId: <LWA_APP_CLIENT_ID>' \
 -H 'Amazon-Advertising-API-Manager-Account: <PARTNER_NETWORK_ACCOUNT_ID>' \
 -H 'Authorization: Bearer <ACCESS_TOKEN>'
```

**Example response**

```
HTTP/1.1 307 Temporary Redirect
location: <S3_DOWNLOAD_URL>
```

### Sample Opportunity data file

The data in the opportunity data file is different for each different type of opportunity. Data fields are explained in the [CSV schema for the reponse](guides/recommendations/partner-opportunities/csv-schema).

For example, a file that includes opportunities to create advertising campaigns for excess inventory ASINs might include the following columns:

|Marketplace	|Ad Product Type	|Encrypted Advertiser Id	|ASIN	|Advertiser Name	|
|---	|---	|---	|---	|---	|
|	|	|	|	|	|

## How to use the Partner Opportunities Apply API in conjunction with Marketing Stream

[Amazon Marketing Stream](guides/amazon-marketing-stream/overview) provides near real-time metrics and recommendations in a push format, directly to your AWS account. Amazon account team recommendations covering suggestions from Amazon account teams on campaign optimization and new campaign creation are also available in Amazon Marketing Stream so that partners can receive these recommendations in near time without needing to call the API repeatedly  .See Amazon Marketing Stream [documentation](guides/amazon-marketing-stream/datasets/sponsored-ads-campaign-diagnostics-recommendations) for complete [catalogue of recommendations](guides/amazon-marketing-stream/datasets/sponsored-ads-campaign-diagnostics-recommendations#recommendation-types) available via marketing stream and detailed [schema description](guides/amazon-marketing-stream/datasets/sponsored-ads-campaign-diagnostics-recommendations#schema) . If you subscribe to the [sponsored-ads-campaign-diagnostics-recommendations](guides/amazon-marketing-stream/data-guide#sponsored-ads-campaign-recommendations-sponsored-ads-campaign-diagnostics-recommendations) dataset through Stream, you will automatically receive a push notifications with recommendations for your campaigns. Each recommendation message contains a recommendation ID that can be used with the Partner Opportunities API to apply the recommendation. 
For example, suppose you receive the following recommendation message from Stream that suggests raising the budget for a Sponsored Products campaign:

### Applying recommendations

To apply the recommendation exactly as proposed in the Stream payload, use the `recommendation_id` you received from Stream in the request body at the [/partnerOpportunities/{partnerOpportunityId}/apply](partner-opportunities#tag/Partner-Opportunities/operation/partnerOpportunitiesApply) resource. You can apply up to 100 recommendation IDs at once.

>[NOTE] If you are exclusively ingesting recommendations from Amazon Marketing Stream or you want to apply multiple opportunities at once, you can apply these by calling `POST /partnerOpportunities/AMAZON_ACCOUNT_TEAM_RECOMMENDATIONS/apply` in the same manner. 

The following attributes are needed for a successful request to PO Apply API. Partners can find the corresponding attribute in Amazon Marketing Stream datasets.

|PO attribute	|Marketing Stream Attributes	|
|---	|---	|
|advertiserType	|Recommendation.advertiser_type	|
|encryptedAdvertiserId	|advertiserId	|
|entityId	|entityId	|
|marketplace	|Recommendation.marketplace_name	|
|recommendationIds	|recommendationIds	|

### SELLER recommendation example

```
{
  "advertiserId": "A2B3WACBWPDWRJ",
  "entityId": "ENTITY1ITEBBD7GQJC8", // NEW
  "marketplaceId": "ATVPDKIKX0DER",
  "datasetId": "sponsored-ads-campaign-diagnostics-recommendations",
  "messageBody": {
    "entity_type": "SELLER_CENTRAL_ENTITY", // NEW
    "advertiser_type": "SELLER", // NEW
    "marketplace_name": "US", // NEW
    "recommendation_id": "1ea17717-c04c-4ef1-817b-1a0ef03a7e07",
    "group_id": "e3c38e01-4309-42d1-83dc-4b06aa2e997a",
    "apply_endpoint": "/recommendations/apply",
    "type": "KEYWORD_BID",
    "published_date": "2024-09-04T17:11:14 UTC",
    "publish_metadata": {
      "published_by": "AMAZON_ADS_ACCOUNT_TEAM",
      "published_to_amazon_ad_console": true
    },
    "expiry_date": "2024-09-18T17:11:14 UTC",
    "explanation": {
      "description": "Campaigns with high clickthrough rate, compared to similar campaigns. Apply these bid recommendations to increase sales.",
      "missed_opportunities": null,
      "estimated_change": null,
      "recommendation_context": {
        "asin_context": null,
        "diagnostic_context": {
          "benchmark_context": {
            "roas": {
              "benchmark_value": 1.56,
              "period": 7
            },
            "impressions": {
              "benchmark_value": 1706,
              "period": 7
            },
            "ad_spend": {
              "benchmark_value": 9.88,
              "period": 7
            },
            "click_through_rate": {
              "benchmark_value": 0.46,
              "period": 7
            },
            "budget_utilization": {
              "benchmark_value": 7.86,
              "period": 7
            }
          },
          "diagnostic_date": "2024-08-31T00:00:00.000Z"
        },
        "grouping_context": {
          "grouping_type": "INCREASE_BID_CONTEXTUAL",
          "brand_id": null,
          "brand_name": null,
          "browse_node_id": null,
          "browseNodeName": null,
          "objective": null,
          "event": null,
          "phase": null
        }
      }
    },
    "campaign_id": "211302794061744",
    "campaign_name": "ECI - [CONS] - AG - Car Pro Shampoo Wash - Manual - SP",
    "ad_product": "SP",
    "current_campaign_settings": {
      "budget": null,
      "ad_groups": [
        {
          "bid": null,
          "bid_optimization": null,
          "asins": null,
          "adgroup_id": "172393940497616",
          "adgroup_name": null,
          "target_type": "KEYWORD",
          "targets": [
            {
              "bid": 0.75,
              "text": "ceramic car shampoo",
              "match_type": "BROAD",
              "target_id": "132038708905255",
              "targeting": "ceramic car shampoo",
              "category": null,
              "expressions": null,
              "audience_target_type": null
            }
          ],
          "creative": null
        }
      ],
      "campaign_targeting": null,
      "bidding_strategy": null,
      "start_date": null,
      "end_date": null
    },
    "recommended_campaign_settings": {
      "budget": null,
      "ad_groups": [
        {
          "bid": null,
          "bid_optimization": null,
          "asins": null,
          "adgroup_id": "172393940497616",
          "adgroup_name": null,
          "target_type": "KEYWORD",
          "targets": [
            {
              "bid": 2.64,
              "text": "ceramic car shampoo",
              "match_type": "BROAD",
              "target_id": "132038708905255",
              "targeting": "ceramic car shampoo",
              "category": null,
              "expressions": null,
              "audience_target_type": null
            }
          ],
          "creative": null
        }
      ],
      "campaign_targeting": null,
      "bidding_strategy": null,
      "start_date": null,
      "end_date": null
    },
    "recommendation_status": "REMOVED",
    "recommendation_group": "sponsored-ads-campaign-diagnostics-recommendations-rec-a-018"
  },
  "eventTimestamp": null
}
```

## Troubleshooting

### 401 “unauthorized” error response

Verify that you have included all the required headers in your API call. For more information about these required headers and their example values, see [getting started with the Partner Opportunities API](guides/recommendations/partner-opportunities/getting-started).

If this does not solve the problem, check that the user account associated with your LwA application is the same account you used to [generate the access and refresh token](guides/get-started/retrieve-access-token#retrieve-your-client-id-and-client-secret) for the API call. To ensure this, before pasting the authorization URL (step 1 of that section), ensure that you are signed out of all Amazon accounts _or_ paste the authorization URL in an ‘incognito’ / ‘private’ window of your browser. Then sign in using an Amazon user account that is associated with your LwA application. For more information see [getting started with the Partner Opportunities API](guides/recommendations/partner-opportunities/getting-started).

Also verify that this user account has **Admin** or **Custom - Editor** permissions in the Partner Network account. An administrator for your Partner Network account can change the access level of the LwA application user to **Admin** or **Custom - Editor** on the **Manage Users** page. For more information, see the [Amazon Ads Support Center documentation on managing Partner Network users](https://advertising.amazon.com/help/G2WDQSK87WSDWPQC). 

Once you've changed these permissions, it can take up to 2 hours to propagate to the API. Please wait at least this long before calling the API again.

If you continue to receive this error, first [create a new authorization code for your LwA application](guides/get-started/create-authorization-grant) ensuring that the current user logged into LwA is the user granted **Admin** or **Custom - Editor** permission above. Then, [retrieve new access and refresh tokens](guides/get-started/retrieve-access-token) and pass these values in the appropriate header. For more information see [getting started with the Partner Opportunities API](guides/recommendations/partner-opportunities/getting-started).

### 403 “forbidden” error response

Verify that the user logged into LwA when the [authorization code was created](guides/get-started/create-authorization-grant) has **Admin** or **Custom - Editor** permissions in the Partner Network account. For more information, see [creating users and managing access levels of users](https://advertising.amazon.com/help/G2WDQSK87WSDWPQC).

If you continue to receive this error, verify that the identifier passed in the `Amazon-Advertising-API-Manager-Account` header is the value associated with the **Partner Network Account ID**. See [Getting started](guides/recommendations/partner-opportunities/getting-started#partner-network-account-id) for information on retrieving this value.

### 404 “not found” error response

This can be a valid response while retrieving `/partnerOpportunities/{partnerOpportunityId}/file` resource if you do not have advertisers or ASINs that match the criteria of the opportunity. This can be determined in the response from `GET /partnerOpportunities` (provided that the request uses an `Accept` header indicating `v1.1` or greater): If you do not have matching data, the value of `opportunities.dataMetadata.rowCount` will be 0. Note that in the Opportunities UI in Partner Network, the download link is grayed out for opportunities lacking data.

Example scenario: If the opportunity asks you to create Sponsored Products campaigns for "excess inventory" ASINs, and your advertisers do not have ASINs that have "excess inventory," then the `rowCount` for that opportunity will be zero and an attempt to retrieve this data file will receive a 404 response.

>[NOTE] Any recent changes you have made to advertisers or ASINs can take up to one day to propagate to the opportunities API. 

If you receive this response while retrieving the `/partnerOpportunities/{partnerOpportunityId}/file` resource, verify that the value of the `partnerOpportunityId` path parameter is valid. You can verify this identifier value by retrieving the `partnerOpportunities` resource and inspecting the `partnerOpportunityId` property in the `opportunities` list. Please note that each opportunity includes a URL populated with the correct `partnerOpportunityId` value in the `dataUrl` property. 

### 415 “unsupported media type” error response

Verify that the `Accept` header in your request is valid. Refer to the `Accept` section in the description of the [API specification](partner-opportunities) for allowed values.

>[NOTE]Wildcards are not a valid value for the `Accept` header. For example, `Accept:  */*` is not allowed.

## Next steps

For more information about the Partner Opportunities API, see the [resource reference](partner-opportunities).
