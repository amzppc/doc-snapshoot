---
title: Tactical recommendations user guide for Amazon Marketing Stream
description: An overview and tutorial for using the tactical recommendations API to improve performance of sponsored ads campaigns.
type: guide
interface: amazon-marketing-stream
tags:
    - Campaign management
    - Reporting
keywords:
    - campaign performance
    - push notification
    - recommendation_id
---

# Tactical recommendations user guide for Amazon Marketing Stream

The tactical recommendations API provides suggestions from Amazon to improve the performance of your sponsored ads campaigns. 

>[NOTE] This feature is currently in beta.

[Amazon Marketing Stream](guides/amazon-marketing-stream/overview) provides near real-time metrics and recommendations in a push format, directly to your AWS account. If you subscribe to the [sponsored-ads-campaign-diagnostics-recommendations](guides/amazon-marketing-stream/data-guide#sponsored-ads-campaign-recommendations-sponsored-ads-campaign-diagnostics-recommendations) dataset through Stream, you will automatically receive a push notifications with recommendations for your campaigns. Each recommendation message contains a recommendation ID that can be used with the tactical recommendations API to apply the recommendation. 

For example, suppose you receive the following recommendation message from Stream that suggests raising the budget for a Sponsored Products campaign:

```json
{
  "recommendation_id": "2d384ce5-8f4d-403a-afac-7437b99313f6",
  "group_id": "67653abe-44fc-11ed-b878-0242ac120002",
  "apply_endpoint": "/recommendations/apply",
  "type": "CAMPAIGN_BUDGET",
  "published_date": "2022-10-04T21:39:49",
  "expiry_date": "2022-10-18T21:39:49",
  "explanation": {
    "description": "High-performing campaign with ROAS greater than 3 and last week's budget utilization greater than 80%. We've estimated this is a missed opportunity in sales, clicks. Apply the recommended budget.",
    "missed_opportunities": {
      "impressions": "200-1000",
      "clicks": "100-3000",
      "conversions": "10-100",
      "time_period": "Last 7 days"
    },
    "campaign_id": "182756610092559",
    "campaign_name": "All Fabrics",
    "ad_product": "SP",
    "current_campaign_settings": {
      "budget": {
        "campaign_budget": 221
      }
    },
    "recommended_campaign_settings": {
      "budget": {
        "campaign_budget": 321
      }
    }
  }
}
```

In this example, recommendation_id is `2d384ce5-8f4d-403a-afac-7437b99313f6`.

## Applying recommendations

To apply the recommendation exactly as proposed in the Stream payload, use the `recommendation_id` you received from Stream in the request body of [POST /recommendations/apply](recommendations#tag/Recommendations/operation/ApplyRecommendations) endpoint. You can apply up to 100 recommendation IDs at once. 

**Example request**:

```shell
curl --location 'https://advertising-api.amazon.com/recommendations/apply' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxxxxx' \
--header 'Authorization: Bearer Atza|xxxxxxxx' \
--header 'Amazon-Advertising-API-Scope: xxxxxxx' \
--data '{
  "maxResults": 0,
  "nextToken": "string",
  "filters": [
    {
      "include": true,
      "field": "RECOMMENDATION_ID",
      "values": [
        "2d384ce5-8f4d-403a-afac-7437b99313f6"
      ],
      "operator": "EXACT"
    }
  ]
}'
```

## Updating recommendations

If you wanted to make a change to the recommendation before applying, you can use the [PUT /recommendations/{recommendationId}](recommendations#tag/Recommendations/operation/UpdateRecommendation) endpoint to update the recommendation. 

**Example request**:

Suppose the recommendation was to update the budget to 321, but you'd prefer to update to 300 instead. This example updates the recommendedValue to 300. 

```shell
curl --location --request PUT 'https://advertising-api.amazon.com/recommendations/2d384ce5-8f4d-403a-afac-7437b99313f6' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxxxx' \
--header 'Authorization: Bearer Atza|xxxxxxxxx' \
--header 'Amazon-Advertising-API-Scope: xxxxxxx' \
--data '{
  "recommendedValue": "300"
}'
```

Once you are done editing the recommendation, use POST /recomendations/apply to apply the recommendation to your campaign.
