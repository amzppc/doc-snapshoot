---
title: Tactical recommendations overview
description: An overview of the tactical recommendations API.
type: guide
interface: api
tags:
    - Campaign management
keywords:
    - campaign performance
---
# Tactical recommendations API overview

The tactical recommendations API provides campaign recommendations across Sponsored Products, Sponsored Brands, and Sponsored Display. Partners and advertisers can use this API to fetch targeted recommendations to manage campaigns more efficiently. These recommendations come in the form of pre-packaged campaign setting changes suggested by Amazon Ads machine learning algorithms or account management teams, based on a diagnosis of the advertiser’s catalogue and campaigns (e.g., campaigns that have opportunity to improve ROAS by optimizing bids, or unadvertised ASINs that can drive incremental sales if added to an ongoing campaign). Each recommendation details the campaign setting changes needed to execute the recommendation. These changes span across campaign optimizations including recommended bids, budgets, or targets, as well as suggested settings for creating new campaigns.

Examples of the recommendation might include:

1. Increasing the budget for campaigns that are performing well but might soon run out of budget.
2. Adding ASINs with higher conversion rates to campaigns that are not performing well.
3. Creating new campaigns with ASINs that have high retail readiness scores but are not advertised.

[View all available recommendations.](recommendations)

### Supported interfaces

There are three ways to get recommendations: 

1. Calling [POST /recommendations/list](recommendations#tag/Recommendations/operation/ListRecommendations) using the tactical recommendations API.
2. Subscribing to the [sponsored-ads-campaign-diagnostics-recommendations dataset](guides/amazon-marketing-stream/data-guide#sponsored-ads-campaign-recommendations-sponsored-ads-campaign-diagnostics-recommendations) in Amazon Marketing Stream.
3. Through the advertising console UI.

## Get started

- [Tactical recommendations API tutorial](guides/recommendations/tactical-recommendations/get-started)
- [Amazon Marketing Stream tutorial for tactical recommendations](guides/recommendations/tactical-recommendations/stream-user-guide)


>[NOTE] In the coming months, we will begin providing explanations with a rationale for each of the recommendations and share estimated impact on impressions, clicks, sales and spend if the recommendations were to be adopted.