---
title: Tactical recommendations user guide
description: A tutorial for using the tactical recommendations API to improve performance of sponsored ads campaigns.
type: guide
interface: api
tags:
    - Campaign management
keywords:
    - campaign performance
---

# Get started with the tactical recommendations  API

## Endpoints

The tactical recommendations API supports following three API endpoints:

1. [POST /recommendations/list](recommendations#tag/Recommendations/operation/ListRecommendations) - Lists all available tactical campaign recommendations for an advertiser. Partners and advertisers can use filters (including ad product, recommendation type, and more) to filter the results.
2. [PUT /recommendations/{recommendationId}](recommendations#tag/Recommendations/operation/UpdateRecommendation) - Updates the value of a recommendation. 
3. [POST /recommendations/apply](recommendations#tag/Recommendations/operation/ApplyRecommendations) - Applies one or more recommendations up to 100 at a time. 

## Before you begin

To call all three endpoints, you will need the following headers for authorization.

| Parameter | Description |
|--------------------------|---------------|
| `Amazon-Advertising-API-ClientId` | The client ID associated with your Login with Amazon application. Used for authentication. |
| `Authorization` | Your [access token](guides/account-management/authorization/access-tokens). Used for authentication. |
| `Amazon-Advertising-API-Scope` | The [profile ID](guides/account-management/authorization/profiles) associated with an advertising account in a specific marketplace. Make sure you are using the [correct base URL](reference/api-overview#api-endpoints) for the marketplace associated to the profile. |

## Listing recommendations

If recommendations are available, advertisers and partners can request a list of recommendations using the [POST /recommendations/list](recommendations#tag/Recommendations/operation/ListRecommendations) endpoint. This endpoint supports a variety of filters you can use to customize what recommendations are returned. 

### Supported filter values

You can use one or more of these filter combinations to narrow down specific recommendations.

|Field	|Possible values	|
|---	|---	|
|RECOMMENDATION\_ID	|string	|
|AD\_PRODUCT	|One of [ SP, SB, SD ]	|
|RECOMMENDATION\_TYPE	| [View descriptions for all recommendation types.](recommendations) One of [ NEW\_CAMPAIGN, NEW\_VIDEO\_CAMPAIGN, NEW\_AD\_GROUP, CAMPAIGN\_BIDDING\_STRATEGY, CAMPAIGN\_BUDGET, CAMPAIGN\_END\_DATE, CAMPAIGN\_TOP\_PLACEMENT, CAMPAIGN\_PRODUCT\_PLACEMENT, CAMPAIGN\_STATE, NEW\_CAMPAIGN\_BIDDING\_RULE, CAMPAIGN\_BIDDING\_RULE, NEW\_CAMPAIGN\_BUDGET\_RULE, CAMPAIGN\_BUDGET\_RULE, AD\_GROUP\_STATE, AD\_GROUP\_DEFAULT\_BID, AD\_GROUP\_BID\_OPTIMIZATION, NEW\_KEYWORD, KEYWORD\_BID, KEYWORD\_STATE, NEW\_NEGATIVE\_KEYWORD, NEGATIVE\_KEYWORD\_STATE, NEW\_PRODUCT\_AD, PRODUCT\_AD\_STATE, NEW\_PAT, PAT\_STATE, PAT\_BID, NEW\_NEGATIVE\_PAT, NEGATIVE\_PAT\_STATE, NEW\_AUDIENCE\_TARGETING, AUDIENCE\_TARGETING\_STATE, AUDIENCE\_TARGETING\_BID, NEW\_NEGATIVE\_AUDIENCE\_TARGETING, NEGATIVE\_AUDIENCE\_TARGETING\_STATE ]	|
|STATUS	|One of [ PUBLISHED, APPLY\_IN\_PROGRESS, APPLY\_SUCCESS, APPLY\_FAILED, REJECTED ]	|
|GROUPING\_TYPE	|One of [ CAMPAIGN\_INCREASE\_CLICKS, UNDERPERFORMING\_CAMPAIGN\_INCREASE\_CLICKS ]	|

### Examples

#### Get all recommendations

To view all recommendations associated to your profile, you can make the call with an empty request body.

**Request**

```
curl --location 'https://advertising-api.amazon.com/recommendations/list' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxxxx' \
--header 'Authorization: Bearer Atza|xxxxxxx' \
--header 'Amazon-Advertising-API-Scope: xxxxxxxxxx' \
--data '{
}'
```


**Response**

This example returns a recommendation to add two new keywords to a Sponsored Brands campaign.

```json
"recommendations": [
        {
            "adGroupId": "2223334444",
            "adId": null,
            "adProduct": "SB",
            "applyFailureReason": null,
            "asin": null,
            "asinGroupTemplateId": null,
            "budgetRecommendation": null,
            "budgetRule": null,
            "campaignId": "11122223333",
            "campaignTemplateId": null,
            "consolidatedRecommendation": null,
            "currentValue": null,
            "estimatedImpact": null,
            "groupingType": null,
            "keywordSortingDimension": null,
            "keywordSortingRank": null,
            "recommendationId": "xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx1",
            "recommendationType": "NEW_KEYWORD",
            "recommendedValue": "1.44",
            "resolvedTargeting": "mens shoes",
            "ruleBasedBidding": null,
            "sku": null,
            "status": "PUBLISHED",
            "targetId": null,
            "targeting": "mens shoes",
            "targetingMatchType": "EXACT"
        },
        {
            "adGroupId": "2223334444",
            "adId": null,
            "adProduct": "SB",
            "applyFailureReason": null,
            "asin": null,
            "asinGroupTemplateId": null,
            "budgetRecommendation": null,
            "budgetRule": null,
            "campaignId": "11122223333",
            "campaignTemplateId": null,
            "consolidatedRecommendation": null,
            "currentValue": null,
            "estimatedImpact": null,
            "groupingType": null,
            "keywordSortingDimension": null,
            "keywordSortingRank": null,
            "recommendationId": "xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx2",
            "recommendationType": "NEW_KEYWORD",
            "recommendedValue": "2.02",
            "resolvedTargeting": "men socks",
            "ruleBasedBidding": null,
            "sku": null,
            "status": "PUBLISHED",
            "targetId": null,
            "targeting": "men socks",
            "targetingMatchType": "EXACT"
        }
]
```

#### Get all Sponsored Products recommendations

This example shows how to request recommendations that are related only to Sponsored Products campaigns.

**Request**

```
{
    "maxResults": 10,
    "filters": [
            {
                "include": true,
                "field": "AD_PRODUCT",
                "values": [
                        "SP"
          ],
                "operator": "EXACT"
            }
   ]
}
```

**Response**

This response shows an example of a recommendation to increase the budget of an existing Sponsored Products campaign.

```json

"recommendations": [
{
    "nextToken": null,
    "recommendations": [{
        "adGroupId": null,
        "adId": null,
        "adProduct": "SP",
        "applyFailureReason": null,
        "asin": null,
        "asinGroupTemplateId": null,
        "budgetRecommendation": {
            "sevenDaysMissedOpportunities": {
                "endDate": "2023-05-13",
                "estimatedMissedClicksLower": 310,
                "estimatedMissedClicksUpper": 937,
                "estimatedMissedImpressionsLower": 21856,
                "estimatedMissedImpressionsUpper": 65585,
                "estimatedMissedSalesLower": 391.0,
                "estimatedMissedSalesUpper": 1180.0,
                "percentTimeInBudget": 0.7434,
                "startDate": "2023-05-07"
            }
        },
        "budgetRule": null,
        "campaignId": "111222333344",
        "campaignTemplateId": null,
        "consolidatedRecommendation": null,
        "currentValue": "33.0",
        "estimatedImpact": null,
        "groupingType": null,
        "keywordSortingDimension": null,
        "keywordSortingRank": null,
        "recommendationId": "xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx2",
        "recommendationType": "CAMPAIGN_BUDGET",
        "recommendedValue": "100.0",
        "resolvedTargeting": null,
        "ruleBasedBidding": null,
        "sku": null,
        "status": "PUBLISHED",
        "targetId": null,
        "targeting": null,
        "targetingMatchType": null
    }],
    "totalResults": 1
} 
]  
```

You can use the `recommendationId` in the response to [update](guides/recommendations/tactical-recommendations/get-started#updating-recommendations) or [apply](guides/recommendations/tactical-recommendations/get-started#applying-recommendations) the recommendation. 

## Updating recommendations

If you would like to make a change to a recommendation returned by the API, you can use [PUT /recommendations/{recommendationId}](recommendations#tag/Recommendations/operation/UpdateRecommendation). The editable fields depend on the type of recommendation. 

### Update recommendation value combinations 

|Recommendation type	|Data Type	|
|---	|---	|
|CAMPAIGN\_BUDGET, CAMPAIGN\_TOP\_PLACEMENT, CAMPAIGN\_PRODUCT\_PLACEMENT, AD\_GROUP\_DEFAULT\_BID, NEW\_KEYWORD, KEYWORD\_BID, NEW\_PRODUCT\_TARGETING, PRODUCT\_TARGETING\_BID, NEW\_AUDIENCE\_TARGETING, AUDIENCE\_TARGETING\_BID	|number	|
|CAMPAIGN\_END\_DATE	|string(YYYY-MM-DD)	|
|AD\_GROUP\_BID\_OPTIMIZATION	|One of [ CLICKS, CONVERSIONS, REACH ]	|
|CAMPAIGN\_BIDDING\_STRATEGY	|One of [ LEGACY\_FOR\_SALES, AUTO\_FOR\_SALES, MANUAL ]	|
|CAMPAIGN\_STATE, AD\_GROUP\_STATE, KEYWORD\_STATE, NEGATIVE\_KEYWORD\_STATE, PRODUCT\_AD\_STATE, PRODUCT\_TARGETING\_STATE, NEGATIVE\_PRODUCT\_TARGETING\_STATE, AUDIENCE\_TARGETING\_STATE, NEGATIVE\_AUDIENCE\_TARGETING\_STATE	|One of [ ENABLED, PAUSED, ARCHIVED ]	|

### Example

In this example, you are updating a budget to `998` instead of `1000`. 

#### Request

```
PUT recommendations/xxxxxxx-xxxx-xxxxx-xxxx-xxxxx
{
    "recommendedValue":"998"
}
```

#### Response

```json
{
    "recommendation": {
        "adGroupId": null,
        "adId": null,
        "adProduct": "SP",
        "applyFailureReason": null,
        "asin": null,
        "asinGroupTemplateId": null,
        "budgetRecommendation": null,
        "budgetRule": null,
        "campaignId": "11122333444",
        "campaignTemplateId": null,
        "consolidatedRecommendation": null,
        "currentValue": "1000",
        "estimatedImpact": null,
        "groupingType": null,
        "keywordSortingDimension": null,
        "keywordSortingRank": null,
        "recommendationId": "xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx",
        "recommendationType": "CAMPAIGN_BUDGET",
        "recommendedValue": "998",
        "resolvedTargeting": null,
        "ruleBasedBidding": null,
        "sku": null,
        "status": "PUBLISHED",
        "targetId": null,
        "targeting": null,
        "targetingMatchType": null
    }
}
```

## Applying recommendations

Once you are satisfied with the recommendation, you can automatically apply them using the [POST /recommendations/apply](recommendations#tag/Recommendations/operation/ApplyRecommendations) endpoint. You can apply one or more recommendations using this endpoint.

### Example

In this example, you are applying a single recommendation ID. 

#### Request

```
POST recommendations/apply

{
    "recommendationIds": [
        "xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx"
    ]
}
```

#### Response

```json
{
    "failures": [],
    "successes": [{
        "index": 0,
        "recommendation": {
            "adGroupId": null,
            "adId": null,
            "adProduct": "SP",
            "applyFailureReason": null,
            "asin": null,
            "asinGroupTemplateId": null,
            "budgetRecommendation": null,
            "budgetRule": null,
            "campaignId": "11122333444",
            "campaignTemplateId": null,
            "consolidatedRecommendation": null,
            "currentValue": "1000",
            "estimatedImpact": null,
            "groupingType": null,
            "keywordSortingDimension": null,
            "keywordSortingRank": null,
            "recommendationId": "xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx",
            "recommendationType": "CAMPAIGN_BUDGET",
            "recommendedValue": "998",
            "resolvedTargeting": null,
            "ruleBasedBidding": null,
            "sku": null,
            "status": "APPLY_SUCCESS",
            "targetId": null,
            "targeting": null,
            "targetingMatchType": null
        },
        "recommendationId": "xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx",
        "success": {
            "code": "OK",
            "message": "Success."
        }
    }]
}
```
