---
title: Overview
description: Learn more about design, use cases, and set up for developers using the Amazon Ads API. 
type: guide
interface: api
tags:
    - Operations
---

# Amazon Ads API overview

The Amazon Ads API enables Amazon advertisers and members of the [Amazon Advertising Partner Network](https://advertising.amazon.com/partners/network) to programmatically manage advertising operations. 
  		  
You can find reference documentation for the API, as well as feature descriptions, release notes, recommendations, and tutorials, by selecting from the menu tree at left.

>[TOPIC:New to the Amazon Ads API?]Find useful first steps in our table of contents at left or in the list below:<ul><li>**[Onboarding](guides/onboarding/overview)**: step-by-step instructions for gaining access.</li><li>**[Getting started](guides/get-started/overview)**: step-by-step instructions for managing auth and making your first requests.</li></ul>

## REST design

The API exposes a RESTful service interface to perform read and write actions on campaign entities. It also supports flexible reporting to request fine-grained performance data. Authenticated HTTP POST, GET, PUT and DELETE methods are used to create, read, update and delete entities and reports respectively. All entity creation or update operations are input using JSON in the request body.

The API is optimized for large batch operations, which may take several seconds to return. It also separates access to campaign structural data from performance metrics. Performance metrics can only be pulled through asynchronous reports. 

It is advisable to keep a local copy of data retrieved through the API to drive your application UI and optimization algorithms. 

## Typical use cases

We expect a typical client will perform the following operations regularly:

1. Make batch requests for all campaigns, ad groups, ads and keywords in paginated requests and store/update a local copy of the data.
2. Request recent performance data for all campaigns, ad groups, ads and keywords in reports and use the IDs to associate and store/update performance data with the entities retrieved from a batch request.
3. Analyze performance and make changes to bids and budgets. Optimize performance using the batch update API for campaigns, ad groups, ads and keywords.

These, as well as operations to modify single entities, are supported use cases for the API.

## API endpoints

The API is accessible through the following URL endpoints:

| URL | Region | Marketplaces |
| --- | --- | --- |
| https://<span></span>advertising-api.amazon.com | North America (NA) | United States (US), Canada (CA), Mexico (MX), Brazil (BR) |
| https://<span></span>advertising-api-eu.amazon.com | Europe (EU) | United Kingdom (UK), France (FR), Italy (IT), Spain (ES), Germany (DE), Netherlands (NL), United Arab Emirates (AE), Poland (PL), Turkey (TR), Egypt (EG), Saudi Arabia (SA), Sweden (SE), Belgium (BE), India (IN), South Africa (ZA) |
| https://<span></span>advertising-api-fe.amazon.com | Far East (FE) | Japan (JP), Australia (AU), Singapore (SG) |
