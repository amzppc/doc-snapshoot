---
title: Amazon DSP Closed beta overview
description: Learn more about the DSP closed beta.
type: guide
interface: api
tags:
    - Operations
---

# Amazon DSP Campaign Management API closed beta overview

The APIs in this section are part of the Amazon DSP Campaign Management API closed beta and are only accessible to closed beta participants at this time. Because these APIs are in closed beta, they don’t follow our normal versioning standards and are subject to change. Additionally, not all planned API endpoints are currently available. If you are interested in joining the closed beta, please submit a [support ticket](https://amzn-clicks.atlassian.net/servicedesk/customer/portal/2/group/2/create/5) and we’ll follow up with additional information.
