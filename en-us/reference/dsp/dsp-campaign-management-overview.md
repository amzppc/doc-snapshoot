---
title: Amazon DSP campaign management API overview
description: Learn more about the Amazon DSP campaign management API.
type: guide
interface: api
tags:
    - Operations
---

# Overview

The new Amazon DSP campaign management APIs enable self-serve advertisers to programmatically manage their Amazon DSP campaign data (campaigns, ad groups, targeting). Use cases include:

* Tracking campaigns in your own tools: Sync campaign metadata with your own data storage solution and then surface the data in a custom UI to track all of your campaigns.
* Simplifying campaign creation: Build templates or custom workflows that reduce the time it takes to create a new campaign.
* Automating campaign optimization: Automatically adjust bids and budgets in real-time to maximize campaign performance.
* Targeting: Target specific inventory and audiences, add domain exclusions, and easily add or remove targets to maximize campaign performance.

For more info, refer to our reference specs within this folder, the [getting started guide](guides/dsp/developer-guide) (for new users), [FAQ](guides/dsp/campaign-management-faq), and [migration guide](reference/migration-guides/adsp-campaign-management) (for users migrating from the previous APIs).
