---
title: Amazon Portfolios to v3
description: Understand the major differences between v2 and using v3 for Portfolios
type: guide
interface: api
tags:
    - Portfolios
---

# Migrating guide from version 2 to version 3 of the Portfolios API

Version 3 of the Portfolios API introduces a simplified interface for creating and managing Portfolios.

Highlights:

* Parity between Amazon Ads API and advertising console
* Enhanced error response for easier troubleshooting
* Consolidated extended and list endpoints
* Ability to request full objects in the response for created or updated in POST and PUT requests
* Filtering of list operations based on name using broad or exact match

## Endpoint equivalencies 


|Version 2 endpoint	|Version 3 endpoint	|
|---	|---	|
|POST v2/portfolios	|POST /portfolios	|
|GET v2/portfolios <br />  GET /v2/portfolios/{portfolioId} <br />  GET /v2/portfolios/extended  <br /> GET /v2/portfolios/extended/{portfolioId}	|POST portfolios/list	|
|PUT v2/portfolios	|PUT /porfolios	|

## Date format

Dates (for example, campaign `startDate`) should be in the format `YYYY-MM-DD`. This is a change from version 2, where the standard date format was `YYYYMMDD`.


## GET vs. LIST operations

Portfolios version 2 had four separate GET endpoints: 

* a batch GET
* a single entity GET
* batch extended
* single extended

In version 3, all read operations are implemented as POST operations. This allows you to use filters in the request body to filter by entity IDs, state, keywords, and name. 

Example call

The following example shows how to list all Portfolios that are active on the account. 


```
curl--location--request POST 'https://advertising-api.amazon.com/portfolios/list'\
    --header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxxxxx'\
    --header 'Authorization: Bearer Atza|xxxxxxxxx'\
    --header 'Amazon-Advertising-API-Scope: xxxxxxxxx'\
    --header 'Accept: application/vnd.spCampaign.v3+json'\
    --header 'Content-Type: application/vnd.spPortfolio.v3+json'\
    --data - raw '{
"stateFilter": {
    "include": [
        "ENABLED"
    ]
}
}
'
```



## Accept headers for versioning

Versioning for version 3 endpoints is controlled by the `Accept` and `Content-Type` header, rather than the path. For each endpoint, check the `content-type` dropdown under [request samples](http://reference/portfolios) to understand what value to use for the `Accept` and `Content-Type` header. 


## Prefer header

You can use the prefer header to specify whether you want the full object representation as part of the response. If you don’t specify the prefer header, you will see just the ID of the created entity in the response.
