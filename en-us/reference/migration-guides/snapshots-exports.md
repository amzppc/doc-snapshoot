---
title: Snapshots to exports migration guide
description: Migration guide for snapshots to exports
type: guide
interface: api 
tags:
    - Sponsored Products
    - Sponsored Brands
    - Sponsored Display
keywords:
    - Migration guide
---

# Snapshots to exports migration guide 

Exports allow you to download details about your campaigns and other entities at a specific moment in time. Exports will replace the functionality previously supported by [snapshots](guides/snapshots/overview). This migration guide explains how to migrate from snapshots to exports.

>[TIP:Export APIs overview guide]For more conceptual information, see this [guide to Amazon Ads export APIs (PDF)](https://m.media-amazon.com/images/S/aapn-assets-prod/3b27bf03-bfe7-4484-9628-aca0a22567c6).

## Highlights

- One endpoint for all ad products
- Download entity details for all ad products in a single call
- Fewer calls needed to request and download an export
- Standardized structure and field names for each entity across ad products
- Parity with LIST endpoints (including extended fields)
- For Sponsored Brands, support for both version 3 and version 4 campaigns

## Supported entities

Currently exports supports limited entity types. We will update the migration guide and release notes as new configurations become available. 

| Export type | Sponsored Products | Sponsored Brands | Sponsored Display |
|---------------|----|----|----|
| campaigns | x | x | x |
| adGroups | x | x | x |
| targets | x | x | x |
| ads | x | x | x |

## Endpoint equivalencies

| Snapshot endpoint | Export endpoint |
|------------------------------------------|---------|
| POST /v2/sp/{recordType}/snapshot<br />POST /v2/hsa/{recordType}/snapshot<br />POST /sd/{recordType}/snapshot | POST /campaigns/export <br /> POST /adGroups/export <br /> POST /targets/export |
| GET /v2/sp/snapshots/(snapshotId) <br /> GET /v2/hsa/snapshots/{snapshotId} <br /> POST /sd/snapshots/(snapshotId) | GET /exports/{exportId} |
| GET /v2/sp/snapshots/(snapshotId)/download <br /> GET /v2/hsa/snapshots/{snapshotId}/download <br /> POST /sd/snapshots/(snapshotId)/download | N/A | 

## Snapshot type comparison

| Snapshot type | Export type |
|---------|---------|
| campaigns | campaigns |
| adGroups | adGroups |
| keywords | targets |
| negativeKeywords | targets |
| campaignNegativeKeywords | targets |
| targets | targets |
| negativeTargets | targets |
| productAds | ads | 

## Common model

Exports represent entities (campaigns and ad groups) in a standard way across all ad products (Sponsored Products, Sponsored Brands, and Sponsored Display). 

Note that field naming in the common model may differ from field names used in the campaign management API. To view mapping between the common model and the latest version of the campaign management APIs, see:

- [Campaign model](reference/common-models/campaigns#ad-product-mapping)
- [Ad group model](reference/common-models/ad-groups#ad-product-mapping)
- [Target model](reference/common-models/targets#ad-product-mapping)
- [Ad model](reference/common-models/ads#ad-product-mapping)

## Sponsored Brands-specific migration details

Exports contains both version 3 and version 4 Sponsored Brands campaigns. The Sponsored Brands version 4 flag `isMultiAdGroupsEnabled` is not included in exports.

## Deprecation

Snapshots APIs will be shut off on October 15, 2024. We recommend migrating to exports going forward. 

## Resources

- [Exports overview](guides/exports/overview)
- [Exports getting started guide](guides/exports/get-started)
- [API reference](exports)
