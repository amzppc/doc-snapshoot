---
title: Sponsored Products version 3 migration guide
description: Migration guide for Sponsored Products version 2 to version 3
type: guide
interface: api 
tags:
    - Sponsored Products
keywords:
    - Migration guide
    - version 2
    - version 3
---

# Sponsored Products version 3 migration guide

Version 3 of the Sponsored Products API introduces a simplified interface for creating and managing Sponsored Products campaigns. 

Highlights:

* Parity between Amazon Ads API and advertising console
* Enhanced error response for easier troubleshooting
* Consolidated extended and list endpoints
* Ability to request full objects in the response for created or updated in POST and PUT requests
* Filtering of list operations based on name using broad or exact match

## Endpoint equivalencies

|Version 2 endpoint 	|Version 3 endpoint	|
|-----------------------------------------------------------------	|---	|
|POST v2/sp/campaigns	|POST /sp/campaigns	|
|PUT /v2/sp/campaigns	|PUT /sp/campaigns	|
|GET /v2/sp/campaigns <br> GET /v2/sp/campaigns/{campaignId} <br> GET /v2/sp/campaigns/extended <br> GET /v2/sp/campaigns/{campaignId}/extended	<br> |POST /sp/campaigns/list	|
|DELETE /v2/sp/campaigns/{campaignId}	|POST /sp/campaigns/delete	|
|POST v2/sp/adGroups	|POST /sp/adGroups	|
|PUT /v2/sp/adGroups	|PUT /sp/adGroups	|
|GET /v2/sp/adGroups <br> GET /v2/sp/adGroups/{adGroupId} <br> GET /v2/sp/adGroups/extended <br> GET /v2/sp/adGroups/{adGroupId}/extended	|POST /sp/adGroups/list	|
|DELETE /v2/sp/adGroups/{adGroupId}	|POST /sp/adGroups/delete	|
|POST v2/sp/productAds	|POST /sp/productAds	|
|PUT /v2/sp/productAds	|PUT /sp/productAds	|
|GET /v2/sp/productAds <br> GET /v2/sp/productAds/{adId} <br> GET /v2/sp/productAds/extended <br> GET /v2/sp/productAds/{adId}/extended	|POST /sp/productAds/list	|
|DELETE /v2/sp/productAds/{adId}	|POST /sp/productAds/delete	|
|POST v2/sp/keywords	|POST /sp/keywords	|
|PUT /v2/sp/keywords	|PUT /sp/keywords	|
|GET /v2/sp/keywords <br> GET /v2/sp/keywords/{keywordId} <br> GET /v2/sp/keywords/extended<br> GET /v2/sp/keywords/{keywordId}/extended |POST /sp/keywords/list	|
|DELETE /v2/sp/keywords/{keywordId}	|POST /sp/keywords/delete	|
|POST v2/sp/negativeKeywords	|POST /sp/negativeKeywords	|
|PUT /v2/sp/negativeKeywords	|PUT /sp/negativeKeywords	|
|GET /v2/sp/negativeKeywords<br>GET /v2/sp/negativeKeywords/{keywordId}<br>GET /v2/sp/negativeKeywords/extended <br>GET /v2/sp/negativeKeywords/{keywordId}/extended	|POST /sp/negativeKeywords/list	|
|DELETE /v2/sp/negativeKeywords/{keywordId}	|POST /sp/negativeKeywords/delete	|
|POST v2/sp/campaignNegativeKeywords	|POST /sp/campaignNegativeKeywords	|
|PUT /v2/sp/campaignNegativeKeywords	|PUT /sp/campaignNegativeKeywords	|
|GET /v2/sp/campaignNegativeKeywords<br>GET /v2/sp/campaignNegativeKeywords/{keywordId}<br>GET /v2/sp/campaignNegativeKeywords/extended<br>GET /v2/sp/campaignNegativeKeywords/{keywordId}/extended	|POST /sp/campaignNegativeKeywords/list	|
|DELETE /v2/sp/campaignNegativeKeywords/{keywordId}	|POST /sp/campaignNegativeKeywords/delete	|
|POST v2/sp/targets	|POST /sp/targets	|
|PUT /v2/sp/targets	|PUT /sp/targets	|
|GET /v2/sp/targets<br>GET /v2/sp/targets/{targetId}<br>GET /v2/sp/targets/extended<br>GET /v2/sp/targets/{targetId}/extended	|POST /sp/targets/list	|
|DELETE /v2/sp/targets/{targetsId}	|POST /sp/targets/delete	|
|POST v2/sp/negativeTargets	|POST /sp/negativeTargets	|
|PUT /v2/sp/negativeTargets	|PUT /sp/negativeTargets	|
|GET /v2/sp/negativeTargets<br>GET /v2/sp/negativeTargets/{targetId}<br>GET /v2/sp/negativeTargets/extended<br>GET /v2/sp/negativeTargets/{targetId}/extended	|POST /sp/negativeTargets/list	|
|DELETE /v2/sp/negativeTargets/{targetsId}	|POST /sp/negativeTargets/delete	|

## IDs

All entity IDs (campaign, ad groups, ads, etc.) are now recognized as strings instead of integers. 

## Date format

Dates (for example, campaign `startDate`) should be in the format `YYYY-MM-DD`. This is a change from version 2, where the standard date format was `YYYYMMDD`.

## GET vs. LIST operations

Sponsored Products version 2 used mostly GET requests to read resources. In version 2, most resources had four separate GET endpoints: a batch get, a single entity get, batch extended, and single extended. In version 3, all read operations are implemented as POST operations. This allows you to use filters in the request body to filter by entity IDs, state, keywords, and more. 

### Example call

The following example shows how to list all enabled Sponsored Products campaigns. 

```
curl --location --request POST 'https://advertising-api.amazon.com/sp/campaigns/list' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxxxxx' \
--header 'Authorization: Bearer Atza|xxxxxxxxx' \
--header 'Amazon-Advertising-API-Scope: xxxxxxxxx' \
--header 'Accept: application/vnd.spCampaign.v3+json' \
--header 'Content-Type: application/vnd.spCampaign.v3+json' \
--data-raw '{
  "stateFilter": {
    "include": [
      "ENABLED"
    ]
  },
  "maxResults": 10
}'
```

## Batch archives

You can now archive multiple entities (campaigns, ad groups, ads, etc.) in a single call.

### Example

```
curl --location --request POST 'https://advertising-api.amazon.com/sp/campaigns/delete' \
--header 'Amazon-Advertising-API-ClientId: amzn1.application-oa2-client.xxxxxxxxxxxx' \
--header 'Authorization: Bearer Atza|xxxxxxxxxx' \
--header 'Amazon-Advertising-API-Scope: xxxxxxxxxx' \
--header 'Accept: application/vnd.spCampaign.v3+json' \
--header 'Content-Type: application/vnd.spCampaign.v3+json' \
--data-raw '{
  "campaignIdFilter": {
    "include": [
      "campaignId1",
      "campaignId2"
    ]
  }
}'
```

## New workflow for checking application of budget rules

In version 2, GET v2/sp/campaigns and GET v2/sp/campaigns/{campaignId} responses include a `ruleBasedBudget` object that specifies whether a budget rule is processing on the campaign, as well as if a new budget has been applied. Version 3 POST sp/campaigns/list do not contain a separate object for budget rules. Instead, if a campaign's budget has been impacted by a budget rule, you will see an `effectiveBudget` field included in the campaign `budget` object returned by [POST /sp/campaigns/list](sponsored-products/3-0/openapi/prod#tag/Campaigns/operation/ListSponsoredProductsCampaigns).  

You can use `effectiveBudget` as an indicator that budget rules have been applied, then retrieve the full budget rules history for that campaign using [GET /sp/campaigns/{campaignId}/budgetRules](sponsored-products/3-0/openapi/prod#tag/BudgetRules/operation/ListAssociatedBudgetRulesForSPCampaigns). 

The response for GET /sp/campaigns/{campaignId}/budgetRules resembles:

```json
{
    "associatedRules": [
        {
            "createdDate": 1666126775339,
            "lastUpdatedDate": 1666126775339,
            "ruleDetails": {
                "budgetIncreaseBy": {
                    "type": "PERCENT",
                    "value": 99.0
                },
                "duration": {
                    "dateRangeTypeRuleDuration": {
                        "endDate": null,
                        "startDate": "20221018"
                    },
                    "eventTypeRuleDuration": null
                },
                "name": "Test 3",
                "performanceMeasureCondition": null,
                "recurrence": {
                    "daysOfWeek": null,
                    "type": "DAILY"
                },
                "ruleType": "SCHEDULE"
            },
            "ruleId": "5582d17b-cea1-468f-9694-faf2fa2705da",
            "ruleState": "ACTIVE",
            "ruleStatus": "ACTIVE",
            "ruleStatusDetails": {
                "reasonCode": null,
                "status": "ACTIVE"
            }
        }
    ]
}
```

You can use the `ruleStatus` field to understand which rule is effecting the campaign. A `ruleStatus` of `ACTIVE` indicates that rule is currently being applied to the campaign budget. You can learn more about possible statuses and their meaning in [Budget rule evaluation](guides/rules/budget-rules/rule-evaluation). 

## Product ads

KDP authors can now create product ads with custom ad copy using the `customText` field. [Learn more.](guides/sponsored-products/product-ads)

## Expanded product targeting

A new predicate type (`ASIN_EXPANDED_FROM`) is available in version 3 product targeting. You can use expanded product targeting to target a single ASIN as well as similar ASINs, using a single targeting clause. 

To learn more, see [Product targeting overview](guides/sponsored-products/product-targeting/overview).

## Campaign negative targeting

Vendors can now create campaign-level negative targets for both auto and manual campaigns. Sellers can create campaign negative targets for only auto campaigns. 

To learn more, see [Negative product targeting](guides/sponsored-products/negative-targeting/product-brand).

## Enum casing

All enums (including state and targeting expression types) in version 3 have moved to an upper-case (UPPER_CASE) convention. 

Example changes for targeting predicates:

|Version 2 	|Version 3 	|
|---	|---	|
|`queryBroadRelMatches`	|`QUERY_BROAD_REL_MATCHES`	|
|`queryHighRelMatches`	|`QUERY_HIGH_REL_MATCHES`	|
|`asinAccessoryRelated`	|`ASIN_ACCESSORY_RELATED`	|
|`asinSubstituteRelated`	|`ASIN_SUBSTITUTE_RELATED`	|
|`asinCategorySameAs`	|`ASIN_CATEGORY_SAME_AS`	|
|`asinBrandSameAs`	|`ASIN_BRAND_SAME_AS`	|
|`asinPriceLessThan`	|`ASIN_PRICE_LESS_THAN`	|
|`asinPriceBetween`	|`ASIN_PRICE_BETWEEN`	|
|`asinPriceGreaterThan`	|`ASIN_PRICE_GREATER_THAN`	|
|`asinReviewRatingLessThan`	|`ASIN_REVIEW_RATING_LESS_THAN`	|
|`asinReviewRatingBetween`	|`ASIN_REVIEW_RATING_BETWEEN`	|
|`asinReviewRatingGreaterThan`	|`ASIN_REVIEW_RATING_GREATER_THAN`	|
|`asinSameAs`	|`ASIN_SAME_AS`	|
|`asinIsPrimeShippingEligible`	|`ASIN_IS_PRIME_SHIPPING_ELIGIBLE`	|
|`asinAgeRangeSameAs`	|`ASIN_AGE_RANGE_SAME_AS`	|
|`asinGenreSameAs`	|`ASIN_GENRE_SAME_AS`	|
|	|	|

## Accept headers for versioning

Versioning for version 3 endpoints is controlled by the `Accept` and `Content-Type` header, rather than the path. For each endpoint, check the **Media-type** dropdown to understand what value to use for the `Accept` and `Content-Type` header.

## Prefer header

You can use the prefer header to specify whether you want the full object representation as part of the response. If you donâ€™t specify the prefer header, you will see just the ID of the created entity in the response.

Index that gives the order that the object was in the original request. 

## Improved documentation

* How to guides available for creating [Manual campaigns](guides/sponsored-products/get-started/manual-campaigns) and [Auto campaigns](guides/sponsored-products/get-started/auto-campaigns).
* Resource guides now available for [Campaigns](guides/sponsored-products/campaigns), [Ad groups](guides/sponsored-products/ad-groups), [Keywords](guides/sponsored-products/keywords), [Product targeting](guides/sponsored-products/product-targeting), [Auto targeting](guides/sponsored-products/auto-targeting) and more.

## Postman collection

All version 3 CRUD endpoints are available in the Amazon Ads API Postman collection. You can download collection and environment files from [GitHub](https://github.com/amzn/ads-advanced-tools-docs/tree/main/postman).

## Version 2 bug fixes

* Product ads are now listed in full, so you can understand what was created successfully or failed.


