---
title: OpenAPI download directory
description: View and download all contracts for the Amazon Ads API on one page.
type: guide
interface: api
tags:
    - Operations
---

# OpenAPI download directory

All API contracts are available for developers to download. Note that each product or category may have multiple files associated. This table contains the latest resources and the contract in which they can be found, as well as links to associated user guides. 

|Category	|Resources	|Download link	|Relevant user guides	|
|---	|---	|---	|---	|
|Accounts	|Profiles	|https://d3a0d0y2hgofx6.cloudfront.net/openapi/en-us/profiles/3-0/openapi.yaml	| [Overview](guides/account-management/authorization/profiles)|
|Accounts	|Manager accounts	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/ManagerAccount_prod_3p.json	| [Overview](guides/account-management/authorization/manager-accounts)	|
|Accounts   |Ads accounts   | https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/AdvertisingAccounts_prod_3p.json | - [Retrieve accounts](guides/account-management/accounts/retrieve-accounts) <br /> - [Create accounts](guides/account-management/accounts/create-accounts) |
|Accounts	|Portfolios	|https://d3a0d0y2hgofx6.cloudfront.net/openapi/en-us/portfolios/openapi.yaml	|	|
|Accounts	| Billing	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/AdvertisingBilling_prod_3p.json	| [Overview](guides/account-management/billing)	|
|Accounts	|Account budgets	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/Advertisers_prod_3p.json	| [Average daily budget user guide](guides/account-management/average-daily-budget)	|
|Accounts	|Test accounts	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/AdvertisingTestAccount_prod_3p.json	|	- [Overview](guides/account-management/test-accounts/overview) <br /> - [Auth](guides/account-management/test-accounts/authorization-for-test-accounts) <br />  - [Get started](guides/account-management/test-accounts/create-test-accounts)|
|Audiences	|Discovery	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/Audiences_prod_3p.json	|	|
|Reporting	|Asynchronous reporting (Version 3)	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/OfflineReport_prod_3p.json	| - [Overview](guides/reporting/v3/overview) <br /> - [Report types](guides/reporting/v3/report-types) <br /> - [Metrics](guides/reporting/v3/columns) <br /> - [Get started](guides/reporting/v3/get-started)	|
|Brand metrics	|Brand metrics	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/BrandMetrics_prod_3p.json	| - [Overview](guides/reporting/brand-metrics/overview) <br />  - [Get started](guides/reporting/brand-metrics/get-started) <br />  - [How-to](guides/reporting/brand-metrics/how-to) <br />  - [Troubleshooting](guides/reporting/brand-metrics/troubleshooting)	|
|Reporting	|Stores analytics	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/Stores_prod_3p.json	|	|
|Sponsored Products	|Bid recommendations, Keyword recommendations, Keywords, Negative keywords, Product targeting, Campaign optimization rules, Budget rules, Product ads, Negative targeting, Budget recommendations (new campaigns), Campaign negative targeting, Budget recommendations (existing campaigns), Targeting, Budget rules recommendations, Campaigns, Ad groups, Consolidated recommendations, Campaign negative keywords, Product recommendations, Budget usage	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/SponsoredProducts_prod_3p.json	| - [Overview](guides/sponsored-products/overview) <br /> - [Campaigns](guides/sponsored-products/campaigns) <br /> - [Ad groups](guides/sponsored-products/ad-groups) <br /> - [Product ads](guides/sponsored-products/product-ads) <br /> - [Keywords](guides/sponsored-products/keywords/overview) <br /> - [Targeting](guides/sponsored-products/product-targeting/overview) <br /> - [Budget recommendations](guides/sponsored-products/budget-recommendations-and-missed-opportunities) <br /> - [Budget rules](guides/rules/budget-rules/overview) <br /> - [Budget usage](guides/budgets/usage/overview) <br /> - [Bid recommendations](guides/sponsored-products/bid-suggestions/theme-based-bid-suggestions-quickstart-guide)  <br /> - [Bidding rules](guides/rules/bidding-rules/overview) <br /> - [Negative product targeting](guides/sponsored-products/negative-targeting/product-brand) <br /> - [Negative keywords](guides/sponsored-products/negative-targeting/keywords)	|
|Sponsored Products	|Snapshots, Suggested keywords	|https://d3a0d0y2hgofx6.cloudfront.net/openapi/en-us/sponsored-products/2-0/openapi.yaml	| [Snapshots](guides/snapshots/overview)	|
|Sponsored Brands	|Budget rules, Ad creatives, Campaigns, Insights, Ads, Ad groups, Budget recommendations, Product targeting categories, Budget rules recommendations, Keyword recommendations, Targeting recommendations, Creative headline recommendations, Budget usage |https://d3a0d0y2hgofx6.cloudfront.net/openapi/en-us/sponsored-brands/4-0/openapi.json	| - [Campaigns](guides/sponsored-brands/campaigns/get-started-with-campaigns) <br /> - [Ads](guides/sponsored-brands/ads/overview) <br /> 	|
|Sponsored Brands	|Keywords, Negative keywords, Product targeting, Negative product targeting, Targeting recommendations, Bid recommendations, Stores, Landing page ASINs, Brands, Moderation, Reports	|https://d3a0d0y2hgofx6.cloudfront.net/openapi/en-us/sponsored-brands/3-0/openapi.yaml	| - [Reporting tutorial](guides/reporting/v2/sponsored-ads-reports)	|
|Sponsored Display	|Campaigns, Ad groups, Product ads, Targeting, Targeting recommendations, Bid recommendations, Negative targeting, Creatives, Brand safety list, Reports, Snapshots, Forecasts	|https://d3a0d0y2hgofx6.cloudfront.net/openapi/en-us/sponsored-display/3-0/openapi.yaml	| - [Audience campaigns](guides/sponsored-display/audience-targeting) <br /> - [Contextual targeting campaigns](guides/sponsored-display/contextual-targeting) <br /> - [Recommendations](guides/sponsored-display/recommendations)	<br /> - [Creatives](guides/sponsored-display/creatives) <br /> - [Budget rules](guides/rules/budget-rules/overview) <br /> - [Budget usage](guides/budgets/usage/overview)	<br /> - [Brand safety](guides/sponsored-display/brand-safety)|
| Sponsored Display | Lead forms | https://d3a0d0y2hgofx6.cloudfront.net/openapi/en-us/sponsored-display/3-0/openapi.yaml | | 
|Amazon DSP	|Measurement	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/Measurement_prod_3p.json	|	|
|Amazon DSP	|Advertisers	|https://d3a0d0y2hgofx6.cloudfront.net/openapi/en-us/dsp/3-0/advertiser.yaml	|	|
|Amazon DSP	|Audiences	|https://d1y2lf8k3vrkfu.cloudfront.net/openapi/en-us/dest/ADSPAudiences_prod_3p.json	|	|
|Amazon DSP | Conversions | https://d1y2lf8k3vrkfu.cloudfront.net/openapi/en-us/dest/ConversionsAPI_prod_3p.json | | 
| Amazon DSP | Target KPI recommendations | https://d1y2lf8k3vrkfu.cloudfront.net/openapi/en-us/dest/GoalSeekingBidderTargetKPIRecommendation_prod_3p.json | | 
|Amazon Attribution	|Publishers, Reports, Tags, Advertisers	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/AmazonAttribution_prod_3p.json	| - [Overview](guides/amazon-attribution/overview) <br />  - [Get started](guides/amazon-attribution/get-started) <br />  - [How-to](guides/amazon-attribution/how-to) <br />  - [Troubleshooting](guides/amazon-attribution/troubleshooting)	|
|Recommendations & insights	|Audience insights	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/Insights_prod_3p.json	| [DSP overlapping audiences tutorial](guides/dsp/overlapping-audiences)	|
|Recommendations & insights	|Partner opportunities	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/PartnerOpportunities_prod_3p.json	| - [Overview](guides/recommendations/partner-opportunities/overview) <br />  - [Get started](guides/recommendations/partner-opportunities/getting-started) <br />  - [How-to](guides/recommendations/partner-opportunities/how-to)	|
|Recommendations & insights	|Tactical recommendations	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/Recommendations_prod_3p.json	| - [Overview](guides/recommendations/tactical-recommendations/overview) <br />  - [Get started](guides/recommendations/tactical-recommendations/get-started)	|
| Recommendations & insights | Persona builder | https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/PersonaBuilderAPI_prod_3p.json | | 
|Creatives	|Assets	|https://d3a0d0y2hgofx6.cloudfront.net/openapi/en-us/creative-asset-library/creative-asset-library-openapi.yaml	|[Creating assets](guides/creative-asset/asset-library-overview)	|
|Change history	|History	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/Changehistory_prod_3p.json	|	|
|Data provider	|Metadata, Records, User deletion	|https://d3a0d0y2hgofx6.cloudfront.net/openapi/en-us/data-provider/openapi.yaml	|	|
|Data provider	|Hashed records	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/HashedRecords_prod_3p.json	|	|
|Products	|Metadata	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/ProductSelector_prod_3p.json	|	|
|Products	|Eligibility	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/Eligibility_prod_3p.json	|	|
|Unified pre-moderation	|Results	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/PreModeration_prod_3p.json 	|	|
|Moderation	|Results	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/Moderation_prod_3p.json	|	|
|Audiences	|Discovery	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/Audiences_prod_3p.json	|	|
|Amazon Marketing Stream	|Subscriptions	|https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/AmazonMarketingStream_prod_3p.json	| - [Overview](guides/amazon-marketing-stream/overview) <br />  - [Get started](guides/amazon-marketing-stream/onboarding)  <br />  - [Data guide](guides/amazon-marketing-stream/data-guide)	|
| Locations | Locations | https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/Locations_prod_3p.json | [Sponsored Display locations user guide](guides/sponsored-display/non-amazon-sellers/locations) |
| Exports | Exports | https://dtrnk0o2zy01c.cloudfront.net/openapi/en-us/dest/AmazonAdsAPIExports_prod_3p.json | [Exports guide](guides/exports/overview) |




