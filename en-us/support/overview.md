---
title: Support
description: Get support for users of the Amazon Ads API or bulk operations. 
type: guide
interface: general
tags:
    - Operations
keywords:
    - Jira
    - Zendesk
    - Help Center
    - help
    - troubleshooting
---

# Support for Amazon Ads advanced tools

## Amazon Ads API

For support with onboarding to the Amazon Ads API, please contact our onboarding support team directly at **ads-api-onboarding@amazon.com**.

For continuing API support, you can reach our support team directly at **ads-api-support@amazon.com** to access our Zendesk support platform.

>[NOTE]To ensure seamless communication, please add **ads-api-support@amazon.com**, **ads-api-onboarding@amazon.com**, and **support@amazon-ads-api.zendesk.com** to your email's safe sender list to prevent our communications from being inadvertently marked as spam. 

The API support team offers continuing support via the [Help Center](https://amazon-ads-api.zendesk.com).

[Learn more about getting support through Zendesk.](https://amazon-ads-api.zendesk.com/hc/en-us/articles/25110088401435-Need-Support-Find-your-Answers-Here)


## Sponsored ads bulk operations 

Support for bulk operations is handled through the **[Sponsored Ads and Stores Support Center](https://advertising.amazon.com/help)**.

## Amazon Marketing Cloud (AMC)

For AMC product-related support, please reach out directly to the AMC support team at **amc-support@amazon.com** or **support@marketingcloud.amazon**.

For support for AMC in the Amazon Ads API, you can reach our API support team directly at **ads-api-support@amazon.com**.

## Traffic Events API

For Traffic Events API support please reach out to **support@trafficevents.amazon**.

## Documentation support

To report issues or offer suggestions on the documentation in the advanced tools center, open an issue in our [GitHub repository](https://github.com/amzn/ads-advanced-tools-docs).

