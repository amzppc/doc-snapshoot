---
title: Bulksheets legacy information
description: Information about the legacy version of bulksheets.
type: guide
interface: bulk-operations
---

# Sponsored Brands Campaign Budget Limit (both for Product Collection and Video)

| Currency | Min Daily Budget | Max Daily Budget | Min Lifetime Budget | Max Lifetime Budget |
|----------|------------------|------------------|---------------------|---------------------|
| GBP      | 1                | 1,000,000        | 100                 | 20,000,000          |
| USD      | 1                | 1,000,000        | 100                 | 20,000,000          |
| CAD      | 1                | 1,000,000        | 100                 | 20,000,000          |
| EUR      | 1                | 1,000,000        | 100                 | 20,000,000          |
| CNY      | 1                | 21,000,000       | 100                 | 200,000,000         |
| JPY      | 100              | 21,000,000       | 10,000              | 2,000,000,000       |
| INR      | 100              | 21,000,000       | 5,000               | 200,000,000         |
| AED      | 4                | 3,700,000        | 367                 | 74,000,000          |
| MXN      | 1                | 21,000,000       | 100                 | 200,000,000         |
| AUD      | 1.4              | 1,500,000        | 141                 | 28,000,000          |