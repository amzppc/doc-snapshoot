---
title: Bulksheets legacy information
description: Information about the legacy version of bulksheets.
type: guide
interface: bulk-operations
---

# Sponsored Brands Bid Limits

| Currency | AdGroup Min Bid | AdGroup Max Bid | Keyword Min Bid | Keyword Max Bid | Min Bid Multipler | Max Bid Multipler |
|----------|-----------------|-----------------|-----------------|-----------------|-------------------|-------------------|
| GBP      | 0.1             | 31              | 0.1             | 31              | -99               | 99.99             |
| USD      | 0.1             | 49              | 0.1             | 49              | -99               | 99.99             |
| CAD      | 0.1             | 49              | 0.1             | 49              | -99               | 99.99             |
| EUR      | 0.1             | 39              | 0.1             | 39              | -99               | 99.99             |
| CNY      | 1               | 50              | 1               | 50              | -99               | 99.99             |
| JPY      | 10              | 7,760           | 10              | 7,760           | -99               | 99.99             |
| INR      | 2               | 500             | 2               | 500             | -99               | 99.99             |
| AED      | 0.4             | 184             | 0.4             | 184             | -99               | 99.99             |
| MXN      | 0.1             | 20,000          | 0.1             | 20000           | -99               | 99.99             |
| AUD      | 0.1             | 70              | 0.1             | 70              | -99               | 99.99             |

# Sponsored Brands Video Bid Limits

| Currency | AdGroup Min Bid | AdGroup Max Bid | Keyword Min Bid | Keyword Max Bid | Min Bid Multipler | Max Bid Multipler |
|----------|-----------------|-----------------|-----------------|-----------------|-------------------|-------------------|
| GBP      | 0.15            | 31              | 0.15            | 31              | -99               | 99.99             |
| USD      | 0.25            | 49              | 0.25            | 49              | -99               | 99.99             |
| EUR      | 0.15            | 39              | 0.15            | 39              | -99               | 99.99             |
