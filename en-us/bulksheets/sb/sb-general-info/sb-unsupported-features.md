---
title: Bulksheets legacy information
description: Information about the legacy version of bulksheets.
type: guide
interface: bulk-operations
---

# Features not supported in Bulksheets

The following Sponsored Brands features are not supported in Bulksheets:

* Product Attribute Targeting for Sponsored Brands 
* Negative Keyword for Sponsored Brands Draft (both product collection and video)
* Campaigns and drafts with "video" ad format are only available in US, UK, and DE.

 <br/>
 <br/>