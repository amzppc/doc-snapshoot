---
title: Bulksheets legacy information
description: Information about the legacy version of bulksheets.
type: guide
interface: bulk-operations
---

# Sponsored Brands Entities

The following diagram shows the entity types and their relationships for Sponsored Brands campaigns.

__Key Points to Note__

* Bulksheets supports creation and update of both Sponsored Brands campaigns and Sponsored Brands Draft campaigns. The latter allows advertisers to create shell campaigns while they continue to fill in the details.
* Sponsored Brands Video campaigns and draft can also be created by specifying ad format. Currently available in US, UK, and DE.
* Product Attribute Targeting for Sponsored Brands is not supported in Bulksheets
* Negative Keyword for Sponsored Brands Draft is not supported in Bulksheets

![Sponsored Brands Entity Hierarchy](/_images/bulksheets/sb/1.png)

