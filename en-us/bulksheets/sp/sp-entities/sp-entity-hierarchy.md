---
title: Bulksheets legacy information
description: Information about the legacy version of bulksheets.
type: guide
interface: bulk-operations
---

# Sponsored Products Entities

As shown below, there are two types of Sponsored Products campaigns, which can be created and managed via Bulksheets:

* Auto targeting campaigns
* Manual targeting campaigns

<br/>
<br/>

The following diagram shows the entity types and their relationships for Sponsored Product campaigns.
<br/>
<br/>

![Sponsored Products Entity Hierarchy](/_images/bulksheets/sp/1.png)
