---
title: Bulksheets legacy information
description: Information about the legacy version of bulksheets.
type: guide
interface: bulk-operations
---

# Updating existing entities in a Sponsored Products Automatic Targeting Campaign

This is similar to the example [Updating a Sponsored Products Manual Targeting campaign to edit existing entities](bulksheets/sp/sp-examples/sp-update-manual-targeting-edit-existing-entities). Refer to the entity details under 'Entities' to know which fields are mutable. 