---
title: Bulksheets legacy information
description: Information about the legacy version of bulksheets.
type: guide
interface: bulk-operations
---

# Sponsored Products Features not supported in Bulksheets

Sponsored Products Draft is not supported in Bulksheets

Sponsored Products Campaign Negative Product Targeting is not supported in Bulksheets
