---
title: Bulksheets legacy information
description: Information about the legacy version of bulksheets.
type: guide
interface: bulk-operations
---

# Sponsored Products Campaign Budget Limit

Note that these limits also apply to Sponsored Display campaigns.

| Currency | Min Budget | Max Budget |
|----------|------------|------------|
| BE       | 1          | 1,000,000  |
| GBP      | 1          | 1,000,000  |
| USD      | 1          | 1,000,000  |
| CAD      | 1          | 1,000,000  |
| EUR      | 1          | 1,000,000  |
| CNY      | 1          | 21,000,000 |
| JPY      | 100        | 21,000,000 |
| INR      | 500        | 21,000,000 |
| AED      | 4          | 3,700,000  |
| MXN      | 1          | 21,000,000 |
| AUD      | 1.4        | 1,500,000  |