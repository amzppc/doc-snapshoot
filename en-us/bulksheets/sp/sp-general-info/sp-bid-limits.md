---
title: Bulksheets legacy information
description: Information about the legacy version of bulksheets.
type: guide
interface: bulk-operations
---

# Sponsored Products Bid Limits

| Currency | Ad Group Min Bid | Ad Group Max Bid | Keyword Min Bid | Keyword Max Bid |
|----------|------------------|------------------|-----------------|-----------------|
| BE       | 0.02             | 1000             | 0.02            | 1000            |
| GBP      | 0.02             | 1000             | 0.02            | 1000            |
| USD      | 0.02             | 1000             | 0.02            | 1000            |
| CAD      | 0.02             | 1000             | 0.02            | 1000            |
| EUR      | 0.02             | 1000             | 0.02            | 1000            |
| CNY      | 0.1              | 1000             | 0.1             | 1000            |
| JPY      | 2                | 100000           | 2               | 100000          |
| INR      | 1                | 5000             | 1               | 5000            |
| AED      | 0.24             | 3670             | 0.24            | 3670            |
| MXN      | 0.1              | 20000            | 0.1             | 20000           |
| AUD      | 0.1              | 1410             | 0.1             | 1410            |