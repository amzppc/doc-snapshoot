---
title: Bulksheets legacy release notes
description: Information about the legacy version of bulksheets.
type: guide
interface: bulk-operations
---

# Release Notes

## November 17, 2020

**Restricted keywords: prescription medications**

Updated keyword restrictions for advertisers. Amazon Ads does not allow the targeting of prescription medicine products or the use of prescription medicine names as targeting keywords or phrases. 

For example, prescription medicine keyword or products, such as "Lipitor" or "prednisone".

As a reminder, keywords or products targeted through Amazon Ads must be relevant to the product being promoted. Ads must not target keywords or products which could result in offensive, insensitive, or undesirable shopper experience. 

## August 24, 2020

Added link to the Bulksheets portal on the Bulk Operations page. 

Simplified the Bulksheets template by removing the "Instructions", "Attribute definitions" and "SD Attribute definitions" tabs from the template. Users can now find detailed instructions and
 examples on using Bulksheets on this portal instead.


## July 31, 2020

Added Bulksheets documentation with detailed user instructions and examples for all supported Sponsored Ads products to this portal