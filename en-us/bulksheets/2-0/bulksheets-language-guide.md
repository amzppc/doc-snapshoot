---
title: Bulksheets language guide
description: Understand the translated values for the blank "Operations" field in a downloaded bulksheets file
type: guide
interface: bulk-operations
tags:
    - Sponsored Products
    - Sponsored Display
    - Sponsored Brands
    - Campaign management
    - Bulksheets
    - Bulk operations
keywords:
    - bulksheets
    - translations
---

# Language guide for bulksheets translations 

You can download a bulksheet in your chosen language by selecting that language from your advertiser profile. For example, if Japanese is the language selected in your profile, you will see Japanese text in bulksheets downloads. When you upload the file, you can use any supported language, regardless of the language selected in your profile. This article provides guidance on what you should enter in the “Operation” field (Column C) if you want to use a language other than English. 

When you download a bulksheets file, the “Operation” field will always be blank. To make changes to any row, you must enter a value in this field depending on what you want to do: Either create, update, archive, or submit. Note that the “submit” value only applies for Sponsored Brands draft campaigns. 


>[TIP]For any rows that do not need changes, you can leave the “Operation” field blank, and our system will ignore those rows. <br><br> You can get the translated values for other fields from within the downloaded file if your preferred language is set in your advertiser profile. For example, if you want to verify the translated value for the “phrase” or “broad” match type, you can find these in the “Match type” column of a downloaded file. <br><br> Note that the “Input guidance” feature for Sponsored Products is only available in English. If your language is not set to English, this checkbox in the bulk operations user interface will be disabled. 

## Translation guide for "Operation" values

The table below shows what you should enter for the “Operation” values if you are using a language other than English. The English terms are in the top row. Refer to your preferred language to see the equivalent values that you should enter into the “Operation” field, depending on what you want to do in that row. Leave the field blank for any rows where you don’t want to take any action.   


|English - EN - AU - CA - GB - IN - SG    |Create    |Update    |Archive    |Submit (for Sponsored Brands draft campaigns only)    |
|---    |---    |---    |---    |---    |
|Arabic - AE    |إنشاء    |تحديث    |الأرشيف    |إرسال    |
|Simplified Chinese - CN    |创建    |更新    |存档    |提交    |
|Traditional Chinese - TW    |建立    |更新    |封存    |提交    |
|French - FR- CA    |Créer    |Mettre à jour    |Archiver    |Soumettre    |
|German - DE    |Erstellen    |Aktualisieren    |Archivieren    |Absenden    |
|Italian - IT    |Crea    |Aggiorna    |Archivia    |Invia    |
|Japanese - JP    |作成    |更新    |非表示にする    |送信    |
|Korean - KO    |생성    |업데이트    |보관    |제출    |
|Swedish - SE    |Skapa    |Uppdatera    |Arkiv    |Skicka in    |
|Spanish - CO - ES - MX    |Crear    |Actualizar    |Archivar    |Enviar    |
|Polish - PL    |Utwórz    |Aktualizuj    |Zarchiwizuj    |Prześlij    |
|Portuguese - BR    |Criar    |Atualização    |Arquivar    |Enviar    |
|Tamil - IN    |உருவாக்கு    |புதுப்பிப்பு    |காப்பகம்    |சமர்ப்பிக்கவும்    |
|Thai - TH    |สร้าง    |อัพเดต    |เก็บถาวร    |ส่ง    |
|Turkish - TR    |Oluştur    |Güncelle    |Arşivle    |Gönder    |
|Vietnamese - VN    |Tạo    |Cập nhật    |Lưu trữ    |Gửi    |
|Dutch - NL    |Maken    |Bijwerken    |Archief    |Indienen    |
|Hindi - IN    |बनाएँ    |अपडेट करें    |आर्काइव करें    |सबमिट करें    |