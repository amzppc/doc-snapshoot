---
title: Bulksheets video tutorials
description: A series of video tutorials to help you learn how to use bulksheets for campaign management
type: guide
interface: bulk-operations
tags:
    - Sponsored Products
    - Sponsored Display
    - Sponsored Brands
    - Campaign management
keywords:
    - videos
    - bulksheets 
    - video tutorials 
---

#  Bulksheets video tutorials

This article contains a series of eight video tutorials with guidelines and tips for success. Topics range from a general bulksheets overview, to campaign creation, optimization, and more. For each video, you can see a brief description of the content, along with timestamps if you want to skip to a topic within the video. 

To jump directly to a particular video, click the links below:

[Overview](#bulksheets-overview) | [Custom templates](#bulksheets-custom-templates) | [Create campaigns](#create-campaigns) | [Formatting tips](#formatting-tips) | [Optimize campaigns](#optimize-campaigns) | [Migrate from legacy](#migrating-from-legacy-to-the-new-version)

## Bulksheets overview

This video is for advertisers interested in using bulksheets who want more information. If you’re using the advertising console and want to scale to manage multiple campaigns more efficiently, this video will explain how. 
*Total time: 04:14*

#### Video highlights

|Timestamp	|Topic	|
|---	|---	|
|00:00	|Intro and video overview	|
|00:30	|What the video will and will not cover	|
|01:00	|Amazon Ads advanced tools: Which one is right for you?	|
|02:20	|Bulksheets benefits	|
|03:04	|Bulksheets common use cases	|
|03:45	|Where to access bulksheets	|
|04:00	|Where to learn more	|

<iframe sandbox="allow-scripts allow-same-origin" width="560" height="315" src="https://www.youtube.com/embed/MWrByUA-bZo" title="YouTube video player" frameborder="0" allowfullscreen></iframe>


## Bulksheets custom templates 

This video is for advertisers who want to use bulksheets and want more clarity on how to get a custom template with the campaign data you want to see. The video also walks through each tab contained within the template. 
*Total time: 07:00*

#### Video highlights

|Timestamp	|Topic	|
|---	|---	|
|00:00	|Intro and video overview	|
|00:40	|Where to find the bulk operations page	|
|01:05	|How to select the data you want to see	|
|02:30	|What to expect when you download and open your bulksheets file	|
|02:48	|Portfolios tab	|
|03:10	|Brand assets data tab	|
|04:10	|Sponsored Products tab	|
|05:05	|Sponsored Brands tab	|
|06:05	|Sponsored Display tab	|
|06:46	|Where to learn more	|

<iframe sandbox="allow-scripts allow-same-origin" width="560" height="315" src="https://www.youtube.com/embed/mHl8eW8IJKM" title="YouTube video player" frameborder="0" allowfullscreen></iframe>


## Create campaigns

### How to create Sponsored Products auto campaigns 

This video is ideal for advertisers who want to manage Sponsored Products campaigns using bulksheets. 
*Total time: 08:59* 

#### Video highlights

|Timestamp	|Topic	|
|---	|---	|
|00:00	|Intro and video overview	|
|00:40	|Download a blank bulksheets template	|
|01:17	|A few tips for success	|
|02:05	|Determine campaign goals	|
|03:00	|Understand campaign structure and hierarchy	|
|04:15	|Step 1: Campaign entity row	|
|05:40	|Step 2: Bidding adjustment entity row	|
|06:27	|Step 3: Ad group entity row	|
|06:47	|Step 4: Product ad entity row	|
|07:16	|Save & upload the file	|
|07:42 	|Important note about product targeting with auto campaigns	|
|08:45	|Where to learn more	|

<iframe sandbox="allow-scripts allow-same-origin" width="560" height="315" src="https://www.youtube.com/embed/vmSLdHOjPtg" title="YouTube video player" frameborder="0" allowfullscreen></iframe>



### How to create Sponsored Brands campaigns

This video is ideal for advertisers who want to manage Sponsored Brands campaigns using bulksheets. *Total time: 06:36*  

#### Video highlights

|Timestamp	|Topic	|
|---	|---	|
|00:00	|Intro and video overview	|
|00:27	|Download a blank bulksheets template	|
|00:55	|A few tips for success	|
|01:45	|Determine campaign goals	|
|03:00	|Understand campaign structure and hierarchy	|
|04:03	|Step 1: Campaign entity row	|
|05:36	|Step 2: Keyword entity row	|
|06:12	|Save & upload the file	|
|06:22	|Where to learn more	|

<iframe sandbox="allow-scripts allow-same-origin" width="560" height="315" src="https://www.youtube.com/embed/sFL_4_Ywev0" title="YouTube video player" frameborder="0" allowfullscreen></iframe>

### How to create Sponsored Display campaigns

This video is ideal for advertisers who want to manage Sponsored Display campaigns using bulksheets. 
*Total time: 09:55*  

#### Video highlights

|Timestamp	|Topic	|
|---	|---	|
|00:00	|Intro and video overview	|
|00:40	|Download a blank bulksheets template	|
|01:08	|A few tips for success	|
|01:50	|Determine campaign goals	|
|03:20	|Understand campaign structure and hierarchy	|
|04:26	|Step 1: Campaign entity row	|
|05:40	|Step 2: Ad group entity row	|
|06:00	|Step 3: Product ad entity row	|
|06:12	|Step 4: Contextual targeting entity row	|
|07:10	|Important tips about targeting expression values	|
|07:45	|Add an audience targeting campaign to the sheet	|
|09:29	|Save & upload the file	|
|09:42	|Where to learn more	|

<iframe sandbox="allow-scripts allow-same-origin" width="560" height="315" src="https://www.youtube.com/embed/KxGUaRbrhEs" title="YouTube video player" frameborder="0" allowfullscreen></iframe>



## Formatting tips

This video is for advertisers who use bulksheets to manage campaigns. The goal is to help you understand bulksheets formatting so you can avoid errors when you upload the file.  
*Total time: 04:54*

#### Video highlights

|Timestamp	|Topic	|
|---	|---	|
|00:00	|Intro and video overview	|
|00:13	|What the video will and will not cover	|
|00:25	|Bulksheets error reports	|
|01:34	|Formatting dates	|
|02:38	|Formatting dollar figures	|
|03:00	|Formatting percent figures	|
|03:34	|Formatting Sponsored Products placements	|
|04:00	|Formatting Sponsored Display targeting tactics	|
|04:24	|Formatting Portfolio terminology	|
|04:37	|Where to learn more	|

<iframe sandbox="allow-scripts allow-same-origin" width="560" height="315" src="https://www.youtube.com/embed/-zszyBgbqg0" title="YouTube video player" frameborder="0" allowfullscreen></iframe>



## Optimize campaigns 

This video is for advertisers who use bulksheets to manage campaigns. The goal is to provide guidelines and tips for reviewing campaigns to optimize performance. You’ll learn how to archive and pause keywords, adjust bids, and more. 
*Total time: 08:38*

#### Video highlights

|Timestamp	|Topic	|
|---	|---	|
|00:00	|Intro and video overview	|
|00:20	|Optimization topics the video will cover	|
|00:36	|Getting a custom template	|
|01:20	|Optimize keywords	|
|04:45	|Optimize bids	|
|07:00	|Optimize ASINs	|
|08:20	|Where to learn more	|

<iframe sandbox="allow-scripts allow-same-origin" width="560" height="315" src="https://www.youtube.com/embed/OnlEXhSgUD0" title="YouTube video player" frameborder="0" allowfullscreen></iframe>



## Migrating from legacy to the new version  

This video is for advertisers who have used the deprecated legacy version of bulksheets and are looking for guidance on using the new version. The content focuses on the differences you’ll see when you start using the new template. The arrow symbols below indicate what has changed from legacy compared with the new version—for example, the “Record Type” field is now labeled “Entity”. 
*Total time: 07:19*

#### Video highlights

|Timestamp	|Topic	|
|---	|---	|
|00:00	|Intro and video overview	|
|00:28	|What the video will and will not cover	|
|01:00	|New: Product, Entity, & Operation columns	|
|01:55	|Record ID → Entity IDs	|
|02:50	|Record Type → Entity	|
|03:37	|Campaign → Campaign Name	|
|03:50	|Campaign Daily Budget → Daily Budget	|
|04:00	|Campaign Start Date → Start Date	|
|04:10	|Status → State	|
|04:45	|Max Bid → Ad Group Default Bid, Bid	|
|05:05	|Keyword or Product Targeting → Keyword Text, Product Targeting Expression	|
|05:38	|Increase bids by placement → Percentage, Placement	|
|06:10	|A few tips for Sponsored Brands	|
|06:35	|Automated bidding → Bid optimization	|
|07:02	|Where to learn more	|

<iframe sandbox="allow-scripts allow-same-origin" width="560" height="315" src="https://www.youtube.com/embed/PuT7-a8A_fE" title="YouTube video player" frameborder="0" allowfullscreen></iframe>



