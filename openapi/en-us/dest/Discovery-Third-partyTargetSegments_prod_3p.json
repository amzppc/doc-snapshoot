{
    "info": {
        "description": "These APIs are part of the Amazon DSP Campaign Management API closed beta and only accessible to closed beta participants at this time. Because these APIs are in closed beta, they don\u2019t follow our normal versioning standards and are subject to change. If you are interested in joining the closed beta, please [submit a support ticket](https://amzn-clicks.atlassian.net/servicedesk/customer/portal/2/group/2/create/5) and we\u2019ll follow up with additional information. <br><br> Retrieves a list of third-party targeting segments for the given target type filter. If no data is present for the given input, an empty segment list will be returned.",
        "title": "Discovery - Third-party Target Segments",
        "version": "3.0"
    },
    "paths": {
        "/dsp/thirdPartyTargetSegments/list": {
            "post": {
                "requestBody": {
                    "content": {
                        "application/vnd.dspdiscoverythirdpartytargetsegments.v1+json": {
                            "schema": {
                                "$ref": "#/components/schemas/DspListThirdPartyTargetSegmentsRequestContentV1"
                            }
                        }
                    },
                    "required": true
                },
                "description": "Retrieves a list of third-party targeting segments for the given target type filter. If no data is present for the given input, an empty segment list will be returned.\n\n**Requires one of these permissions**:\n[]",
                "operationId": "DspListDiscoveryThirdPartyTargetSegments",
                "responses": {
                    "200": {
                        "description": "Successful operation.",
                        "content": {
                            "application/vnd.dspdiscoverythirdpartytargetsegments.v1+json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspListThirdPartyTargetSegmentsResponseContentV1"
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request or request body is not matching with input model.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspBadRequestExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "401": {
                        "description": "Unauthenticated request.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspUnauthorizedExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "500": {
                        "description": "Internal Server Error.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspInternalServerExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "403": {
                        "description": "Forbidden - Request failed because user is not authorized to access a resource.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspForbiddenExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "404": {
                        "description": "Not Found - Requested resource does not exist or is not visible for the authenticated user.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspNotFoundExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "415": {
                        "description": "Unsupported Media Type - Version not supported.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspUnsupportedMediaTypeExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "429": {
                        "description": "Too Many Requests - Request was rate-limited. Retry later.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspTooManyRequestsExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "409": {
                        "description": "DspConflictExceptionV1 409 response",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspConflictExceptionV1ResponseContent"
                                }
                            }
                        }
                    }
                },
                "parameters": [
                    {
                        "schema": {
                            "description": "Account identifier you use to access the DSP. This is your Amazon DSP Advertiser ID.",
                            "type": "string"
                        },
                        "in": "header",
                        "name": "Amazon-Ads-AccountId",
                        "description": "Account identifier you use to access the DSP. This is your Amazon DSP Advertiser ID.",
                        "required": true
                    },
                    {
                        "schema": {
                            "description": "The identifier of a client associated with a \"Login with Amazon\" account.",
                            "type": "string"
                        },
                        "in": "header",
                        "name": "Amazon-Advertising-API-ClientId",
                        "description": "The identifier of a client associated with a \"Login with Amazon\" account.",
                        "required": true
                    },
                    {
                        "schema": {
                            "description": "The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id `profileId` from the response to pass it as input.",
                            "type": "string"
                        },
                        "in": "header",
                        "name": "Amazon-Advertising-API-Scope",
                        "description": "The identifier of a profile associated with the advertiser account. Use `GET` method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header and choose profile id `profileId` from the response to pass it as input.",
                        "required": true
                    }
                ],
                "tags": [
                    "Discovery - Third-party Target Segments"
                ]
            }
        }
    },
    "components": {
        "headers": {},
        "examples": {},
        "schemas": {
            "DspThirdPartyTargetSegmentsInfoV1": {
                "type": "object",
                "properties": {
                    "segmentId": {
                        "description": "The segment identifier.",
                        "type": "string"
                    },
                    "thirdPartyTargetType": {
                        "$ref": "#/components/schemas/ThirdPartyTargetTypesV1"
                    },
                    "segmentDescription": {
                        "description": "The segment description.",
                        "type": "string"
                    },
                    "segmentName": {
                        "description": "The segment name.",
                        "type": "string"
                    }
                },
                "required": [
                    "thirdPartyTargetType"
                ]
            },
            "DspSubErrorV1": {
                "type": "object",
                "properties": {
                    "fieldName": {
                        "type": "string"
                    },
                    "errorType": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    }
                },
                "required": [
                    "errorType",
                    "message"
                ]
            },
            "DspUnsupportedMediaTypeExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "DspConflictExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "DspListThirdPartyTargetSegmentsRequestContentV1": {
                "type": "object",
                "properties": {
                    "nextToken": {
                        "description": "Operations that return paginated results include a pagination token in this field. To retrieve the next page of results, call the same operation and specify this token in the request. If the nextToken field is empty, there are no further results.",
                        "type": "string"
                    },
                    "maxResults": {
                        "description": "Sets the maximum number of results in the returned array. Use in conjunction with the nextToken parameter to control pagination. For example, supplying maxResults=20 with a previously returned token will fetch up to the next 20 items. In some cases, fewer items may be returned. ``Default: 25``.",
                        "maximum": 1000,
                        "type": "integer",
                        "minimum": 1
                    },
                    "thirdPartyTargetTypeFilter": {
                        "minItems": 1,
                        "maxItems": 10,
                        "uniqueItems": true,
                        "description": "Third-party target type filter.",
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/ThirdPartyTargetTypesV1"
                        }
                    }
                },
                "required": [
                    "thirdPartyTargetTypeFilter"
                ]
            },
            "DspBadRequestExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "DspInternalServerExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "DspTooManyRequestsExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "DspListThirdPartyTargetSegmentsResponseContentV1": {
                "type": "object",
                "properties": {
                    "thirdPartyTargetSegments": {
                        "minItems": 0,
                        "maxItems": 1000,
                        "description": "List of segments associated to given thirdPartyTargetType",
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspThirdPartyTargetSegmentsInfoV1"
                        }
                    },
                    "nextToken": {
                        "description": "Token from a previous request. Use in conjunction with the ``maxResults`` parameter to control pagination of the returned array.",
                        "type": "string"
                    }
                },
                "required": [
                    "thirdPartyTargetSegments"
                ]
            },
            "DspForbiddenExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "DspUnauthorizedExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "ThirdPartyTargetTypesV1": {
                "type": "string",
                "enum": [
                    "DOUBLE_VERIFY_CUSTOM_CONTEXTUAL",
                    "ORACLE_DATA_CLOUD_CUSTOM_PREDICTS",
                    "ORACLE_DATA_CLOUD_STANDARD_PREDICTS"
                ]
            },
            "DspNotFoundExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            }
        },
        "requestBodies": {},
        "responses": {},
        "callbacks": {},
        "links": {},
        "securitySchemes": {},
        "parameters": {}
    },
    "openapi": "3.0.1"
}