{
    "info": {
        "description": "These APIs are part of the Amazon DSP Campaign Management API closed beta and only accessible to closed beta participants at this time.     Because these APIs are in closed beta, they don\u2019t follow our normal versioning standards and are subject to change. If you are interested     in joining the closed beta, please [submit a support ticket](https://amzn-clicks.atlassian.net/servicedesk/customer/portal/2/group/2/create/5)     and we\u2019ll follow up with additional information. <br><br>     Gets a list of targetable exchange inventory sources, including third-party exchanges, Amazon owned & operated inventory, and Amazon Publisher Direct.",
        "title": "Discovery - Inventory Source Exchanges",
        "version": "3.0"
    },
    "paths": {
        "/dsp/inventorySourceExchanges/list": {
            "post": {
                "requestBody": {
                    "content": {
                        "application/vnd.dspinventoryexchanges.v1+json": {
                            "schema": {
                                "$ref": "#/components/schemas/ListCMInventoryDiscoveryExchangesRequestContent"
                            }
                        }
                    },
                    "required": true
                },
                "operationId": "ListCMInventoryDiscoveryExchanges",
                "responses": {
                    "200": {
                        "description": "Successful operation.",
                        "content": {
                            "application/vnd.dspinventoryexchanges.v1+json": {
                                "schema": {
                                    "$ref": "#/components/schemas/ListCMInventoryDiscoveryExchangesResponseContent"
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request or request body is not matching with input model.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspBadRequestExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "401": {
                        "description": "Unauthenticated request.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspUnauthorizedExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "500": {
                        "description": "Internal Server Error.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspInternalServerExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "403": {
                        "description": "Forbidden - Request failed because user is not authorized to access a resource.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspForbiddenExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "404": {
                        "description": "Not Found - Requested resource does not exist or is not visible for the authenticated user.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspNotFoundExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "415": {
                        "description": "Unsupported Media Type - Version not supported.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspUnsupportedMediaTypeExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "429": {
                        "description": "Too Many Requests - Request was rate-limited. Retry later.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspTooManyRequestsExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "409": {
                        "description": "DspConflictExceptionV1 409 response",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspConflictExceptionV1ResponseContent"
                                }
                            }
                        }
                    }
                },
                "description": "\n\n**Requires one of these permissions**:\n[]",
                "parameters": [
                    {
                        "schema": {
                            "description": "Account identifier you use to access the DSP. This is your Amazon DSP Advertiser ID.",
                            "type": "string"
                        },
                        "in": "header",
                        "name": "Amazon-Ads-AccountId",
                        "description": "Account identifier you use to access the DSP. This is your Amazon DSP Advertiser ID.",
                        "required": true
                    },
                    {
                        "schema": {
                            "description": "The identifier of a client associated with a \"Login with Amazon\" account.",
                            "type": "string"
                        },
                        "in": "header",
                        "name": "Amazon-Advertising-API-ClientId",
                        "description": "The identifier of a client associated with a \"Login with Amazon\" account.",
                        "required": true
                    },
                    {
                        "schema": {
                            "description": "The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.",
                            "type": "string"
                        },
                        "in": "header",
                        "name": "Amazon-Advertising-API-Scope",
                        "description": "The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.",
                        "required": true
                    }
                ],
                "tags": [
                    "Discovery - Inventory Source Exchanges"
                ]
            }
        }
    },
    "components": {
        "headers": {},
        "examples": {},
        "schemas": {
            "InventoryType": {
                "type": "string",
                "enum": [
                    "STANDARD_DISPLAY",
                    "AMAZON_MOBILE_DISPLAY",
                    "AAP_MOBILE_APP",
                    "DISPLAY",
                    "VIDEO"
                ]
            },
            "ListCMInventoryDiscoveryExchangesResponseContent": {
                "type": "object",
                "properties": {
                    "exchanges": {
                        "minItems": 0,
                        "maxItems": 1000,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/Exchange"
                        }
                    }
                }
            },
            "DspSubErrorV1": {
                "type": "object",
                "properties": {
                    "fieldName": {
                        "type": "string"
                    },
                    "errorType": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    }
                },
                "required": [
                    "errorType",
                    "message"
                ]
            },
            "DspUnsupportedMediaTypeExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "InventorySourceType": {
                "type": "string",
                "enum": [
                    "AMAZON",
                    "AMAZON_PUBLISHER_DIRECT",
                    "OTHER",
                    "THIRD_PARTY_EXCHANGE"
                ]
            },
            "DspConflictExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "Exchange": {
                "type": "object",
                "properties": {
                    "inventoryType": {
                        "$ref": "#/components/schemas/InventoryType"
                    },
                    "inventorySourceType": {
                        "$ref": "#/components/schemas/InventorySourceType"
                    },
                    "name": {
                        "description": "The name of the exchange.",
                        "type": "string"
                    },
                    "id": {
                        "description": "The ID of the exchange.",
                        "type": "string"
                    }
                }
            },
            "Filter": {
                "type": "object",
                "properties": {
                    "include": {
                        "description": "Indicates whether results that match the filter value should be included or excluded from the response.",
                        "type": "boolean"
                    },
                    "field": {
                        "$ref": "#/components/schemas/FilterField"
                    },
                    "value": {
                        "description": "The value by which the Exchanges in the responses will be filtered against.\n\n| Filter Field | Accepted Values |\n|-----|-----|\n| INVENTORY_TYPE   | STANDARD_DISPLAY<br>AMAZON_MOBILE_DISPLAY<br>AAP_MOBILE_APP<br>VIDEO |\n| INVENTORY_SOURCE_TYPE | AMAZON<br>AMAZON_PUBLISHER_DIRECT<br>OTHER<br>THIRD_PARTY_EXCHANGE |\n\n    ",
                        "type": "string"
                    }
                },
                "required": [
                    "field",
                    "include",
                    "value"
                ]
            },
            "DspBadRequestExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "DspInternalServerExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "DspTooManyRequestsExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "DspForbiddenExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "DspUnauthorizedExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "FilterField": {
                "type": "string",
                "enum": [
                    "INVENTORY_TYPE",
                    "INVENTORY_SOURCE_TYPE"
                ]
            },
            "ListCMInventoryDiscoveryExchangesRequestContent": {
                "type": "object",
                "properties": {
                    "filters": {
                        "minItems": 2,
                        "maxItems": 10,
                        "description": "List of filters to be applied to the result set of exchanges.\n     At least two filters, one for InventoryType and one for InventorySourceType, are required for a successful request.",
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/Filter"
                        }
                    }
                },
                "required": [
                    "filters"
                ]
            },
            "DspNotFoundExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            }
        },
        "requestBodies": {},
        "responses": {},
        "callbacks": {},
        "links": {},
        "securitySchemes": {},
        "parameters": {}
    },
    "openapi": "3.0.1"
}