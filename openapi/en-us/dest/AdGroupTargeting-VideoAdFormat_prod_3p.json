{
    "info": {
        "description": "These APIs are part of the Amazon DSP Campaign Management API closed beta and only accessible to closed beta participants at this time. \t \t Because these APIs are in closed beta, they don\u2019t follow our normal versioning standards and are subject to change. If you are interested \t \t in joining the closed beta, please [submit a support ticket](https://amzn-clicks.atlassian.net/servicedesk/customer/portal/2/group/2/create/5) \t \t and we\u2019ll follow up with additional information.",
        "title": "Ad Group Targeting - Video Ad Format",
        "version": "3.0"
    },
    "paths": {
        "/dsp/adGroups/{adGroupId}/targetingTypes/videoAdFormat/targets/delete": {
            "post": {
                "requestBody": {
                    "content": {
                        "application/vnd.dspAdGroupTargetingVideoAdFormat.v1+json": {
                            "schema": {
                                "$ref": "#/components/schemas/DspDeleteAdGroupTargetingVideoAdFormatRequestContentV1"
                            }
                        }
                    },
                    "required": true
                },
                "description": "Removes one or more video ad format targets from an ad group.\n\n**Requires one of these permissions**:\n[]",
                "operationId": "DspDeleteAdGroupVideoAdFormatTargets",
                "responses": {
                    "400": {
                        "description": "Bad Request or request body is not matching with input model.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspBadRequestExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "401": {
                        "description": "Unauthenticated request.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspUnauthorizedExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "500": {
                        "description": "Internal Server Error.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspInternalServerExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "204": {
                        "description": "Success with no body content.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspDeleteVideoAdFormatResponseV1"
                                }
                            }
                        }
                    },
                    "403": {
                        "description": "Forbidden - Request failed because user is not authorized to access a resource.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspForbiddenExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "404": {
                        "description": "Not Found - Requested resource does not exist or is not visible for the authenticated user.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspNotFoundExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "415": {
                        "description": "Unsupported Media Type - Version not supported.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspUnsupportedMediaTypeExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "429": {
                        "description": "Too Many Requests - Request was rate-limited. Retry later.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspTooManyRequestsExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "409": {
                        "description": "DspConflictExceptionV1 409 response",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspConflictExceptionV1ResponseContent"
                                }
                            }
                        }
                    }
                },
                "parameters": [
                    {
                        "schema": {
                            "description": "The ad group identifier.",
                            "type": "string"
                        },
                        "in": "path",
                        "name": "adGroupId",
                        "description": "The ad group identifier.",
                        "required": true
                    },
                    {
                        "schema": {
                            "description": "Account identifier you use to access the DSP. This is your Amazon DSP Advertiser ID.",
                            "type": "string"
                        },
                        "in": "header",
                        "name": "Amazon-Ads-AccountId",
                        "description": "Account identifier you use to access the DSP. This is your Amazon DSP Advertiser ID.",
                        "required": true
                    },
                    {
                        "schema": {
                            "description": "The identifier of a client associated with a \"Login with Amazon\" account.",
                            "type": "string"
                        },
                        "in": "header",
                        "name": "Amazon-Advertising-API-ClientId",
                        "description": "The identifier of a client associated with a \"Login with Amazon\" account.",
                        "required": true
                    },
                    {
                        "schema": {
                            "description": "The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.",
                            "type": "string"
                        },
                        "in": "header",
                        "name": "Amazon-Advertising-API-Scope",
                        "description": "The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.",
                        "required": true
                    }
                ],
                "tags": [
                    "Ad Group Targeting - Video Ad Format"
                ]
            }
        },
        "/dsp/adGroups/{adGroupId}/targetingTypes/videoAdFormat/targets": {
            "post": {
                "requestBody": {
                    "content": {
                        "application/vnd.dspAdGroupTargetingVideoAdFormat.v1+json": {
                            "schema": {
                                "$ref": "#/components/schemas/DspCreateAdGroupTargetingVideoAdFormatRequestContentV1"
                            }
                        }
                    },
                    "required": true
                },
                "description": "Creates and associates one or more video ad format targets to an ad group.\n\n**Requires one of these permissions**:\n[]",
                "operationId": "DspCreateAdGroupVideoAdFormatTargets",
                "responses": {
                    "400": {
                        "description": "Bad Request or request body is not matching with input model.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspBadRequestExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "401": {
                        "description": "Unauthenticated request.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspUnauthorizedExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "500": {
                        "description": "Internal Server Error.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspInternalServerExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "204": {
                        "description": "Success with no body content.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspCreateVideoAdFormatResponseV1"
                                }
                            }
                        }
                    },
                    "403": {
                        "description": "Forbidden - Request failed because user is not authorized to access a resource.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspForbiddenExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "404": {
                        "description": "Not Found - Requested resource does not exist or is not visible for the authenticated user.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspNotFoundExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "415": {
                        "description": "Unsupported Media Type - Version not supported.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspUnsupportedMediaTypeExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "429": {
                        "description": "Too Many Requests - Request was rate-limited. Retry later.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspTooManyRequestsExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "409": {
                        "description": "DspConflictExceptionV1 409 response",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspConflictExceptionV1ResponseContent"
                                }
                            }
                        }
                    }
                },
                "parameters": [
                    {
                        "schema": {
                            "description": "The ad group identifier.",
                            "type": "string"
                        },
                        "in": "path",
                        "name": "adGroupId",
                        "description": "The ad group identifier.",
                        "required": true
                    },
                    {
                        "schema": {
                            "description": "Account identifier you use to access the DSP. This is your Amazon DSP Advertiser ID.",
                            "type": "string"
                        },
                        "in": "header",
                        "name": "Amazon-Ads-AccountId",
                        "description": "Account identifier you use to access the DSP. This is your Amazon DSP Advertiser ID.",
                        "required": true
                    },
                    {
                        "schema": {
                            "description": "The identifier of a client associated with a \"Login with Amazon\" account.",
                            "type": "string"
                        },
                        "in": "header",
                        "name": "Amazon-Advertising-API-ClientId",
                        "description": "The identifier of a client associated with a \"Login with Amazon\" account.",
                        "required": true
                    },
                    {
                        "schema": {
                            "description": "The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.",
                            "type": "string"
                        },
                        "in": "header",
                        "name": "Amazon-Advertising-API-Scope",
                        "description": "The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.",
                        "required": true
                    }
                ],
                "tags": [
                    "Ad Group Targeting - Video Ad Format"
                ]
            },
            "get": {
                "description": "Gets a list of video ad format targets associated to an ad group.\n\n**Requires one of these permissions**:\n[]",
                "operationId": "DspGetAdGroupVideoAdFormatTargets",
                "responses": {
                    "200": {
                        "description": "Successful operation.",
                        "content": {
                            "application/vnd.dspAdGroupTargetingVideoAdFormat.v1+json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspGetAdGroupVideoAdFormatTargetsResponseContent"
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request or request body is not matching with input model.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspBadRequestExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "401": {
                        "description": "Unauthenticated request.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspUnauthorizedExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "500": {
                        "description": "Internal Server Error.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspInternalServerExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "403": {
                        "description": "Forbidden - Request failed because user is not authorized to access a resource.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspForbiddenExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "404": {
                        "description": "Not Found - Requested resource does not exist or is not visible for the authenticated user.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspNotFoundExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "415": {
                        "description": "Unsupported Media Type - Version not supported.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspUnsupportedMediaTypeExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "429": {
                        "description": "Too Many Requests - Request was rate-limited. Retry later.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspTooManyRequestsExceptionV1ResponseContent"
                                }
                            }
                        }
                    },
                    "409": {
                        "description": "DspConflictExceptionV1 409 response",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspConflictExceptionV1ResponseContent"
                                }
                            }
                        }
                    }
                },
                "parameters": [
                    {
                        "schema": {
                            "description": "The ad group identifier.",
                            "type": "string"
                        },
                        "in": "path",
                        "name": "adGroupId",
                        "description": "The ad group identifier.",
                        "required": true
                    },
                    {
                        "schema": {
                            "description": "Operations that return paginated results include a pagination token in this field. To retrieve the next page of results, call the same operation and specify this token in the request. If the nextToken field is empty, there are no further results.",
                            "type": "string"
                        },
                        "in": "query",
                        "name": "nextToken",
                        "description": "Operations that return paginated results include a pagination token in this field. To retrieve the next page of results, call the same operation and specify this token in the request. If the nextToken field is empty, there are no further results."
                    },
                    {
                        "schema": {
                            "description": "Sets the maximum number of objects in the returned array. Use in conjunction with the nextToken parameter to control pagination. For example, supplying maxResults=20 with a previously returned token will fetch up to the next 20 items. In some cases, fewer items may be returned.",
                            "type": "integer"
                        },
                        "in": "query",
                        "name": "maxResults",
                        "description": "Sets the maximum number of objects in the returned array. Use in conjunction with the nextToken parameter to control pagination. For example, supplying maxResults=20 with a previously returned token will fetch up to the next 20 items. In some cases, fewer items may be returned."
                    },
                    {
                        "schema": {
                            "description": "Account identifier you use to access the DSP. This is your Amazon DSP Advertiser ID.",
                            "type": "string"
                        },
                        "in": "header",
                        "name": "Amazon-Ads-AccountId",
                        "description": "Account identifier you use to access the DSP. This is your Amazon DSP Advertiser ID.",
                        "required": true
                    },
                    {
                        "schema": {
                            "description": "The identifier of a client associated with a \"Login with Amazon\" account.",
                            "type": "string"
                        },
                        "in": "header",
                        "name": "Amazon-Advertising-API-ClientId",
                        "description": "The identifier of a client associated with a \"Login with Amazon\" account.",
                        "required": true
                    },
                    {
                        "schema": {
                            "description": "The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.",
                            "type": "string"
                        },
                        "in": "header",
                        "name": "Amazon-Advertising-API-Scope",
                        "description": "The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.",
                        "required": true
                    }
                ],
                "tags": [
                    "Ad Group Targeting - Video Ad Format"
                ]
            }
        }
    },
    "components": {
        "headers": {},
        "examples": {},
        "schemas": {
            "DspGetAdGroupVideoAdFormatTargetsResponseContent": {
                "type": "object",
                "properties": {
                    "videoAdFormatTargets": {
                        "minItems": 1,
                        "maxItems": 2,
                        "description": "A list of targets.",
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/VideoAdFormatTargetV1"
                        }
                    },
                    "nextToken": {
                        "description": "Operations that return paginated results include a pagination token in this field. To retrieve the next page of results, call the same operation and specify this token in the request. If the nextToken field is empty, there are no further results.",
                        "type": "string"
                    }
                },
                "required": [
                    "videoAdFormatTargets"
                ]
            },
            "DspSubErrorV1": {
                "type": "object",
                "properties": {
                    "fieldName": {
                        "type": "string"
                    },
                    "errorType": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    }
                },
                "required": [
                    "errorType",
                    "message"
                ]
            },
            "DspDeleteAdGroupTargetingVideoAdFormatRequestContentV1": {
                "description": "The delete operation request.",
                "type": "object",
                "properties": {
                    "videoAdFormatTargets": {
                        "minItems": 1,
                        "maxItems": 2,
                        "description": "A list of video ad format targets.",
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/VideoAdFormatTargetV1"
                        }
                    }
                },
                "required": [
                    "videoAdFormatTargets"
                ]
            },
            "DspUnsupportedMediaTypeExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "VideoAdFormatV1": {
                "description": "Target a specific type of ad slot that will be used to serve the ad. The available types are:\n    * INSTREAM: Includes pre-roll inventory that plays before an online video.\n    * FULL_EPISODE_PLAYER: Limit IN STREAM ad slot to full episode players (FEP) on Amazon Publisher Direct STV inventory.\n    * OUTSTREAM: Includes in-read or in-feed formats, which appear within written content such as an online news article, or interstitials, which appear as full-screen pop up ads, typically before an expected content page or between transitions in an app.",
                "type": "string",
                "enum": [
                    "INSTREAM",
                    "FULL_EPISODE_PLAYER",
                    "OUTSTREAM"
                ]
            },
            "DspFunctionalPatchDefaultAdGroupVideoAdFormatTargetsOutputPayload": {},
            "DspFunctionalPatchDefaultAdGroupVideoAdFormatTargetsRequestContent": {
                "type": "object",
                "properties": {
                    "resourceId": {
                        "type": "string"
                    },
                    "create": {
                        "type": "boolean"
                    },
                    "finalize": {
                        "type": "boolean"
                    },
                    "flights": {
                        "type": "array"
                    },
                    "validate": {
                        "type": "boolean"
                    }
                }
            },
            "DspConflictExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "DspVideoAdFormatErrorV1": {
                "type": "object",
                "properties": {
                    "index": {
                        "description": "The index of the object in the array from the request body.",
                        "type": "integer",
                        "minimum": 0
                    },
                    "errors": {
                        "minItems": 0,
                        "maxItems": 10,
                        "description": "A list of validation errors.",
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                },
                "required": [
                    "errors",
                    "index"
                ]
            },
            "VideoAdFormatTargetV1": {
                "type": "object",
                "properties": {
                    "videoAdFormat": {
                        "$ref": "#/components/schemas/VideoAdFormatV1"
                    }
                },
                "required": [
                    "videoAdFormat"
                ]
            },
            "DspCreateVideoAdFormatResponseV1": {
                "description": "The create operation response.",
                "type": "object",
                "properties": {
                    "success": {
                        "minItems": 0,
                        "maxItems": 2,
                        "description": "The list of successful targeting objects.",
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspVideoAdFormatSuccessV1"
                        }
                    },
                    "error": {
                        "minItems": 0,
                        "maxItems": 2,
                        "description": "The list of unsuccessful targeting objects.",
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspVideoAdFormatErrorV1"
                        }
                    }
                }
            },
            "DspDeleteVideoAdFormatResponseV1": {
                "description": "The delete operation response.",
                "type": "object",
                "properties": {
                    "success": {
                        "minItems": 0,
                        "maxItems": 2,
                        "description": "The list of successful deletions.",
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspVideoAdFormatSuccessV1"
                        }
                    },
                    "error": {
                        "minItems": 0,
                        "maxItems": 2,
                        "description": "The list of unsuccessful deletions.",
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspVideoAdFormatErrorV1"
                        }
                    }
                }
            },
            "DspFunctionalPatchDefaultAdGroupVideoAdFormatTargetsV2RequestContent": {
                "type": "object",
                "properties": {
                    "resourceId": {
                        "type": "string"
                    },
                    "create": {
                        "type": "boolean"
                    },
                    "finalize": {
                        "type": "boolean"
                    },
                    "flights": {
                        "type": "array"
                    },
                    "validate": {
                        "type": "boolean"
                    }
                }
            },
            "DspBadRequestExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "DspInternalServerExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "DspTooManyRequestsExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "DspFunctionalPatchDefaultAdGroupVideoAdFormatTargetsV2ResponseContent": {
                "description": "Result ",
                "type": "object",
                "properties": {
                    "flights": {
                        "type": "array"
                    }
                }
            },
            "DspVideoAdFormatSuccessV1": {
                "description": "The index of targeting objects.",
                "type": "object",
                "properties": {
                    "index": {
                        "description": "The index of the object from the request body.",
                        "type": "integer",
                        "minimum": 0
                    }
                },
                "required": [
                    "index"
                ]
            },
            "DspForbiddenExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "DspUnauthorizedExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            },
            "DspCreateAdGroupTargetingVideoAdFormatRequestContentV1": {
                "description": "The create operation request.",
                "type": "object",
                "properties": {
                    "videoAdFormatTargets": {
                        "minItems": 1,
                        "maxItems": 2,
                        "description": "A list of video ad format targets.",
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/VideoAdFormatTargetV1"
                        }
                    }
                },
                "required": [
                    "videoAdFormatTargets"
                ]
            },
            "DspNotFoundExceptionV1ResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "message": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubErrorV1"
                        }
                    }
                }
            }
        },
        "requestBodies": {},
        "responses": {},
        "callbacks": {},
        "links": {},
        "securitySchemes": {},
        "parameters": {}
    },
    "openapi": "3.0.1"
}