{
    "info": {
        "description": "Gets mobile or streaming TV apps based on app Ids or text queries at either advertiser or manager account level. Use Amazon-Ads-AccountId to get exchanges at advertiser level. Use Amazon-Advertising-API-Scope and leave Amazon-Ads-AccountId empty to get exchanges at manager account level. Either appIdFilter or textQuery must be supplied, but not both.",
        "title": "Discovery - Apps - V1",
        "version": "3.0"
    },
    "paths": {
        "/dsp/v1/apps/list": {
            "post": {
                "requestBody": {
                    "content": {
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/DspListDiscoveryAppsRequestContent"
                            }
                        }
                    },
                    "required": true
                },
                "description": "Gets mobile or streaming TV apps based on app Ids or text queries. Either appIdFilter or textQuery must be supplied, but not both.\n\n**Requires one of these permissions**:\n[]",
                "operationId": "DspListDiscoveryAppsV1",
                "responses": {
                    "200": {
                        "description": "Successful operation.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspListDiscoveryAppsResponseContent"
                                }
                            }
                        }
                    },
                    "400": {
                        "description": "Bad Request or request body is not matching with input model.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspBadRequestExceptionResponseContent"
                                }
                            }
                        }
                    },
                    "401": {
                        "description": "Unauthenticated request.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspUnauthorizedExceptionResponseContent"
                                }
                            }
                        }
                    },
                    "500": {
                        "description": "Internal Server Error.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspInternalServerExceptionResponseContent"
                                }
                            }
                        }
                    },
                    "403": {
                        "description": "Forbidden - Request failed because user is not authorized to access a resource.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspForbiddenExceptionResponseContent"
                                }
                            }
                        }
                    },
                    "404": {
                        "description": "Not Found - Requested resource does not exist or is not visible for the authenticated user.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspNotFoundExceptionResponseContent"
                                }
                            }
                        }
                    },
                    "415": {
                        "description": "Unsupported Media Type - Version not supported.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspUnsupportedMediaTypeExceptionResponseContent"
                                }
                            }
                        }
                    },
                    "429": {
                        "description": "Too Many Requests - Request was rate-limited. Retry later.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspTooManyRequestsExceptionResponseContent"
                                }
                            }
                        }
                    },
                    "409": {
                        "description": "Conflict detected. A change has been made to the line items data since these draft changes were started. Create a new draft set and retry the commit with the new set.",
                        "content": {
                            "application/json": {
                                "schema": {
                                    "$ref": "#/components/schemas/DspConflictExceptionResponseContent"
                                }
                            }
                        }
                    }
                },
                "parameters": [
                    {
                        "schema": {
                            "description": "Account identifier you use to access the DSP. This is your Amazon DSP Advertiser ID. Use Amazon-Ads-AccountId to get resources at advertiser level. Use Amazon-Advertising-API-Scope and leave     Amazon-Ads-AccountId empty to get resources at manager account level.",
                            "type": "string"
                        },
                        "in": "header",
                        "name": "Amazon-Ads-AccountId",
                        "description": "Account identifier you use to access the DSP. This is your Amazon DSP Advertiser ID. Use Amazon-Ads-AccountId to get resources at advertiser level. Use Amazon-Advertising-API-Scope and leave     Amazon-Ads-AccountId empty to get resources at manager account level."
                    },
                    {
                        "schema": {
                            "description": "The identifier of a client associated with a \"Login with Amazon\" account.",
                            "type": "string"
                        },
                        "in": "header",
                        "name": "Amazon-Advertising-API-ClientId",
                        "description": "The identifier of a client associated with a \"Login with Amazon\" account.",
                        "required": true
                    },
                    {
                        "schema": {
                            "description": "The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header.",
                            "type": "string"
                        },
                        "in": "header",
                        "name": "Amazon-Advertising-API-Scope",
                        "description": "The identifier of a profile associated with the advertiser account. Use GET method on Profiles resource to list profiles associated with the access token passed in the HTTP Authorization header."
                    }
                ],
                "tags": [
                    "Discovery - Apps"
                ]
            }
        }
    },
    "components": {
        "headers": {},
        "examples": {},
        "schemas": {
            "DspInternalServerExceptionResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubError"
                        }
                    }
                }
            },
            "DspBadRequestExceptionResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubError"
                        }
                    }
                }
            },
            "DspListDiscoveryAppsRequestContent": {
                "type": "object",
                "properties": {
                    "appType": {
                        "$ref": "#/components/schemas/DspListDiscoveryAppType"
                    },
                    "nextToken": {
                        "description": "Operations that return paginated results include a pagination token in this field. To retrieve the next page of results, call the same operation and specify this token in the request. If the nextToken field is empty, there are no further results.",
                        "type": "string"
                    },
                    "maxResults": {
                        "description": "Sets the maximum number of objects in the returned array. Use in conjunction with the nextToken parameter to control pagination. For example, supplying maxResults=20 with a previously returned token will fetch up to the next 20 items. In some cases, fewer items may be returned. Default: 100",
                        "maximum": 5000,
                        "type": "integer",
                        "minimum": 1
                    },
                    "appIdFilter": {
                        "minItems": 1,
                        "maxItems": 1000,
                        "description": "An array of mobile or streaming TV app Ids to retrieve. You may include up to 1000 in one request.",
                        "type": "array",
                        "items": {
                            "minLength": 1,
                            "type": "string",
                            "maxLength": 255
                        }
                    },
                    "textQuery": {
                        "minLength": 3,
                        "description": "Filter by text.",
                        "type": "string",
                        "maxLength": 255
                    }
                }
            },
            "DspSubError": {
                "type": "object",
                "properties": {
                    "errorMessage": {
                        "type": "string"
                    },
                    "errorCode": {
                        "type": "string"
                    },
                    "errorId": {
                        "type": "string"
                    }
                },
                "required": [
                    "errorCode",
                    "errorMessage"
                ]
            },
            "DspForbiddenExceptionResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubError"
                        }
                    }
                }
            },
            "DspTooManyRequestsExceptionResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubError"
                        }
                    }
                }
            },
            "DspConflictExceptionResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubError"
                        }
                    }
                }
            },
            "DspUnsupportedMediaTypeExceptionResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubError"
                        }
                    }
                }
            },
            "DspListDiscoveryAppType": {
                "type": "string",
                "enum": [
                    "MOBILE",
                    "STV"
                ]
            },
            "DspUnauthorizedExceptionResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubError"
                        }
                    }
                }
            },
            "DspDiscoveryApp": {
                "type": "object",
                "properties": {
                    "name": {
                        "description": "The app name.",
                        "type": "string"
                    },
                    "legacyId": {
                        "description": "Legacy app identifier. Only applicable when `appType=STV`.",
                        "type": "string"
                    },
                    "id": {
                        "description": "The app identifier.",
                        "type": "string"
                    }
                }
            },
            "DspListDiscoveryAppsResponseContent": {
                "type": "object",
                "properties": {
                    "nextToken": {
                        "type": "string"
                    },
                    "apps": {
                        "minItems": 0,
                        "maxItems": 5000,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspDiscoveryApp"
                        }
                    }
                }
            },
            "DspNotFoundExceptionResponseContent": {
                "type": "object",
                "properties": {
                    "requestId": {
                        "type": "string"
                    },
                    "errors": {
                        "minItems": 1,
                        "maxItems": 100,
                        "type": "array",
                        "items": {
                            "$ref": "#/components/schemas/DspSubError"
                        }
                    }
                }
            }
        },
        "requestBodies": {},
        "responses": {},
        "callbacks": {},
        "links": {},
        "securitySchemes": {},
        "parameters": {}
    },
    "openapi": "3.0.1"
}